//
//  AssignPlayersViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/19/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core

class AssignPlayersViewController: BaseViewController , UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    @IBOutlet weak var tableViewPlayers : UITableView!
    
    var resultSearchController = UISearchController()
    var items = Array<[String:Any]>()
    var filteredTableData = [String]()
    
    
    override func viewDidLoad() {
        if var dt = (self.data as? [String:Any]) {
            self.navigationItem.title = dt["name"] as? String
            if dt["selected"] == nil {
                self.items = []
            }else{
                self.items  = (dt["coaches"] as? Array<[String:Any]>)!
            }
        }
        
        resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            
            tableViewPlayers.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        // Reload the table
        tableViewPlayers.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredTableData.removeAll(keepingCapacity: false)
        
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        let array = (items as NSArray).filtered(using: searchPredicate)
        filteredTableData = array as! [String]
        
        self.tableViewPlayers.reloadData()
    }
    
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
        
        if  (resultSearchController.isActive) {
            return filteredTableData.count
        } else {
            return self.items.count
        }
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        
        if cell is ChatViewTableViewCell {
            (cell as! ChatViewTableViewCell).setCoaches(stat: self.items[indexPath.row])
        }
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            cell.accessoryType = .checkmark
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            cell.accessoryType = .none
        }
    }
    
    
    override func onBack(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//
//  ScheduleViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/12/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core
import SkeletonView

class ScheduleViewController : BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var cvPlayers: UICollectionView!
    @IBOutlet weak var cvCoach: UICollectionView!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblLocation : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblCountPlayers : UILabel!
    @IBOutlet weak var lblCountCoaches : UILabel!
    @IBOutlet weak var lblSlots : UILabel!
    @IBOutlet weak var skeletonableView: UIView!
    
    var viewModel: ScheduleViewModel!
    var loadingViews: [UIView] = []
    var items = ["1", "2", "3", "4", "5", "6", "7"]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.vwHeaderRight.setProgram(selectedProgram: SharedAppResource.instance.selectedProgram)
        if(viewModel == nil){
            viewModel = ScheduleViewModel()
        }
        showSkeletonView()
        self.vwHeaderRight.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadProgramDetails()
    }
    
    private func showSkeletonView() {
        loadingViews.append(skeletonableView)
        for loadingView in loadingViews {
            for subView in loadingView.subviews where subView.isSkeletonable {
                subView.skeletonCornerRadius = 10
                subView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation,transition: .crossDissolve(0.25))
            }
        }
    }
    
    func stopSkeletonView() {
        for loadingView in loadingViews {
            for subView in loadingView.subviews where subView.isSkeletonable {
                subView.hideSkeleton(transition: .crossDissolve(0.25))
            }
        }
    }
    
    fileprivate func loadProgramDetails(){
        HUDManager.shared.manageHUD(.loading())
        self.viewModel.getDetailAssignedPrograms { (detail) in
            HUDManager.shared.manageHUD(.dismiss)
            self.stopSkeletonView()
            if let result = detail.result, let program = result.program{
                self.updateUI(programDetail: program)
            }else{
                self.alert(message: detail.error?.error ?? "Can not retrieve program details")
            }
        }
    }
    
    
    fileprivate func updateUI(programDetail: ProgramDetail){
        dateFormatter.dateFormat = "dd"
        self.lblDate.text = dateFormatter.string(from : (programDetail.on?.date(format: Constants.SERVER_DATE_FORMAT))!)
        dateFormatter.dateFormat = "yyyy-MM-dd"

        self.lblLocation.text = programDetail.location?.name?.uppercased() ?? "TBD"
        self.lblTime.text = String(format:"%@ - %@",programDetail.startTime?.date(format: "HH:mm:ss")?.toString(format: "hh:mm") ?? "N/A", programDetail.endTime?.date(format: "HH:mm:ss")?.toString(format: Constants.TIME_FORMAT) ?? "N/A")
        self.lblCountCoaches.text = "\(programDetail.coaches.count)"
        self.lblCountPlayers.text = "\(programDetail.players.count)"
        
        self.lblSlots.text = String(format:"%d more slots available", programDetail.remainingSlots)
        self.lblSlots.isHidden = (programDetail.remainingSlots == 0)
        self.cvPlayers.dataSource = self
        self.cvPlayers.delegate = self
        self.cvCoach.dataSource = self
        self.cvCoach.delegate = self
        self.cvCoach.reloadData()
        self.cvPlayers.reloadData()
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var rcount :Int;
        if(collectionView == cvCoach){
            rcount = self.viewModel.programDetail?.coaches.count ?? 0
        }else {
            rcount = self.viewModel.programDetail?.players.count ?? 0
        }
        return rcount;
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath)
        if(collectionView == cvCoach){
            let sortered = self.viewModel.programDetail?.coaches.sorted(by: { (coach1, coach2) -> Bool in
                return (coach1.id == sharedInstance.loggedInUser?.id)
            })
            if let c = cell as? ButtonViewCell, let user = sortered?[indexPath.row]{
                c.set(user: user, mode: .coach) { (action) in
                    switch(action){
                    case .Add:
                        self.performSegue(withIdentifier: "sugueSelectedCoach", sender: user)
                        break;
                    case .Remove:
//                        self.performSegue(withIdentifier: "sugueSelectedPlayer", sender: user)
                        break;
                    }
                }
            }
        }else{
            if let c = cell as? PlayerCollectionViewCell, let player = self.viewModel.programDetail?.players[indexPath.row]{
                if(self.viewModel.programDetail?.programTypeId.value == 7){
                    player.ballTypeColor = Ui.ColorCodesHex.Red
                }else if(self.viewModel.programDetail?.programTypeId.value == 8){
                    player.ballTypeColor = Ui.ColorCodesHex.Orange
                }else if(self.viewModel.programDetail?.programTypeId.value == 9){
                    player.ballTypeColor = Ui.ColorCodesHex.Green
                }
                c.set(player: player, mode:.palyer)
            }
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
         let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headercell", for: indexPath);
        
        return sectionHeader
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView.tag == 0){
            //self.performSegue(withIdentifier: "sugueSelectedCoach", sender: dict)
        }else{
            if let player = self.viewModel.programDetail?.players[indexPath.row]{
                SharedAppResource.instance.selectedPlayer = player
                self.navigateToProgressView(program: self.viewModel.selectedProgram, selectedUser: player)
            }
        }
    }
    
    func loadData () -> Void{
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sugueSelectedCoach" {
            if let destinaion = segue.destination as? CoachProfileViewController {
                destinaion.coachDetails = sender as! DependantModel;
            }
        }else if segue.identifier == "segueChat" {
            navigateToChat(program: self.viewModel.selectedProgram, selectedUser: SharedAppResource.instance.selectedPlayer)
//            if let destinaion = segue.destination as? UINavigationController {
//                if let first = destinaion.viewControllers.first as? ChatListViewController{
//                    var dt:[String:Any] = ((data as? [String:Any])!)
//                    dt["selected"] = sender;
//                    first.data = dt;
//                }
//            }
        }
    }
}


extension ScheduleViewController: HeaderViewDelegate{
    func onChatClick() {
        navigateToChat(program: self.viewModel.selectedProgram, selectedUser: SharedAppResource.instance.selectedPlayer)
    }
}

//
//  ScheduleVM.swift
//  aimnow
//
//  Created by Isuru Ranasinghe on 3/11/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import aimnow_core
import SwiftyUserDefaults
import Realm
import RealmSwift


struct ArrayResultObject<T> {
    var hasMoreRecords: Bool
    var result:[T]?
    var error: errorMessage?
}

struct ResultObject<T> {
    var result:T?
    var error: errorMessage?
}

class ScheduleViewModel: NSObject {
    
    let realm = try! Realm()

    var pageIndex: Int = 1
    var selectedProgram: AssignedProgramModel!
    var domain: ProgramDomain!
    var groupedPrograms: [GroupedScheduledPrograms] = []
    var programDetail: ProgramDetail?
    
    init(domain : ProgramDomain = ProgramDomain.instance) {
        self.domain = domain
    }
    
    func getAssignedPrograms(_ completion: @escaping(_ result: ArrayResultObject<AssignedProgramModel>) -> ()) {
        if let requester = SharedAppResource.instance.loggedInUser, let requesterId = requester.id{
            let request = AssignedProgramRequestModel.init(requesterId: requesterId, assignmentTypeId: 1, unitId: 1, pageIndex: self.pageIndex)
            self.domain.getAssingedProgramsFromAPI(router: NetworkRouter.getAssignedPrograms(request)) { (assignedPrograms) in
                if let assignedPrograms = assignedPrograms.assignedPrograms{
                    if(assignedPrograms.count > 0){
                        try! self.realm.write {
                            self.realm.add(assignedPrograms, update: Realm.UpdatePolicy.all)
                        }
                    }
                }
                if(assignedPrograms.total ?? 0 > (self.pageIndex * Constants.PAGE_SIZE)){
                    self.pageIndex += 1
                    completion(ArrayResultObject(hasMoreRecords: true, result: assignedPrograms.assignedPrograms!, error: nil))
                }else{
                    completion(ArrayResultObject(hasMoreRecords: false, result: assignedPrograms.assignedPrograms!, error: nil))
                }
            }
        }
    }
    
    func getDetailAssignedPrograms(_ completion: @escaping(_ result: ResultObject<ProgramDetailResponse>) -> ()) {
        let on = selectedProgram?.on?.date(format: Constants.SERVER_DATE_FORMAT)!.apiDateString(format: Constants.SERVER_DATE_FORMAT)
        let request = AssignedProgramDetailRequestModel.init( on:on, id: self.selectedProgram.referenceId.value, unitId: 1)
        self.domain.getAssingedProgramDetail(router: NetworkRouter.getDetailedAssignedPrograms(request)) { (programDetails, error)  in
            if(error == nil){
                try! self.realm.write {
                    self.realm.add((programDetails?.program!)!, update: Realm.UpdatePolicy.all)
                }
                self.programDetail = programDetails?.program
            }
            completion(ResultObject(result: programDetails, error: errorMessage.init(error: error?.localizedDescription)))
        }
    }
    
    public func getProgramScheduleFor(player: DependantModel, date selectedDate: Date? = nil, completion:@escaping(_ groupedPrograms: [GroupedScheduledPrograms] ) -> ()){
        let sheduledPrograms = self.domain.getProgramScheduleForDate(date: selectedDate, Player: player,program: selectedProgram)
        self.groupedPrograms = sheduledPrograms
        completion(self.groupedPrograms)
    }
}

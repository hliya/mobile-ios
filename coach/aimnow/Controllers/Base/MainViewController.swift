//
//  MainViewController.swift
//
//  Created by Jon Kent on 11/12/15.
//  Copyright © 2015 Jon Kent. All rights reserved.
//

import SideMenu
import aimnow_core
import FSCalendar
import CoreData
import SwiftyUserDefaults
import RealmSwift

class   MainViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,
FSCalendarDataSource,FSCalendarDelegate{
    
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var calendar :  FSCalendar!
    
//    var items = [(key: Date?, value: [[String : Any]])]()
    var viewModel: MainViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSideMenu() 
    
        calendar.delegate = self;
        calendar.dataSource = self;
        calendar.select(Date())
        calendar.appearance.eventSelectionColor = .orange
        calendar.appearance.eventDefaultColor = .orange

        if(viewModel == nil){
            viewModel = MainViewModel()
        }
        FirebaseHelper.instance.initiateMultipartyChatObserver()
        self.setupObservers()
        ChatListViewModel().getEligibleUserList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadAssignedPrograms()
    }
    
    override func setupObservers() {
        Observers.instance.programSelectObserver.asObserver().subscribe(onNext: { (program) in
            self.sharedInstance.selectedProgram = program
            super.viewWillAppear(true)
        }).disposed(by: disposeBag)
        
        Observers.instance.fcmTokenUpdate.subscribe(onNext: { (token) in
            if(!(token?.isBlank ?? true)){
                UserDomain.instance.updateFirebaseToken(isFromRefresh: true)
            }
        }).disposed(by: disposeBag)
    }
    
    func loadAssignedPrograms() {
       HUDManager.shared.manageHUD(.loading())
        self.viewModel.getAssignedPrograms(forDate: calendar.selectedDate!) { (result: ArrayResultObject<AssignedProgramModel>) in
            if(result.hasMoreRecords){
                self.loadAssignedPrograms()
            }else{
                self.loadProgamList()
                self.loadCalendarEvents()
            }
        }
    }
    
    func loadProgamList(){
        self.viewModel.getAssignedPrograms(on:calendar.selectedDate!, completion: {(programs) in
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.reloadData()
            HUDManager.shared.manageHUD(.dismiss)
        })
    }
    
    func loadCalendarEvents(){
        self.viewModel.getAssignedPrograms(completion: {(programs) in
            for program in programs {
                self.viewModel.events.append(self.dateFormatter.string(from: (program.on?.date(format: Constants.SERVER_DATE_FORMAT))!))
            }
            self.calendar.reloadData()
        })
    }
    
    // MARK: - UICollectionViewDataSource protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if ( self.viewModel.programList.count == 0) {
            collectionView.setEmptyMessage("No data available!")
        } else {
            collectionView.restore()
        }
        return self.viewModel.programList.count;
    }

    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ScheduleCollectionViewCell
        cell.setCoach(program : self.viewModel.programList[indexPath.item])
        cell.onRequestNewProgramClosure = {
            let selectedProgram = self.viewModel.programList[indexPath.item]
            print(selectedProgram.name!)
        }
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedProgram = self.viewModel.programList[indexPath.item]
        SharedAppResource.instance.selectedProgram = selectedProgram
        self.performSegue(withIdentifier: "sugueSelected", sender: self.viewModel.programList[indexPath.row])
        
        
        
        
        
    }
    
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.default.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        //SideMenuManager.default.menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: //"RightMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        // Set up a cool background image for demo purposes
        //SideMenuManager.default.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        
        
        SideMenuManager.default.menuWidth = view.frame.width * CGFloat(0.7)
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.loadProgamList()
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateString = dateFormatter.string(from: date)
        if  self.viewModel.events.contains(dateString) {
            return 1
        }
        return 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sugueSelected" {
            if let destinaion = segue.destination as? ScheduleViewController {
                destinaion.viewModel = ScheduleViewModel()
                destinaion.viewModel.selectedProgram = sender as! AssignedProgramModel
            }
        }
    }
    
    @IBAction fileprivate func changeSwitch(_ switchControl: UISwitch) {
        SideMenuManager.default.menuFadeStatusBar = switchControl.isOn
    }
    
    @IBAction func onClickWeekView() {
        self.navigateToWeekView(programs: self.viewModel.programList)
    }
}

extension MainViewController: UISideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }
    
    func sideMenuDidAppear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }
    
    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
    
}

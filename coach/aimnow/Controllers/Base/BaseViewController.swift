//
//  BaseViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/14/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import LocalAuthentication
import aimnow_core
import RxSwift
import SwiftyUserDefaults
import RealmSwift
import SkeletonView
import JGProgressHUD

class BaseViewController : UIViewController{
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var vwHeaderRight : HeaderRightView!;
    var data: Any?
    let sharedInstance = SharedAppResource.instance
    var selectedProgram: ProgramModel? = nil
    let realm = try! Realm()
    var disposeBag = DisposeBag()
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    let reuseIdentifierHeader = "headercell"
    let dateFormatter = DateFormatter()
    var hud = SharedAppResource.instance.hud
    var viewType : E.ViewType = E.ViewType.none
    let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
    let gradient = SkeletonGradient(baseColor: UIColor.midnightBlue)
    
    @IBAction func onBack(sender:UIBarButtonItem) -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        dateFormatter.dateFormat = Constants.DATE_FORMAT
        setupObservers()
        
        MainViewModel.init().getNotificationSummary()
        Timer.scheduledTimer(withTimeInterval: TimeInterval(Constants.NOTIFICATION_INTERVAL), repeats: true) { timer in
            MainViewModel.init().getNotificationSummary()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.vwHeaderRight != nil &&  SharedAppResource.instance.selectedPlayer  != nil{
            self.vwHeaderRight.set(player:  SharedAppResource.instance.selectedPlayer!)
            self.vwHeaderRight.setProgram(selectedProgram: SharedAppResource.instance.selectedProgram)
        }
    }
     
    func setupObservers(){
        Observers.instance.playerSelectObsrver.asObserver().subscribe(onNext: { (player) in
            if let selectedPlayer = player{
                self.sharedInstance.selectedPlayer = selectedPlayer
                self.viewWillAppear(false)
            }
        }).disposed(by: disposeBag)
        
        Observers.instance.notificationSummaryObserver.asObserver().subscribe(onNext: { (summary) in
            if(self.isKind(of: NotificationViewController.self)){
                if let naviationBar2 = (self.navigationController as? TTNavigationController){
                    naviationBar2.tabBarItem.badgeValue = "12"
                }
            }
            
        }).disposed(by: disposeBag)
        
        Observers.instance.logoutObserver.asObserver().subscribe(onNext: { (clearData) in
            self.alertWithBoolResponse(message: "message_logout".localized,okActionTitle: "action_yes".localized, cancelActionTitle:"action_no".localized) { response in
                if(response){
                    if(clearData){
                        self.clearUserData()
                    }
                    self.navigateToLogin()
                }
            }
        }).disposed(by: disposeBag)
        
        Observers.instance.errorMessageObserver.asObserver().subscribe(onNext: { (error) in
            HUDManager.shared.manageHUD(.dismiss)
            if(error?.code == 401){
                self.clearUserData()
            }
            HUDManager.shared.manageHUD(.dismiss)
            self.alert(message: error?.error ?? "Something whent wrong", title: error?.title ?? "Error")
        }).disposed(by: disposeBag)
        
        let hudManager = HUDManager.shared
        hudManager.loadingIndicator
            .subscribe(onNext: { (hudType) in
                guard let hud = hudType else {
                    self.dismissHUD()
                    return
                }
                switch(hud) {
                    case .success(_):
                        self.showSuccessHUD(hud)
                    case .loading(_):
                        self.showLoadingHUD(hud)
                    case .downloading(_):
                        self.showDownloadingHUD(hud)
                    case .error(_):
                        self.showErrorHUD(hud)
                    case .dismiss:
                        self.dismissHUD()
                }
            }).disposed(by: disposeBag)
        
        Observers.instance.programSelectObserver.asObserver().subscribe(onNext: { (program) in
                   if let selectedProgram = program{
                       self.sharedInstance.selectedProgram = selectedProgram
                       self.viewWillAppear(false)
                   }
               }).disposed(by: disposeBag)
        
        Observers.instance.onUserProfileUpdating.asObserver().subscribe(onNext: { (user) in
             
            SharedAppResource.instance.loggedInUser = user;
            
            Observers.instance.onUserProfileUpdated.onNext(user);
            
        }).disposed(by: disposeBag)
    }
    
    private func clearUserData(){
        Defaults[\.token] = nil
        Defaults[\.isLoggedIn] = false
        Defaults[\.isDeviceRegistered] = false
        Defaults[\.userDetail] = nil
        Defaults[\.fcmTokenKey] = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func OnChatPressed(){
        navigateToChat(program: (sharedInstance.selectedProgram as? AssignedProgramModel), selectedUser: sharedInstance.selectedPlayer)
    }
    
    func showLoadingHUD(_ hudType: HUDType) {
        hud.textLabel.text = hudType.string()
        hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        hud.show(in: getRootVC().view)
    }
    
    func showSuccessHUD(_ hudType: HUDType) {
        hud.textLabel.text = hudType.string()
        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        hud.dismiss(afterDelay: 2.0)
        hud.show(in: getRootVC().view)
    }
    
    func showErrorHUD(_ hudType: HUDType) {
        hud.textLabel.text = hudType.string()
        hud.indicatorView = JGProgressHUDErrorIndicatorView()
        hud.show(in: getRootVC().view)
        hud.dismiss(afterDelay: 2.0)
    }

    func showDownloadingHUD(_ hudType: HUDType) {
        hud.vibrancyEnabled = true
        hud.indicatorView = JGProgressHUDPieIndicatorView()
        hud.detailTextLabel.text = "0% Complete"
        hud.textLabel.text = hudType.string()
        hud.show(in: getRootVC().view)
    }

    func dismissHUD() {
        DispatchQueue.main.async {
            self.hud.dismiss(animated: true)
        }
    }
    
    func getRootVC() -> UIViewController {
        let delegate = (UIApplication.shared.delegate) as! AppDelegate
        let vc = delegate.window?.rootViewController
        var topController = vc ?? self
        while let newTopController = topController.presentedViewController {
            topController = newTopController
        }
        return topController
    }
}

extension UIViewController{
        var appDelegate: AppDelegate {
            return UIApplication.shared.delegate as! AppDelegate
        }
}

// Applicaton navigation

extension BaseViewController{
    func navigateToCalendarController(program: ProgramModel, groupedScheduledPrograms: [GroupedScheduledPrograms], shouldPresentModal: Bool = true) {
        let storyboardName = CoachConstants.Storyboards.Calendar
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let calendarVC = storyboard.instantiateViewController(withIdentifier: "CalenderViewController") as! CalenderViewController
        calendarVC.viewModel = CalenderViewModel()
        calendarVC.viewModel.selectedProgram = program
        calendarVC.viewModel.groupedScheduledPrograms = groupedScheduledPrograms
        if(shouldPresentModal){
            navigationController?.present(calendarVC, animated: true, completion: nil)
        }else{
            navigationController?.pushViewController(calendarVC, animated: true)
        }
    }

    func navigateToWeekView(programs: [AssignedProgramModel]) {
        let storyboardName = CoachConstants.Storyboards.Main
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let weekView = storyboard.instantiateViewController(withIdentifier: "WeekViewController") as! WeekViewController
        weekView.viewModel = WeekViewModel()
        weekView.viewModel.programList = programs
        navigationController?.pushViewController(weekView, animated: true)
    }
    
    func navigateToProgressView(program: AssignedProgramModel, selectedUser: DependantModel) {
        let storyboardName = CoachConstants.Storyboards.Progress
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let progressView = storyboard.instantiateViewController(withIdentifier: "ProgressViewController") as! ProgressViewController
        progressView.viewModel = ProgressViewModel()
        progressView.viewModel.selectedProgram = program
        progressView.viewModel.user = selectedUser
        navigationController?.pushViewController(progressView, animated: true)
    }
    
    func navigateToProgressDetail(program: AssignedProgramModel, programProgressDetail: ProgramProgressDetail, selectedUser: DependantModel) {
        let storyboardName = CoachConstants.Storyboards.Progress
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let progressDetail = storyboard.instantiateViewController(withIdentifier: "ProgressDetailViewController") as! ProgressDetailViewController
        progressDetail.viewModel = ProgressViewModel()
        progressDetail.viewModel.selectedProgram = program
        progressDetail.viewModel.programProgressDetail = programProgressDetail
        progressDetail.viewModel.user = selectedUser
        navigationController?.pushViewController(progressDetail, animated: true)
    }
    
    func navigateToChat(program: AssignedProgramModel?, selectedUser: DependantModel?) {
        let storyboardName = CoachConstants.Storyboards.Chat
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let ChatListViewController = storyboard.instantiateViewController(withIdentifier: "ChatListViewController") as! ChatListViewController
        ChatListViewController.viewModel = ChatListViewModel()
        ChatListViewController.viewModel.selectedProgram = program
        ChatListViewController.viewModel.user = selectedUser
        ChatListViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(ChatListViewController, animated: true)
    }
    
    func navigateToLogin(){
        self.navigationController?.isNavigationBarHidden = true
        let storyboardName = CoachConstants.Storyboards.Main
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let rootViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
        let navigation = UINavigationController.init(rootViewController: rootViewController)
        appDelegate.window?.rootViewController = navigation
    }
    
    func navigateToProfile(viewType : E.ViewType) {
        let storyboardName = CoachConstants.Storyboards.Profile
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let ProfileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        ProfileViewController.viewModel = ProfileVM()
        ProfileViewController.viewType = viewType
        ProfileViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(ProfileViewController, animated: true)
    }
    
    func navigateToStatsView(program: AssignedProgramModel, selectedUser: DependantModel) {
        let storyboardName = CoachConstants.Storyboards.Stats
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: "StatsViewController") as! StatsViewController
        view.viewModel = StatsVM()
        view.viewModel.selectedProgram = program
        view.viewModel.user = selectedUser
        view.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(view, animated: true)
    }
}


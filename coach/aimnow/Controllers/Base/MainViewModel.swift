//
//  MainViewModel.swift
//  aimnow
//
//  Created by Isuru Ranasinghe on 3/29/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import aimnow_core
import RealmSwift
import SwiftyUserDefaults

class MainViewModel: NSObject {
    
    let realm = try! Realm()

    var pageIndex: Int = 1
    var domain: ProgramDomain!
    
    var programList: [AssignedProgramModel] = []
    var events = Array<String>()

    init(domain : ProgramDomain = ProgramDomain.instance) {
        self.domain = domain
        self.programList = []
        SharedAppResource.instance.loggedInUser = Defaults[\.userDetail]
    }
    
    func getAssignedPrograms(forDate date: Date = Date(), _ completion: @escaping(_ result: ArrayResultObject<AssignedProgramModel>) -> ()) {
        if let requester = SharedAppResource.instance.loggedInUser, let requesterId = requester.id{
            let request = AssignedProgramRequestModel.init(requesterId: requesterId, assignmentTypeId: 1, unitId: 1, pageIndex: self.pageIndex)
            self.domain.getAssingedProgramsFromAPI(router: NetworkRouter.getAssignedPrograms(request)) { (assignedPrograms) in
                if let assignedPrograms = assignedPrograms.assignedPrograms{
                    if(assignedPrograms.count > 0){
                        try! self.realm.write {
                            self.realm.add(assignedPrograms, update: Realm.UpdatePolicy.all)
                        }
                    }
                }
                if(assignedPrograms.total ?? 0 > (self.pageIndex * Constants.PAGE_SIZE)){
                    self.pageIndex += 1
                    completion(ArrayResultObject(hasMoreRecords: true, result: assignedPrograms.assignedPrograms!, error: nil))
                }else{
                    completion(ArrayResultObject(hasMoreRecords: false, result: assignedPrograms.assignedPrograms!, error: nil))
                }
            }
        }
    }
    
    func getAssignedPrograms(on: Date? = nil, completion:@escaping(_ programs: [AssignedProgramModel]) -> ()) {
        ProgramDomain.instance.getAssignedPrograms { (programs) in
            if let date = on {
                let filteredPrograms = programs.filter({$0.parentId.value == nil && ($0.on == date.apiDateString(format: Constants.SERVER_DATE_FORMAT))})
                self.programList = filteredPrograms
                completion(filteredPrograms)
            }else{
                let events = programs.filter({$0.parentId.value == nil})
                completion(events)
            }
        }
    }
    
    func getDependantUsers() {
        UserDomain.instance.getDependantUsers { (dependants) in
            let dependant = dependants.last
            SharedAppResource.instance.selectedPlayer = dependant
            Observers.instance.playerSelectObsrver.onNext(dependant)
        }
    }
    
    func getNotificationSummary(){
        if(SharedAppResource.instance.loggedInUser != nil){
            UserDomain.instance.getNotificationSummary { (summary) in
                Observers.instance.notificationSummaryObserver.onNext(summary)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notifications.notificationsAvailable), object: summary, userInfo:nil)
            }
        }
    }
}
 

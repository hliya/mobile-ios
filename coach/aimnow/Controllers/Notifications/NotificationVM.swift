//
//  NotificationVM.swift
//  parent
//
//  Created by Isuru Ranasinghe on 10/17/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import RealmSwift
import aimnow_core
import RxSwift
import RxCocoa

class NotificationVM: NSObject {
    
    let realm = try! Realm()

    var pageIndex: Int = 1
    var selectedProgram: ProgramModel!
    var domain: UserDomain!
    var notifications = BehaviorRelay<[NotificationItem]>(value: [])
    private var _totalCount = 0
    let didLoadData = PublishSubject<DataLoadingStatus>.init()

    init(domain : UserDomain = UserDomain.instance) {
        self.domain = domain
    }
    
    func getNotifications(_ loadingStatus: DataLoadingStatus = .initial) {
        if (loadingStatus == .initial || loadingStatus == .pullHeader) {
            pageIndex = 1
            if loadingStatus == .initial { HUDManager.shared.manageHUD(HUDType.loading("Loading")) }
        }
        if let requester = SharedAppResource.instance.loggedInUser, let requesterId = requester.id{
            let request = NotificationRequestModel(from: nil, to: nil, unitId: 1, id: nil, requesterId: requesterId, pageIndex: pageIndex)
            self.domain.getNotifiction(router: NetworkRouter.getNotifications(request)) { (notifications) in
                self.removeHUD()
                if (loadingStatus == .initial || loadingStatus == .pullHeader) {
                    self.notifications.removeAll()
                }
                guard let notificationModel = notifications, let notifications = notificationModel.pagedItems else { return }
                self._totalCount = notificationModel.total ?? 0
                self.notifications.accept(self.notifications.value + notifications)
                self.didLoadData.onNext((loadingStatus))
                self.nextPagingInfo()
            }
        }
    }
    
    func updateNotificationAsRead(notificationId: Int,hasSeen: Bool) {
        if let requester = SharedAppResource.instance.loggedInUser, let requesterId = requester.id{
            self.domain.updateReadNotifiction(userId: requesterId, notificationId: notificationId, hasSeen : hasSeen) { (notification) in
                self.removeHUD()
                guard let notificationModel = notification else { return }
                if let index = self.notifications.value.firstIndex(where: {$0.id == notificationModel.id}){
                    self.notifications.remove(at: index)
                    self.notifications.insert(notificationModel, at: index)
                }
                self.getNotificationSummary();
            }
        }
    }
    
    func getNotificationSummary(){
        if(SharedAppResource.instance.loggedInUser != nil){
            self.domain.getNotificationSummary { (summary) in 
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notifications.notificationsAvailable), object: summary, userInfo:nil)
            }
        }
    }
    
    fileprivate func removeHUD() {
        HUDManager.shared.manageHUD(HUDType.dismiss)
    }
    
    private func nextPagingInfo() {
        if notifications.value.count == _totalCount {
            didLoadData.onNext(.endRecord)
        } else {
            pageIndex += 1
        }
    }
}

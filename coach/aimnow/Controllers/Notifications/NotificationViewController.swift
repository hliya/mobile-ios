//
//  NotificationViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/16/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core
import RxSwift
import RxCocoa

class NotificationViewController: BaseViewController{
    
    @IBOutlet weak var tblNotifications : UITableView!
    var viewModel: NotificationVM!
    private let _notificationCell = "NotificationItemCell"
    
    override func viewDidLoad() {
        if(viewModel == nil){
            viewModel = NotificationVM()
        }
        super.viewDidLoad()
        setupUI()
        setupObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.viewModel.getNotifications(.initial)
    }
    
    fileprivate func setupUI() {
        tblNotifications.dataSource = nil
        tblNotifications.register(UINib.init(nibName: _notificationCell, bundle: nil), forCellReuseIdentifier: _notificationCell)
        tblNotifications.delegate = self
        self.tblNotifications.configRefreshHeader(with: AnowRefresher.header(), container: self) { [weak self] in
            self?.viewModel.getNotifications(.pullHeader)
        }
    }
    
    func configureTblFooter() {
        self.tblNotifications.configRefreshFooter(with: AnowRefresher.footer(), container:self) { [weak self] in
            self?.viewModel.getNotifications(.pullFooter)
        }
    }
    
    override func setupObservers() {
        viewModel.didLoadData.subscribe(onNext: { (status) in
            self.tblNotifications.setEmptyMessage("")
            guard self.viewModel.notifications.value.count > 0 else {
                self.tblNotifications.setEmptyMessage("No data available!")
                return
            }
        }).disposed(by: disposeBag)

        viewModel.didLoadData.asObservable().subscribe(onNext: { (loadingStatus) in
            if loadingStatus == .initial {
                self.configureTblFooter()
            } else if loadingStatus == .pullHeader {
                self.tblNotifications.switchRefreshHeader(to: .normal(.success, 0.5))
                self.configureTblFooter()
            } else if loadingStatus == .pullFooter {
                self.tblNotifications.switchRefreshFooter(to: .normal)
            } else if loadingStatus == .endRecord {
                self.tblNotifications.switchRefreshFooter(to: .removed)
            }
        }).disposed(by: disposeBag)
        
        viewModel.notifications.asObservable().bind(to: tblNotifications.rx.items){(tv, row, item) -> UITableViewCell in
            let cell = tv.dequeueReusableCell(withIdentifier: self._notificationCell, for: IndexPath.init(row: row, section: 0)) as! NotificationItemCell
            cell.set(item: item)
            cell.selectionStyle = .none 
            return cell
        }.disposed(by: disposeBag)
        
        self.tblNotifications.rx.itemSelected
            .map { indexPath in
                return (indexPath, self.viewModel.notifications.value[indexPath.row])
            }
            .subscribe(onNext: { pair in
                if(!(pair.1.hasSeen ?? false)){
                    self.viewModel.updateNotificationAsRead(notificationId: pair.1.id!, hasSeen: true)
                }
            }).disposed(by: disposeBag)
    }
    
    @IBAction func onNotificationClear(_ sender: UIButton) {
        if( self.viewModel.notifications.value.count > 0){
            // This logic is placed to clear all notificaion in backend for this user
            self.viewModel.updateNotificationAsRead(notificationId: self.viewModel.notifications.value[0].id!, hasSeen: false)
            self.viewModel.notifications.removeAll()
        }
    }
}

extension NotificationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self._notificationCell, for: indexPath)
        cell.selectionStyle = .none
        return cell
    }
}

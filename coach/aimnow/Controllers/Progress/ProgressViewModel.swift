//
//  ProgressViewModel.swift
//  aimnow
//
//  Created by Isuru Ranasinghe on 3/26/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import aimnow_core
import RealmSwift
import SwiftyUserDefaults
import RxSwift
import RxCocoa
import RealmSwift


class ProgressViewModel: NSObject {
    
    let realm = try! Realm()
    var selectedProgram: AssignedProgramModel!
    var groupedScheduledPrograms: [GroupedScheduledPrograms] = []
    var domain: ProgramDomain!
    var programStagesInfo: ProgramStagesInfo?
    var programProgressDetail: ProgramProgressDetail?
    var filterBy :E.FilterMode = E.FilterMode.month
    var user: DependantModel!
    var feedbacks: [FeedBack] = []
    var feedbackStatistics : [Statistic] = []
    var dataManager: DataManager?
    var stageInfo: [ProgramStagesInfo] = []
    var colors = [Ui.ColorCodesHex.Red, Ui.ColorCodesHex.Orange, Ui.ColorCodesHex.Yellow, Ui.ColorCodesHex.Purple, Ui.ColorCodesHex.Blue, Ui.ColorCodesHex.Pink]
    
    init(domain : ProgramDomain = ProgramDomain.instance) {
        self.domain = domain
        dataManager = DataManager.sharedInstance
    }
    
    func getProgramStatus(completion:@escaping(_ stageInfo: [ProgramStagesInfo]) -> ()){
        let currentStageName = self.user.currentStage?.name ?? ""
        let totalStages = self.selectedProgram.config?.stages.value ?? 3
        let currentStage: Int =  (currentStageName.split(separator: " ").last! as NSString).integerValue
        
        self.programStagesInfo = ProgramStagesInfo(name: currentStageName, currentStage: currentStage, totalStages: totalStages, currentStageId: 0, programTypeId: 0)
        self.stageInfo = [self.programStagesInfo!]
        completion(self.stageInfo)
    }
    
    func getProgramStatus2(completion:@escaping(_ stageInfo: [ProgramStagesInfo]) -> ()){
        if let programModel = self.selectedProgram, let player = SharedAppResource.instance.selectedPlayer{
            self.domain.getProgramProgressStatus(player: player, program: programModel) { (userStages) in
                self.stageInfo = userStages
                completion(userStages)
            }
        }
        
    }
    
    public func getProgramProgress(stage: Int, completion: @escaping(_ programProgress: [ProgramStageInfo]) -> ()) {
        self.domain.getProgramProgressFor(player: self.user, masterId: selectedProgram.masterId) { (programProgressSummary) in
            
            let stageList = programProgressSummary.filter({$0.date?.removeTimeStamp?.toString(format: Constants.SERVER_DATE_FORMAT) == self.selectedProgram.on})
            var stageInfo: ProgramStageInfo = ProgramStageInfo()
            stageInfo.stage = stageList.first!.stage
            stageInfo.stageName = stageList.first!.stageName
            stageInfo.progressStageSummary = stageList.first!.progressStageSummary
            
            for index in (0...stageInfo.progressStageSummary.count - 1){
                stageInfo.progressStageSummary[index].precentage = Float(stageInfo.progressStageSummary[index].precentage! / Float(stageList.count))
                stageInfo.progressStageSummary[index].total = stageInfo.progressStageSummary[index].total! / stageList.count
                for innerIndex in (0...stageInfo.progressStageSummary[index].categories.count - 1){
                    stageInfo.progressStageSummary[index].categories[innerIndex].stat  = (stageInfo.progressStageSummary[index].categories[innerIndex].stat! / stageList.count)
                }
            }
            completion([stageInfo])
        }
    }
    
    public func upsertDetails(id: Int, score: Int, assignmentId: Int, completion:@escaping(_ result: Statistic?) -> ()){
        if let userDetails = SharedAppResource.instance.loggedInUser, let coachId = userDetails.id {
            let request = PostStatisticModel.init(id: id, requesterId: coachId, ownerId: self.user.id!, score: score, unitId: 1,badgeTypeId: 1, assignmentId: assignmentId)
            FeedbackDomain.instance.updateAssesmentStats(router: NetworkRouter.updateAssesmentStats(request)) { (result, error) in
                if(error == nil){
                    if let statistic = result{
                        try! self.realm.write {
                            if let stats = self.user.currentAssigments.first(where: {$0.id.value == assignmentId})?.statistics{
                                if stats.contains(where: {$0.id == statistic.id}){
                                    stats.realm?.add(statistic, update: Realm.UpdatePolicy.all)
                                }else{
                                    stats.append(statistic)
                                }
                                self.user.currentAssigments.first(where: {$0.id.value == assignmentId})?.realm?.add(stats, update: Realm.UpdatePolicy.all)
                            }
                        }
                        completion(result)
                    }
                }else{
                    Observers.instance.errorMessageObserver.onNext(errorMessage.init(title: error))
                }
            }
        }
    }
    
    public func getFeedbackDetails(request:FeedbackRequestModel,  completion:@escaping(_ result: [FeedBack]?) -> ()){
        let router = NetworkRouter.getFeedbackDetails(request)
        FeedbackDomain.instance.getFeedbacks(router: router) { (feedbacks, error) in
            if(error == nil){
                if let feedbackList = feedbacks{
                    self.feedbacks = feedbackList
                }
                completion(feedbacks)
            }else{
                Observers.instance.errorMessageObserver.onNext(errorMessage.init(title: error))
            }
        }
    }
    
    public func getStageOverviewSummary(player: DependantModel, stage: ProgramStagesInfo, completion:@escaping (_ summary: [OverallSummaryItem]) -> ()){
        let currentStageId =
        OverallSummaryRequest.init(from: nil, to:nil, unitId: 1, assignmentTypeId: 2, id: selectedProgram.referenceId.value, requesterId: player.id,isSummaryMaster: false, isSummary: true)
        
        let router = NetworkRouter.getProgramOverallSummary(currentStageId)
        self.domain.getProgramOverallSummary(router: router) { (response, error) in
            if(error == nil){
                let x = Dictionary(grouping: response, by: { $0.programTypeId })
                var summaryList = [OverallSummaryItem]()
                for (index, x1) in x.enumerated() {
                    var scores = [Int]()
                    var summary:OverallSummaryItem!
                    for month in 1 ... 12{
                        if let value = x1.value.first(where: {$0.month == month}){
                            scores.append(Int(Double((value.score ?? 0)) * 0.1))
                        }else{
                            scores.append(0)
                        }
                    }
                    if let item = x1.value.first{
                        summary = OverallSummaryItem.init(type: "column", color: self.colors[index], data: scores, yAxis: 0, name: item.programType!, typeId: item.programTypeId!)
                        summaryList.append(summary)
                    }
                }
                let sortedList2 = summaryList.sorted { (item1, item2) -> Bool in
                    return (item1.typeId! < item2.typeId!)
                }
                completion(sortedList2)
            }else{
                Observers.instance.errorMessageObserver.onNext(errorMessage.init(title: error?.localizedDescription))
            }
        }
    }
    
    public func getFeedbackStatistics(request:StatisticRequestModel,  completion:@escaping(_ result: [Statistic]?) -> ()){
        let router = NetworkRouter.getFeedbackStatistics(request)
        FeedbackDomain.instance.getStatistics(router: router) { (statistics, error) in
            if(error == nil){
                completion(statistics)
            }else{
                Observers.instance.errorMessageObserver.onNext(errorMessage.init(title: error))
            }
        }
    }

}

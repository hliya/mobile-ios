//
//  ProgressViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/13/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core
import FlexibleSteppedProgressBar

class ProgressViewController : BaseViewController,UICollectionViewDelegate,
UICollectionViewDataSource, UIWebViewDelegate{
    
    @IBOutlet weak var webviewCharts : UIWebView!
    @IBOutlet weak var collectionViewProgress : UICollectionView!
    @IBOutlet weak var collectionViewDetail : UICollectionView!
    @IBOutlet weak var tabBarButtonDetail : UIBarButtonItem!
    @IBOutlet weak var lblStage : UILabel!
    @IBOutlet weak var btnDay : UIButton!
    @IBOutlet weak var btnWeek : UIButton!
    @IBOutlet weak var btnMonth : UIButton!
    @IBOutlet weak var progressView: FlexibleSteppedProgressBar!

    var filterBy :E.FilterMode = E.FilterMode.month
    
    var viewModel: ProgressViewModel!
    var maxIndex = -1
    var currentStage = 0
    var backgroundColor = UIColor(hexString: Ui.ColorCodesHex.Gray) // gray
    var progressColor = UIColor(hexString: Ui.ColorCodesHex.Green) // green
    var textColorHere = UIColor(hexString: Ui.ColorCodesHex.DarkGray)// dark gray
    
    var programProgressSummary = [ProgramProgressSummary]()
    var categories = [ProgramProgressDetail]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(viewModel == nil){
            viewModel = ProgressViewModel()
        }
        self.setupProgressBar()
        webviewCharts.delegate = self;
        
        if let url = Bundle.main.url(forResource: "index-rings", withExtension: "html") {
            webviewCharts.loadRequest(URLRequest(url: url))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sharedInstance.selectedProgram = self.viewModel.selectedProgram
        self.loadData();
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        (self.webviewCharts.stringByEvaluatingJavaScript(from:  "pushData([]])"))
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (self.programProgressSummary.count == 0) {
            collectionView.setEmptyMessage("No data available!")
        } else {
            collectionView.restore()
        }
        if collectionView.tag == 1 {
            return self.programProgressSummary[section].categories.count
        } else if collectionView.tag == 0 {
            return 0 //(self.program["stages"] as? Int)!
        }
        return 0;
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        var count = 0;
        if collectionView.tag == 1 {
            count = self.programProgressSummary.count
        } else if collectionView.tag == 0 {
            count = 1
        }
        return count;
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath)
        if cell is ProgressCollectionViewCell {
            let header = (cell as! ProgressCollectionViewCell)
            if collectionView.tag == 1 {
                let dt = self.programProgressSummary[indexPath.section].categories[indexPath.row]
                header.set(stat: dt) { (value) in
                    let score = Int(value * 10)
                    self.programProgressSummary[indexPath.section].categories[indexPath.row].stat = score
                    HUDManager.shared.manageHUD(.loading())
                    self.viewModel.upsertDetails(id: dt.lastStatId!, score: score, assignmentId: dt.id!) { (result) in
                        HUDManager.shared.manageHUD(.dismiss)
                        FirebaseHelper.instance.recordUpdateRequired(sender: (self.sharedInstance.loggedInUser?.id!)!, receiver: self.viewModel.user.id!, type: .statistics)
                        self.loadProgramProgress()
                    }
                }
            } else{
                header.setStage(programStagesInfo: (self.viewModel?.programStagesInfo!)! , index : indexPath.row)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headercell", for: indexPath);
        
        if sectionHeader is GroupHeaderCollectionViewCell {
            let header = (sectionHeader as! GroupHeaderCollectionViewCell)
            if collectionView.tag == 1 {
                let dt = self.programProgressSummary[indexPath.section].categories[indexPath.row]
                header.lblName.text = (dt.type!).uppercased();
                header.lblStat1.text = String((self.programProgressSummary[indexPath.section].total ?? 0))
                header.lblStat2.text = String(format: "%.1f%%", (self.programProgressSummary[indexPath.section].precentage ?? 0))
            }
        }
        
        return sectionHeader
    }
    
    func loadCharts() -> Void {
        self.viewModel?.getProgramStatus2 {(progressStatus) in
            self.progressView.numberOfPoints = self.viewModel.stageInfo.last!.totalStages
            self.currentStage = (self.viewModel.stageInfo.last!.currentStage - 1)
            self.progressView.currentIndex =  self.currentStage
            self.maxIndex = self.currentStage
            self.progressView.completedTillIndex = self.currentStage
            self.loadProgamOverviewSummary();
        }
    }
    
    func loadProgamOverviewSummary (){
       HUDManager.shared.manageHUD(.loading())
        let stage = self.currentStage + 1
        self.lblStage.text = "STAGE \(stage)"
        self.webviewCharts.reload()
        self.viewModel.getStageOverviewSummary(player: sharedInstance.selectedPlayer!, stage: self.viewModel.stageInfo[self.currentStage]) { (summaryResponse) in
            HUDManager.shared.manageHUD(.dismiss)
            let result = try? String.init(data: summaryResponse.encoded(), encoding: .utf8)!.replacingOccurrences(of: "'\'", with: "")
            (self.webviewCharts.stringByEvaluatingJavaScript(from:  "pushData(\(result!))"))
        }
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        if collectionView.tag == 1 {
            let dt = self.programProgressSummary[indexPath.section].categories[indexPath.row]
            navigateToProgressDetail(program: self.viewModel.selectedProgram, programProgressDetail: dt, selectedUser: self.viewModel.user)
        }
    }
    
    @IBAction  func showDetail(sender: UIBarButtonItem){
        if (sender.tag == 0){
            sender.image = UIImage(named: "lists", in: nil, compatibleWith: nil)
            sender.tag = 1
            self.webviewCharts.isHidden = false
            self.collectionViewDetail.isHidden = true;
            self.loadCharts()
        }else {
            sender.image = UIImage(named: "overview", in: nil, compatibleWith: nil)
            sender.tag = 0
            
            self.webviewCharts.isHidden = true
            self.collectionViewDetail.isHidden = false;
        }
        // self.loadData();
    }
    
    @IBAction func onFilterPressed (sender : UIButton){
        filterBy = E.FilterMode(rawValue: sender.tag)!
        
        self.btnDay.setImage(UIImage(named: ""), for: UIControl.State.normal)
        self.btnWeek.setImage(UIImage(named: ""), for: UIControl.State.normal)
        self.btnMonth.setImage(UIImage(named: ""), for: UIControl.State.normal)
        
        sender.setImage(UIImage(named: "active"), for: UIControl.State.normal)
        
        self.loadData();
    }
}

extension ProgressViewController: FlexibleSteppedProgressBarDelegate {
    fileprivate func setupProgressBar() {
        progressView.lineHeight = 3
        progressView.radius = 4
        progressView.progressRadius = 6
        progressView.progressLineHeight = 3
        progressView.delegate = self
        progressView.useLastState = true
        progressView.lastStateCenterColor = UIColor.orange
        progressView.selectedBackgoundColor = UIColor.orange
        progressView.selectedOuterCircleStrokeColor = UIColor.green
        progressView.lastStateOuterCircleStrokeColor = UIColor.orange
        progressView.currentSelectedCenterColor = UIColor.green
        progressView.stepTextColor = UIColor.clear
        progressView.currentSelectedTextColor = UIColor.clear
        progressView.completedTillIndex = 0
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     didSelectItemAtIndex index: Int) {
        progressBar.currentIndex = index
        self.currentStage = index
        
        if(self.webviewCharts.isHidden){
            self.loadProgramProgress()
        }else{
            self.loadProgamOverviewSummary()
        }
        
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     canSelectItemAtIndex index: Int) -> Bool {
        return (index <= maxIndex)
    }
    
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                        textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
           return ""
       }
    
    func loadData(){
        self.viewModel?.getProgramStatus2 {(progressStatus) in
            if (progressStatus.count > 0){
                self.progressView.numberOfPoints = progressStatus.last!.totalStages
                self.currentStage = (progressStatus.last!.currentStage - 1)
                self.progressView.currentIndex =  self.currentStage
                self.maxIndex = self.currentStage
                self.progressView.completedTillIndex = self.currentStage
                self.loadProgramProgress();
            }else{
                self.alertWithBoolResponse(message: "No records available") { (response) in
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func loadProgramProgress (){
       HUDManager.shared.manageHUD(.loading())
        self.lblStage.text = "STAGE \(self.currentStage + 1)"
        let stageId = self.viewModel.stageInfo[currentStage].currentStageId
        self.viewModel.getProgramProgress(stage: currentStage) { (programProgress) in
            HUDManager.shared.manageHUD(.dismiss)
            if let progressStageSummary = programProgress.first{
                self.programProgressSummary = progressStageSummary.progressStageSummary
                self.categories = self.programProgressSummary.first?.categories ?? []
                self.collectionViewDetail.reloadData()
            }
        }
    }
}

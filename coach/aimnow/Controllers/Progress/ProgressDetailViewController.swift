//
//  ProgressDetailViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/14/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import aimnow_core
import SwiftyJSON
import ImageViewer

class ProgressDetailViewController : BaseViewController, UIWebViewDelegate{
    
    @IBOutlet weak var webviewSingle : UIWebView!
    @IBOutlet weak var webviewBar : UIWebView!
    @IBOutlet weak var tabBarButtonDetail : UIBarButtonItem!
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblStage : UILabel!
    @IBOutlet weak var tvDescription : UITextView!
    
    @IBOutlet weak var cvFeedbacks: UICollectionView!
    var kFeedbackSummaryCell = "FeedbackSummaryCell"

    var items = [(key: String?, value: [[String : Any]])]()
    var program = [String:Any]()
    var player = [String:Any]()
    var skill = [String:Any]()
    var summary = Array<[String:Any]>()
    var filterBy :E.FilterMode = E.FilterMode.month
    var viewModel: ProgressViewModel!
    var playerProgramStats:[[Int : Double]] = []
    var avPlayer: AVPlayer!
    
    let kFeedbackCellIdentifier = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webviewSingle.delegate = self;
        webviewBar.delegate = self;
        
        if(viewModel == nil){
            viewModel = ProgressViewModel()
        }
        
        cvFeedbacks.register(UINib.init(nibName: kFeedbackSummaryCell, bundle: nil), forCellWithReuseIdentifier: kFeedbackSummaryCell)
        
        if let url = Bundle.main.url(forResource: "index-single", withExtension: "html") {
            webviewSingle.loadRequest(URLRequest(url: url))
        }
        
        if let url = Bundle.main.url(forResource: "index-bar", withExtension: "html") {
            webviewBar.loadRequest(URLRequest(url: url))
        }
        
        lblName.text = self.viewModel.programProgressDetail?.name
        tvDescription.text = self.viewModel.programProgressDetail?.descriptionValue
        self.viewModel?.getProgramStatus {(progressStatus) in
            self.lblStage.text = progressStatus.last?.name.uppercased()
        }
        self.loadData();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadFeedbackDataSet()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.webviewSingle.stringByEvaluatingJavaScript(from:  String(format: "pushDataRing(%d)", (self.viewModel.programProgressDetail?.stat ?? 0)!))
        if(webView == self.webviewBar){
            
            
            var srequest = StatisticRequestModel.init()
            srequest.requesterId = SharedAppResource.instance.loggedInUser?.id
            srequest.ownerId = self.viewModel.user.id
            srequest.id = self.viewModel.programProgressDetail?.referenceId
            srequest.assignmentId = self.viewModel.programProgressDetail?.id
            srequest.assignmentTypeId = 2
            srequest.ascending = false
            srequest.unitId = 1
            srequest.pageIndex = 1
            srequest.pageSize = 50
            self.viewModel.getFeedbackStatistics(request: srequest) { (statistics) in
                self.viewModel.domain.getPlayerStatsForProgram(player: self.viewModel.user, statistics: statistics!) { (programStats) in
                    self.playerProgramStats = programStats
                    self.webviewBar.stringByEvaluatingJavaScript(from: "pushData(\(self.playerProgramStats.description.replacingOccurrences(of: ": ", with: ",")))")
                }
            }
        }
    }
    
    fileprivate func reloadFeedbackDataSet(){
        sharedInstance.selectedProgram = self.viewModel.selectedProgram
        var request = FeedbackRequestModel.init()
        request.requesterId = SharedAppResource.instance.loggedInUser?.id
        request.ownerId = self.viewModel.user.id
        request.assignmentId = self.viewModel.programProgressDetail?.id
        request.assignmentTypeId = 2
        request.ascending = false
        request.unitId = 1
        request.pageIndex = 1
        request.pageSize = 50
        self.viewModel.getFeedbackDetails(request: request) { (feedbacks) in
            self.cvFeedbacks.reloadData()
            let lastSection = self.cvFeedbacks.numberOfSections - 1
            let lastRow = self.cvFeedbacks.numberOfItems(inSection: lastSection)
            let indexPath = IndexPath(row: lastRow - 1, section: lastSection)
            self.cvFeedbacks.scrollToItem(at: indexPath, at: .right, animated: true)
        }
    }
    
    @IBAction func playSlef(){
        
        //play(url: URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")!)
    }
    
    @IBAction func playBestPractice(){
        play(url: URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")!)
    }
    
    func play(url:URL) -> Void {
        avPlayer = AVPlayer(url: url)
        let controller = AVPlayerViewController()
        controller.player = avPlayer
        controller.showsPlaybackControls = true
        present(controller, animated: true) {
            self.avPlayer.play()
        }
    }
    
    func loadData () -> Void{
       HUDManager.shared.manageHUD(.loading())
        self.viewModel.getProgramProgress(stage: 0, completion: { (programProgress) in
            HUDManager.shared.manageHUD(.dismiss)
            self.webviewSingle.reload()
        })
        
    }
    
    @IBAction func onClickNewFeedback(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Progress", bundle: nil)
        let feedbackPopup: FeedbackPopUpController = (storyboard.instantiateViewController(withIdentifier: "FeedbackPopUpController") as! FeedbackPopUpController)
        feedbackPopup.viewModel = FeedbackViewModel()
        feedbackPopup.viewModel.selectedProgram = self.viewModel.selectedProgram
        feedbackPopup.viewModel.programProgressDetail = self.viewModel.programProgressDetail
        feedbackPopup.viewModel.user = self.viewModel.user
        feedbackPopup.modalPresentationStyle = .overFullScreen
        feedbackPopup.FeedbackSuccess = { feedback in
            if let feedback = feedback{
                self.viewModel.feedbacks.append(feedback)
                self.reloadFeedbackDataSet()
            }
        }
        self.topMostViewController().navigationController?.present(feedbackPopup, animated: true, completion: nil)
    }
    
    @IBAction func onOverview(_ sender: UIButton) {
        self.navigateToStatsView(program: self.viewModel.selectedProgram, selectedUser: self.viewModel.user)
    }
}

extension ProgressDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.viewModel.feedbacks.count == 0){
            collectionView.setEmptyMessage("No feedback given")
            return 0
        }else{
            collectionView.setEmptyMessage("")
            return self.viewModel.feedbacks.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kFeedbackSummaryCell, for: indexPath as IndexPath) as! FeedbackSummaryCell
        let feedback = self.viewModel.feedbacks[indexPath.row]
        cell.setFeedbackInfo(feedback: feedback)
        cell.onFeedbackVideoSelect = {feedback in
            if let attachment = feedback.attachment{
                if let path = attachment.path{ 
                    self.play(url: URL(string: Utils.getRelativeUrlPath(path: path))!)
                }
            }
        }
        cell.onFeedbackImageSelect = { imageView in
            ImageViewer.show(imageView, presentingVC: self)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize.init(width: self.view.bounds.width - 50, height: 100)
     }
    
 
}

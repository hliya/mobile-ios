//
//  FeedbackViewModel.swift
//  coach
//
//  Created by Isuru Ranasinghe on 6/3/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import aimnow_core
import SwiftyUserDefaults
import Alamofire

class FeedbackViewModel: NSObject {
    
    var selectedProgram: AssignedProgramModel!
    var programProgressDetail: ProgramProgressDetail?
    var user: DependantModel!
    
    var attachment: [AttachmentResponse?]?
    
    var domain: FeedbackDomain!
    var path: NetworkRouter?

    init(domain : FeedbackDomain = FeedbackDomain.instance) {
        self.domain = domain
    }
    
    func uploadImage(image:UIImage, shouldCompressed:Bool = true, callback:@escaping([AttachmentResponse?])->(), failure:@escaping(_ error: String) -> ()) {
        var img: UIImage = image
        if(shouldCompressed){
            img = UIImage.init(data: Utils.compressImage(image: image))!
        }
        var params = [String: String]()
        params = ["requesterId": "\((SharedAppResource.instance.loggedInUser?.id!)!)", "assignmentId": "\(selectedProgram.id)", "unitId": "\(1)", "typeId": "\(Enums.eAttachmentFileTypes.Images.rawValue)"]
        let routerPath = NetworkRouter.uploadAttachment
        DataManager.sharedInstance.uploadAttachment(path: routerPath, param: params,fileData: img.pngData()!, callback: { (attachment) in
            callback(attachment)
        }) { (error) in
            failure(error)
        }
    }
    
    func uploadVideo (videoUrl:URL,callback:@escaping([AttachmentResponse?])->(), failure:@escaping(_ error: String) -> ()) {
        do {
            var params = [String: String]()
            params = ["requesterId": "\((SharedAppResource.instance.loggedInUser?.id!)!)", "assignmentId": "\(programProgressDetail!.id!)", "unitId": "1", "typeId": "\(Enums.eAttachmentFileTypes.Videos.rawValue)"]
            let routerPath = NetworkRouter.uploadAttachment
            DataManager.sharedInstance.uploadAttachment(path: routerPath, param: params, fileData: try Data.init(contentsOf: videoUrl), callback: { (attachment) in
                callback(attachment)
            }) { (error) in
                failure(error)
            }
        } catch{
            let error = "Video loading from local storate failed"
            failure(error)
            return
        }
    }
    
    public func upsertDetails(id: Int, score: Int, assignmentId: Int, completion:@escaping(_ result: Statistic?) -> ()){
        if let userDetails = SharedAppResource.instance.loggedInUser, let coachId = userDetails.id {
            let request = PostStatisticModel.init(id: id, requesterId: coachId, ownerId: self.user.id!, score: score, unitId: 1,badgeTypeId: 1, assignmentId: assignmentId)
            self.domain.updateAssesmentStats(router: NetworkRouter.updateAssesmentStats(request)) { (result, error) in
                if(error == nil){
                    completion(result)
                }else{
                    Observers.instance.errorMessageObserver.onNext(errorMessage.init(title: error))
                }
            }
        }
    }
    
    public func upsertFeedback(feedback: UpsertFeedbackModel, completion:@escaping(_ result: FeedBack?) -> ()){
        let request = NetworkRouter.UpsertFeedbackModel(feedback)
        self.domain.updateFeedback(router: request) { (result, error) in
            if(error == nil){
                completion(result)
            }else{
                Observers.instance.errorMessageObserver.onNext(errorMessage.init(title: error))
            }
        }
    }
}

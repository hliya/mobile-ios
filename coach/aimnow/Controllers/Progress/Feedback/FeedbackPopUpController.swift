//
//  FeedbackPopUpController.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 6/3/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import aimnow_core
import MobileCoreServices
import AVFoundation

class FeedbackPopUpController: BaseViewController {

    @IBOutlet var feedbackViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var feedbackView: UIView!
    @IBOutlet var txtFeedback: UITextView!
    @IBOutlet weak var imgAttachment: UIImageView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnCameraCenterConstraint: NSLayoutConstraint!
    
    var viewModel: FeedbackViewModel!
    var upsertedFeedBack: FeedBack!
    var FeedbackSuccess:((_ feedback:FeedBack?) ->())!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(viewModel == nil){
            viewModel = FeedbackViewModel()
        }
    
        self.feedbackViewBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.75) {
            self.view.layoutIfNeeded()
        }
        self.hideWhenTappedAround()
    }
    
    func hideWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissFeedbackView(shouldUpdate:)))
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {}
    override func viewWillDisappear(_ animated: Bool) {}
    
    @IBAction func onClickCamera(_ sender: UIButton) {
        self.showAttachmentPicker(sender)
    }
    
    @IBAction func onClickSend(_ sender: UIButton) {
        guard !(self.txtFeedback.text?.isBlank ?? true) else {
            self.alert(message: "Please enter your feedback")
            return
        }
        
        var model = UpsertFeedbackModel.init()
        model.comment = txtFeedback.text
        model.assignmentId = self.viewModel.programProgressDetail?.id
        model.feedbackTypeId = 1
        model.requesterId = SharedAppResource.instance.loggedInUser?.id
        model.ownerId = self.viewModel.user.id
        model.statisticsId = self.viewModel.programProgressDetail?.lastStatId
        model.unitId = 1
        model.rating = self.viewModel.programProgressDetail?.stat
        if let attachments = self.viewModel.attachment, let feedbackAtt = attachments.last {
            model.attachmentId = feedbackAtt?.id
        }
        self.viewModel.upsertFeedback(feedback: model) { (response) in
            self.upsertedFeedBack = response
            self.alertWithBoolResponse(message: "Feedback submitted successfully") { (result) in
                self.dismissFeedbackView(shouldUpdate: true)
            }
        }
    }
    
    @objc func dismissFeedbackView(shouldUpdate: Bool = false) {
        self.view.endEditing(true)
        self.feedbackViewBottomConstraint.constant = -450
        self.view.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.75, animations: {
            self.view.layoutIfNeeded()
        }) { (result) in
            self.dismiss(animated: true, completion: {
                if(shouldUpdate){
                    if let popUpDismiss = self.FeedbackSuccess{
                        popUpDismiss(self.upsertedFeedBack)
                    }
                }
                
            })
        }
    }
}

extension FeedbackPopUpController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func checkCameraPermision(completion: ((AVAuthorizationStatus) -> Void)?) {
        let mediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: mediaType)
        switch cameraAuthorizationStatus {
        case .authorized:
            completion?(cameraAuthorizationStatus)
        case .denied:
            completion?(cameraAuthorizationStatus)
        case .restricted:
            completion?(cameraAuthorizationStatus)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: mediaType, completionHandler: { (granted) in
                DispatchQueue.main.async {
                    if granted {
                        completion?(.authorized)
                    } else {
                        completion?(.denied)
                    }
                }
            })
        @unknown default:
            completion?(.restricted)
        }
    }
    
    func showAttachmentPicker(_ sender: UIButton?) {
        let camera = CameraHelper(delegate_: self, duration: 60.0, size: 25)
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        optionMenu.popoverPresentationController?.permittedArrowDirections = .up
        
        let takePhoto = UIAlertAction(title: "Camera", style: .default) { (alert : UIAlertAction!) in
            self.checkCameraPermision { (status) in
                if status != .authorized {
                    self.alert(message: "", title: "The camera could not be turned on, please check your device's settings.")
                    return
                }
                camera.presentPhotoCamera(target: self, canEdit: false)
            }
        }
        let takeVideo = UIAlertAction(title: "Video", style: .default) { (alert : UIAlertAction!) in
            self.checkCameraPermision { (status) in
                if status != .authorized {
                    self.alert(message: "", title: "The camera could not be turned on, please check your device's settings.")
                    return
                }
                camera.presentVideoCamera(target: self, canEdit: false)
            }
        }
        let shareVideo = UIAlertAction(title: "Video Library", style: .default) { (alert : UIAlertAction) in
            camera.presentVideoLibrary(target: self, canEdit: false)
        }
        let sharePhoto = UIAlertAction(title: "Image Library", style: .default) { (alert : UIAlertAction) in
            camera.presentPhotoLibrary(target: self, canEdit: false)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (alert : UIAlertAction) in
        }
        
        optionMenu.addAction(takePhoto)
        optionMenu.addAction(takeVideo)
        optionMenu.addAction(shareVideo)
        optionMenu.addAction(sharePhoto)
        optionMenu.addAction(cancel)
        
        Utils.presentUniversalAlertController(controller: self, alertController: optionMenu, sourceView: sender)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        let mediaType:String = (info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaType)] as! String)
        if (mediaType == kUTTypeMovie as String || mediaType == kUTTypeVideo as String){
            let videoUrl:URL =  (info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as! URL)
            Utils.delay(0.1) {
               HUDManager.shared.manageHUD(.loading())
                self.btnSend.isUserInteractionEnabled = false
                self.moveCameraToLeft()
            }
            Utils.compressVideo(videoPath: videoUrl) { (videoUrl) in
                self.btnSend.isUserInteractionEnabled = true
                if let videoUrl = videoUrl{                    
                    self.viewModel.uploadVideo(videoUrl: videoUrl, callback: { (attachement) in
                        HUDManager.shared.manageHUD(.dismiss)
                        self.viewModel.attachment = attachement
                        Utils.getThumbForVideo(videoPath: videoUrl, completion: { image in
                            self.imgAttachment.image = image
                        })
                    }, failure: {(error) in
                        HUDManager.shared.manageHUD(.dismiss)
                        print(error)
                    })
                }
            }
        } else {
            let image = (info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage)
            let img = image.fixedOrientation()
            Utils.delay(0.1) {
               HUDManager.shared.manageHUD(.loading())
                self.btnSend.isUserInteractionEnabled = false
                self.moveCameraToLeft()
            }
            let compressedImg = Utils.compressImage(image: img)
            self.viewModel.uploadImage(image: UIImage.init(data: compressedImg)!, shouldCompressed: true, callback: { (attachement) in
                self.btnSend.isUserInteractionEnabled = true
                HUDManager.shared.manageHUD(.dismiss)
                self.viewModel.attachment = attachement
                self.imgAttachment.image = img
            }, failure: {(error) in
                HUDManager.shared.manageHUD(.dismiss)
                print(error)
            })
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func moveCameraToLeft(){
        self.btnCameraCenterConstraint.constant = -100
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
        return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
    }

    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
        return input.rawValue
    }
}

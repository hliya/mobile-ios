//
//  OverViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/19/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import SideMenu
import aimnow_core

class OverViewController: BaseViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblLocation : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblCountPlayers : UILabel!
    @IBOutlet weak var lblCountCoaches : UILabel!
    @IBOutlet weak var lblSlots : UILabel!
    @IBOutlet weak var skeletonableView: UIView!
    @IBOutlet weak var btnQr: UIButton!

    var imagePicker: UIImagePickerController!
    
    var viewModel: OverViewVM!
    var loadingViews: [UIView] = []
    
    enum ImageSource {
        case photoLibrary
        case camera
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        setupSideMenu()
        if(viewModel == nil){
            viewModel = OverViewVM()
        }

        NotificationCenter.default.addObserver(self, selector: #selector(OnPlayerSelected), name: NSNotification.Name(rawValue: K.NotificationKey.OnPlayerSelected),object: nil)
        
        self.resetUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sharedInstance.selectedProgram = self.viewModel.selectedProgram
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showSkeletonView()
        self.loadProgramDetails()
    }
    
    
    private func showSkeletonView() {
        loadingViews.append(skeletonableView)
        for loadingView in loadingViews {
            for subView in loadingView.subviews where subView.isSkeletonable {
                subView.skeletonCornerRadius = 10
                subView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation,transition: .crossDissolve(0.25))
            }
        }
    }
    
    func stopSkeletonView() {
        for loadingView in loadingViews {
            for subView in loadingView.subviews where subView.isSkeletonable {
                subView.hideSkeleton(transition: .crossDissolve(0.25))
            }
        }
    }
    
    @IBAction func onQrcodePressed() -> Void{
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            selectImageFrom(.photoLibrary)
            return
        }
        selectImageFrom(.camera)
    }
    
    
    func selectImageFrom(_ source: ImageSource){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        switch source {
        case .camera:
            imagePicker.sourceType = .camera
        case .photoLibrary:
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var rcount :Int;
        return self.viewModel.programDetail?.players.count ?? 0;
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath)
        if let c = cell as? PlayerCollectionViewCell, let player = self.viewModel.programDetail?.players[indexPath.row]{
            if(self.viewModel.programDetail?.programTypeId.value == 7){
                player.ballTypeColor = Ui.ColorCodesHex.Red
            }else if(self.viewModel.programDetail?.programTypeId.value == 8){
                player.ballTypeColor = Ui.ColorCodesHex.Orange
            }else if(self.viewModel.programDetail?.programTypeId.value == 9){
                player.ballTypeColor = Ui.ColorCodesHex.Green
            }
            c.setOverViewFor(player: player, mode:.palyer)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headercell", for: indexPath);
        
        return sectionHeader
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let player = self.viewModel.programDetail?.players[indexPath.row]
        sharedInstance.selectedPlayer = player
        let selectedProgram = self.viewModel.programDetail! as AssignedProgramModel
        self.navigateToProgressView(program: selectedProgram, selectedUser: player!)
    }
    
    fileprivate func loadProgramDetails(){
        HUDManager.shared.manageHUD(.loading())
        self.viewModel.getDetailAssignedPrograms { (detail) in
            HUDManager.shared.manageHUD(.dismiss)
            if(!SharedAppResource.instance.isCoachAttend){
                self.viewModel.recordCoachAttendance { (result) in
                    SharedAppResource.instance.isCoachAttend = result
                }
            }
            if let result = detail.result, let program = result.program{
                self.stopSkeletonView()
                self.updateUI(programDetail: program)
            }else{
                self.resetUI()
                self.alertWithBoolResponse(message: detail.error?.error ?? "Can not retrieve program details") { (response) in
                    if(response){
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    fileprivate func updateUI(programDetail: ProgramDetail){
        sharedInstance.selectedProgram = programDetail
        dateFormatter.dateFormat = "dd"
        self.lblDate.text = dateFormatter.string(from : (programDetail.on?.date(format: Constants.SERVER_DATE_FORMAT))!)
            dateFormatter.dateFormat = "yyyy-MM-dd"

        self.navigationItem.title = programDetail.name
            
        self.lblLocation.text =  programDetail.location?.name?.uppercased() ?? "TBD"
        self.lblTime.text = String(format:"%@ - %@",programDetail.startTime?.date(format: "HH:mm:ss")?.toString(format: "hh:mm") ?? "N/A", programDetail.endTime?.date(format: "HH:mm:ss")?.toString(format: Constants.TIME_FORMAT) ?? "N/A")
        self.lblCountPlayers.text = "\(programDetail.players.count)"
            
        self.lblSlots.text = String(format:"%d more slots available", programDetail.remainingSlots)
        self.lblSlots.isHidden = (programDetail.remainingSlots == 0)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.reloadData()
        self.btnQr.isEnabled = true;
    }
    
    fileprivate func resetUI(){
        self.btnQr.isEnabled = false
        self.lblDate.text = "TBD"
        self.lblLocation.text = "TBD"
        self.lblTime.text = "TBD"
        self.lblCountPlayers.text = "0"
        self.lblSlots.isHidden = true
        self.viewModel.programDetail = nil;
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.reloadData()
        self.navigationItem.title = "no assignment available".uppercased()
    }
    
    @objc func  OnPlayerSelected(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            self.performSegue(withIdentifier: "sugueSelectedPlayer", sender: dict)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sugueSelectedCoach" {
            if let destinaion = segue.destination as? CoachProfileViewController {
                destinaion.data = sender;
            }
        } else if segue.identifier == "sugueSelectedPlayer" {
            if let destinaion = segue.destination as? ProgressViewController {
                var dt:[String:Any] = ((data as? [String:Any])!)
                dt["selected_player"] = sender;
                destinaion.data = dt;
            }
        }
    }
    
    
    
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.default.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        //SideMenuManager.default.menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: //"RightMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        // Set up a cool background image for demo purposes
        //SideMenuManager.default.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        
        
        SideMenuManager.default.menuWidth = view.frame.width * CGFloat(0.7)
    }
    
    @IBAction fileprivate func changeSwitch(_ switchControl: UISwitch) {
        SideMenuManager.default.menuFadeStatusBar = switchControl.isOn
    }
    
}


extension OverViewController: UISideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }
    
    func sideMenuDidAppear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }
    
    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
    
}

extension OverViewController {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage,
            let detector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy: CIDetectorAccuracyHigh]),
            let ciImage = CIImage(image: pickedImage),
            let features = detector.features(in: ciImage) as? [CIQRCodeFeature] {
            let playerId = features.reduce("") { $0 + ($1.messageString ?? "") }
           HUDManager.shared.manageHUD(.loading())
            self.viewModel.recordAttendanceForPlayer(playerId: playerId , completion: { (response) in
                HUDManager.shared.manageHUD(.dismiss)
                if(response == true){
                    self.loadProgramDetails()
                    print("Recored added successfully")
                }
            })
        }
        dismiss(animated: true, completion: nil)
    }
}

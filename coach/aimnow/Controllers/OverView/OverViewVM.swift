//
//  OverViewVM.swift
//  coach
//
//  Created by Isuru Ranasinghe on 5/17/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import aimnow_core
import RealmSwift
import SwiftyUserDefaults

class OverViewVM: NSObject {
    
    let realm = try! Realm()

    var pageIndex: Int = 1
    var selectedProgram: AssignedProgramModel!
    var domain: ProgramDomain!
    var groupedPrograms: [GroupedScheduledPrograms] = []
    var programDetail: ProgramDetail?
    
    init(domain : ProgramDomain = ProgramDomain.instance) {
        self.domain = domain
    }
        
    func getDetailAssignedPrograms(_ completion: @escaping(_ result: ResultObject<ProgramDetailResponse>) -> ()) {
        domain.getAssignedProgramsFor(date: Date()) { (assignedPrograms) in
            if let program = assignedPrograms.first(where: {Date().toString(format: "HH:mm:ss").date(format: "HH:mm:ss", includeUTC: true)!.isInRange(date: ($0.startTime?.date(format: "HH:mm:ss", includeUTC: true))!, and: ($0.endTime?.date(format: "HH:mm:ss", includeUTC: true))!)}){
                let on = Date().apiDateString(format: Constants.SERVER_DATE_FORMAT)
                let request = AssignedProgramDetailRequestModel.init( on:on,id: program.masterId, unitId: 1)
                self.domain.getAssingedProgramDetail(router: NetworkRouter.getDetailedAssignedPrograms(request)) { (programDetails, error)  in
                    if(error == nil){
                        try! self.realm.write {
                            self.realm.add((programDetails?.program!)!, update: Realm.UpdatePolicy.all)
                        }
                        self.programDetail = programDetails?.program
                    }
                    completion(ResultObject(result: programDetails, error: nil))
                }
            }else{
                completion(ResultObject(result: nil, error: errorMessage.init(error: "No programs scheduled for now")))
            }
        }
    }
    
    func recordAttendanceForPlayer(playerId: String, completion:@escaping(_ response: Bool) -> ()){
        let time = Date().toString(format: "HH:mm")
        //var playerId = "1b22e165-f1d6-499d-9332-0e174d83ec48";
        if let playerAssignments = self.programDetail?.players.first(where: {$0.id == playerId})?.currentAssigments, let programId = playerAssignments.first(where: {$0.masterId.value == programDetail!.masterId})?.id{
            if let requester = SharedAppResource.instance.loggedInUser, let requesterId = requester.id{
                let request = AttendanceRequest.init(on: Date().apiDateString(format: Constants.DATE_FORMAT), id: programId.value, unitId: 1, assignmentTypeId: 2, deviceTypeId: 1, ownerId: playerId, requesterId: requesterId, time: time)
                self.domain.recordPlayerAttendance(router: NetworkRouter.recordPlayerAttendance(request)) { (response, error) in
                    if(error == nil){
                        completion(true)
                    }else{
                        completion(false)
                    }
                }
            }
        }
    }
    
    func recordCoachAttendance(completion:@escaping(_ response: Bool) -> ()){
        let time = Date().toString(format: "HH:mm")
        if let requester = SharedAppResource.instance.loggedInUser, let requesterId = requester.id{
            let request = AttendanceRequest.init(on: Date().apiDateString(format: Constants.DATE_FORMAT), id: programDetail?.id, unitId: 1, assignmentTypeId: 1, deviceTypeId: 1, ownerId: requesterId, requesterId: requesterId, time: time)
            
            self.domain.recordPlayerAttendance(router: NetworkRouter.recordPlayerAttendance(request)) { (response, error) in
                if(error == nil){
                    completion(true)
                }else{
                    completion(false)
                }
            }
        }
        
    }
}

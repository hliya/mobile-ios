//
//  WeekViewModel.swift
//  coach
//
//  Created by Isuru Ranasinghe on 5/16/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import aimnow_core
import Realm
import RealmSwift

class WeekViewModel: NSObject {
    
    let realm = try! Realm()
    var programsForDate: [AssignedProgramModel] = []
    var domain: ProgramDomain!
    var selectedDate: Date!
    var startTime: String!
    var endTime: String!
    var sharedInstance = SharedAppResource.instance
    
    var programList: [AssignedProgramModel] = []
    var events = Array<String>()
    
    init(domain : ProgramDomain = ProgramDomain.instance) {
        self.domain = domain
    }
    
}

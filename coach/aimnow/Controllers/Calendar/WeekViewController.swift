//
//  WeekViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/18/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import FSCalendar
import aimnow_core

class WeekViewController : BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,
FSCalendarDataSource,FSCalendarDelegate{
    
    @IBOutlet weak var  calendar: FSCalendar!
    @IBOutlet weak var collectionView : UICollectionView!
    
    var viewModel:WeekViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.calendar.scope = FSCalendarScope.week;
        
        calendar.delegate = self;
        calendar.dataSource = self;
        calendar.select(Date())
        calendar.appearance.eventSelectionColor = .orange
        calendar.appearance.eventDefaultColor = .orange

        self.loadData(date: calendar.selectedDate!)
        
        self.loadCalendarEvents()
    }
    
    override func onBack(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UICollectionViewDataSource protocol
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.programsForDate.count;
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if ( self.viewModel.programsForDate.count == 0) {
            collectionView.setEmptyMessage("No data available!")
        } else {
            collectionView.restore()
        }
        return 1;
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath)
        if cell is ScheduleCollectionViewCell{
            (cell as! ScheduleCollectionViewCell).setCoachWeekView(program : self.viewModel.programsForDate[indexPath.row])
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headercell", for: indexPath);
        return sectionHeader
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.loadData(date: date)
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: nil)
        Observers.instance.programSelectObserver.onNext(self.viewModel.programsForDate[indexPath.row])
    }
    
    func loadCalendarEvents(){
        for program in self.viewModel.programList {
            self.viewModel.events.append(self.dateFormatter.string(from: (program.on?.date(format: Constants.SERVER_DATE_FORMAT))!))
        }
        self.calendar.reloadData()
    }
        
    func loadData (date:Date) -> Void{
       HUDManager.shared.manageHUD(.loading())
        let programs = self.viewModel.programList.filter({$0.on == date.apiDateString(format: Constants.SERVER_DATE_FORMAT)})
        self.viewModel.programsForDate = programs
        self.collectionView.reloadData()
        HUDManager.shared.manageHUD(.dismiss)
    }
}

//
//  CalenderViewModel.swift
//  aimnow
//
//  Created by Isuru Ranasinghe on 3/25/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import RealmSwift
import aimnow_core
import SwiftyUserDefaults

class CalenderViewModel: NSObject {
    
    let realm = try! Realm()
    var selectedProgram: ProgramModel!
    var groupedScheduledPrograms: [GroupedScheduledPrograms] = []
    var domain: ProgramDomain!
    
    var selectedDate: Date!
    var startTime: String!
    var endTime: String!
    var sharedInstance = SharedAppResource.instance
    
    
    init(domain : ProgramDomain = ProgramDomain.instance) {
        self.domain = domain
    }
    
    func getAvailableLocationsForProgram(completion:@escaping([Location]) -> ()) {
        self.domain.getProgramLocations { (locations) in
            completion(locations)
        }
    }
    
    func requestNewProgram(completion:@escaping(_ isSuccess: Bool,_ error: String?) -> ()){
        let loggedInUser = SharedAppResource.instance.loggedInUser
        let newProgram = NewProgramRequestModel(programID: self.selectedProgram.id, requesterId: loggedInUser!.id!, requestedTo: self.sharedInstance.selectedPlayer!.id!, date: selectedDate.apiDateString(format: Constants.DATE_FORMAT), time: startTime)
        let router = NetworkRouter.requestForProgram(newProgram)
        self.domain.requestNewProgram(router: router) { (response, error) in
            if(error != nil){
                completion(false, error)
            }else{
                completion(true, nil)
            }
        }
    }
}

//
//  SettingsViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/16/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core
import SwiftyUserDefaults

class SettingsViewController: BaseViewController {
    
    @IBOutlet weak var swTouch: UISwitch!
    @IBOutlet weak var imgImage: UIImageView!
    @IBOutlet weak var tvSettings: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tvSettings.delegate = self;
        self.tvSettings.dataSource = self;
        //self.swTouch.isOn = Defaults[\.isTouchIDEnabled] ?? false
    }
    
    @IBAction func enableTouchId(sender:UISwitch) -> Void{
        
        if sender.isOn {
            authenticationWithTouchID(mode: TouchEnableViewKeys.settings)
        }else {
             Defaults[\.isTouchIDEnabled] = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sugueProfile" {
            if let destinaion = segue.destination as? ProfileViewController {
                destinaion.data = sender;
                destinaion.viewType = E.ViewType.profile
            }
        }
    }
}

extension SettingsViewController: UITableViewDelegate,UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(format: "cell%i", indexPath.row), for: indexPath)
        cell.selectionStyle = .none
        
        if(indexPath.row == 1){
            for sv in  cell.contentView.subviews {
                if(sv.isKind(of: UISwitch.self)){
                    (sv as! UISwitch).isOn = Defaults[\.isTouchIDEnabled] ?? false
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 0){
            self.navigateToProfile(viewType: E.ViewType.profile)
        }
    }
}


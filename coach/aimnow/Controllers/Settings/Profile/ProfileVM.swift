//
//  ProfileVM.swift
//  coach
//
//  Created by Isuru Ranasinghe on 9/27/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import Alamofire
import aimnow_core

class ProfileVM: NSObject {
    
    var domain: UserDomain = UserDomain.instance
    var profileInfo: [Profile] = []
    var attachment: [AttachmentResponse?]?  
    var path: NetworkRouter?
    
    
    func getCoachProfile(userId: String, completion: @escaping(_ profileInfo: [Profile], _ error: Error?) -> ()) {
        domain.getProfile(userId: userId) { (profiles, error) in
            self.profileInfo = profiles
            completion(profiles, error)
        }
    }
     
    func updateProfile(profile: UserProfileUpdateRequest, callback:@escaping(UserModel)->(), failure:@escaping(_ error: Error?) -> ()) {
        
        domain.updateUserProfile(profile: profile, completion:  { (userModel, error ) in
            if((error) != nil){
                failure(error);
            }else{
                callback(userModel!)
            }
        })
    }
}

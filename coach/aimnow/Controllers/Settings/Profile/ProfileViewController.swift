//
//  ProfileViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/18/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core
import AlamofireImage
import MobileCoreServices
import AVFoundation

class ProfileViewController: BaseViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var imgAvatar : UIImageView!
    @IBOutlet weak var txtName : UITextField!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtPhone : UITextField!
    @IBOutlet weak var txtAge : UITextField!
    @IBOutlet weak var cvCurrentAssignments: UICollectionView!
    @IBOutlet weak var lblInformation : UILabel!
    
    var coachDetails: DependantModel?
    var viewModel: ProfileVM?
    var imagePicker: UIImagePickerController!
    var compressedImg :Data?
    
    let kCurrentAssignmentCell = "CurrentAssignmentCell"
    let kProfileCell = "ProfileInfoCell"
    
    override func onBack(sender: UIBarButtonItem) {
       super.onBack(sender:sender);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(viewModel == nil){
            viewModel = ProfileVM.init()
        }
        self.cvCurrentAssignments.register(UINib.init(nibName: self.kProfileCell, bundle: nil),
        forCellWithReuseIdentifier: self.kProfileCell)
        self.updateUi();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
    }
    
    override func setupObservers() {
        super.setupObservers();
        
        Observers.instance.onUserProfileUpdated.asObserver().subscribe(onNext: { (user) in
            self.updateUi(); 
        }).disposed(by: disposeBag)
    }
    
    func updateUi() -> Void {
        if let user = SharedAppResource.instance.loggedInUser {
            
            if let profileImage = user.profileImage, let url = URL.init(string: Utils.getRelativeUrlPath(path: profileImage)){
                self.imgAvatar.af.setImage(withURL:  url)
            }else{
                self.imgAvatar.image = UIImage.init(named: "profile")
            }
            
            self.txtName.text = user.name
            self.txtEmail.text = user.email
            self.txtPhone.text = user.phoneNumber
            self.txtAge.text = "\(user.age!)"
            
            viewModel?.getCoachProfile(userId : (user.id!), completion: { (profiles, error) in
                self.cvCurrentAssignments.reloadData()
            })
        }
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
        return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
    }

    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
        return input.rawValue
    }
    
    
    func checkCameraPermision(completion: ((AVAuthorizationStatus) -> Void)?) {
        let mediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: mediaType)
        switch cameraAuthorizationStatus {
        case .authorized:
            completion?(cameraAuthorizationStatus)
        case .denied:
            completion?(cameraAuthorizationStatus)
        case .restricted:
            completion?(cameraAuthorizationStatus)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: mediaType, completionHandler: { (granted) in
                DispatchQueue.main.async {
                    if granted {
                        completion?(.authorized)
                    } else {
                        completion?(.denied)
                    }
                }
            })
        @unknown default:
            completion?(.restricted)
        }
    }
    
    func showAttachmentPicker(_ sender: UIButton?) {
        let camera = CameraHelper(delegate_: self, duration: 60.0, size: 25)
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        optionMenu.popoverPresentationController?.permittedArrowDirections = .up
        
        let takePhoto = UIAlertAction(title: "Camera", style: .default) { (alert : UIAlertAction!) in
            self.checkCameraPermision { (status) in
                if status != .authorized {
                    self.alert(message: "", title: "The camera could not be turned on, please check your device's settings.")
                    return
                }
                camera.presentPhotoCamera(target: self, canEdit: false)
            }
        }
        let takeVideo = UIAlertAction(title: "Video", style: .default) { (alert : UIAlertAction!) in
            self.checkCameraPermision { (status) in
                if status != .authorized {
                    self.alert(message: "", title: "The camera could not be turned on, please check your device's settings.")
                    return
                }
                camera.presentVideoCamera(target: self, canEdit: false)
            }
        }
        let shareVideo = UIAlertAction(title: "Video Library", style: .default) { (alert : UIAlertAction) in
            camera.presentVideoLibrary(target: self, canEdit: false)
        }
        let sharePhoto = UIAlertAction(title: "Image Library", style: .default) { (alert : UIAlertAction) in
            camera.presentPhotoLibrary(target: self, canEdit: false)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (alert : UIAlertAction) in
        }
        
        optionMenu.addAction(takePhoto)
        optionMenu.addAction(takeVideo)
        optionMenu.addAction(shareVideo)
        optionMenu.addAction(sharePhoto)
        optionMenu.addAction(cancel)
        
        Utils.presentUniversalAlertController(controller: self, alertController: optionMenu, sourceView: sender)
    }
    
    @IBAction func onProfileUpdate(sender: UIButton) {
        Utils.delay(0.1) {
           HUDManager.shared.manageHUD(.loading())
        }
        
        var user = UserProfileUpdateRequest.init();
        let names = self.txtName.text?.split(separator: " ")
        if( names != nil && names!.count > 1){
            user.firstName = String(names![0])
            user.lastName =  String(names![1])
        }
        user.email = self.txtEmail.text
        user.phoneNumber = self.txtPhone.text
        user.age = Int(self.txtAge.text!) ?? 0;
        if( compressedImg != nil) {
            user.profileImage = UIImage.init(data: compressedImg!)!
        }else{
            user.profileImage = self.imgAvatar.image
        }
        user.id = SharedAppResource.instance.loggedInUser?.id!; 
        self.viewModel!.updateProfile(profile:  user , callback: { (user) in
            HUDManager.shared.manageHUD(.dismiss)
            Observers.instance.onUserProfileUpdating.onNext(user);
        }, failure: {(error) in
            HUDManager.shared.manageHUD(.dismiss)
        })
    }
}

extension ProfileViewController : UIImagePickerControllerDelegate {

    enum ImageSource {
        case photoLibrary
        case camera
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        let image = (info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage)
        let img = image.fixedOrientation()
        self.imgAvatar.image = img
        self.compressedImg = Utils.compressImage(image: img)
        dismiss(animated: true, completion: nil)
    }
    
    func selectImageFrom(_ source: ImageSource){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        switch source {
        case .camera:
            imagePicker.sourceType = .camera
        case .photoLibrary:
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func onProfileImageChange(sender: UIButton) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            selectImageFrom(.photoLibrary)
            return
        }
        selectImageFrom(.camera)
    }
}

extension ProfileViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.viewModel?.profileInfo.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ProfileHeader", for: indexPath) as? ProfileHeader {
            let header = eProfileHeader(rawValue:(self.viewModel?.profileInfo[indexPath.section].type)!)?.description
            sectionHeader.lblTitle.text = header
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: collectionView.bounds.width - 20, height: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kProfileCell, for: indexPath) as! ProfileInfoCell
            if let profile = self.viewModel?.profileInfo[indexPath.row + indexPath.section]{
                cell.setProfileInfo(info: profile)
            }
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kProfileCell, for: indexPath) as! ProfileInfoCell
        if let profile = self.viewModel?.profileInfo[indexPath.row]{
            cell.setProfileInfo(info: profile)
            let size = Utils.sizeForLabel(profile.content ?? "", font: cell.lblContent.font ?? UIFont.systemFont(ofSize: 14))
            return CGSize.init(width: collectionView.bounds.width, height: size.height + 20)

        }
        return CGSize.zero
    }
}

//
//  CoachProfileViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/18/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core
import AlamofireImage

class CoachProfileViewController: BaseViewController {
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var lblPhone : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var imgAvatar : UIImageView!
    @IBOutlet weak var cvCurrentAssignments: UICollectionView!
    
    @IBOutlet weak var sgProfile: UISegmentedControl!
    
    var coachDetails: DependantModel?
    var viewModel: ProfileVM?
     
    let kCurrentAssignmentCell = "CurrentAssignmentCell"
    let kProfileCell = "ProfileInfoCell"
    
    override func onBack(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(viewModel == nil){
            viewModel = ProfileVM.init()
        }
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        sgProfile.setTitleTextAttributes(titleTextAttributes, for: UIControl.State.selected)
        sgProfile.setTitleTextAttributes(titleTextAttributes, for: UIControl.State.normal)
        
        self.cvCurrentAssignments.register(UINib.init(nibName: self.kCurrentAssignmentCell, bundle: nil),
        forCellWithReuseIdentifier: self.kCurrentAssignmentCell)
        self.cvCurrentAssignments.register(UINib.init(nibName: self.kProfileCell, bundle: nil),
        forCellWithReuseIdentifier: self.kProfileCell)
        
        if let coach = self.coachDetails {
            self.lblName.text = coach.name
            if let profileImage = coach.profileImage, let url = URL.init(string:  Utils.getRelativeUrlPath(path:profileImage)){
                self.imgAvatar.af.setImage(withURL: url)
            }else{
                self.imgAvatar.image = UIImage.init(named: "profile")
            }
            lblPhone.text = coach.phoneNumber
            lblEmail.text = coach.email
            
            if let typeId = coach.typeId.value{
                self.lblDesignation.text = Enums.eUserType.init(rawValue:typeId)?.description
            }else{
                self.lblDesignation.text = "Designation not assigned"
            }
            if let address = coach.address {
                self.lblAddress.text = address
                self.lblAddress.isHidden = false
            }else{
                self.lblAddress.isHidden = true
            }
        }
    }
    
    @IBAction func onSegmentChange(_ sender: UISegmentedControl) {
        if(sender.selectedSegmentIndex == 1){
            viewModel?.getCoachProfile(userId : (self.coachDetails?.id!)!, completion: { (profiles, error) in
                self.cvCurrentAssignments.reloadData()
            })
        }else{
            self.cvCurrentAssignments.reloadData()
        }
    }
    
}

extension CoachProfileViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if(sgProfile.selectedSegmentIndex == 0){
            return 1
        }else{
            return self.viewModel?.profileInfo.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ProfileHeader", for: indexPath) as? ProfileHeader {
            let header = eProfileHeader(rawValue:(self.viewModel?.profileInfo[indexPath.section].type)!)?.description
            sectionHeader.lblTitle.text = header
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if(sgProfile.selectedSegmentIndex == 0){
            return CGSize.zero
        }else{
            return CGSize.init(width: collectionView.bounds.width - 20, height: 20)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(sgProfile.selectedSegmentIndex == 0){
            return self.coachDetails?.currentAssigments.count ?? 0
        }else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(sgProfile.selectedSegmentIndex == 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCurrentAssignmentCell, for: indexPath) as! CurrentAssignmentCell
            let assignment = self.coachDetails?.currentAssigments[indexPath.row]
            cell.setCurrentAssignment(assignment: assignment!)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kProfileCell, for: indexPath) as! ProfileInfoCell
            if let profile = self.viewModel?.profileInfo[indexPath.row + indexPath.section]{
                cell.setProfileInfo(info: profile)
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(sgProfile.selectedSegmentIndex == 0){
            return CGSize.init(width: collectionView.bounds.width, height: 75)
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kProfileCell, for: indexPath) as! ProfileInfoCell
            if let profile = self.viewModel?.profileInfo[indexPath.row]{
                cell.setProfileInfo(info: profile)
                let size = Utils.sizeForLabel(profile.content ?? "", font: cell.lblContent.font ?? UIFont.systemFont(ofSize: 14))
                return CGSize.init(width: collectionView.bounds.width, height: size.height + 20)

            }
            return CGSize.zero
        }
    }
}

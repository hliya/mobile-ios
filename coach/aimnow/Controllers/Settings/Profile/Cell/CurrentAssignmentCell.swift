//
//  CurrentAssignmentCell.swift
//  coach
//
//  Created by Isuru Ranasinghe on 6/2/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core

class CurrentAssignmentCell: UICollectionViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime1: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var vwStatus: UIView!
    
    var assignment: Assigment!
    
    func setCurrentAssignment(assignment: Assigment){
        self.assignment = assignment
        self.lblName.text = assignment.name
        self.lblLocation.text = assignment.location?.name ?? "TBD"
        self.lblTime1.text = String(format:"%@ - %@",assignment.startTime?.date(format: "HH:mm:ss")?.toString(format: "hh:mm") ?? "N/A", assignment.endTime?.date(format: "HH:mm:ss")?.toString(format: Constants.TIME_FORMAT) ?? "N/A")
        let canceled = assignment.statusTypeId.value == Enums.eStatus.Completed.rawValue
        if !canceled {
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Green)
        }else {
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Gray)
            self.contentView.alpha = 0.3
        }
    }
}

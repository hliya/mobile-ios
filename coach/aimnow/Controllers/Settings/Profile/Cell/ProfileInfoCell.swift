//
//  ProfileInfoCell.swift
//  coach
//
//  Created by Isuru Ranasinghe on 9/27/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core

class ProfileInfoCell: UICollectionViewCell {

    @IBOutlet weak var lblContent: UILabel!
    
    var profileInfo: Profile!
    
    func setProfileInfo(info: Profile){
        self.profileInfo = info
        self.lblContent.text = info.content
    }
}

//
//  SenderCell.swift

import Foundation
import UIKit
import AlamofireImage
import SwiftyUserDefaults
import aimnow_core

public class SenderCell: UICollectionViewCell {
    @IBOutlet weak var imgBubble: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    let kSeenCell =  "SeenCell"
    
    public var message: ChatMessageModel!
    
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func setMessageDetails(messages: ChatMessageModel) -> Void {
        if let message = messages.message{
            self.message = messages
            self.lblMessage.text = message
        }
        self.lblMessage.font = self.lblMessage.font.withSize(CGFloat(12))
        if(messages.timestamp?.apiDate()?.isToday ?? false){
            self.lblTime.text = "Today " + (messages.timestamp?.apiDate()?.toString(format: Constants.TIME_FORMAT_WITHOUT_AMPM))!
        }else{
            self.lblTime.text = messages.timestamp?.apiDate()?.toString(format: Constants.CHAT_DATE_DISPLAY_FORMAT)
        }
    }
}

//
//  ChatDetailViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/19/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core

class ChatDetailViewController: BaseViewController {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewAvailable: UIView!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var txtChat: UITextView!
    @IBOutlet weak var cvChats: UICollectionView!
    @IBOutlet weak var cvChatFlowlayout: UICollectionViewFlowLayout!
    @IBOutlet weak var lblChatTitle: AfteEditTextField!
    @IBOutlet weak var chatViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var lblNoOfParticipants: UILabel!
    @IBOutlet var noOfParticipantHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnEditTitle: UIButton!

    var btnAddUser: UIButton!
    
    var viewModel:ChatDetailViewModel!
    var delegate: ChatUserDelegate!
    
    let kCellIdentifierSenderCell = "SenderCell"
    let kCellIdentifierReceiverCell = "ReceiverCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (self.viewModel == nil) {
            self.viewModel = ChatDetailViewModel()
        }
        
        self.cvChats?.register(UINib(nibName: self.kCellIdentifierSenderCell, bundle: nil), forCellWithReuseIdentifier: self.kCellIdentifierSenderCell)
        self.cvChats?.register(UINib(nibName: self.kCellIdentifierReceiverCell, bundle: nil), forCellWithReuseIdentifier: self.kCellIdentifierReceiverCell)
        self.txtChat.delegate = self
        
        btnAddUser = UIButton.init(type: .custom)
        btnAddUser.setImage(UIImage(named: "icon_add_users"), for: .normal)
        btnAddUser.tintColor = UIColor.systemOrange
        btnAddUser.addTarget(self, action: #selector(onClickAddUser(_:)), for: .touchUpInside)
        
        // Adding navigation action buttons
        self.addRightBarButtonItems()
        
        self.cvChats.transform = CGAffineTransform.init(rotationAngle: (-(CGFloat)(Double.pi)))
        
        if let conversation = self.viewModel.conversation, let conversationId = conversation.conversationId{
            FirebaseHelper.instance.updateReadCount(conversationId: conversationId)
            
        }
        
        if let conversation = self.viewModel.conversation{
            if let isPrivate = conversation.isPrivate, !isPrivate{
                self.lblChatTitle.isUserInteractionEnabled = true
                self.btnEditTitle.isHidden = false
            }else{
                self.lblChatTitle.isUserInteractionEnabled = false
                self.btnEditTitle.isHidden = true
            }
        }else{
            self.lblChatTitle.isUserInteractionEnabled = false
            self.btnEditTitle.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initializeFirebaseListners()
        super.viewWillAppear(animated)
        self.reloadUI()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    fileprivate func initializeFirebaseListners(){
        if let conversation = self.viewModel.conversation{
            FirebaseHelper.instance.listenToConversationMessages(conversationId: conversation.conversationId!)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(onMessageReceived(_:)),
                                                   name: NSNotification.Name(rawValue: String.init(format: Constants.Notifications.multipartyChatMessageReceived,conversation.conversationId!)),
                                                   object: nil)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(onConversationMessageModified),
                                                   name: NSNotification.Name(rawValue: String.init(format: Constants.Notifications.multipartyChatMessageModified,conversation.conversationId!)),
                                                   object: nil)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(onConversationModified(_:)),
                                                   name: NSNotification.Name(rawValue: Constants.Notifications.multipartyChatModified),
                                                   object: nil)
            
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(onUserReceived(_:)),
                                                   name: NSNotification.Name(rawValue: String.init(format: Constants.Notifications.multipartyChatUserReceived,conversation.conversationId!)),
                                                   object: nil)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(keyboardWillShow),
                                                   name: UIResponder.keyboardWillShowNotification,
                                                   object: nil)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(keyboardWillHide),
                                                   name: UIResponder.keyboardWillHideNotification,
                                                   object: nil)
            
        }
    }
    
    func addRightBarButtonItems()
    {
        let stackview = UIStackView.init(arrangedSubviews: [btnAddUser])

        stackview.distribution = .equalSpacing
        stackview.axis = .horizontal
        stackview.alignment = .center
        stackview.spacing = 15
        
        let rightBarButton = UIBarButtonItem(customView: stackview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.scrolToButtom()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        if let programId = self.viewModel.program?.id {
            SharedAppResource.instance.openChatIds.remove(object:programId)
        } else {
            SharedAppResource.instance.openChatIds.removeAll()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Keyboard show/hide actions
    @objc func keyboardWillShow() {
        chatViewBottomConstraint.constant = 0
        chatView.updateConstraints()
    }
    
    @objc func keyboardWillHide() {
        chatViewBottomConstraint.constant = 0
        chatView.updateConstraints()
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        self.sendMessage()
    }

    @IBAction func onClickTitleEdit(_ sender: UIButton) {
        if(isEditing == false){
            self.lblChatTitle.isUserInteractionEnabled = true
            self.lblChatTitle.becomeFirstResponder()
            self.isEditing = true
            self.btnEditTitle.setImage(UIImage.init(named: "icon_checkmark"), for: .normal)
        }else{
             self.lblChatTitle.isUserInteractionEnabled = false
                       self.isEditing = false
                       self.btnEditTitle.setImage(UIImage.init(named: "icon_edit"), for: .normal)
            if let conversationId = viewModel.conversation.conversationId{
                FirebaseHelper.instance.updateConversationTitle(conversationId: conversationId, title: (self.lblChatTitle.text ?? viewModel.conversation.conversationName!), callBack: { (success, error) in
                    self.lblChatTitle.endEditing(true)
                })
            }
        }
    }

    // MARK:- Private methods
    
    func scrolToButtom() {
        let lastSectionIndex = 0
        let lastRowIndex = 0
        let pathToLastRow = NSIndexPath.init(row: lastRowIndex, section: lastSectionIndex)
        if (lastRowIndex > 0) {
            self.cvChats.scrollToItem(at: pathToLastRow as IndexPath, at: .top, animated: true)
        }
    }
    
    @objc func onMessageReceived(_ notification:Notification) {
        guard let chat = notification.object as? ChatMessageModel else {
            return
        }
        if let index = self.viewModel.chats.firstIndex(where: {$0.conversationMessageId == chat.conversationMessageId}){
            self.viewModel.chats[index] = chat
        }else{
            self.viewModel.chats.append(chat)
        }
        for chatInfo in self.viewModel.chats{
            if let sUserId = chatInfo.senderId, sUserId == SharedAppResource.instance.loggedInUser?.id{
                chatInfo.senderImage = SharedAppResource.instance.loggedInUser?.profileImage
            }else{
                chatInfo.senderImage = chat.senderImage

            }
            
        }
        self.cvChats.reloadData()
        self.scrolToButtom()
    }
    
    @objc func onConversationMessageModified(_ notification:Notification) {
        guard let chat = notification.object as? ChatMessageModel else {
            return
        }
        if let index = self.viewModel.chats.firstIndex(where: {$0.conversationMessageId == chat.conversationMessageId}){
            self.viewModel.chats[index] = chat
        }else{
            self.viewModel.chats.append(chat)
        }
        self.cvChats.reloadData()
        
        if let conversation = notification.object as? ChatModel{
            self.viewModel.conversation = conversation
            self.lblChatTitle.text = conversation.conversationName?.trim() ?? ""
        }

        self.scrolToButtom()
    }
    
    @objc func onConversationModified(_ notification:Notification) {
        guard let conversation = notification.object as? ChatModel else {
            return
        }
        self.lblChatTitle.text = conversation.conversationName?.trim() ?? ""
    }
    
    @IBAction func onClickAddUser(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: CoachConstants.Storyboards.Chat, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ContactsViewController") as! ContactsViewController
        controller.viewModel = ChatListViewModel()
        controller.viewModel.selectedProgram = self.viewModel.program
        controller.delegate = self
        controller.viewModel.senderUser = self.viewModel.senderUser
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
    
    @objc func onUserReceived(_ notification:Notification) {
        guard let _ = notification.object as? ChatUsers else {
            return
        }
    }
    
    func reloadChatList(conversationId: String) {
        if let conversationMessage = SharedAppResource.instance.conversationChats.first(where: {$0.conversationId == conversationId}){
            self.viewModel.chats = conversationMessage.messages
            self.cvChats.reloadData()
        }else if(self.viewModel.subType == .program){
            var subtypeData: CurrentSubTypeData?
            if let program = self.viewModel.program{
                subtypeData = CurrentSubTypeData()
                subtypeData?.id = "\(program.id)"
                subtypeData?.descriptionValue = program.descriptionValue
                subtypeData?.title = program.name
            }
            self.viewModel.makeConversationSpecific(subTypeData: subtypeData!, conversationId: conversationId) { (success, error) in}
            self.viewModel.sendMessage(txtMessage: String.init(format: "\(sharedInstance.loggedInUser?.name ?? "Parent") started %@ : %@ specific conversation.", self.viewModel.subType.description, self.viewModel.title ?? ""), isInit: true, subTypeData: subtypeData) { (success, error) in
                if(success){
                    self.txtChat.text = ""
                }else{
                    print(error?.localizedDescription ?? "Message sending failed")
                }
            }
        }
    }
    
    func reloadUI () {
        if let conversation = self.viewModel.conversation{
            if let conversationId = conversation.conversationId{
                FirebaseHelper.instance.getSubTypeDataForConversation(conversationId: conversationId) { (subTypeData) in
                    self.viewModel.subTypeData = subTypeData
                    if let title = subTypeData.title, let description = subTypeData.descriptionValue{
                        self.lblNoOfParticipants.text = String.init(format: "%@ : %@", title, description)
                        self.noOfParticipantHeightConstraint.constant = 20
                        self.lblNoOfParticipants.isHidden = false
                    }
                }
            }
            self.lblChatTitle.text = conversation.conversationName?.trim() ?? ""
            
            FirebaseHelper.instance.getConversationUsers(conversationId: conversation.conversationId!) { (users) in
                self.viewModel.chatUsers = users
                if(users.count > 2){
                    self.lblNoOfParticipants.text = String.init(format: "%d participants", self.viewModel.chatUsers.count)
                    self.noOfParticipantHeightConstraint.constant = 20
                }else{
                    self.lblNoOfParticipants.isHidden = true
                    self.noOfParticipantHeightConstraint.constant = 0
                }
                
                if((users.count + self.viewModel.receivers.count) > 2){
                    self.lblNoOfParticipants.text = String.init(format: "%d participants", (users.count + self.viewModel.receivers.count))
                    self.noOfParticipantHeightConstraint.constant = 20
                }
                
                if((conversation.isPrivate ?? true) && (users.count + self.viewModel.receivers.count) > 2){
                    var name = (conversation.conversationName?.trim() ?? "") + ", "
                    for (index,user) in self.viewModel.receivers.enumerated(){
                        let userName = String.init(format: "%@%@",(user.firstName ?? ""), (index == (self.viewModel.receivers.count - 1)) ? "": ", ")
                        name.append(userName)
                    }
                    self.lblChatTitle.text = name
                }
            }
            
            if let profileUrl = conversation.receiverImageUrl, let url = URL.init(string: Utils.getRelativeUrlPath(path: profileUrl))  {
                Utils.getImageWithoutCach(url:url, imageView: self.imgProfile, completion: nil)
            }
            self.reloadChatList(conversationId: conversation.conversationId!)
        }else{
            if(self.viewModel.receivers.count > 1){
                var name = ""
                for (index,user) in self.viewModel.receivers.enumerated(){
                    let userName = String.init(format: "%@%@",(user.firstName ?? ""), (index == (self.viewModel.receivers.count - 1)) ? "": ", ")
                    name.append(userName)
                }
                self.lblChatTitle.text = name
            }else{
                self.lblChatTitle.text = self.viewModel.receivers.first?.getDisplayName()
            }
        }
    }
    
    func sendMessage() {
        if let _ = self.viewModel.conversation{
            if((self.viewModel.conversation.isPrivate ?? true) && (self.viewModel.chatUsers.count + self.viewModel.receivers.count) > 2){
                
                for user in self.viewModel.chatUsers{
                    if(user != sharedInstance.loggedInUser?.id){
                        if let user = sharedInstance.eligibleChatUsers.first(where: {$0.userId == user}){
                            if(!self.viewModel.receivers.contains(where: {$0.userId == user.userId})){
                                self.viewModel.receivers.append(user)
                            }
                        }
                    }
                }
                var subtypeData: CurrentSubTypeData?
                if let program = self.viewModel.program{
                    subtypeData = CurrentSubTypeData()
                    subtypeData?.id = "\(program.masterId)"
                    subtypeData?.descriptionValue = program.descriptionValue
                    subtypeData?.title = program.name
                }
                FirebaseHelper.instance.initGroupConversation(sender: self.viewModel.senderUser!, receivers: self.viewModel.receivers, subTypeData: subtypeData, callBack: { (success, error, conversationModelReceiver) in
                    self.onCreateGroupChat(conversation: conversationModelReceiver)
                    self.pushMessage(subTypeData: subtypeData)
                })
            }else{
                self.pushMessage()
            }
        }else{
            var programId: String? = nil
            if let program = self.viewModel.program{
                programId = "\(program.masterId)"
            }
            let sender = ChatUsers.init()
            if let user = SharedAppResource.instance.loggedInUser{
                sender.firstName = user.firstName
                sender.lastName = user.lastName
                sender.userId = user.id
                sender.profileImageUrl = user.profileImage
            }
            if(self.viewModel.receivers.count > 1){
                var subtypeData: CurrentSubTypeData?
                if let program = self.viewModel.program{
                    subtypeData = CurrentSubTypeData()
                    subtypeData?.id = "\(program.masterId)"
                    subtypeData?.descriptionValue = program.descriptionValue
                    subtypeData?.title = program.name
                }
                FirebaseHelper.instance.initGroupConversation(conversationId: self.viewModel.conversation?.conversationId,sender: self.viewModel.senderUser!, receivers: self.viewModel.receivers, subTypeData: subtypeData, callBack: { (success, error, conversationModelReceiver) in
                    self.onCreateGroupChat(conversation: conversationModelReceiver)
                    self.pushMessage(subTypeData: subtypeData)
                })
            }else{
                self.viewModel.getChatWithSpecificUser(userId: (viewModel.receivers.first?.userId!)!, programId: programId, completion: { (conversation) in
                    var subtypeData: CurrentSubTypeData?
                    if let program = self.viewModel.program{
                        subtypeData = CurrentSubTypeData()
                        subtypeData?.id = "\(program.masterId)"
                        subtypeData?.descriptionValue = program.descriptionValue
                        subtypeData?.title = program.name
                    }
                    if let chat = conversation{
                        self.viewModel.conversation = chat
                        self.pushMessage(subTypeData: subtypeData)
                    }else{
                        FirebaseHelper.instance.initConversation(sender: self.viewModel.senderUser!, receiver: self.viewModel.receivers.first!, subTypeData: subtypeData, callBack: { (success, error, conversationModelReceiver) in
                            self.viewModel.conversation = conversationModelReceiver
                            self.pushMessage(subTypeData: subtypeData)
                        })
                    }
                })
            }
        }
    }
    
    fileprivate func checkAvailabilityAndNotify(receivers:[ChatUsers], message: String){
        let myGroup = DispatchGroup()
        var offlineUsers: [String] = []
        var allUsers: [String] = []
        for user in self.viewModel.chatUsers{
            allUsers.append(user)
        }
        for receiverUser in receivers{
            if let userId = receiverUser.userId, !allUsers.contains(userId){
                allUsers.append(userId)
            }
        }
        for receiver in allUsers{
            myGroup.enter()
            FirebaseHelper.instance.checkUserAvailability(userId: receiver, callback: { (isOnline) in
                if(!isOnline){
                    offlineUsers.append(receiver)
                }
                myGroup.leave()
            })
        }
        myGroup.notify(queue: .main) {
            UserDomain.instance.postNotifiction(userId: self.viewModel.senderUser?.userId, pool: offlineUsers, message: message.trim(), notificationTemplate: .ProgramScheduledOnDay) { (notificationItem) in
            }
        }
    }
    
    fileprivate func pushMessage(subTypeData: CurrentSubTypeData? = nil){
        self.initializeFirebaseListners()
        self.reloadUI()
        self.viewModel.sendMessage(txtMessage: txtChat.text.trim(), subTypeData: subTypeData) { (success, error) in
            if(success){
                self.checkAvailabilityAndNotify(receivers: self.viewModel.receivers, message: self.txtChat.text)
                self.viewModel.receivers = []
                self.txtChat.text = ""
            }else{
                print(error?.localizedDescription ?? "Message sending failed")
            }
        }
    }
}

// MARK:- UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout

extension ChatDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.chats.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let message : ChatMessageModel = self.viewModel.chats.reversed()[indexPath.row]

        let userId = SharedAppResource.instance.loggedInUser?.id
        let isSender = (message.senderId == userId)
        if (isSender) {
            let cell: SenderCell = collectionView.dequeueReusableCell(withReuseIdentifier: kCellIdentifierSenderCell, for: indexPath) as! SenderCell
            cell.setMessageDetails(messages: message)
            cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            return cell
        } else {
            var nextMessage:ChatMessageModel?
            if (indexPath.row < (self.viewModel.chats.count - 1)) {
                nextMessage = self.viewModel.chats[indexPath.row + 1]
            }
            
            let cell: ReceiverCell = collectionView.dequeueReusableCell(withReuseIdentifier: kCellIdentifierReceiverCell, for: indexPath) as! ReceiverCell
            cell.setMessageDetails(message: message, nextMessage: nextMessage)
            cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let insets = self.cvChatFlowlayout.sectionInset
        let width = (self.cvChats?.bounds.size.width)! - (insets.left + insets.right)
        let message : ChatMessageModel = self.viewModel.chats.reversed()[indexPath.row]
        let helveticaFont = UIFont.systemFont(ofSize: 13)
        var height:CGFloat = 0.0
        if let msg = message.message {
            let lblMSGHeight = Utils.sizeForLabel(msg, font: helveticaFont, numberOfLines: 30,width: (collectionView.bounds.width * 0.75)).size.height
            height = lblMSGHeight + 35.0
            return CGSize.init(width: width, height: CGFloat(height))
        }
        return CGSize.zero
    }
    
}

extension ChatDetailViewController:ManageUserDelegate, UITextViewDelegate, ContactDelegate{
    func onCreateGroupChat(conversation: ChatModel) {
        self.viewModel.conversation = conversation
        self.viewModel.chats.removeAll()
        self.cvChats.reloadData()
    }
    
    func updateUserCount(user: [ChatUsers]) {
        if(user.count > 2){
            self.lblNoOfParticipants.text = String.init(format: "%d participants", user.count)
            self.noOfParticipantHeightConstraint.constant = 20
        }else{
            self.lblNoOfParticipants.isHidden = true
            self.noOfParticipantHeightConstraint.constant = 0
        }
    }
    
    func onUserLeaveGroup() {
        if let delegate = self.delegate{
            delegate.onLeaveConversation(conversation: self.viewModel.conversation)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        let unreadChats = self.viewModel.chats.filter({$0.seenBy?.count == 0})
        for unreadChat in unreadChats{
            if !((unreadChat.seenBy?.contains(where: {$0.userId == sharedInstance.loggedInUser?.id}))!){
                FirebaseHelper.instance.updateSeenByMessage(conversationId: self.viewModel.conversation.conversationId!, conversation: unreadChat.conversationMessageId!)
            }
        }
    }
    func onSelectContact(chatUser: ChatUsers, senderUser: ChatUsers?) {
        self.viewModel.receivers.append(chatUser)
        self.reloadUI()
    }
}

//
//  TTVedioPlayerView.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/14/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class TTVedioPlayerView : UIView {
    
    func set(url: URL) -> Void {
     
        
        let player = AVPlayer(url: url)
        let avPlayerController = AVPlayerViewController()
        
        avPlayerController.player = player;
        avPlayerController.view.frame = self.bounds;
        
        self.addSubview(avPlayerController.view);
    }
    
    func play()-> Void {
        
       // self.player.play();
    }
}


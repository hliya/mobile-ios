//
//  AppDelegate.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 5/11/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core
import SwiftyUserDefaults
import IQKeyboardManagerSwift
import FirebaseCore
import FirebaseMessaging
import RealmSwift
import SwiftyJSON
import AppSpectorSDK

let appDelegate = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    
    var window: UIWindow?
    var shouldRotate = false
    let realm = try! Realm()
    let gcmMessageIDKey = "gcm.message_id"
    var userInfo:[AnyHashable: Any]!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.setUpIqKeyBoardManger()
        self.registerFirebase(application: application)
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        SharedAppResource.instance.appId = "2"
        print(NSHomeDirectory())
        Realm.Configuration.defaultConfiguration.deleteRealmIfMigrationNeeded = true
        
        let config = AppSpectorConfig(apiKey: "ios_MTVkYWFlMWEtMDI3YS00OTY2LTg2ZmQtMDBlYWI4MWU1YzZj", monitorIDs: [Monitor.http])
        AppSpector.run(with: config)
        
        try! realm.write {
            realm.delete(realm.objects(Assigment.self))
            realm.delete(realm.objects(AssignedProgramModel.self))
            realm.delete(realm.objects(Reference.self))
            realm.delete(realm.objects(Statistic.self))
            realm.delete(realm.objects(ProgramDetail.self))
            realm.delete(realm.objects(DependantModel.self))
            realm.delete(realm.objects(ProgramModel.self))
            realm.delete(realm.objects(Attendance.self))
        }
        return true
    }
    
    fileprivate func registerFirebase(application: UIApplication){
        let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
        guard let fileopts = FirebaseOptions(contentsOfFile: filePath!)
            else {
                assert(false, "Couldn't load config file")
                return
            }
        FirebaseHelper.instance.configure(options: fileopts)
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }
}

extension AppDelegate: MessagingDelegate {
    func setUpIqKeyBoardManger(){
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        if let fcmToken = fcmToken{
            Defaults[\.fcmTokenKey] = fcmToken
            Observers.instance.fcmTokenUpdate.onNext(fcmToken)
        }
    }
}

// MARK:- UNUserNotificationCenterDelegate
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print(JSON(userInfo))
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        completionHandler()
        print(userInfo)
        self.userInfo = userInfo
    }
}

//
//  Constants.swift
//  coach
//
//  Created by Isuru Ranasinghe on 5/2/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import aimnow_core

class CoachConstants{
    
    struct Storyboards {
        static let Main                             = "Main"
        static let Calendar                         = "Calendar"
        static let Schedule                         = "Schedule"
        static let Progress                         = "Progress"
        static let Authentication                   = "Authentication"
        static let Chat                             = "Chat"
        static let Profile                          = "Profile"
        static let Stats                            = "Stats"
    }
    
}

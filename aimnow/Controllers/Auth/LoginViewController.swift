//
//  LoginViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 5/12/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import CoreData
import aimnow_core
import SwiftyUserDefaults

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    var viewModel: LoginVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Utils.documentDirectory())
        if(viewModel == nil){
            viewModel = LoginVM()
        }
        self.navigationController?.navigationBar.isHidden = true;
        self.txtPassword.setPlaceHolderTextColor(color: UIColor.white);
        self.txtUserName.setPlaceHolderTextColor(color: UIColor.white);
        self.txtUserName.text = "sr@aimnow.com"
        self.txtPassword.text = "Caim@123#"
        
        
        if (Defaults[\.isTouchIDEnabled] ?? false) {
            authenticationWithTouchID(mode: TouchEnableViewKeys.login)
        }
        
        if(Defaults[\.isLoggedIn]){
            self.performSegue(withIdentifier: "mainController", sender: nil)
        }
    }
    
    @IBAction func onLoginPress() -> Void {
        self.loading.startAnimating()
        guard let userName = txtUserName.text else {
            print("User name required")
            return
        }
        
        guard let password = txtPassword.text else {
            print("Password required")
            return
        }
        
        self.viewModel.authenticateUser(userName: userName, password: password) { (userModel , error) in
            
            self.loading.stopAnimating()
//            if(!(Defaults[\.isMasterSync] ?? false)){
                MasterSync.shared.startSync(delegate: nil)
//            }
            if (error == nil){
                self.performSegue(withIdentifier: "segueLogin", sender: nil)
            }
        }
    }
}

extension LoginViewController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
         performSegue(withIdentifier: "segueLogin", sender: nil)
    }
}

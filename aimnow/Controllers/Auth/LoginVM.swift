//
//  LoginVM.swift
//  aimnow
//
//  Created by Isuru Ranasinghe on 3/9/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import aimnow_core

class LoginVM: NSObject {
    
    var domain: UserDomain = UserDomain.instance
    
    func authenticateUser(userName: String, password: String, completion: @escaping(_ userModel: UserModel?, _ error: Error?) -> ()) {
        domain.authenticateUser(userName: userName, password: password) { (userModel, error ) in
            completion(userModel, error)
        }
    }
}

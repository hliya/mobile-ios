//
//  ProgramOverviewCell.swift
//  parent
//
//  Created by Isuru Ranasinghe on 6/6/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core

class ProgramOverviewCell: UICollectionViewCell {

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblProgram: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var vwStatus: UIView!

    var program: AssignedProgramModel?
    
    func setupProgramOverview(program: AssignedProgramModel){
        self.program = program
        self.lblTime.text = program.startTime?.date(format: "HH:mm:ss", includeUTC: false)?.toString(format: Constants.TIME_FORMAT)
        self.lblProgram.text = program.name
        self.lblLocation.text = program.location?.name ?? "TBD"
        
        
//        let updateOn = lastStat.updatedOn?.date(format: Constants.SERVER_DATE_FORMAT)?.toString(format: Constants.DATE_FORMAT)
//        if(updateOn == Date().toString(format: Constants.DATE_FORMAT)){
//            detail.lastStatId = lastStat.id
//        }

        if let precentage = program.statistics.last?.score.value{ // .first(where: {$0.id == 1})?.score.value{
            if precentage >= 0 && precentage <= 25 {
                self.lblRating.backgroundColor = UIColor.init(hexString: "#ffe334")
            } else if precentage > 25 && precentage <= 50 {
                self.lblRating.backgroundColor = UIColor.init(hexString: "#ffa634")
            } else if precentage > 50 && precentage <= 75 {
                self.lblRating.backgroundColor = UIColor.init(hexString: "#34b5ff")
            }else{
                self.lblRating.backgroundColor = UIColor.init(hexString: "#3dd35b")
            }
            self.lblRating.text =  "\(Int(Double(precentage) * 0.1))";
        }else{
            self.lblRating.text = "-"
            self.lblRating.backgroundColor = UIColor.lightGray
        }
        
        let canceled = program.location?.isActive.value ?? true;
        if canceled {
            self.vwStatus.backgroundColor = UIColor.init(hexString: "#55ff00")
        }else {
            self.vwStatus.backgroundColor = UIColor.init(hexString: "#868989")
            self.contentView.alpha = 0.3;
        }
    }

}

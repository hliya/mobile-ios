//
//  ProgramOverviewItem.swift
//  parent
//
//  Created by Isuru Ranasinghe on 6/9/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core

protocol onCellselect {
    func onCellSelect(overviewItem: ProgramProgressDetail)
}


class ProgramOverviewItem: UICollectionViewCell {

    let kProgramCellItem = "ProgramOverviewCell"
    
    @IBOutlet public weak var lblName: UILabel!
    @IBOutlet public weak var lblScore: UILabel!
    @IBOutlet public weak var lblPrecentage: UILabel!
    @IBOutlet weak var cvOverview: UICollectionView!
    @IBOutlet weak var imgLeft: UIImageView!
    @IBOutlet weak var imgRight: UIImageView!
    
    var delegate: onCellselect?
    
    
    var overviewItem: ProgramProgressSummary!
    
    func setupProgramOverviewItem(overviewItem: ProgramProgressSummary, cellPossitin:Enums.eCellPossition) {
        self.overviewItem = overviewItem

        self.lblName.text = (self.overviewItem.categories.first?.type!)!.uppercased();
        self.lblScore.text = String((self.overviewItem.total ?? 0))
        self.lblPrecentage.text = String(format: "%.1f%%", (self.overviewItem.precentage ?? 0))
        
        self.cvOverview.register(UINib.init(nibName: kProgramCellItem, bundle: nil), forCellWithReuseIdentifier: kProgramCellItem)
        
        self.cvOverview.reloadData()
        switch cellPossitin {
        case .first:
            self.imgLeft.isHidden = true
            self.imgRight.isHidden = false
            self.imgRight.blink()
        case .last:
            self.imgLeft.isHidden = false
            self.imgRight.isHidden = true
            self.imgLeft.blink()
        case .middle:
            self.imgLeft.isHidden = false
            self.imgRight.isHidden = false
            self.imgLeft.blink()
            self.imgRight.blink()
        }
    }
}

extension ProgramOverviewItem: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.overviewItem.categories.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kProgramCellItem, for: indexPath as IndexPath) as! ProgramOverviewCell
        let assignedProgram = self.overviewItem.categories[indexPath.row].assignedProgramRef
        cell.setupProgramOverview(program: assignedProgram!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let delegate = self.delegate{
            delegate.onCellSelect(overviewItem: self.overviewItem.categories[indexPath.row])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.bounds.width, height: 70)
    }
    
    override func prepareForReuse() {
        self.overviewItem.categories = []
        self.cvOverview.reloadData()
        self.imgRight.isHidden = true
        self.imgLeft.isHidden = true
    }
}

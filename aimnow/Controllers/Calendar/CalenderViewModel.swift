//
//  CalenderViewModel.swift
//  aimnow
//
//  Created by Isuru Ranasinghe on 3/25/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import RealmSwift
import aimnow_core
import SwiftyUserDefaults

class CalenderViewModel: NSObject {
    
    let realm = try! Realm()
    var selectedProgram: ProgramModel!
    var groupedScheduledPrograms: [GroupedScheduledPrograms] = []
    var domain: ProgramDomain!

    var selectedDate: Date!
    var startTime: String!
    var endTime: String!
    var sharedInstance = SharedAppResource.instance
    var programStagesInfo: ProgramStagesInfo?
    var stageInfo: [ProgramStagesInfo] = []

    var colors = ["#e76f51", "#f77f00", "#fcbf49", "#6C28E4", "#00a6fb", "#6a00f4"]
    init(domain : ProgramDomain = ProgramDomain.instance) {
        self.domain = domain
    }
    
    func getAvailableLocationsForProgram(completion:@escaping([Location]) -> ()) {
        self.domain.getProgramLocations { (locations) in
            completion(locations)
        }
    }
    
    public func getCalendarEvents(player: DependantModel, completion:@escaping(_ eventDates: [Date] ) -> ()){
        let eventDates = self.domain.getCalendarEvents(Player: player)
        completion(eventDates)
    }
    
    public func getProgramScheduleFor(player: DependantModel, date selectedDate: Date? = nil, completion:@escaping(_ groupedPrograms: [GroupedScheduledPrograms] ) -> ()){
        let sheduledPrograms = self.domain.getProgramScheduleForDate(date: selectedDate, Player: player,program: selectedProgram)
        self.groupedScheduledPrograms = sheduledPrograms
        completion(sheduledPrograms)
    }
    
    public func getProgramOverview(player: DependantModel, date selectedDate: Date, completion: @escaping(_ programProgress: [ProgramProgressSummaryDate]) -> ()) {
        self.domain.getProgramOverview(date: selectedDate, player: player, program: selectedProgram.id) { (programs) in
            completion(programs)
        }
    }
    
    public func getStageOverviewSummary(player: DependantModel, stage: ProgramStagesInfo, completion:@escaping (_ summary: [OverallSummaryItem]) -> ()){
        
        let currentStageId = OverallSummaryRequest.init(on: nil, subProgramId: stage.currentStageId, unitId: 1, assignmentTypeId: 2, id: selectedProgram.id, requesterId: sharedInstance.selectedPlayer?.id)
        let router = NetworkRouter.getProgramOverallSummary(currentStageId)
        
        self.domain.getProgramOverallSummary(router: router) { (response, error) in
            if(error == nil){
                let x = Dictionary(grouping: response, by: { $0.programTypeId })
                var summaryList = [OverallSummaryItem]()
                for (index, x1) in x.enumerated() {
                    var scores = [Int]()
                    var summary:OverallSummaryItem!
                    for month in 1 ... 12{
                        if let value = x1.value.first(where: {$0.month == month}){
                            scores.append(Int(Double((value.score ?? 0)) * 0.1))
                        }else{
                            scores.append(0)
                        }
                    }
                    if let item = x1.value.first{
                        summary = OverallSummaryItem.init(type: "column", color: self.colors[index], data: scores, yAxis: 0, name: item.programType!, typeId: item.programTypeId!)
                        summaryList.append(summary)
                    }
                }
                let sortedList2 = summaryList.sorted { (item1, item2) -> Bool in
                    return (item1.typeId! < item2.typeId!)
                }
                completion(sortedList2)
            }else{
                Observers.instance.errorMessageObserver.onNext(errorMessage.init(title: error?.localizedDescription))
            }
        }
    }
    
    func getProgramStatus(completion:@escaping(_ stageInfo: [ProgramStagesInfo]) -> ()){
        self.domain.getProgramProgressStatus(player: SharedAppResource.instance.selectedPlayer!, program: selectedProgram) { (userStages) in
            self.stageInfo = userStages
            completion(userStages)
        }
    }
    
    func requestNewProgram(completion:@escaping(_ isSuccess: Bool,_ error: String?) -> ()){
        let loggedInUser = SharedAppResource.instance.loggedInUser
        let newProgram = NewProgramRequestModel(programID: self.selectedProgram.id, requesterId: loggedInUser!.id!, requestedTo: self.sharedInstance.selectedPlayer!.id!, date: selectedDate.toString(format: Constants.DATE_FORMAT), time: startTime)
        let router = NetworkRouter.requestForProgram(newProgram)
        self.domain.requestNewProgram(router: router) { (response, error) in
            if(error != nil){
                completion(false, error)
            }else{
                completion(true, nil)
            }
        }
    }
}

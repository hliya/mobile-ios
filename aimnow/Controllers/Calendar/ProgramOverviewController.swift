//
//  ProgramOverviewController.swift
//  parent
//
//  Created by Isuru Ranasinghe on 6/6/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import FSCalendar
import aimnow_core
import FlexibleSteppedProgressBar
import SwiftyJSON

class ProgramOverviewController: BaseViewController, FSCalendarDataSource, FSCalendarDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIWebViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var  calendar: FSCalendar!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var btnDetail: UIBarButtonItem!
    @IBOutlet weak var webviewCharts : UIWebView!
    @IBOutlet weak var stageView: UIView!
    @IBOutlet weak var progressView: FlexibleSteppedProgressBar!
    @IBOutlet weak var lblStage : UILabel!
    
    var events = Array<String>()
    var viewModel: CalenderViewModel!
    var programProgressSummary = [ProgramProgressSummary]()
    var categories = [ProgramProgressDetail]()
    var maxIndex = -1
    var currentStage = 0
    
    let kProgramOverviewCell = "ProgramOverviewItem"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UINib.init(nibName: kProgramOverviewCell, bundle: nil), forCellWithReuseIdentifier: kProgramOverviewCell)
        
        if(viewModel == nil){
            viewModel = CalenderViewModel()
        }
        calendar.delegate = self;
        calendar.dataSource = self;
        calendar.select(Date())
        calendar.appearance.eventSelectionColor = .orange
        calendar.appearance.eventDefaultColor = .orange
        webviewCharts.delegate = self
        
        webviewCharts.isUserInteractionEnabled = true
        
        if let url = Bundle.main.url(forResource: "index-rings", withExtension: "html") {
            webviewCharts.loadRequest(URLRequest(url: url))
        }
        self.setupProgressBar()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        (self.webviewCharts.stringByEvaluatingJavaScript(from:  "pushData([]])"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadCalendarEvents()
    }
    
    func loadProgramSummary(){
        
    }
    
    fileprivate func loadCalendarEvents(){
        self.loading.startAnimating()
        self.viewModel.getCalendarEvents (player: sharedInstance.selectedPlayer!) { (eventDates) in
            self.loading.stopAnimating()
            for event in eventDates {
                self.events.append(self.dateFormatter.string(from: event))
            }
            self.calendar.reloadData()
            self.loadSecheduledData(date: Date())
        }
    }
    
    fileprivate func loadSecheduledData(date: Date){
        self.programProgressSummary = []
        self.collectionView.reloadData()
        self.loading.startAnimating()
        self.viewModel.getProgramOverview(player: sharedInstance.selectedPlayer!, date: date) { (programs) in
            self.loading.stopAnimating()
            if let progressStageSummary = programs.last?.progressSummary.last{
                self.programProgressSummary = progressStageSummary.progressStageSummary
                self.categories = self.programProgressSummary.last?.categories ?? []
                self.collectionView.reloadData()
            }
        }
    }
    
    @IBAction func onClickBackPress(sender: UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadCharts() -> Void {
        self.viewModel?.getProgramStatus {(progressStatus) in
            if(self.viewModel.stageInfo.count > 0){
                self.progressView.numberOfPoints = self.viewModel.stageInfo.last?.totalStages ?? 0
                if let lastStage = self.viewModel.stageInfo.last{
                    self.currentStage = (lastStage.currentStage - 1)
                }else{
                    self.currentStage = 0
                }
                self.progressView.currentIndex =  self.currentStage
                self.maxIndex = self.currentStage
                self.progressView.completedTillIndex = self.currentStage
                self.loadProgamOverviewSummary();
            }else{
                self.alert(message: "No statistical record available")
            }
        }
    }
    
    @IBAction  func showDetail(sender: UIBarButtonItem){
        if (sender.tag == 0){
            sender.image = UIImage(named: "calendar", in: nil, compatibleWith: nil)
            sender.tag = 1
            self.webviewCharts.isHidden = false
            self.collectionView.isHidden = true;
            self.stageView.isHidden = false
            self.calendar.isHidden = true
            self.webviewCharts.reload()
            self.loadCharts()
        }else {
            sender.image = UIImage(named: "overview", in: nil, compatibleWith: nil)
            sender.tag = 0
            self.calendar.isHidden = false
            self.stageView.isHidden = true
            self.webviewCharts.isHidden = true
            self.collectionView.isHidden = false;
        }
    }
    
    // MARK: - UICollectionViewDataSource protocol

    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.programProgressSummary.count;
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kProgramOverviewCell, for: indexPath as IndexPath) as! ProgramOverviewItem
        let assignedProgram = self.programProgressSummary[indexPath.row]
        var cellPossition: Enums.eCellPossition = .first
        if(indexPath.row == 0){
            cellPossition = .first
        }else if (indexPath.row == self.programProgressSummary.count - 1){
            cellPossition = .last
        }else{
            cellPossition = .middle
        }
        cell.setupProgramOverviewItem(overviewItem: assignedProgram, cellPossitin: cellPossition)
        cell.delegate = self
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.view.bounds.width - 20, height: collectionView.bounds.height)
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.loadSecheduledData(date: date)
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateString = dateFormatter.string(from: date)
        if  self.events.contains(dateString) {
            return 1
        }
        return 0
    }
}

extension ProgramOverviewController: FlexibleSteppedProgressBarDelegate, onCellselect {
    fileprivate func setupProgressBar() {
        progressView.lineHeight = 3
        progressView.radius = 4
        progressView.progressRadius = 6
        progressView.progressLineHeight = 3
        progressView.delegate = self
        progressView.useLastState = true
        progressView.lastStateCenterColor = UIColor.orange
        progressView.selectedBackgoundColor = UIColor.orange
        progressView.selectedOuterCircleStrokeColor = UIColor.green
        progressView.lastStateOuterCircleStrokeColor = UIColor.orange
        progressView.currentSelectedCenterColor = UIColor.green
        progressView.stepTextColor = UIColor.clear
        progressView.currentSelectedTextColor = UIColor.clear
        progressView.completedTillIndex = 0
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     didSelectItemAtIndex index: Int) {
        progressBar.currentIndex = index
        self.currentStage = index
        self.loadProgamOverviewSummary()
    }
    
    func loadProgamOverviewSummary (){
        self.loading.startAnimating()
        let stage = self.currentStage + 1
        self.lblStage.text = "STAGE \(stage)"
        if(self.viewModel.stageInfo.count > self.currentStage){
            self.viewModel.getStageOverviewSummary(player: sharedInstance.selectedPlayer!, stage: self.viewModel.stageInfo[self.currentStage]) { (summaryResponse) in
                self.loading.stopAnimating()
                let result = try? String.init(data: summaryResponse.encoded(), encoding: .utf8)!.replacingOccurrences(of: "'\'", with: "")
                (self.webviewCharts.stringByEvaluatingJavaScript(from:  "pushData(\(result!))"))
            }
        }
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     canSelectItemAtIndex index: Int) -> Bool {
        return (index <= maxIndex)
    }
    
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
        return ""
    }
    
    func onCellSelect(overviewItem: ProgramProgressDetail) {
        navigateToProgressDetail(program: self.viewModel.selectedProgram, programProgressDetail: overviewItem)
    }
}

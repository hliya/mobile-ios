//
//  CalenderViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/14/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import FSCalendar
import aimnow_core

class CalenderViewController: BaseViewController, FSCalendarDataSource, FSCalendarDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var  calendar: FSCalendar!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var lblProgram: UILabel!
    
    var events = Array<String>()
    var viewModel: CalenderViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calendar.delegate = self;
        calendar.dataSource = self;
        calendar.select(Date())
        calendar.appearance.eventSelectionColor = .orange
        calendar.appearance.eventDefaultColor = .orange

        if(viewModel == nil){
            viewModel = CalenderViewModel()
        }
        
        if let program = self.viewModel.selectedProgram{
            lblProgram.text = program.name
            
            for (index, _) in self.viewModel.groupedScheduledPrograms.enumerated() {
                self.events.append(self.dateFormatter.string(from: self.viewModel.groupedScheduledPrograms[index].date))
            }
            self.calendar.reloadData()
            
            self.loadData(date: calendar.selectedDate!)
        }
    }
    
    override func onBack(sender: UIBarButtonItem) { 
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.groupedScheduledPrograms[section].programs.count;
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if ( self.viewModel.groupedScheduledPrograms.count == 0) {
            collectionView.setEmptyMessage("No data available!")
        } else {
            collectionView.restore()
        }
        return self.viewModel.groupedScheduledPrograms.count;
    } 
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ScheduleCollectionViewCell
        cell.setForSchedule(assignedProgram: self.viewModel.groupedScheduledPrograms[indexPath.section].programs[indexPath.row])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headercell", for: indexPath);
        
        return sectionHeader
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.loadData(date: date)
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateString = dateFormatter.string(from: date)
        if  self.events.contains(dateString) {
            return 1
        }
        return 0
    }
    
    func loadData (date:Date) -> Void{
        self.loading.startAnimating()
        if let program = self.viewModel.selectedProgram{
            let sheduledPrograms = self.viewModel.domain.getProgramScheduleForDate(date:date, Player: sharedInstance.selectedPlayer!)
            self.loading.stopAnimating()
            self.viewModel.groupedScheduledPrograms = sheduledPrograms
            self.collectionView.reloadData()
        }
    }
}

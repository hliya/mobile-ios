//
//  SettingsViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/16/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class SettingsViewController: BaseViewController {
    
    @IBOutlet weak var swTouch: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.swTouch.isOn = Defaults[\.isTouchIDEnabled] ?? false
    }
    
    @IBAction func enableTouchId(sender:UISwitch) -> Void{
        
        if sender.isOn {
            authenticationWithTouchID(mode: TouchEnableViewKeys.settings)
        }else {
             Defaults[\.isTouchIDEnabled] = false
        }
    }
    
}

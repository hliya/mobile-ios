//
//  BaseViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/14/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import LocalAuthentication
import aimnow_core
import RxSwift
import SwiftyUserDefaults
import RealmSwift
import SkeletonView

class BaseViewController : UIViewController{
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var vwHeaderRight : HeaderRightView!;
    var data: Any?
    let sharedInstance = SharedAppResource.instance
    var selectedProgram: ProgramModel? = nil
    let realm = try! Realm()
    var disposeBag = DisposeBag()
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    let reuseIdentifierHeader = "headercell"
    let dateFormatter = DateFormatter()
    
    @IBAction func onBack(sender:UIBarButtonItem) -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        dateFormatter.dateFormat = Constants.DATE_FORMAT        
        setupObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.vwHeaderRight != nil &&  SharedAppResource.instance.selectedPlayer  != nil{
            self.vwHeaderRight.set(player:  SharedAppResource.instance.selectedPlayer!)
            self.vwHeaderRight.setProgram(selectedProgram: SharedAppResource.instance.selectedProgram)
        }
    }
    
    func setupObservers(){
        Observers.instance.playerSelectObsrver.asObserver().subscribe(onNext: { (player) in
            if let selectedPlayer = player{
                self.sharedInstance.selectedPlayer = selectedPlayer
                self.viewWillAppear(false)
            }
        }).disposed(by: disposeBag)
        Observers.instance.errorMessageObserver.asObserver().subscribe(onNext: { (error) in
            self.loading?.stopAnimating()
            if(error?.code == 401){
                self.clearUserData()
            }
            self.alert(message: error?.error ?? "message_some_thing_whent_worng".localized, title: error?.title ?? "error".localized)
        }).disposed(by: disposeBag)
    }
    
    private func clearUserData(){
        Defaults[\.token] = nil
        Defaults[\.isLoggedIn] = false
        Defaults[\.isDeviceRegistered] = false
        Defaults[\.userDetail] = nil
        Defaults[\.fcmTokenKey] = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func OnChatPressed(){
        navigateToChat(program: sharedInstance.selectedProgram as! ProgramModel, selectedUser: sharedInstance.selectedPlayer)
    }
}

extension UIViewController{
        var appDelegate: AppDelegate {
            return UIApplication.shared.delegate as! AppDelegate
        }
}





// Applicaton navigation

extension BaseViewController{
    func navigateToCalendarController(program: ProgramModel, groupedScheduledPrograms: [GroupedScheduledPrograms], shouldPresentModal: Bool = true) {
        let storyboardName = ParentConstants.Storyboards.Calendar
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let calendarVC = storyboard.instantiateViewController(withIdentifier: "CalenderViewController") as! CalenderViewController
        calendarVC.viewModel = CalenderViewModel()
        calendarVC.viewModel.selectedProgram = program
        calendarVC.viewModel.groupedScheduledPrograms = groupedScheduledPrograms
        if(shouldPresentModal){
            navigationController?.present(calendarVC, animated: true, completion: nil)
        }else{
            navigationController?.pushViewController(calendarVC, animated: true)
        }
    }
    
    func navigateToProgressView(program: ProgramModel, groupedScheduledPrograms: [GroupedScheduledPrograms]) {
        let storyboardName = ParentConstants.Storyboards.Progress
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let progressView = storyboard.instantiateViewController(withIdentifier: "ProgressViewController") as! ProgressViewController
        progressView.viewModel = ProgressViewModel()
        progressView.viewModel.selectedProgram = program
        progressView.viewModel.groupedScheduledPrograms = groupedScheduledPrograms
        navigationController?.pushViewController(progressView, animated: true)
    }
    
    func navigateToProgressDetail(program: ProgramModel, programProgressDetail: ProgramProgressDetail) {
        let storyboardName = ParentConstants.Storyboards.Progress
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let progressDetail = storyboard.instantiateViewController(withIdentifier: "ProgressDetailViewController") as! ProgressDetailViewController
        progressDetail.viewModel = ProgressViewModel()
        progressDetail.viewModel.selectedProgram = program
        progressDetail.viewModel.programProgressDetail = programProgressDetail
        navigationController?.pushViewController(progressDetail, animated: true)
    }
    
    func navigateToNewProgramRequest(program: ProgramModel){
        let storyboardName = ParentConstants.Storyboards.Calendar
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let newProgramRequest = storyboard.instantiateViewController(withIdentifier: "NewProgramRequestController") as! NewProgramRequestController
        newProgramRequest.viewModel = CalenderViewModel()
        newProgramRequest.viewModel.selectedProgram = program
        navigationController?.present(newProgramRequest, animated: true, completion: nil)
    }
    
    func navigateToProgramOverview(program: ProgramModel){
        let storyboardName = ParentConstants.Storyboards.Calendar
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let programOverview = storyboard.instantiateViewController(withIdentifier: "ProgramOverviewController") as! ProgramOverviewController
        programOverview.viewModel = CalenderViewModel()
        programOverview.viewModel.selectedProgram = program
        navigationController?.pushViewController(programOverview, animated: true)
    }
    
    func navigateToChat(program: ProgramModel, selectedUser: DependantModel?) {
           let storyboardName = ParentConstants.Storyboards.Chat
           let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
           let ChatListViewController = storyboard.instantiateViewController(withIdentifier: "ChatListViewController") as! ChatListViewController
           ChatListViewController.viewModel = ChatListViewModel()
           ChatListViewController.viewModel.selectedProgram = program
           ChatListViewController.viewModel.user = selectedUser
           navigationController?.pushViewController(ChatListViewController, animated: true)
       }
}


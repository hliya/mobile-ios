//
//  MainViewModel.swift
//  aimnow
//
//  Created by Isuru Ranasinghe on 3/29/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import aimnow_core
import RealmSwift
import SwiftyUserDefaults

class MainViewModel: NSObject {
    
    let realm = try! Realm()

    var pageIndex: Int = 1
    var domain: ProgramDomain!
    
    var programList: [ProgramModel] = []

    init(domain : ProgramDomain = ProgramDomain.instance) {
        SharedAppResource.instance.loggedInUser = Defaults[\.userDetail]
        self.domain = domain
        self.programList = []
    }
    
    func getAssignedPrograms(_ completion: @escaping(_ result: ResultObject<AssignedProgramModel>) -> ()) {
        let assignmentTypeId = (Defaults[\.userDetail]?.typeId!)!
        if let player = SharedAppResource.instance.selectedPlayer, let playerId = player.id{
            let request = AssignedProgramRequestModel.init(requesterId: playerId, assignmentTypeId: assignmentTypeId, unitId: 1, pageIndex: self.pageIndex)
            self.domain.getAssingedProgramsFromAPI(router: NetworkRouter.getAssignedPrograms(request)) { (assignedPrograms) in
                if let assignedPrograms = assignedPrograms.assignedPrograms{
                    if(assignedPrograms.count > 0){
                        try! self.realm.write {
                            self.realm.add(assignedPrograms, update: Realm.UpdatePolicy.all)
                        }
                    }
                }
                if(assignedPrograms.total ?? 0 > (self.pageIndex * Constants.PAGE_SIZE)){
                    self.pageIndex += 1
                    completion(ResultObject(hasMoreRecords: true, result: assignedPrograms.assignedPrograms!, error: nil))
                }else{
                    completion(ResultObject(hasMoreRecords: false, result: assignedPrograms.assignedPrograms!, error: nil))
                }
            }
        }
    }
    
    func getPrograms(completion:@escaping(_ programs: [ProgramModel]) -> ()) {
        ProgramDomain.instance.getAllPrograms { (programRes) in
            if let programs = programRes.programModel{
                for program in programs{
                    let assPrograms = self.realm.objects(AssignedProgramModel.self).filter("referenceId = \(program.id)")
                    if(assPrograms.count > 0){
                        program.isEnrolled = true
                    }
                }
                if(programs.count > 0){
                    try! self.realm.write {
                        self.realm.add(programs, update: Realm.UpdatePolicy.all)
                    }
                }
                self.programList = programs.filter({$0.parentId.value == nil}).sorted(by: { (p1, p2) -> Bool in
                    return (p1.isEnrolled == true)
                })
                completion(self.programList)
            }
        }
    }
    
    func getDependantUsers() {
        UserDomain.instance.getDependantUsers { (dependants) in
            let dependant = dependants.last
            SharedAppResource.instance.selectedPlayer = dependant
            Observers.instance.playerSelectObsrver.onNext(dependant)
        }
    }
    
    func getProgramStats(completion: @escaping(_ programProgress: ProgramStatsResponse?, _ error: Error?) -> ()){
         if let player = SharedAppResource.instance.selectedPlayer, let playerId = player.id{
            let request = ProgramStatsRequestModel.init(on: "", id: 0, requestId: playerId, assignmentTypeId: 2, unitId: 1)
            self.domain.getProgramStats(router: NetworkRouter.getProgramStats(request)) { (response, error) in
                if(error != nil){
                    completion(nil, error)
                }else{
                    completion(response, nil)
                }
            }
        }
    }
}

//
//  ChatDetailViewModel.swift
//  parent
//
//  Created by Isuru Ranasinghe on 6/28/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import aimnow_core

class ChatDetailViewModel: NSObject {
    var receivers: [ChatUsers] = []
    var chats:[ChatMessageModel] = []
    var chatUsers: [String] = []
    var conversation: ChatModel!
    var eligibleUserList:[ChatUsers] = []
    var program: ProgramModel?
    var subType: eConversationSubType = .global
    var subTypeData : CurrentSubTypeData!
    var senderUser: ChatUsers?

    var title:String?
    var descriptionValue: String?
    
    func changeConversationTitle(title: String)  {
        
    }
    func sendMessage(txtMessage: String, isInit: Bool = false, subTypeData: CurrentSubTypeData?,callBack:@escaping (( _ success: Bool,  _ error: Error?) -> Void)) {
        if let user = SharedAppResource.instance.loggedInUser, let userId = user.id{
            if (!txtMessage.isBlank) {
                let newMessage = ChatMessageModel.init()
                newMessage.senderId = userId
                newMessage.senderFirstName = user.firstName
                newMessage.message = txtMessage.trim()
                newMessage.senderImage = user.profileImage ?? "/images/default-profile.png"
                newMessage.senderName = user.name
                newMessage.sentDate = Date.init()
                newMessage.timestamp = Date.init().toString(format: Constants.SERVER_DATE_FORMAT)
                newMessage.subType = subType.rawValue
                 newMessage.isEmoteText = false
                if(subType != .global && isInit){
                    newMessage.isEmoteText = true
                }
                FirebaseHelper.instance.updateConversationMessage(conversationId: self.conversation.conversationId!, message: newMessage) { (success, error) in
                    if(success){
                        if let subTypeData = self.subTypeData{
                            self.makeConversationSpecific(subTypeData: subTypeData, conversationId: self.conversation.conversationId!) { (success, error) in
                                callBack(success, error)
                            }
                        }else{
                            callBack(success, error)
                        }
                    }else{
                        callBack(success, error)
                    }
                    callBack(false, error)
                }
            }
        }
    }
    
    func makeConversationSpecific(subTypeData: CurrentSubTypeData,conversationId: String,callBack:@escaping (( _ success: Bool,  _ error: Error?) -> Void)) {
        FirebaseHelper.instance.makeConversationSpecific(conversationId: conversationId, subType: subTypeData) { (success, error) in
            callBack(success, error)
        }
    }
    
    func getChatWithSpecificUser(userId: String, programId: String? = nil, completion:@escaping(_ conversation:ChatModel?) ->()){
        let userConversation = SharedAppResource.instance.multipartyChats
        if let programId = programId {
            if let conversation = userConversation.first(where: {$0.receiver == userId && $0.isPrivate == true && $0.subTypeData.id == programId}){
                completion(conversation)
            }else{
                completion(nil)
            }
        }else{
            if let conversation = userConversation.first(where: {$0.receiver == userId && $0.isPrivate == true}){
                completion(conversation)
            }else{
                completion(nil)
            }
        }
    }
}

//
//  NotificationItemCell.swift
//  parent
//
//  Created by Isuru Ranasinghe on 10/18/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core

class NotificationItemCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblStatus: UIButton!
    @IBOutlet weak var vwStatus: UIView!
    var closure: ((_ onItemSelect: NotificationItem) -> ())?
    
    var item: NotificationItem? = nil
    
    public func set(item : NotificationItem){
        self.item = item;
        self.imgPlayer.isHidden = true;
        self.title.text = item.message
        if(item.hasSeen ?? false){
            self.vwStatus.backgroundColor = UIColor.init(hexString: "#00ED17")
        }else{
            self.vwStatus.backgroundColor = UIColor.init(hexString: "#FFCE09")
        }
    }
    
     @IBAction func onNotificationDetail(_ sender: UIButton) {
           if let closure = self.closure , !(item?.hasSeen ?? false) {
               closure(item!)
           }
       }
}

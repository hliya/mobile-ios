//
//  NotificationViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/16/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core
import RxSwift
import RxCocoa
import JGProgressHUD

class NotificationViewController: BaseViewController{
    
    @IBOutlet weak var tblNotifications : UITableView!
    var viewModel: NotificationVM!
    private let _notificationCell = "NotificationItemCell"
    var hud = SharedAppResource.instance.hud

    
    override func viewDidLoad() {
        if(viewModel == nil){
            viewModel = NotificationVM()
        }
        super.viewDidLoad()
        setupUI()
        setupObservers()
        self.viewModel.getNotifications(.initial)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    fileprivate func setupUI() {
        tblNotifications.dataSource = nil
        tblNotifications.register(UINib.init(nibName: _notificationCell, bundle: nil), forCellReuseIdentifier: _notificationCell)
        tblNotifications.delegate = self
        self.tblNotifications.configRefreshHeader(with: AnowRefresher.header(), container: self) { [weak self] in
            self?.viewModel.getNotifications(.pullHeader)
        }
    }
    
    func configureTblFooter() {
        self.tblNotifications.configRefreshFooter(with: AnowRefresher.footer(), container:self) { [weak self] in
            self?.viewModel.getNotifications(.pullFooter)
        }
    }
    
    override func setupObservers() {
        viewModel.didLoadData.subscribe(onNext: { (status) in
            guard self.viewModel.notifications.value.count > 0 else {
                self.tblNotifications.setEmptyMessage("No data available!")
                return
            }
        }).disposed(by: disposeBag)

        let hudManager = HUDManager.shared
        hudManager.loadingIndicator
            .subscribe(onNext: { (hudType) in
                guard let hud = hudType else {
                    self.dismissHUD()
                    return
                }
                switch(hud) {
                    case .success(_):
                        self.showSuccessHUD(hud)
                    case .loading(_):
                        self.showLoadingHUD(hud)
                    case .downloading(_):
                        self.showDownloadingHUD(hud)
                    case .error(_):
                        self.showErrorHUD(hud)
                    case .dismiss:
                        self.dismissHUD()
                }
            }).disposed(by: disposeBag)

        
        viewModel.didLoadData.asObservable().subscribe(onNext: { (loadingStatus) in
            if loadingStatus == .initial {
                self.configureTblFooter()
            } else if loadingStatus == .pullHeader {
                self.tblNotifications.switchRefreshHeader(to: .normal(.success, 0.5))
                self.configureTblFooter()
            } else if loadingStatus == .pullFooter {
                self.tblNotifications.switchRefreshFooter(to: .normal)
            } else if loadingStatus == .endRecord {
                self.tblNotifications.switchRefreshFooter(to: .removed)
            }
        }).disposed(by: disposeBag)
        
        viewModel.notifications.asObservable().bind(to: tblNotifications.rx.items){(tv, row, item) -> UITableViewCell in
            let cell = tv.dequeueReusableCell(withIdentifier: self._notificationCell, for: IndexPath.init(row: row, section: 0)) as! NotificationItemCell
            cell.set(item: item)
            cell.closure = { notification in
                
            }
            return cell
        }.disposed(by: disposeBag)
    
    }
    
    func showLoadingHUD(_ hudType: HUDType) {
        hud.textLabel.text = hudType.string()
        hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        hud.show(in: getRootVC().view)
    }
    
    func showSuccessHUD(_ hudType: HUDType) {
        hud.textLabel.text = hudType.string()
        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        hud.dismiss(afterDelay: 2.0)
        hud.show(in: getRootVC().view)
    }
    
    func showErrorHUD(_ hudType: HUDType) {
        hud.textLabel.text = hudType.string()
        hud.indicatorView = JGProgressHUDErrorIndicatorView()
        hud.show(in: getRootVC().view)
        hud.dismiss(afterDelay: 2.0)
    }

    func showDownloadingHUD(_ hudType: HUDType) {
        hud.vibrancyEnabled = true
        hud.indicatorView = JGProgressHUDPieIndicatorView()
        hud.detailTextLabel.text = "0% Complete"
        hud.textLabel.text = hudType.string()
        hud.show(in: getRootVC().view)
    }

    func dismissHUD() {
        DispatchQueue.main.async {
            self.hud.dismiss(animated: true)
        }
    }
    
    func getRootVC() -> UIViewController {
        let delegate = (UIApplication.shared.delegate) as! AppDelegate
        let vc = delegate.window?.rootViewController
        var topController = vc ?? self
        while let newTopController = topController.presentedViewController {
            topController = newTopController
        }
        return topController
    }
}

extension NotificationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78.0
    }
}

//
//  NotificationVM.swift
//  parent
//
//  Created by Isuru Ranasinghe on 10/17/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import RealmSwift
import aimnow_core
import RxSwift
import RxCocoa

class NotificationVM: NSObject {
    
    let realm = try! Realm()

    var pageIndex: Int = 1
    var selectedProgram: ProgramModel!
    var domain: UserDomain!
    var notifications = BehaviorRelay<[NotificationItem]>(value: [])
    private var _totalCount = 0
    let didLoadData = PublishSubject<DataLoadingStatus>.init()

    init(domain : UserDomain = UserDomain.instance) {
        self.domain = domain
    }
    
    func getNotifications(_ loadingStatus: DataLoadingStatus = .initial) {
        if (loadingStatus == .initial || loadingStatus == .pullHeader) {
            pageIndex = 1
            if loadingStatus == .initial { HUDManager.shared.manageHUD(HUDType.loading("Loading")) }
        }
        if let player = SharedAppResource.instance.selectedPlayer, let playerId = player.id{
            let request = NotificationRequestModel(from: nil, to: nil, unitId: 1, id: nil, requesterId: playerId, pageIndex: pageIndex)
            self.domain.getNotifiction(router: NetworkRouter.getNotifications(request)) { (notifications) in
                if (loadingStatus == .initial || loadingStatus == .pullHeader) {
                    self.notifications.removeAll()
                    self.removeHUD()
                }
                guard let notificationModel = notifications, let notifications = notificationModel.pagedItems else { return }
                self._totalCount = notificationModel.total ?? 0
                self.notifications.accept(self.notifications.value + notifications)
                self.didLoadData.onNext((loadingStatus))
                self.nextPagingInfo()
            }
        }
    }
    
    fileprivate func removeHUD() {
        HUDManager.shared.manageHUD(HUDType.dismiss)
    }
    
    private func nextPagingInfo() {
        if notifications.value.count == _totalCount {
            didLoadData.onNext(.endRecord)
        } else {
            pageIndex += 1
        }
    }
}

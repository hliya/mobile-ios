//
//  ProgressViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/13/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core
import FlexibleSteppedProgressBar

class ProgressViewController : BaseViewController, UIWebViewDelegate{
    
    @IBOutlet weak var webviewCharts : UIWebView!
    @IBOutlet weak var collectionViewDetail : UICollectionView!
    @IBOutlet weak var tabBarButtonDetail : UIBarButtonItem!
    @IBOutlet weak var lblStage : UILabel!
    @IBOutlet weak var btnDay : UIButton!
    @IBOutlet weak var btnWeek : UIButton!
    @IBOutlet weak var btnMonth : UIButton!
    @IBOutlet weak var progressView: FlexibleSteppedProgressBar!
    
    var programProgressSummary = [ProgramProgressSummary]()
    var categories = [ProgramProgressDetail]()

    var viewModel: ProgressViewModel!
    var maxIndex = -1
    var currentStage = 0
    var backgroundColor = UIColor(red: 218.0 / 255.0, green: 218.0 / 255.0, blue: 218.0 / 255.0, alpha: 1.0) // gray
    var progressColor = UIColor(red: 53.0 / 255.0, green: 226.0 / 255.0, blue: 195.0 / 255.0, alpha: 1.0) // green
    var textColorHere = UIColor(red: 153.0 / 255.0, green: 153.0 / 255.0, blue: 153.0 / 255.0, alpha: 1.0)// dark gray
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(viewModel == nil){
            viewModel = ProgressViewModel()
        }
        self.setupProgressBar()
        webviewCharts.delegate = self;
        if let url = Bundle.main.url(forResource: "index-rings", withExtension: "html") {
            webviewCharts.loadRequest(URLRequest(url: url))
        }
        
        self.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sharedInstance.selectedProgram = self.viewModel.selectedProgram
    }
    
    func loadData(){
        self.viewModel?.getProgramStatus {(progressStatus) in
            
            if let progressStatus = progressStatus{
                self.progressView.numberOfPoints = progressStatus.totalStages 
                self.currentStage = (progressStatus.currentStage - 1)
                self.progressView.currentIndex =  self.currentStage
                self.maxIndex = self.currentStage
                self.progressView.completedTillIndex = self.currentStage
                self.loadProgramProgress();
            }else{
                self.alertWithBoolResponse(message: "No records available") { (response) in
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.loadCharts();
    }
    
    internal override func setupObservers(){
        Observers.instance.playerSelectObsrver.asObserver().subscribe(onNext: { (player) in
            if let selectedPlayer = player{
                self.sharedInstance.selectedPlayer = selectedPlayer
                self.loadData()
            }
        }).disposed(by: disposeBag)
    }
    
    @IBAction  func showDetail(sender: UIBarButtonItem){
        if (sender.tag == 0){
            sender.image = UIImage(named: "overview", in: nil, compatibleWith: nil)
            sender.tag = 1
            self.webviewCharts.isHidden = true
            self.collectionViewDetail.isHidden = false;
        }else {
            sender.image = UIImage(named: "lists", in: nil, compatibleWith: nil)
            sender.tag = 0
            
            self.webviewCharts.isHidden = false
            self.collectionViewDetail.isHidden = true;
        } 
    }
    
    @IBAction func onFilterPressed (sender : UIButton){
        self.viewModel.filterBy = E.FilterMode(rawValue: sender.tag)!
        
        self.btnDay.setImage(UIImage(named: ""), for: UIControl.State.normal)
        self.btnWeek.setImage(UIImage(named: ""), for: UIControl.State.normal)
        self.btnMonth.setImage(UIImage(named: ""), for: UIControl.State.normal)
        
        sender.setImage(UIImage(named: "active"), for: UIControl.State.normal)
        
        self.loadProgramProgress();
    }
    
    func loadProgramProgress (){
        self.loading.startAnimating()
        self.lblStage.text = "STAGE \(self.currentStage + 1)"
        self.viewModel.getProgramProgress(currentStage: self.currentStage) { (programProgress) in
            self.loading.stopAnimating()
            if let progressStageSummary = programProgress.last?.progressSummary.last{
                self.programProgressSummary = progressStageSummary.progressStageSummary
                self.categories = self.programProgressSummary.first?.categories ?? []
                self.collectionViewDetail.reloadData()
            }
        }
        self.webviewCharts.reload()
        
    }
    
    func loadCharts(){
        self.viewModel.getProgramStats { (stats, error) in
            if(error == nil){
                (self.webviewCharts.stringByEvaluatingJavaScript(from:  String(format: "pushDataRing(%d,%f,%f)", stats?.total?.precAttendace ?? 0, stats?.total?.precTennisTime ?? 0, stats?.total?.precCompleted ?? 0)))
            }else{
                self.alert(message: error?.localizedDescription ?? "Something went wrong")
            }
        }
    }

}


extension ProgressViewController: UICollectionViewDelegate, UICollectionViewDataSource, FlexibleSteppedProgressBarDelegate {
    
    fileprivate func setupProgressBar() {
        progressView.lineHeight = 3
        progressView.radius = 4
        progressView.progressRadius = 6
        progressView.progressLineHeight = 3
        progressView.delegate = self
        progressView.useLastState = true
        progressView.lastStateCenterColor = UIColor.orange
        progressView.selectedBackgoundColor = UIColor.orange
        progressView.selectedOuterCircleStrokeColor = UIColor.green
        progressView.lastStateOuterCircleStrokeColor = UIColor.orange
        progressView.currentSelectedCenterColor = UIColor.green
        progressView.stepTextColor = UIColor.clear
        progressView.currentSelectedTextColor = UIColor.clear
        progressView.completedTillIndex = 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (self.programProgressSummary.count == 0) {
            collectionView.setEmptyMessage("No data available!")
        } else {
            collectionView.restore()
        }
        if collectionView.tag == 1 {
            return self.programProgressSummary[section].categories.count
        } else if collectionView.tag == 0 {
            return 0 //(self.program["stages"] as? Int)!
        }
        return 0;
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        var count = 0;
        if collectionView.tag == 1 {
            count = self.programProgressSummary.count
        } else if collectionView.tag == 0 {
            count = 1
        }
        return count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath)
        if cell is ProgressCollectionViewCell {
            let header = (cell as! ProgressCollectionViewCell)
            if collectionView.tag == 1 {
                let dt = self.programProgressSummary[indexPath.section].categories[indexPath.row]
                header.set(stat: dt)
            } else{
                header.setStage(programStagesInfo: (self.viewModel?.programStagesInfo!)! , index : indexPath.row)
            }
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headercell", for: indexPath);
        
        if sectionHeader is GroupHeaderCollectionViewCell {
            let header = (sectionHeader as! GroupHeaderCollectionViewCell)
            if collectionView.tag == 1 {
                let dt = self.programProgressSummary[indexPath.section].categories[indexPath.row]
                header.lblName.text = (dt.type!).uppercased();
                header.lblStat1.text = String((self.programProgressSummary[indexPath.section].total ?? 0))
                header.lblStat2.text = String(format: "%.1f%%", (self.programProgressSummary[indexPath.section].precentage ?? 0))
            }
        }
        
        return sectionHeader
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1 {
            let dt = self.programProgressSummary[indexPath.section].categories[indexPath.row]
            navigateToProgressDetail(program: self.viewModel.selectedProgram, programProgressDetail: dt)
        }
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     didSelectItemAtIndex index: Int) {
        progressBar.currentIndex = index
        self.currentStage = index
        self.loadProgramProgress()
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     canSelectItemAtIndex index: Int) -> Bool {
        return (index <= maxIndex)
    }
    
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                        textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
           return ""
       }
}

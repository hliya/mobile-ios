//
//  ProgressDetailViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/14/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import aimnow_core
import ImageViewer

class ProgressDetailViewController : BaseViewController, UIWebViewDelegate{
    
    @IBOutlet weak var webviewSingle : UIWebView!
    @IBOutlet weak var webviewBar : UIWebView!
    @IBOutlet weak var tabBarButtonDetail : UIBarButtonItem!
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblStage : UILabel!
    @IBOutlet weak var tvDescription : UITextView!
    
    @IBOutlet weak var cvFeedbacks: UICollectionView!
    var kFeedbackSummaryCell = "FeedbackSummaryCell"
    
    var filterBy :E.FilterMode = E.FilterMode.month
    var avPlayer: AVPlayer!
    var viewModel: ProgressViewModel!
    var playerProgramStats:[[Int : Double]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webviewSingle.delegate = self;
        webviewBar.delegate = self
        
        cvFeedbacks.register(UINib.init(nibName: kFeedbackSummaryCell, bundle: nil), forCellWithReuseIdentifier: kFeedbackSummaryCell)
        
        if(viewModel == nil){
            viewModel = ProgressViewModel()
        }
        if let url = Bundle.main.url(forResource: "index-single", withExtension: "html") {
            webviewSingle.loadRequest(URLRequest(url: url))
        }
        
        if let url = Bundle.main.url(forResource: "index-bar", withExtension: "html") {
            webviewBar.loadRequest(URLRequest(url: url))
        }
        
        lblName.text = self.viewModel.programProgressDetail?.name
        tvDescription.text = self.viewModel.programProgressDetail?.descriptionValue
        self.viewModel?.getProgramStatus {(progressStatus) in
            if let progressStatus = progressStatus{
                self.lblStage.text = progressStatus.name.uppercased()
            }else{
                self.lblStage.text = "Stage not yet started"
            }
        }
        self.loadData();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sharedInstance.selectedProgram = self.viewModel.selectedProgram
        var request = FeedbackRequestModel.init()
        request.requesterId = SharedAppResource.instance.loggedInUser?.id
        request.ownerId = SharedAppResource.instance.selectedPlayer?.id
        request.assignmentId = self.viewModel.programProgressDetail?.id
        request.assignmentTypeId = 2
        request.ascending = false
        request.unitId = 1
        request.pageIndex = 1
        request.pageSize = 50
        self.viewModel.getFeedbackDetails(request: request) { (feedbacks) in
            self.reloadFeedbackDataSet()
        }
    }
    
    fileprivate func reloadFeedbackDataSet(){
        self.cvFeedbacks.reloadData()
        let lastSection = self.cvFeedbacks.numberOfSections - 1
        let lastRow = self.cvFeedbacks.numberOfItems(inSection: lastSection)
        let indexPath = IndexPath(row: lastRow - 1, section: lastSection)
        self.cvFeedbacks.scrollToItem(at: indexPath, at: .right, animated: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        lblName.text = self.viewModel.programProgressDetail?.name
        self.webviewSingle.stringByEvaluatingJavaScript(from:  String(format: "pushDataRing(%d)", (self.viewModel.programProgressDetail?.stat ?? 0)!))
        if(webView == self.webviewBar){
            self.viewModel.domain.getPlayerStatsForProgram(player: SharedAppResource.instance.selectedPlayer!, programId: (self.viewModel.programProgressDetail?.id!)!, filterBy: .palyer) { (programStats) in
                self.playerProgramStats = programStats
                self.webviewBar.stringByEvaluatingJavaScript(from: "pushData(\(self.playerProgramStats.description.replacingOccurrences(of: ": ", with: ",")))")
            }
        }
    }
    
    @IBAction func playBestPractice(){
        play(url: URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")!)
    }
    
    func play(url:URL) -> Void {
        avPlayer = AVPlayer(url: url)
        let controller = AVPlayerViewController()
        controller.player = avPlayer
        controller.showsPlaybackControls = true
        present(controller, animated: true) {
            self.avPlayer.play()
        }
    }
    
    func loadData () -> Void{
        self.loading.startAnimating()
        self.viewModel.getProgramProgress(completion: { (programProgress) in
            self.loading.stopAnimating()
            self.webviewSingle.reload()
        })
    }
    
    @IBAction func onClickNewFeedback(_ sender: UIButton) {
            
    }
}

extension ProgressDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.viewModel.feedbacks.count == 0){
            collectionView.setEmptyMessage("No feedback given")
            return 0
        }else{
            collectionView.setEmptyMessage("")
            return self.viewModel.feedbacks.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kFeedbackSummaryCell, for: indexPath as IndexPath) as! FeedbackSummaryCell
        let feedback = self.viewModel.feedbacks[indexPath.row]
        cell.setFeedbackInfo(feedback: feedback)
        cell.onFeedbackVideoSelect = {feedback in
            if let attachment = feedback.attachment{
                if let path = attachment.relativePath{
                    let resolvedPath = path.replacingOccurrences(of: "\\", with: "/")
                    self.play(url: URL(string: Constants.BASE_API_URL+resolvedPath)!)
                }
            }
        }
        cell.onFeedbackImageSelect = { imageView in
            ImageViewer.show(imageView, presentingVC: self)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize.init(width: self.view.bounds.width - 50, height: 100)
     }
    
 
}

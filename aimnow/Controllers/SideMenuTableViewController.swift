//
//  SideMenuTableViewController.swift
//  SideMenu
//
//  Created by Jon Kent on 4/5/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation
import SideMenu
import aimnow_core
import SwiftyUserDefaults
import RxSwift

class SideMenuTableViewController: UITableViewController,UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var collectionView : UICollectionView!
    

    let reuseIdentifier = "cell"
    var items:[DependantModel]? = nil
    var disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // refresh cell blur effect in case it changed
        guard SideMenuManager.default.menuBlurEffectStyle == nil else {
            return
        }
    }
    
    override func viewDidLoad() {
        if let user: UserModel = SharedAppResource.instance.loggedInUser{
            if let profileImage = user.profileImage{
                self.imgAvatar.downloaded(from: profileImage , contentMode: .scaleAspectFit);
            }else{
                self.imgAvatar.image = UIImage.init(named: "profile")
            }
            self.lblEmail.text = user.email
            self.lblName.text = user.name
            
            // For the moment I'm hardcoding unitId
            self.subscribeObservers()
            UserDomain.instance.getDependantUsers { (dependents) in
                self.items = dependents
                self.collectionView.reloadData()
            }
        }
        
    }
    
    private func subscribeObservers(){
        Observers.instance.playerSelectObsrver.asObserver().subscribe(onNext: { (player) in
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath) as! UITableViewVibrantCell
        
        cell.blurEffectStyle = SideMenuManager.default.menuBlurEffectStyle
        
        return cell
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items?.count ?? 0
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath)
        if cell is PlayerCollectionViewCell {
            let player = self.items![indexPath.item]
            (cell as! PlayerCollectionViewCell).set(player:player , selected: (SharedAppResource.instance.selectedPlayer?.id == player.id))
        }
        
        return cell
    }
}

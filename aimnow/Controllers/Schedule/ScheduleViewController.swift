//
//  ScheduleViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/12/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core
import RealmSwift
import Realm

class ScheduleViewController : BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource , UIWebViewDelegate{
    
    @IBOutlet weak var webviewCharts : UIWebView!
    @IBOutlet weak var collectionView : UICollectionView!
    
    var viewModel: ScheduleViewModel!
    
    var kScheduleCell = "ScheduleCollectionViewCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(viewModel == nil){
            viewModel = ScheduleViewModel()
        }
        
        collectionView.register(UINib.init(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: kScheduleCell)
        webviewCharts.delegate = self;
        if let url = Bundle.main.url(forResource: "index", withExtension: "html") {
            webviewCharts.loadRequest(URLRequest(url: url))
        }
        self.loadAssignedPrograms()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sharedInstance.selectedProgram = self.viewModel.selectedProgram
    }
    
    func loadAssignedPrograms() {
        self.loading.startAnimating()
        self.viewModel.getAssignedPrograms { (result : ResultObject<AssignedProgramModel>) in
            if(result.hasMoreRecords){
                self.loadAssignedPrograms()
            }else{
                self.loading.stopAnimating()
                self.loadSecheduledData()
            }
        }
    }
    
    fileprivate func loadSecheduledData(){
        self.viewModel.getProgramScheduleFor (player: sharedInstance.selectedPlayer!) { (groupedPrograms) in
            self.collectionView.reloadData()
        }
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.loading.startAnimating()
        self.viewModel.getProgramStats { (stats, error) in
            if(error == nil){
                (self.webviewCharts.stringByEvaluatingJavaScript(from:  String(format: "pushDataRing(%f,%f,%f)", stats?.total?.precTennisTime ?? 0, stats?.total?.precCompleted ?? 0, stats?.total?.precAttendace ?? 0)))
            }else{
                self.alert(message: error?.localizedDescription ?? "Something went wrong")
            }
        }
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if ( self.viewModel.groupedPrograms.count == 0) {
            collectionView.setEmptyMessage("No data available!")
        } else {
            collectionView.restore()
        }
        return self.viewModel.groupedPrograms[section].programs.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.viewModel.groupedPrograms.count;
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ScheduleCollectionViewCell
        
        cell.setForSchedule(assignedProgram: self.viewModel.groupedPrograms[indexPath.section].programs[indexPath.row])
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headercell", for: indexPath) as! GroupHeaderCollectionViewCell;
        sectionHeader.lblDate.text = dateFormatter.string(from: self.viewModel.groupedPrograms[indexPath.section].date)
        return sectionHeader
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    @IBAction func onClickProgressView(sender: UIButton){
        self.navigateToProgressView(program: self.viewModel.selectedProgram, groupedScheduledPrograms: self.viewModel.groupedPrograms)
    }
    
    @IBAction func onCalendarViewSelect(){
        self.navigateToCalendarController(program: self.viewModel.selectedProgram, groupedScheduledPrograms: viewModel.groupedPrograms)
    }
}

//
//  ACNotificatonRouter.swift
//  Vendor-Compare
//
//  Created by Isuru Ranasinghe on 3/19/18.
//  Copyright © 2018 Vendor Compare. All rights reserved.
//

import Foundation
import UIKit
import JGProgressHUD
import RxSwift
import RxCocoa

class ACNotificatonRouter: NSObject {
    final let CATEGORY = "categoryId"
    final let PRIMARYID = "primaryId"
    final let SECONDARYID = "secondaryId"
    
    public static var navigationController: UINavigationController!
    private static var instance:ACNotificatonRouter!
    let KRProgressHUD = JGProgressHUD.init(style: .light)
    let disposeBag = DisposeBag()

    public static func getInstance() -> ACNotificatonRouter{
        if(instance == nil || navigationController == nil){
           instance = ACNotificatonRouter()
        }
        return instance
    }
    
    // Default initializer
    override init() {
        if let rootController = appDelegate.window?.rootViewController{
            ACNotificatonRouter.navigationController = rootController.topMostViewController().navigationController
        }
    }
    
    public func handleNotification(navigationController:UINavigationController, notificationInfo:[AnyHashable: Any]) {
        ACNotificatonRouter.navigationController = navigationController
        self.handleNotification(notificationInfo: notificationInfo)
    }
    
    public func handleNotification(notificationInfo:[AnyHashable: Any]) {
        // Saving push notification in shared preference
         SharedAppResource.instance.pushNotificationInfo = notificationInfo
        
        if let categoryVal = notificationInfo[CATEGORY], let _ = SharedAppResource.instance.acUserProfile{

            let categoryId = Int(categoryVal as! String)
            let primaryId = notificationInfo[PRIMARYID] as! String
            guard let secondaryId = notificationInfo[SECONDARYID] as? String else { return }
            let optionalValue = notificationInfo[Constants.Ancillary.NotificationKeys.OptionalValue] as? String
            
            appDelegate.userInfo = nil
            
            switch categoryId! {
            case eNotificationCategories.Chat.rawValue: 
                ACFirebaseHelper.instance.getUserProfile(userId: primaryId, callback: { (user) in
                    if let user = user{
                        self.loadChat(user: user) // TODO need to fix here
                    }
                })
                break
            case eNotificationCategories.JobApprovedByOwner.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.JobApprovedByPM.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: true)
                break
            case eNotificationCategories.JobEndedConsumer.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.JobEndedProvider.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.JobEndedPM.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.JobWaitingForApproveByOwner.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false, selectedProviderType: eJobsListType.AvailableJobs)
                break
            case eNotificationCategories.JobWaitingForApproveByPM.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false, selectedProviderType: eJobsListType.AvailableJobs)
                break
            case eNotificationCategories.MatchingJobAvailable.rawValue:
                self.loadFindJobDetailI(jobId: primaryId, selectedProviderType: eJobsListType.MatchingJobs)
                break
            case eNotificationCategories.NewJobPosted.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false, selectedProviderType: eJobsListType.AvailableJobs)
                break
            case eNotificationCategories.ProviderArrived.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.ProviderOnTheWay.rawValue:
                self.loadProviderMapView(jobId: primaryId)
                break
            case eNotificationCategories.QuoteAcceptedConsumer.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.QuoteAcceptedProvider.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.QuoteAcceptedPM.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.QuotePlacedConsumer.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.QuotePlacedProvider.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.QuoteRejected.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false, selectedProviderType: eJobsListType.QuoteRejectedJobs)
                break
            case eNotificationCategories.TaskUpdated.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false, isTaskUpdate: true)
                break
            case eNotificationCategories.QuoteFeedbackProvided.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.QuoteFeedbackReceived.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.JobApprovalRejected.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.JobPostedPM.rawValue:
                self.pmloadJobDetailI(jobId: primaryId)
                break
            case eNotificationCategories.QuotePlacedPM.rawValue:
                self.pmloadJobDetailI(jobId: primaryId)
                break
            case eNotificationCategories.JobPostedTenant.rawValue:
                self.pmloadJobDetailI(jobId: primaryId)
                break
            case eNotificationCategories.QuoteFeedbackProvidedPM.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.ShareSchedule.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false, shouldLoadEvent: true)
                break
            case eNotificationCategories.VideoCall.rawValue:
                self.videoCall(callerName: optionalValue!, callerId: primaryId)
                break
            case eNotificationCategories.JobCompletedByProvider.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false, selectedProviderType: eJobsListType.CompletedJobs)
                break
            case eNotificationCategories.RevisedQuoteAvailable.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.QuoteRevisionRequested.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.JobReviewRejected.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false)
                break
            case eNotificationCategories.JobScheduledByProvider.rawValue:
                self.loadJobDetailI(jobId: primaryId, selectedProviderType: eJobsListType.None)
                break
            case eNotificationCategories.ScheduledJobNotification.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false, shouldLoadEvent: true)
                break
            case eNotificationCategories.JobScheduleStatusUpdate.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false, shouldLoadEvent: true)
                break
            case eNotificationCategories.Conversation.rawValue:
                let secondaryId = notificationInfo[SECONDARYID] as! String
                self.navigateToConversation(conversationId: secondaryId)
                break
            case eNotificationCategories.JobAssignedToSubTrade.rawValue:
                let jobId = notificationInfo[SECONDARYID] as! String
                self.loadJobDetailI(jobId: jobId, isPMApproval: false, selectedProviderType:eJobsListType.None)
                break
            case eNotificationCategories.PropertyInspectionDraftJobReminder.rawValue:
                self.loadInspectionJobsAssetsView(inspectionId: primaryId, propertyId: secondaryId)
                break
            case eNotificationCategories.PropertyInspectionScheduleReminder.rawValue:
                self.presentInspecrtionReminder(inspectionId: primaryId)
                break
            case eNotificationCategories.PMStartInspectionNotification.rawValue:
                SharedAppResource.instance.isPMMonitoringAvailable = true
                SharedAppResource.instance.onTheWayInspectionId = primaryId
                SharedAppResource.instance.onTheWayPMId = secondaryId
                break
            case eNotificationCategories.JobVariationAvailable.rawValue:
                if Utils.isPM() {
                    self.pmloadJobDetailI(jobId: primaryId)
                } else if Utils.isProvider() {
                    self.loadFindJobDetailI(jobId: primaryId)
                }
            case eNotificationCategories.MissedVideoCall.rawValue:
                self.videoCall(callerName: optionalValue!, callerId: primaryId)
                break
            case eNotificationCategories.JobWithPendingInvoiceState.rawValue:
                self.loadJobDetailI(jobId: primaryId, isPMApproval: false, selectedProviderType: eJobsListType.CompletedJobs)
                break
                // Updated by Sachithra
            case eNotificationCategories.DraftedJobPostedByPm.rawValue:
                self.loadJobDetailI(jobId: primaryId, selectedProviderType: eJobsListType.None)
                break
            case eNotificationCategories.ProviderJobAssignedNotification.rawValue:
                self.loadJobDetailI(jobId: secondaryId, selectedProviderType: eJobsListType.None)
                break
            case eNotificationCategories.PayerChanged.rawValue:
                //self.loadJobDetailI(jobId: primaryId)
                break
            case eNotificationCategories.PayerAssigned.rawValue:
                //self.loadJobDetailI(jobId: primaryId)
                break
            case eNotificationCategories.InvoiceReceived.rawValue:
                self.loadJobDetailI(jobId: primaryId, selectedProviderType: eJobsListType.None)
                break
            case eNotificationCategories.JobCompletePaymentFailure.rawValue:
                self.loadJobDetailI(jobId: primaryId, selectedProviderType: eJobsListType.None)
                break
            case eNotificationCategories.PendingReviewReminder.rawValue:
                self.loadJobDetailI(jobId: primaryId, selectedProviderType: eJobsListType.None)
                break
            case eNotificationCategories.PendingPaymentProcessed.rawValue:
                self.loadJobDetailI(jobId: primaryId)
                break
            case eNotificationCategories.LicenceOrInsuranceUpdateNotificationToSuperAdmin.rawValue:
                //self.loadJobDetailI(jobId: primaryId)
                break
            case eNotificationCategories.JobWithPendingInvoiceState.rawValue:
                self.loadJobDetailI(jobId: primaryId, selectedProviderType: eJobsListType.None)
                break
            case eNotificationCategories.SyncFailed.rawValue:
                
                break
            case eNotificationCategories.JobEdited.rawValue:
                self.loadJobDetailI(jobId: primaryId)
                break
            case eNotificationCategories.EditedJobPriceRevised.rawValue:
                self.loadJobDetailI(jobId: primaryId)
                break
            case eNotificationCategories.DeactivateJobByPm.rawValue:
                self.loadJobDetailI(jobId: primaryId, selectedProviderType: eJobsListType.None)
                break
            case eNotificationCategories.JobSnoozeExpired.rawValue:
                //TODO: Handle notification
                break
            case eNotificationCategories.JobSnoozeExpiring.rawValue:
                //TODO: Handle notification
                break
            case eNotificationCategories.PropertyInspectionReportGenerated.rawValue:
                let inspection = PropertyInspection.init()
                inspection.id = primaryId
                self.showInspectionSummary(inspection: inspection)
                break
                
            case eNotificationCategories.QuoteEditProvider.rawValue,
                 eNotificationCategories.QuoteDraftProvider.rawValue,
                 eNotificationCategories.SiteVisitAssigned.rawValue,
                 eNotificationCategories.SiteVisitUpdated.rawValue:
                self.loadJobDetailI(jobId: primaryId, selectedProviderType: eJobsListType.None)
                break
            case eNotificationCategories.CompleteJobRejectedNotification.rawValue:
                self.loadJobDetailI(jobId: secondaryId, selectedProviderType: eJobsListType.None)
                break
            case eNotificationCategories.DraftedBidSiteVisit.rawValue, eNotificationCategories.PlasedBidSiteVisit.rawValue:
                self.loadJobDetailI(jobId: primaryId)
                break
            case eNotificationCategories.QuoteLimitMet.rawValue, eNotificationCategories.QuoteLimitIncreased.rawValue:
                self.loadJobDetailI(jobId: primaryId)
                break
            case eNotificationCategories.PmOnTheWayForTheInspection.rawValue:
                self.presentPMOnTheWayMapForNonPMs(inspectionId: primaryId, pmId: secondaryId)
                break
            default:
                break
            }
        }
    }
    
    func showInspectionSummary(inspection: PropertyInspection) {
        let storyboard: UIStoryboard = UIStoryboard(name: Constants.Ancillary.Storyboards.common, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "InspectionSummaryController") as! InspectionSummaryController
        let viewModel = InspectionSummaryViewModel(inspection: inspection)
        controller.viewModel = viewModel
        ACNotificatonRouter.navigationController.pushViewController(controller, animated: true)
    }
    
    func presentInspecrtionReminder (inspectionId:String) {
        let storyboard = UIStoryboard(name: Constants.Ancillary.Storyboards.propertyManager, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ReminderController") as! ReminderController
        controller.modalPresentationStyle = .custom
        controller.viewModel = ReminderViewModel(inspectionId: inspectionId)
        ACNotificatonRouter.navigationController.present(controller, animated: true, completion: nil)
    }
    
    private func navigateToConversation (conversationId:String) {
        let conversation = ConversationModel()
        conversation.conversationId = conversationId
        conversation.conversationName = "Conversation"
        conversation.isPrivate = false
        
        let storyboard = UIStoryboard(name: Constants.Ancillary.Storyboards.common, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ACChatController") as! ACChatController
        controller.viewModel = ACChatViewModel()
        controller.viewModel.conversation = conversation
        ACNotificatonRouter.navigationController.pushViewController(controller, animated: true)
    }
    
    private func loadChat(user: ACProvider) {
        let storyboard = UIStoryboard(name: Constants.Ancillary.Storyboards.common, bundle: nil)
        let controller: ACChatController = storyboard.instantiateViewController(withIdentifier: "ACChatController") as! ACChatController
        controller.viewModel = ACChatViewModel()
        controller.viewModel.user = ACChatUsers.init(userId: user.userId!, displayName: user.getFullName() , profileImage: user.profileImageUrl ?? "")
        
        ACNotificatonRouter.navigationController.pushViewController(controller, animated: true)
    }
    
    private func loadJobDetailI(jobId: String = "", isPMApproval: Bool = false, shouldLoadEvent: Bool = false, selectedProviderType:eJobsListType = eJobsListType.None, isTaskUpdate: Bool = false){
        let user = SharedAppResource.instance.acUserProfile
        if (user?.workingCountryId != nil) {
            let storyboard = UIStoryboard(name: Constants.Ancillary.Storyboards.trade, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "SPJobDetailsViewController") as! SPJobDetailsViewController
            controller.viewModel = SPJobDetailsViewModel()
            controller.viewModel.jobId = jobId
            controller.viewModel.selectedBucket = selectedProviderType
            ACNotificatonRouter.navigationController.pushViewController(controller, animated: true)
        } else {
            self.pmloadJobDetailI(jobId: jobId)
        }
    }
    
    func presentPMOnTheWayMapForNonPMs(inspectionId: String, pmId: String) {
        let identifier = String(describing: PMTrackerController.self)
        let storyboard = UIStoryboard(name: Constants.Ancillary.Storyboards.common, bundle: nil)
        let controller =
            storyboard.instantiateViewController(withIdentifier: identifier) as! PMTrackerController
        let viewModel = PMTrackerViewModel(inspectionId: inspectionId, pmId: pmId)
        controller.viewModel = viewModel
        ACNotificatonRouter.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func loadFindJobDetailI(jobId: String, shouldLoadEvent: Bool = false, selectedProviderType:eJobsListType = eJobsListType.UpcomingJobs){
        let storyboard = UIStoryboard(name: Constants.Ancillary.Storyboards.trade, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SPJobDetailsViewController") as! SPJobDetailsViewController
        controller.viewModel = SPJobDetailsViewModel()
        controller.viewModel.jobId = jobId
        controller.viewModel.selectedBucket = eJobsListType.None
        ACNotificatonRouter.navigationController.pushViewController(controller, animated: true)
    }
    
    private func pmloadJobDetailI(jobId: String) {
        KRProgressHUD.show(in:  (ACNotificatonRouter.navigationController.topViewController?.view)!, animated: true)
        let jdVM = JobDetailViewModel()
        jdVM.jobDetailObservable.subscribe(onNext: { (jobDetail) in
            self.KRProgressHUD.dismiss(animated: true)
            let storyboard = UIStoryboard(name: Constants.Ancillary.Storyboards.propertyManager, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "JobDetailController") as! JobDetailController
            controller.viewModel = JobDetailViewModel()
            let job = ACJob()
            job.id = jobId
            controller.viewModel.job = job
            controller.viewModel.jobDetail = jobDetail
            controller.viewModel.selectedBucket =  eJobsListType.None
            ACNotificatonRouter.navigationController.pushViewController(controller, animated: true)
        }).disposed(by: disposeBag)
        jdVM.jobDetail(jobId: jobId)
    }
    
    private func loadProviderMapView(jobId: String) {
        guard let user = SharedAppResource.instance.acUserProfile else { return }
        if user.isPropertyManager {
            let storyboard = UIStoryboard(name: Constants.Ancillary.Storyboards.consumer, bundle: nil)
            let controller =
                storyboard.instantiateViewController(withIdentifier: "ProviderMonitoringMapController") as! ProviderMonitoringMapController
            ACNotificatonRouter.navigationController?.pushViewController(controller, animated: true)
        } else if user.isPropertyTenant || user.isPropertyOwner {
            let storyboard = UIStoryboard(name: Constants.Ancillary.Storyboards.tenantOwner, bundle: nil)
            let controller =
                storyboard.instantiateViewController(withIdentifier: "SPMonitoringTenantOwnerController") as! SPMonitoringTenantOwnerController
            let viewModel = SPMonitoringTenantOwnerViewModel(jobId: jobId)
            controller.viewModel = viewModel
            ACNotificatonRouter.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func showApproveButton (job:ACJob) -> Bool {
        let user = SharedAppResource.instance.acUserProfile
        var showApproveButton = false
        if (job.workflowStatusId == eWorkflowStatus.JobAwaitingOwnerApproval.rawValue) {
            showApproveButton = !(job.workflowStatusId == eWorkflowStatus.JobAwaitingOwnerApproval.rawValue &&
                job.serviceConsumerId != user!.userId!)
        } else if (user!.isPropertyManager && job.workflowStatusId == eWorkflowStatus.JobAwaitingForApproval.rawValue) {
            showApproveButton = true
        }
        
        return showApproveButton
    }
    
    private func videoCall (callerName:String, callerId:String) {
            Utils.startVideoCall(callerId: callerId,
                                 callerName: callerName,
                                 presentController: ACNotificatonRouter.navigationController)
    }
    
    private func loadInspectionJobsAssetsView(inspectionId: String, propertyId: String) {
        let storyboard = UIStoryboard(name: Constants.Ancillary.Storyboards.propertyManager, bundle: nil)
        let inspectionJobAssetVC = storyboard.instantiateViewController(withIdentifier: "InspectionJobAssetViewController") as! InspectionJobAssetViewController
        inspectionJobAssetVC.viewModel = InspectionJobAssetViewModel()
        SharedAppResource.instance.inspectionId = inspectionId
        SharedAppResource.instance.propertyId = propertyId
        inspectionJobAssetVC.viewModel.isFromNotificationClick = true
        ACNotificatonRouter.navigationController.pushViewController(inspectionJobAssetVC, animated: true)
    }
}


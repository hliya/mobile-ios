//
//  Extentions.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/5/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import UIKit
import Realm
import RealmSwift

public extension String {
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in:NSCharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    
    var localized: String {
        if let _ = UserDefaults.standard.string(forKey: "language") {} else {
            UserDefaults.standard.set("en", forKey: "language")
            UserDefaults.standard.synchronize()
        }
        
        let lang = UserDefaults.standard.string(forKey: "language")
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}

extension Date: Strideable {
    public func distance(to other: Date) -> TimeInterval {
        return other.timeIntervalSinceReferenceDate - self.timeIntervalSinceReferenceDate
    }

    public func advanced(by n: TimeInterval) -> Date {
        return self + n
    }
}

public extension UIViewController {
    func alert(message: String, title: String = "", okActionTitle:String = "OK", cancelActionTitle:String = "", callback:@escaping (_ action: UIAlertAction) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: okActionTitle, style: .default, handler: callback)
        alertController.addAction(OKAction)
        
        var cancelAction : UIAlertAction!
        if (!cancelActionTitle.isBlank){
            cancelAction = UIAlertAction(title: cancelActionTitle, style: .default, handler: callback)
            alertController.addAction(cancelAction)
        }
        Utils.presentUniversalAlertController(controller: self, alertController: alertController)
    }
    
    func alertWithBoolResponse(message: String, title: String = "", okActionTitle:String = "OK", cancelActionTitle:String = "", callback:@escaping (_ result: Bool) -> ()) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: okActionTitle, style: .default, handler: { action in
            _ = callback(true)
        })
        
        alertController.addAction(OKAction)
        
        var cancelAction : UIAlertAction!
        if (!cancelActionTitle.isBlank){
            cancelAction = UIAlertAction(title: cancelActionTitle, style: .default, handler: { action in
                _ = callback(false)
            })
            alertController.addAction(cancelAction)
        }
        Utils.presentUniversalAlertController(controller: self, alertController: alertController)
    }
    
    func alertWithTextField (message: String,
                             title: String = "",
                             textFieldPlaceHolder:String,
                             okActionTitle:String = "OK",
                             cancelActionTitle:String = "Cancel",
                             isSecurityTextField: Bool = false,
                             callback:@escaping (_ text:String?) -> Void) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: okActionTitle, style: .default, handler: {
            alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            textField.isSecureTextEntry = isSecurityTextField
            callback(textField.text?.trim())
        })
        alertController.addAction(action)
        alertController.addTextField { (textField) in
            textField.placeholder = textFieldPlaceHolder
        }
        
        var cancelAction : UIAlertAction!
        if (!cancelActionTitle.isBlank){
            cancelAction = UIAlertAction(title: cancelActionTitle, style: .default, handler: nil)
            alertController.addAction(cancelAction)
        }
        Utils.presentUniversalAlertController(controller: self, alertController: alertController)
    }
    
    func alertWithTextFieldWithResponse (message: String,
                                         title: String = "",
                                         textFieldPlaceHolder:String,
                                         textFieldValue:String = "",
                                         okActionTitle:String = "OK",
                                         cancelActionTitle:String = "Cancel",
                                         keyboardType: UIKeyboardType = UIKeyboardType.default,
                                         callback:@escaping (_ text:String?, _ userAction:Bool) -> Void) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: okActionTitle, style: .default, handler: {
            alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            callback(textField.text?.trim(), true)
        })
        alertController.addAction(action)
        alertController.addTextField { (textField) in
            textField.placeholder = textFieldPlaceHolder
            textField.text = textFieldValue
            textField.keyboardType = keyboardType
        }
        
        var cancelAction : UIAlertAction!
        if (!cancelActionTitle.isBlank){
            cancelAction = UIAlertAction(title: cancelActionTitle, style: .default, handler: {
                alert -> Void in
                callback(nil, false)
            })
            alertController.addAction(cancelAction)
        }
        Utils.presentUniversalAlertController(controller: self, alertController: alertController)
    }
    
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "action_ok".localized, style: .default, handler: nil)
        alertController.addAction(OKAction)
        Utils.presentUniversalAlertController(controller: self, alertController: alertController)
    }
    
    func add(_ child: UIViewController, frame: CGRect? = nil) {
        addChild(child)
        
        if let frame = frame {
            child.view.frame = frame
        }
        
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func remove() {
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
    
    func topMostViewController() -> UIViewController {
        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
        }
        
        return self
    }
    
    func hideKeyboardWhenTappedAround(isActive: Bool = true) {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func presentLikePush(_ viewControllerToPresent: UIViewController) {
           let transition = CATransition()
           transition.duration = 0.25
           transition.type = CATransitionType.push
           transition.subtype = CATransitionSubtype.fromRight
           self.view.window?.layer.add(transition, forKey: kCATransition)

           present(viewControllerToPresent, animated: false)
       }

       func dismissLikePush() {
           let transition = CATransition()
           transition.duration = 0.25
           transition.type = CATransitionType.push
           transition.subtype = CATransitionSubtype.fromLeft
           self.view.window!.layer.add(transition, forKey: kCATransition)
           dismiss(animated: false)
       }
}

protocol CascadeDeleting {
    func delete<S: Sequence>(_ objects: S, cascading: Bool) where S.Iterator.Element: Object
    func delete<Entity: Object>(_ entity: Entity, cascading: Bool)
}

extension Realm: CascadeDeleting {
    func delete<S: Sequence>(_ objects: S, cascading: Bool) where S.Iterator.Element: Object {
        for obj in objects {
            delete(obj, cascading: cascading)
        }
    }
    
    func delete<Entity: Object>(_ entity: Entity, cascading: Bool) {
        if cascading {
            cascadeDelete(entity)
        } else {
            delete(entity)
        }
    }
}

private extension Realm {
    private func cascadeDelete(_ entity: RLMObjectBase) {
        guard let entity = entity as? Object else { return }
        var toBeDeleted = Set<RLMObjectBase>()
        toBeDeleted.insert(entity)
        while !toBeDeleted.isEmpty {
            guard let element = toBeDeleted.removeFirst() as? Object,
                !element.isInvalidated else { continue }
            resolve(element: element, toBeDeleted: &toBeDeleted)
        }
    }
    
    private func resolve(element: Object, toBeDeleted: inout Set<RLMObjectBase>) {
        element.objectSchema.properties.forEach {
            guard let value = element.value(forKey: $0.name) else { return }
            if let entity = value as? RLMObjectBase {
                toBeDeleted.insert(entity)
            } else if let list = value as? RealmSwift.ListBase {
                for index in 0 ..< list._rlmArray.count {
                    if let realmObject = list._rlmArray.object(at: index) as? RLMObjectBase {
                        toBeDeleted.insert(realmObject)
                    }
                }
            }
        }
        delete(element)
    }
}

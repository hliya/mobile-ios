//
//  ProfileInfoCell.swift
//  parent
//
//  Created by Isuru Ranasinghe on 9/27/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core

class ProfileInfoCell: UICollectionViewCell {

    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var imgDetail : UIImageView!
    
    var profileInfo: Profile!
    var dependantInfo: DependantModel!
    
    func setProfileInfo(info: Profile){
        self.profileInfo = info
        self.lblContent.text = info.content
        self.imgDetail.isHidden = true
    }
    
    func setDependantInfo(info: DependantModel){
        self.dependantInfo = info
        self.lblContent.text = info.name
        self.imgDetail.isHidden = false
    }
}

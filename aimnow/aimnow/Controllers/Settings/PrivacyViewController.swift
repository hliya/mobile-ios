//
//  PrivacyViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/16/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import WebKit

class PrivacyViewController: BaseViewController, WKUIDelegate, WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!

        override func viewDidLoad() {
            super.viewDidLoad()

            webView.uiDelegate = self
            webView.navigationDelegate = self

            let url = Bundle.main.url(forResource: "privacy", withExtension: "html")!
            webView.loadFileURL(url, allowingReadAccessTo: url)
            let request = URLRequest(url: url)
            webView.load(request)
        }
}

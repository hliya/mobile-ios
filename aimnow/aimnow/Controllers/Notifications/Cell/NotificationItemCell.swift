//
//  NotificationItemCell.swift
//  parent
//
//  Created by Isuru Ranasinghe on 10/18/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core

class NotificationItemCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var vwStatus: UIView!
    var closure: ((_ onItemSelect: NotificationItem) -> ())?
    
    var item: NotificationItem? = nil
    
    public func set(item : NotificationItem){
        self.item = item;
        self.imgPlayer.isHidden = true;
        self.lblTime.text = (item.updatedOn?.date(format: Constants.SERVER_DATE_FORMAT_WITH_ZONE)?.toString(format: Constants.CHAT_DATE_DISPLAY_FORMAT)) ?? "";
        if (item.sender?.firstName == item.sender?.lastName){
            self.subTitle.text = item.sender?.firstName;
        }else{
            self.subTitle.text = String.init(format: "%@ %@", item.sender?.firstName ?? "", item.sender?.lastName ?? "")
        }
        self.imgPlayer.isHidden = true;
        self.title.text = item.message
        if(item.hasSeen ?? false){
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Green)
        }else{
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Yellow)
        }
    }
    
     @IBAction func onNotificationDetail(_ sender: UIButton) {
           if let closure = self.closure , !(item?.hasSeen ?? false) {
               closure(item!)
           }
       }
    
    override func didMoveToSuperview() {
         selectionStyle = .none
     }
}

//
//  ScheduleVM.swift
//  aimnow
//
//  Created by Isuru Ranasinghe on 3/11/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import aimnow_core
import SwiftyUserDefaults
import Realm
import RealmSwift

class ScheduleViewModel: NSObject {
    
    let realm = try! Realm()

    var pageIndex: Int = 1
    var selectedProgram: ProgramModel!
    var domain: ProgramDomain!
    var assignedPrograms:[AssignedProgramModel]?
    var groupedPrograms: [GroupedScheduledPrograms] = []
    
    init(domain : ProgramDomain = ProgramDomain.instance) {
        self.domain = domain
        self.assignedPrograms = []
    }
    
    func getAssignedPrograms(_ completion: @escaping(_ result: ResultObject<AssignedProgramModel>) -> ()) {
        let assignmentTypeId = (SharedAppResource.instance.loggedInUser?.typeId!)!
        if let player = SharedAppResource.instance.selectedPlayer, let playerId = player.id{
            let request = AssignedProgramRequestModel.init(requesterId: playerId, assignmentTypeId: assignmentTypeId, unitId: 1, pageIndex: self.pageIndex)
            self.domain.getAssingedProgramsFromAPI(router: NetworkRouter.getAssignedPrograms(request)) { (assignedPrograms) in
                if let assignedPrograms = assignedPrograms.assignedPrograms{
                    if(assignedPrograms.count > 0){
                        try! self.realm.write {
                            self.realm.add(assignedPrograms, update: Realm.UpdatePolicy.all)
                        }
                    }
                }
                if(assignedPrograms.total ?? 0 > (self.pageIndex * Constants.PAGE_SIZE)){
                    self.pageIndex += 1
                    completion(ResultObject(hasMoreRecords: true, result: assignedPrograms.assignedPrograms!, error: nil))
                }else{
                    completion(ResultObject(hasMoreRecords: false, result: assignedPrograms.assignedPrograms!, error: nil))
                }
            }
        }
    }
    
    public func getProgramScheduleFor(player: DependantModel, date selectedDate: Date? = nil, completion:@escaping(_ groupedPrograms: [GroupedScheduledPrograms] ) -> ()){
        let sheduledPrograms = self.domain.getProgramScheduleForDate(date: selectedDate, Player: player,program: selectedProgram)
        self.groupedPrograms = sheduledPrograms
        completion(self.groupedPrograms)
    }
    
    func getProgramStats(completion: @escaping(_ programProgress: ProgramStatsResponse?, _ error: Error?) -> ()){
         if let player = SharedAppResource.instance.selectedPlayer, let playerId = player.id{
            let request = ProgramStatsRequestModel.init(on: "", id: self.selectedProgram.id, requestId: playerId, assignmentTypeId: 2, unitId: 1)
            self.domain.getProgramStats(router: NetworkRouter.getProgramStats(request)) { (response, error) in
                if(error != nil){
                    completion(nil, error)
                }else{
                    completion(response, nil)
                }
            }
        }
    }
}

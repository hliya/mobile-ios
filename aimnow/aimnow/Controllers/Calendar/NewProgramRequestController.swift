//
//  NewProgramRequestController.swift
//  aimnow
//
//  Created by Isuru Ranasinghe on 4/2/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import FSCalendar
import aimnow_core
import SwiftyUserDefaults

class NewProgramRequestController: BaseViewController{

    @IBOutlet weak var  calendar: FSCalendar!
    @IBOutlet weak var lblProgram: UILabel!
    var viewModel: CalenderViewModel!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var viewBallType: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(viewModel == nil){
            viewModel = CalenderViewModel()
        }
        
        self.viewModel.selectedDate = Date()
                
        self.viewModel.getAvailableLocationsForProgram { (locations) in
            print(locations.map({$0.getFullAddress()}))
        }
        
        setupUI()
    }
    
    func setupUI() {
        let from = Date()
        let to = Date().adding(minutes: 60)
        self.txtFrom.text = from.toString(format: Constants.TIME_FORMAT)
        self.txtTo.text = to.toString(format: Constants.TIME_FORMAT)
        self.lblProgram.text = self.viewModel.selectedProgram.name
        
        if(self.viewModel.selectedProgram.typeId.value == 7){
            viewBallType.backgroundColor = UIColor.init(hexString:Ui.ColorCodesHex.Red)
        }else if(self.viewModel.selectedProgram.typeId.value == 8){
            viewBallType.backgroundColor = UIColor.init(hexString:Ui.ColorCodesHex.Orange)
        }else if(self.viewModel.selectedProgram.typeId.value == 9){
            viewBallType.backgroundColor = UIColor.init(hexString:Ui.ColorCodesHex.Green)
        }
        self.viewModel.startTime = from.toString(format: Constants.TIME_FORMAT_WITHOUT_AMPM)
        self.viewModel.endTime = to.toString(format: Constants.TIME_FORMAT_WITHOUT_AMPM)
        self.txtFrom.setInputViewDatePicker(pickerType: .time ,selectedDate: Date(),  target: self, selector: #selector(fromDateDone))
        self.txtTo.setInputViewDatePicker(pickerType: .time ,selectedDate: Date().adding(minutes: 60),  target: self, selector: #selector(toDateDone))
    }
    
    
    @objc func fromDateDone() {
        if let datePicker = self.txtFrom.inputView as? UIDatePicker {
            self.txtFrom.text = datePicker.date.toString(format: Constants.TIME_FORMAT)
            self.viewModel.startTime = datePicker.date.toString(format: Constants.TIME_FORMAT_WITHOUT_AMPM)
        }
        self.txtFrom.resignFirstResponder()
    }
    
    
    @objc func toDateDone() {
        if let datePicker = self.txtTo.inputView as? UIDatePicker {
            self.txtTo.text = datePicker.date.toString(format: Constants.TIME_FORMAT)
            self.viewModel.endTime = datePicker.date.toString(format: Constants.TIME_FORMAT_WITHOUT_AMPM)
        }
        self.txtTo.resignFirstResponder()
    }
    
    @IBAction func onClickRequestProgram(_ sender: UIButton) {
        guard self.viewModel.selectedDate != nil else{
            alert(message: "Please select the date")
            return
        }
        guard self.viewModel.startTime != nil else{
            alert(message: "Please select the time")
            return
        }
        guard self.sharedInstance.selectedPlayer != nil else{
            alert(message: "Please select the player")
            return
        }
        guard self.viewModel.selectedProgram != nil else{
            alert(message: "Please select the program wish to participate")
            return
        }
        guard SharedAppResource.instance.loggedInUser != nil else{
            alert(message: "Logged in user can not identify")
            return
        }
       HUDManager.shared.manageHUD(.loading())
        self.viewModel.requestNewProgram { (isSuccess, error) in
            HUDManager.shared.manageHUD(.dismiss)
            if(isSuccess){
                self.alertWithBoolResponse(message: "New program request successful") { (action) in
                    if(action){
                        self.dismiss(animated: true)
                    }
                }
            }else{
                self.alert(message: error ?? "Something went worng", title: "Error")
            }
        }
    }
    
    @IBAction func onClose(sender:UIButton) {
        self.dismiss(animated: true)
    }
}

extension NewProgramRequestController: FSCalendarDataSource, FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.viewModel.selectedDate = date
        
    }
}

//
//  MainViewController.swift
//
//  Created by Jon Kent on 11/12/15.
//  Copyright © 2015 Jon Kent. All rights reserved.
//

import SideMenu
import CoreData
import aimnow_core
import SwiftyUserDefaults
import RealmSwift

class MainViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource , UIWebViewDelegate{
    
    @IBOutlet weak var webviewCharts : UIWebView!
    @IBOutlet weak var collectionView : UICollectionView!
    var items:[[String:Any]] = [[:]]
    var viewModel: MainViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSideMenu()
        
        if(viewModel == nil){
            viewModel = MainViewModel()
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onConversationReceived(_:)),
                                               name: NSNotification.Name(rawValue: Constants.Notifications.multipartyChatReceived),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onConversationModified(_:)),
                                               name: NSNotification.Name(rawValue: Constants.Notifications.multipartyChatModified),
                                               object: nil)
        
        
        FirebaseHelper.instance.initiateMultipartyChatObserver()
        webviewCharts.delegate = self
        self.setupObservers()
        self.viewModel.getDependantUsers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let _ =  self.sharedInstance.selectedPlayer{
            loadAssignedPrograms()
        }
    }
    
    override func loadAssignedPrograms() {
        HUDManager.shared.manageHUD(.loading())
        self.viewModel.getAssignedPrograms { (result : ResultObject<AssignedProgramModel>) in
            if(result.hasMoreRecords){
                self.loadAssignedPrograms()
            }else{
                self.loadProgamList()
            }
        }
        if let url = Bundle.main.url(forResource: "index", withExtension: "html") {
            webviewCharts.loadRequest(URLRequest(url: url))
        }
    }
    
    override func refreshStats() {
        self.loadProgamList()
    }

    func loadProgamList(){
        self.viewModel.getPrograms(completion: {(programs) in
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.reloadData()
            HUDManager.shared.manageHUD(.dismiss)
        })
    }
    
    override func setupObservers() {
        Observers.instance.playerSelectObsrver.asObserver().subscribe(onNext: { (player) in
            if let player = player {
                self.sharedInstance.selectedPlayer = player
                self.viewModel.pageIndex = 1
                Utils.delay(0.5) {
                    self.setupHeader();
                    self.loadAssignedPrograms()
                }
            }else {
                self.alert(message: "no_dependent_players".localized)
            }
        }).disposed(by: disposeBag)
        
        Observers.instance.fcmTokenUpdate.subscribe(onNext: { (token) in
            if(!(token?.isBlank ?? true)){
                       UserDomain.instance.updateFirebaseToken(isFromRefresh: true)
                   }
        }).disposed(by: disposeBag)
    }
    
    
     // MARK: - UIWebViewDelegate protocol
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.viewModel.getProgramStats { (stats, error) in
            if(error == nil){
                (self.webviewCharts.stringByEvaluatingJavaScript(from:  String(format: "pushDataRing(%f,%f,%f)", stats?.total?.precTennisTime ?? 0, stats?.total?.precCompleted ?? 0, stats?.total?.precAttendace ?? 0)))
            }else{
                self.alert(message: error?.localizedDescription ?? "Something went wrong")
            }
        }
    }
    
    // MARK: - UICollectionViewDataSource protocol

    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if ( self.viewModel.programList.count == 0) {
            collectionView.setEmptyMessage("No data available!")
        } else {
            collectionView.restore()
        }
        return self.viewModel.programList.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ScheduleCollectionViewCell
        cell.set(program : self.viewModel.programList[indexPath.item])
        cell.onRequestNewProgramClosure = {
            let selectedProgram = self.viewModel.programList[indexPath.item]
            self.navigateToNewProgramRequest(program: selectedProgram)
        }
        cell.onChatClickClosure = {(program) in
            self.navigateToChat(program: program, selectedUser: SharedAppResource.instance.selectedPlayer)
        }
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedProgram = self.viewModel.programList[indexPath.item]
        if(selectedProgram.isEnrolled){
            SharedAppResource.instance.selectedProgram = selectedProgram
            self.navigateToProgramOverview(program: selectedProgram)
        }else{
            self.alert(message: "error_not_enroled".localized)
        }
        
    }
   
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.default.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        //SideMenuManager.default.menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: //"RightMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        // Set up a cool background image for demo purposes
        //SideMenuManager.default.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "background")!)
    
        SideMenuManager.default.menuWidth = view.frame.width * CGFloat(0.7)
    }
    
    @IBAction fileprivate func changeSwitch(_ switchControl: UISwitch) {
        SideMenuManager.default.menuFadeStatusBar = switchControl.isOn
    }

}

extension MainViewController: UISideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
    }
    
    func sideMenuDidAppear(menu: UISideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: UISideMenuNavigationController, animated: Bool) {
       // print("SideMenu Disappearing! (animated: \(animated))")
    }
    
    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
       // print("SideMenu Disappeared! (animated: \(animated))")
    }
    
}

extension MainViewController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        collectionView.reloadData()
    }
}

extension MainViewController {
    @objc func onConversationReceived(_ notification:Notification) {
        guard let conversation = notification.object as? ChatModel else {
            return
        }
        
        if let index = self.viewModel.programList.firstIndex(where: {"\($0.masterId)" == conversation.subTypeData.id}), (conversation.unReadMessageCount ?? 0 > 0){
            self.viewModel.programList[index].hasChatNotifications = true
//            collectionView.reloadItems(at: [IndexPath.init(row: index, section: 0)])
            collectionView.reloadData()
        }
    }
    
    @objc func onConversationModified(_ notification:Notification) {
        guard let conversation = notification.object as? ChatModel else {
            return
        }
        if let index = self.viewModel.programList.firstIndex(where: {"\($0.masterId)" == conversation.subTypeData.id}), (conversation.unReadMessageCount ?? 0 > 0){
            self.viewModel.programList[index].hasChatNotifications = true
//            collectionView.reloadItems(at: [IndexPath.init(row: index, section: 0)])
            collectionView.reloadData()
        }
    }
}

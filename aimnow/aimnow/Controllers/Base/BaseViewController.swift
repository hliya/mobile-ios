//
//  BaseViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/14/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import LocalAuthentication
import aimnow_core
import RxSwift
import SwiftyUserDefaults
import RealmSwift
import SkeletonView
import JGProgressHUD

class BaseViewController : UIViewController{
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var vwHeaderRight : HeaderRightView!;
    var data: Any?
    let sharedInstance = SharedAppResource.instance
    var selectedProgram: ProgramModel? = nil
    let realm = try! Realm()
    var disposeBag = DisposeBag()
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    let reuseIdentifierHeader = "headercell"
    let dateFormatter = DateFormatter()
    var hud = SharedAppResource.instance.hud
    var mainVM = MainViewModel()
    var viewType : E.ViewType = E.ViewType.none
    
    @IBAction func onBack(sender:UIBarButtonItem) -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        dateFormatter.dateFormat = Constants.DATE_FORMAT        
        setupObservers()
        MainViewModel.init().getNotificationSummary()
        Timer.scheduledTimer(withTimeInterval:TimeInterval(Constants.NOTIFICATION_INTERVAL), repeats: true) { timer in
            MainViewModel.init().getNotificationSummary()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) { 
        setupHeader() 
    }
    
    func setupHeader(){
        if self.vwHeaderRight != nil &&  SharedAppResource.instance.selectedPlayer  != nil{
            self.vwHeaderRight.set(player:  SharedAppResource.instance.selectedPlayer!)
            self.vwHeaderRight.setProgram(selectedProgram: SharedAppResource.instance.selectedProgram)
        }
    }
    
    func setupObservers(){
        Observers.instance.playerSelectObsrver.asObserver().subscribe(onNext: { (player) in
            if let selectedPlayer = player{
                self.sharedInstance.selectedPlayer = selectedPlayer
                FirebaseHelper.instance.initiateUpdateObserver(userId: selectedPlayer.id!)
                self.viewWillAppear(false)
            }
        }).disposed(by: disposeBag)
        
        Observers.instance.logoutObserver.asObserver().subscribe(onNext: { (clearData) in
            self.alertWithBoolResponse(message: "message_logout".localized,okActionTitle: "action_yes".localized, cancelActionTitle:"action_no".localized) { response in
                if(response){
                    if(clearData){
                        self.clearUserData()
                    }
                    self.navigateToLogin()
                }
            }
        }).disposed(by: disposeBag)
        
        let hudManager = HUDManager.shared
        hudManager.loadingIndicator
            .subscribe(onNext: { (hudType) in
                guard let hud = hudType else {
                    self.dismissHUD()
                    return
                }
                switch(hud) {
                case .success(_):
                    self.showSuccessHUD(hud)
                case .loading(_):
                    self.showLoadingHUD(hud)
                case .downloading(_):
                    self.showDownloadingHUD(hud)
                case .error(_):
                    self.showErrorHUD(hud)
                case .dismiss:
                    self.dismissHUD()
                }
            }).disposed(by: disposeBag)
        
        Observers.instance.errorMessageObserver.asObserver().subscribe(onNext: { (error) in
            HUDManager.shared.manageHUD(.dismiss)
            if(error?.code == 401){
                self.clearUserData()
            }
            self.alert(message: error?.error ?? "message_some_thing_whent_worng".localized, title: error?.title ?? "error".localized)
        }).disposed(by: disposeBag)
        
        Observers.instance.updateObserver.asObserver().subscribe(onNext: {(update) in
                                                                    switch(eUpdateType.init(rawValue: update.updateType ?? 0)){
                                                                    case .statistics:
                                                                        if(update.receiver == SharedAppResource.instance.selectedPlayer?.id){
                                                                            self.mainVM.pageIndex = 1
                                                                            self.loadAssignedPrograms()
                                                                        }
                                                                        break;
                                                                    default:
                                                                        break;
                                                                    }}).disposed(by: disposeBag)
        
        Observers.instance.onUserProfileUpdating.asObserver().subscribe(onNext: { (user) in
             
            SharedAppResource.instance.loggedInUser = user;
            
            Observers.instance.onUserProfileUpdated.onNext(user);
            
        }).disposed(by: disposeBag)
    }
    
    
    func loadAssignedPrograms(){
        self.mainVM.getAssignedPrograms { [self] (result : ResultObject<AssignedProgramModel>) in
            if(result.hasMoreRecords){
                self.loadAssignedPrograms()
            }else{
                refreshStats()
            }
        }
    }
    
    
    func refreshStats(){}
    
    private func clearUserData(){
        Defaults[\.token] = nil
        Defaults[\.isLoggedIn] = false
        Defaults[\.isDeviceRegistered] = false
        Defaults[\.userDetail] = nil
        Defaults[\.fcmTokenKey] = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func OnChatPressed(){
        navigateToChat(program: sharedInstance.selectedProgram, selectedUser: sharedInstance.selectedPlayer)
    }
    
    func showLoadingHUD(_ hudType: HUDType) {
        hud.textLabel.text = hudType.string()
        hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        hud.show(in: getRootVC().view)
    }
    
    func showSuccessHUD(_ hudType: HUDType) {
        hud.textLabel.text = hudType.string()
        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        hud.dismiss(afterDelay: 2.0)
        hud.show(in: getRootVC().view)
    }
    
    func showErrorHUD(_ hudType: HUDType) {
        hud.textLabel.text = hudType.string()
        hud.indicatorView = JGProgressHUDErrorIndicatorView()
        hud.show(in: getRootVC().view)
        hud.dismiss(afterDelay: 2.0)
    }
    
    func showDownloadingHUD(_ hudType: HUDType) {
        hud.vibrancyEnabled = true
        hud.indicatorView = JGProgressHUDPieIndicatorView()
        hud.detailTextLabel.text = "0% Complete"
        hud.textLabel.text = hudType.string()
        hud.show(in: getRootVC().view)
    }
    
    func dismissHUD() {
        DispatchQueue.main.async {
            self.hud.dismiss(animated: true)
        }
    }
    
    func getRootVC() -> UIViewController {
        let delegate = (UIApplication.shared.delegate) as! AppDelegate
        let vc = delegate.window?.rootViewController
        var topController = vc ?? self
        while let newTopController = topController.presentedViewController {
            topController = newTopController
        }
        return topController
    }
}

extension UIViewController{
    var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}

// Applicaton navigation

extension BaseViewController{
    func navigateToCalendarController(program: ProgramModel, groupedScheduledPrograms: [GroupedScheduledPrograms], shouldPresentModal: Bool = true) {
        let storyboardName = ParentConstants.Storyboards.Calendar
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let calendarVC = storyboard.instantiateViewController(withIdentifier: "CalenderViewController") as! CalenderViewController
        calendarVC.viewModel = CalenderViewModel()
        calendarVC.viewModel.selectedProgram = program
        calendarVC.viewModel.groupedScheduledPrograms = groupedScheduledPrograms
        if(shouldPresentModal){
            navigationController?.present(calendarVC, animated: true, completion: nil)
        }else{
            navigationController?.pushViewController(calendarVC, animated: true)
        }
    }
    
    func navigateToProgressView(program: ProgramModel, groupedScheduledPrograms: [GroupedScheduledPrograms]) {
        let storyboardName = ParentConstants.Storyboards.Progress
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let progressView = storyboard.instantiateViewController(withIdentifier: "ProgressViewController") as! ProgressViewController
        progressView.viewModel = ProgressViewModel()
        progressView.viewModel.selectedProgram = program
        progressView.viewModel.groupedScheduledPrograms = groupedScheduledPrograms
        navigationController?.pushViewController(progressView, animated: true)
    }
    
    func navigateToProgressDetail(program: ProgramModel, programProgressDetail: ProgramProgressDetail) {
        let storyboardName = ParentConstants.Storyboards.Progress
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let progressDetail = storyboard.instantiateViewController(withIdentifier: "ProgressDetailViewController") as! ProgressDetailViewController
        progressDetail.viewModel = ProgressViewModel()
        progressDetail.viewModel.selectedProgram = program
        progressDetail.viewModel.programProgressDetail = programProgressDetail
        navigationController?.pushViewController(progressDetail, animated: true)
    }
    
    func navigateToNewProgramRequest(program: ProgramModel){
        let storyboardName = ParentConstants.Storyboards.Calendar
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let newProgramRequest = storyboard.instantiateViewController(withIdentifier: "NewProgramRequestController") as! NewProgramRequestController
        newProgramRequest.viewModel = CalenderViewModel()
        newProgramRequest.viewModel.selectedProgram = program
        navigationController?.present(newProgramRequest, animated: true, completion: nil)
    }
    
    func navigateToProgramOverview(program: ProgramModel){
        let storyboardName = ParentConstants.Storyboards.Calendar
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let programOverview = storyboard.instantiateViewController(withIdentifier: "ProgramOverviewController") as! ProgramOverviewController
        programOverview.viewModel = CalenderViewModel()
        programOverview.viewModel.selectedProgram = program
        navigationController?.pushViewController(programOverview, animated: true)
    }
    
    func navigateToChat(program: ProgramModel?, selectedUser: DependantModel?) {
        let storyboardName = ParentConstants.Storyboards.Chat
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let ChatListViewController = storyboard.instantiateViewController(withIdentifier: "ChatListViewController") as! ChatListViewController
        ChatListViewController.viewModel = ChatListViewModel()
        ChatListViewController.viewModel.selectedProgram = program
        ChatListViewController.viewModel.user = selectedUser
        ChatListViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(ChatListViewController, animated: true)
    }
    
    func navigateToLogin(){
        self.navigationController?.isNavigationBarHidden = true
        let storyboardName = ParentConstants.Storyboards.Main
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let rootViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
        let navigation = UINavigationController.init(rootViewController: rootViewController)
        appDelegate.window?.rootViewController = navigation
    }
    
    
    func navigateToProfile(viewType : E.ViewType, dependant : DependantModel?) {
        let storyboardName = ParentConstants.Storyboards.Profile
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let ProfileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        ProfileViewController.viewModel = ProfileVM()
        ProfileViewController.viewType = viewType
        ProfileViewController.viewModel?.selectedDepenant = dependant
        ProfileViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(ProfileViewController, animated: true)
    }
}


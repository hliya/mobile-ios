//
//  ProgressViewModel.swift
//  aimnow
//
//  Created by Isuru Ranasinghe on 3/26/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import aimnow_core
import RealmSwift



class ProgressViewModel: NSObject {
    
    let realm = try! Realm()
    var selectedProgram: ProgramModel!
    var groupedScheduledPrograms: [GroupedScheduledPrograms] = []
    var domain: ProgramDomain!
    var programStagesInfo: ProgramStagesInfo?
    var programProgressDetail: ProgramProgressDetail?
    var filterBy :E.FilterMode = E.FilterMode.month
    var feedbacks: [FeedBack] = []
    var dataManager: DataManager?

    init(domain : ProgramDomain = ProgramDomain.instance) {
        self.domain = domain
        dataManager = DataManager.sharedInstance
    }
    
    
    func getProgramStatus(completion:@escaping(_ stageInfo: ProgramStagesInfo?) -> ()){
        self.domain.getProgramProgressStatus(player: SharedAppResource.instance.selectedPlayer!, program: selectedProgram) { (userStages) in
            completion(userStages.last)
        }
    }
    
    func getProgramProgress(currentStage:Int? = nil, completion: @escaping(_ programProgress: [ProgramProgressSummaryDate]) -> ()){
        self.domain.getProgramProgress(player: SharedAppResource.instance.selectedPlayer!, programId: selectedProgram.id, filterBy: filterBy) { (programProgress) in
            completion(programProgress)
        }
    }
    
    func getProgramStats(completion: @escaping(_ programProgress: ProgramStatsResponse?, _ error: Error?) -> ()){
         if let player = SharedAppResource.instance.selectedPlayer, let playerId = player.id{
            let request = ProgramStatsRequestModel.init(on: "", id: self.selectedProgram.id, requestId: playerId, assignmentTypeId: 2, unitId: 1)
            self.domain.getProgramStats(router: NetworkRouter.getProgramStats(request)) { (response, error) in
                if(error != nil){
                    completion(nil, error)
                }else{
                    completion(response, nil)
                }
            }
        }
    }
    
    public func getFeedbackDetails(request:FeedbackRequestModel,  completion:@escaping(_ result: [FeedBack]?) -> ()){
        let router = NetworkRouter.getFeedbackDetails(request)
        FeedbackDomain.instance.getFeedbacks(router: router) { (feedbacks, error) in
            if(error == nil){
                if let feedbackList = feedbacks{
                    self.feedbacks = feedbackList
                }
                completion(feedbacks)
            }else{
                Observers.instance.errorMessageObserver.onNext(errorMessage.init(title: error))
            }
        }
    }
}

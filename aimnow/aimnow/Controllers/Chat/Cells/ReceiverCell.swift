//
//  ReceiverCell.swift
//
//

import Foundation
import UIKit
import SwiftyUserDefaults
import aimnow_core

public class ReceiverCell: UICollectionViewCell {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgBubble: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    public func setMessageDetails(message: ChatMessageModel, nextMessage:ChatMessageModel?) -> Void {
//        self.imgBubble.tintColor = ThemeManager.currentTheme().chatBubbleReceiverColor
        self.imgProfile.isHidden = false
        self.imgProfile.setRound()
        self.lblMessage.text = message.message!
        self.lblMessage.font = self.lblMessage.font.withSize(CGFloat(12))

        if(message.sentDate?.isToday ?? false){
            self.lblTime.text = "Today ," + (message.sentDate?.toString(format: Constants.TIME_FORMAT_WITHOUT_AMPM))!
        }else{
            self.lblTime.text = message.sentDate?.toString(format: Constants.CHAT_DATE_DISPLAY_FORMAT)
        }
        
        if let firstName = message.senderName{
            self.lblName.text = String.init(format: "%@,",firstName)
        }
        let placeHolderImage = UIImage(named: "profile")!
        self.imgProfile.image = placeHolderImage
        if let profile = message.senderImage{
            self.imgProfile.downloaded(from: Utils.getRelativeUrlPath(path: profile));
        }else{
            self.imgProfile.image = UIImage.init(named: "profile")
        }
    }
}

//
//  ChatListViewController.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/19/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core
import RxCocoa
import RxSwift

class ChatListViewController: BaseViewController {
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var cvUserList: UICollectionView!
    @IBOutlet var segmentController: UISegmentedControl!

    let kCellEligibleUser = "GlobleChatUserViewCell"
    let kSearchUserCell = "SearchUserCell"
    
    var viewModel: ChatListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (self.viewModel == nil) {
            self.viewModel = ChatListViewModel()
        }
//        self.viewModel.loadingObservable.bind { (shouldShow) in
//            if(shouldShow){
//                self.showProgress()
//            }else {
//                self.dismissProgress {}
//            }
//        }.disposed(by: disposeBag)
        
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.systemOrange])
        
        segmentController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.systemOrange], for: UIControl.State.selected)
        segmentController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.systemOrange], for: UIControl.State.normal)

        
        self.setupChatSearchTextField()
        self.setuprCollectionEvent()
        self.cvUserList.rx.setDelegate(self).disposed(by: disposeBag)
        
        if let bundle = Bundle.init(identifier: Constants.BundleIdentifiers.CORE){
            self.cvUserList?.register(UINib(nibName: self.kCellEligibleUser, bundle: bundle), forCellWithReuseIdentifier: kCellEligibleUser)
            self.cvUserList?.register(UINib(nibName: self.kSearchUserCell, bundle: bundle), forCellWithReuseIdentifier: kSearchUserCell)
        }
        self.viewModel.getEligibleUserList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        switch self.viewModel.selectedSection {
            case .conversations:
                self.setupConversationCollectionView()
                break
            case .users:
                self.setupUserCollectionView()
            break
        }
        
        // register child notification
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onConversationReceived(_:)),
                                               name: NSNotification.Name(rawValue: Constants.Notifications.multipartyChatReceived),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onConversationModified(_:)),
                                               name: NSNotification.Name(rawValue: Constants.Notifications.multipartyChatModified),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onUserAvailabilityChanged(_:)),
                                               name: NSNotification.Name(rawValue: Constants.Notifications.userAvailable),
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func onNewChat(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: ParentConstants.Storyboards.Chat, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ContactsViewController") as! ContactsViewController
        controller.viewModel = ChatListViewModel()
        controller.viewModel.selectedProgram = self.viewModel.selectedProgram
        controller.delegate = self
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
    
    // MARK:- Private Methods
    func setupChatSearchTextField () {
        self.txtSearch.rx.text.changed
            .asDriver()
            .debounce(.seconds(1))
            .drive(onNext:{ (query) in
                switch (self.viewModel.selectedSection){
                    case .users:
                        self.viewModel.filteredUserRelay.accept(self.viewModel.originalUserRelay.value)
                        if((query?.count ?? 0) > 0){
                            let filterData = self.viewModel.filteredUserRelay.value.filter{($0.displayName?.lowercased().contains(query?.lowercased() ?? ""))!}
                            self.viewModel.filteredUserRelay.accept(filterData)
                        }
                        break;
                    case .conversations:
                        self.viewModel.filteredConversationRelay.accept(self.viewModel.conversationRelay.value)
                        if((query?.count ?? 0) > 0){
                            let filterData = self.viewModel.filteredConversationRelay.value.filter{
                                if let conversationName = $0.conversationName{
                                    return (
                                        conversationName.lowercased().contains(query?.lowercased() ?? "")
                                    )
                                }else if let senderName = $0.senderName{
                                    return (
                                        senderName.lowercased().contains(query?.lowercased() ?? "")
                                    )
                                }else if let receiverName = $0.receiverName{
                                    return (
                                        receiverName.lowercased().contains(query?.lowercased() ?? "")
                                    )
                                }
                                else{
                                    return false
                                }
                            }
                            self.viewModel.filteredConversationRelay.accept(filterData)
                        }
                        break;
                }
            }).disposed(by: disposeBag)
    }
    
    func setupUserCollectionView() {
        cvUserList.dataSource = nil
        self.viewModel.filteredUserRelay.asObservable()
            .bind(to: cvUserList.rx.items(cellIdentifier: self.kSearchUserCell, cellType: SearchUserCell.self)) { (row, element, cell) in
                cell.setChatUser(user: element)
                cell.onChatUserChatObservr.subscribe(onNext: { (user) in
                    self.chat(user: user)
                }).disposed(by: cell.disposeBag)
                cell.layoutIfNeeded()
            }
            .disposed(by: disposeBag)
    }
    
    func setupConversationCollectionView() {
        cvUserList.dataSource = nil
        self.viewModel.filteredConversationObservable
            .bind(to: cvUserList.rx.items(cellIdentifier: self.kCellEligibleUser, cellType: GlobleChatUserViewCell.self)) { (row, element, cell) in
                cell.setConversationDetails(conversation: element)
                cell.layoutIfNeeded()
            }
            .disposed(by: disposeBag)
        
        var conversations:[ChatModel] = [];
        if let masterId = self.viewModel.selectedProgram?.masterId{
            conversations = SharedAppResource.instance.multipartyChats.filter({$0.subTypeData.id == "\(masterId)"}).sorted(by: { (conversation1, conversation2) -> Bool in
                return conversation1.unReadMessageCount ?? 0 > conversation2.unReadMessageCount ?? 0
            })
        }else{
            conversations = SharedAppResource.instance.multipartyChats.sorted(by: { (conversation1, conversation2) -> Bool in
                return conversation1.unReadMessageCount ?? 0 > conversation2.unReadMessageCount ?? 0
            })
        }
        self.viewModel.conversationRelay.accept(conversations)
        self.viewModel.filteredConversationRelay.accept(conversations)
    }
    
    fileprivate func setuprCollectionEvent(){
        cvUserList.rx.itemSelected.subscribe(onNext: { (index) in
            switch self.viewModel.selectedSection{
            case .conversations:
                let conversation = self.viewModel.filteredConversationRelay.value[index.row]
                let storyboard = UIStoryboard(name: ParentConstants.Storyboards.Chat, bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "ChatDetailViewController") as! ChatDetailViewController
                controller.viewModel = ChatDetailViewModel()
                controller.delegate = self
                controller.viewModel.conversation = conversation
                controller.viewModel.program = self.viewModel.selectedProgram
                let sender = ChatUsers.init()
                if let user = SharedAppResource.instance.loggedInUser{
                    sender.firstName = user.firstName
                    sender.lastName = user.lastName
                    sender.userId = user.id
                    sender.profileImageUrl = user.profileImage
                }
                controller.viewModel.senderUser = sender
                self.navigationController?.pushViewController(controller, animated: true)
                break;
            case .users:
                 self.txtSearch.becomeFirstResponder()
                let user = self.viewModel.filteredUserRelay.value[index.row]
                self.chat(user: user)
                break;
            }
        }).disposed(by: disposeBag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClickSegment(_ sender: UISegmentedControl) {
        self.txtSearch.text = ""
        switch sender.selectedSegmentIndex {
        case 0:
            self.viewModel.selectedSection = eMultipartyChatSelection.conversations
             self.setupConversationCollectionView()
            break
        case 1:
            self.viewModel.selectedSection = eMultipartyChatSelection.users
            self.setupUserCollectionView()
            break
        default:
            break
        }
    }
    
    fileprivate func chat(user: ChatUsers, senderUser: ChatUsers? = nil) {
        let storyboard = UIStoryboard(name: ParentConstants.Storyboards.Chat, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChatDetailViewController") as! ChatDetailViewController
        controller.viewModel = ChatDetailViewModel()
        controller.delegate = self
        controller.viewModel.receivers.append(user)
        controller.viewModel.program = self.viewModel.selectedProgram

        let sender = ChatUsers.init()
        if let user = SharedAppResource.instance.loggedInUser{
            sender.firstName = user.firstName
            sender.lastName = user.lastName
            sender.userId = user.id
            sender.profileImageUrl = user.profileImage
        }
        controller.viewModel.senderUser = sender


        self.viewModel.getChatWithSpecificUser(userId: user.userId!, programId: "\(self.viewModel.selectedProgram?.masterId ?? nil)", completion: { (conversation) in
            if let chat = conversation{
                controller.viewModel.conversation = chat
                self.navigationController?.pushViewController(controller, animated: true)
            }else{
                self.navigationController?.pushViewController(controller, animated: true)
            }
        })
    }
}

// MARK:- UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout

extension ChatListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch self.viewModel.selectedSection {
        case .conversations:
            return CGSize.init(width: (self.cvUserList.bounds.size.width - 32), height: CGFloat(70))
        case .users:
            return CGSize.init(width: (self.cvUserList.bounds.size.width - 32), height: CGFloat(70))
        }
    }
}

extension ChatListViewController: ChatUserDelegate, ContactDelegate{
    @objc func onConversationReceived(_ notification:Notification) {
        guard let conversation = notification.object as? ChatModel else {
            return
        }
        self.viewModel.conversationRelay.append(conversation)
        self.viewModel.filteredConversationRelay.accept(self.viewModel.conversationRelay.value)
    }
    
    @objc func onConversationModified(_ notification:Notification) {
        guard let conversation = notification.object as? ChatModel else {
            return
        }
        if let index = self.viewModel.conversationRelay.value.firstIndex(where: {$0.conversationId == conversation.conversationId}){
            self.viewModel.conversationRelay.remove(at: index)
            self.viewModel.conversationRelay.insert(conversation, at: 0)
            self.viewModel.filteredConversationRelay.accept(self.viewModel.conversationRelay.value)
        }
    }
    
    @objc func onUserAvailabilityChanged(_ notification:Notification) {
        guard let availability = notification.object as? Bool else {
            return
        }
        
        if let userId = (notification.userInfo)!["userId"] as? String{
            if let position = self.viewModel.originalUserRelay.value.firstIndex(where: {$0.userId == userId}){
                self.viewModel.originalUserRelay.value[position].isAvailable = availability
            }
            if let position = self.viewModel.filteredUserRelay.value.firstIndex(where: {$0.userId == userId}){
                self.viewModel.filteredUserRelay.value[position].isAvailable = availability
            }
        }
    }
    
    func onLeaveConversation(conversation: ChatModel) {
        if let index = self.viewModel.conversationRelay.value.firstIndex(where: {$0.conversationId == conversation.conversationId}){
            self.viewModel.conversationRelay.remove(at: index)
            self.viewModel.filteredConversationRelay.accept(self.viewModel.conversationRelay.value)
        }
    }
    
    func onSelectContact(chatUser: ChatUsers, senderUser: ChatUsers?) {
        self.chat(user: chatUser)
    }
}

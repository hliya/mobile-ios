//
//  ChatListViewModel.swift
//  parent
//
//  Created by Isuru Ranasinghe on 6/28/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import aimnow_core
import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

class ChatListViewModel: NSObject{
    
    var selectedProgram: ProgramModel?
    var user: DependantModel?
    var chats:[ChatMessageModel] = []
    var originalUserList: [ChatUsers] = []
    var originalUserRelay = BehaviorRelay<[ChatUsers]>.init(value: [])
    var filteredUserRelay = BehaviorRelay<[ChatUsers]>.init(value: [])
    var currentPage = 1
    var hasMoreRecords:Bool = true
    var conversationRelay  = BehaviorRelay<[ChatModel]>.init(value: [])
    var filteredConversationRelay  = BehaviorRelay<[ChatModel]>.init(value: [])
    var senderUser: ChatUsers?
    var selectedSection: eMultipartyChatSelection = eMultipartyChatSelection.conversations
    var selectedType : eUserFilter = .parent
    let searchFilter = PublishSubject<String>()
    var originalUserObservable : Observable<[ChatUsers]> {
        return originalUserRelay.asObservable()
    }
    
    var filteredUserObservable : Observable<[ChatUsers]> {
        return filteredUserRelay.asObservable()
    }
    
    var conversationObservable : Observable<[ChatModel]> {
        return conversationRelay.asObservable()
    }
    
    var filteredConversationObservable : Observable<[ChatModel]> {
        return filteredConversationRelay.asObservable()
    }
    
    override init() {
    }
    
    func getEligibleUserList() {
        var users = [ChatUsers]()
        HUDManager.shared.manageHUD(.loading())
        if let requester = SharedAppResource.instance.loggedInUser, let requesterId = requester.id{
            let request = ProgramUsersRequestModel.init(unitId: 1, assignmentTypeId: 2, requesterId: requesterId)
            ProgramDomain.instance.getProgramUsers(router: NetworkRouter.getProgramUsers(request)) { (chatUsers, error) in
                HUDManager.shared.manageHUD(.dismiss)
                if(error == nil){
                    self.senderUser = chatUsers.map({$0.getChatUser()}).first(where:{$0.userId == SharedAppResource.instance.loggedInUser?.id})
                    users = chatUsers.map({$0.getChatUser()}).filter({$0.userId != SharedAppResource.instance.loggedInUser?.id})
                    self.originalUserRelay.removeAll()
                    self.filteredUserRelay.removeAll()
                    for user in users{
                        FirebaseHelper.instance.initiateUserAvailability(userId: user.userId!)
                        self.originalUserRelay.append(user)
                    }
                    self.filterUsers(filter: self.selectedType)
                    SharedAppResource.instance.eligibleChatUsers = users
                }
            }
        }
    }
    
    func getChatWithSpecificUser(userId: String, programId: String? = nil, completion:@escaping(_ conversation:ChatModel?) ->()){
        let userConversation = SharedAppResource.instance.multipartyChats
        if let programId = programId {
            if let conversation = userConversation.first(where: {$0.receiver == userId && $0.isPrivate == true && $0.subTypeData.id == programId}){
                completion(conversation)
            }else{
                completion(nil)
            }
        }else{
            if let conversation = userConversation.first(where: {$0.receiver == userId && $0.isPrivate == true}){
                completion(conversation)
            }else{
                completion(nil)
            }
        }
    }
    
    func filterUsers(filter: eUserFilter) {
        switch filter {
        case .parent:
            let users = SharedAppResource.instance.eligibleChatUsers.filter({$0.typeId == 2})
            self.filteredUserRelay.accept(users)
        case .coach:
            let users = SharedAppResource.instance.eligibleChatUsers.filter({$0.typeId == 4 || $0.typeId == 5})
            self.filteredUserRelay.accept(users)
        }
    }
}


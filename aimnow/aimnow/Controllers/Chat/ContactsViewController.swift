//
//  ContactsViewController.swift
//  parent
//
//  Created by Isuru Ranasinghe on 7/14/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import aimnow_core

import UIKit
import aimnow_core

protocol ContactDelegate {
    func onSelectContact(chatUser:ChatUsers, senderUser: ChatUsers?)
}

class ContactsViewController: BaseViewController {
    
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var cvUserList: UICollectionView!
    @IBOutlet var segmentController: UISegmentedControl!
    
    var delegate: ContactDelegate?
    
    let kSearchUserCell = "SearchUserCell"
    var viewModel: ChatListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (self.viewModel == nil) {
            self.viewModel = ChatListViewModel()
        }
        
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.systemOrange])
        
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentController.setTitleTextAttributes(titleTextAttributes, for: .normal)
        segmentController.setTitleTextAttributes(titleTextAttributes, for: .selected)
        
        self.setupChatSearchTextField()
        self.setuprCollectionEvent()
        self.cvUserList.rx.setDelegate(self).disposed(by: disposeBag)
        
        if let bundle = Bundle.init(identifier: Constants.BundleIdentifiers.CORE){
            self.cvUserList?.register(UINib(nibName: self.kSearchUserCell, bundle: bundle), forCellWithReuseIdentifier: kSearchUserCell)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupUserCollectionView()
        
        // register child notification
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onUserAvailabilityChanged(_:)),
                                               name: NSNotification.Name(rawValue: Constants.Notifications.userAvailable),
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClickClose(_ sender: UIButton) {
          dismiss(animated: true, completion: nil)
      }
    
    @IBAction func onClickSegment(_ sender: UISegmentedControl) {
        if(sender.selectedSegmentIndex == 0){
            self.viewModel.filterUsers(filter: .parent)
        }else{
            self.viewModel.filterUsers(filter: .coach)
        }
    }
    
    // MARK:- Private Methods
    func setupChatSearchTextField () {
        self.txtSearch.rx.text.changed
            .asDriver()
            .debounce(.seconds(1))
            .drive(onNext:{ (query) in
                self.viewModel.filteredUserRelay.accept(self.viewModel.originalUserRelay.value)
                if((query?.count ?? 0) > 0){
                    let filterData = self.viewModel.filteredUserRelay.value.filter{($0.displayName?.lowercased().contains(query?.lowercased() ?? ""))!}
                    self.viewModel.filteredUserRelay.accept(filterData)
                }
            }).disposed(by: disposeBag)
    }
    
    func setupUserCollectionView() {
        cvUserList.dataSource = nil
        self.viewModel.filteredUserObservable
            .bind(to: cvUserList.rx.items(cellIdentifier: self.kSearchUserCell, cellType: SearchUserCell.self)) { (row, element, cell) in
                cell.setChatUser(user: element)
                cell.onChatUserChatObservr.subscribe(onNext: { (user) in
                    self.chat(user: user)
                }).disposed(by: cell.disposeBag)
                cell.layoutIfNeeded()
        }
        .disposed(by: disposeBag)
        self.viewModel.getEligibleUserList()
    }
    
    fileprivate func setuprCollectionEvent(){
        cvUserList.rx.itemSelected.subscribe(onNext: { (index) in
            self.txtSearch.becomeFirstResponder()
            let user = self.viewModel.filteredUserRelay.value[index.row]
            self.chat(user: user)
        }).disposed(by: disposeBag)
    }
    
    fileprivate func chat(user: ChatUsers) {
        dismiss(animated: true) {
            if let delegate = self.delegate{
                delegate.onSelectContact(chatUser: user, senderUser: self.viewModel.senderUser)
            }
        }
        
    }
}

extension ContactsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch self.viewModel.selectedSection {
        case .conversations:
            return CGSize.init(width: (self.cvUserList.bounds.size.width - 32), height: CGFloat(70))
        case .users:
            return CGSize.init(width: (self.cvUserList.bounds.size.width - 32), height: CGFloat(70))
        }
    }
}

extension ContactsViewController: ChatUserDelegate{
    @objc func onUserAvailabilityChanged(_ notification:Notification) {
        guard let availability = notification.object as? Bool else {
            return
        }
        
        if let userId = (notification.userInfo)!["userId"] as? String{
            if let position = self.viewModel.originalUserRelay.value.firstIndex(where: {$0.userId == userId}){
                self.viewModel.originalUserRelay.value[position].isAvailable = availability
            }
            if let position = self.viewModel.filteredUserRelay.value.firstIndex(where: {$0.userId == userId}){
                self.viewModel.filteredUserRelay.value[position].isAvailable = availability
            }
        }
    }
}

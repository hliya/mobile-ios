//
//  Utils.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/11/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AlamofireImage

public class Utils: NSObject {
    
    static func presentUniversalAlertController(controller: UIViewController?, alertController: UIAlertController, sourceView: UIView? = nil, animated: Bool = true, completion: (() -> Void)? = nil){
        if UIDevice.current.userInterfaceIdiom == .pad {
            if sourceView != nil {
                alertController.popoverPresentationController?.sourceView = sourceView
                alertController.popoverPresentationController?.permittedArrowDirections = .any
            }
        }
        controller?.present(alertController, animated: animated, completion: completion)
    }
    
    static func documentDirectory() -> String {
        let docDir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        return docDir
    }
    
    static func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    static func compressVideo(videoPath:URL, completion:@escaping (_ videoPath: URL?) ->()) {
        let destinationPath = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("compressed.mp4")
        try? FileManager.default.removeItem(at: destinationPath)
        _ = compressh264VideoInBackground(
            videoToCompress: videoPath,
            destinationPath: destinationPath,
            size: nil ,
            compressionTransform: .keepSame,
            compressionConfig: .defaultConfig,
            completionHandler: { path in
                completion(path)
            },
            errorHandler: { e in
                print("Error: ", e)
        },
            cancelHandler: {
                print("Cancel")
        }
        )
    }
    
    static func compressImage(image:UIImage) -> Data {
        // Reducing file size to a 10th
        var actualHeight    : CGFloat = image.size.height
        var actualWidth     : CGFloat = image.size.width
        let maxHeight       : CGFloat = 1136.0
        let maxWidth        : CGFloat = 640.0
        var imgRatio        : CGFloat = actualWidth/actualHeight
        let maxRatio        : CGFloat = maxWidth/maxHeight
        let compressionQuality : CGFloat = (actualHeight < maxHeight) ? 1.0: 0.50
        
        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }else if(imgRatio > maxRatio){
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = maxWidth;
            }else{
                actualHeight = maxHeight;
                actualWidth = maxWidth;
            }
        }
        let rect = CGRect.init(x: 0, y: 0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size);
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext();
        let imageData = img!.jpegData(compressionQuality: compressionQuality);
        UIGraphicsEndImageContext();
        return imageData!;
    }
    
    static func sizeForLabel(_ text:String, font:UIFont, numberOfLines: Int = 0, width:CGFloat = CGFloat.greatestFiniteMagnitude) -> CGRect
    {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = numberOfLines
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame
    }
    
    static func getImageWithoutCach(url: URL?, imageView: UIImageView?, completion:((_ image: UIImage) ->())?){
        guard let nsurl = url else { return }
        var imgView = imageView
        let urlRequest = URLRequest(url: nsurl, cachePolicy: .reloadIgnoringCacheData)
        
        let imageDownloader = ImageDownloader.default
        if let imageCache = imageDownloader.imageCache as? AutoPurgingImageCache, let urlCache = imageDownloader.session.session.configuration.urlCache {
            _ = imageCache.removeImages(matching: urlRequest)
            urlCache.removeCachedResponse(for: urlRequest)
        }
        if(imgView == nil){
            imgView = UIImageView.init()
        }
        imgView!.af_setImage(withURL: urlRequest.url!) { (response) in
            if let completion = completion{
                completion(response.value!)
            }
        }
    }
}

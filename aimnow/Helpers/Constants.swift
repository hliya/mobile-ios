//
//  Constants.swift
//  aimnow
//
//  Created by Isuru Ranasinghe on 3/25/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

class ParentConstants{
    
    struct Storyboards {
        static let Main                             = "Main"
        static let Calendar                         = "Calendar"
        static let Schedule                         = "Schedule"
        static let Progress                         = "Progress"
        static let Authentication                   = "Authentication"
        static let Chat                             = "Chat"
    }
    
}

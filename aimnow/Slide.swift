//
//  Slide.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/10/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import Foundation

import UIKit

class Slide: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelTitle: UITextView!
    @IBOutlet weak var labelDesc: UITextView!
    
}

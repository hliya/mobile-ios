//
//  HUDManager.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 10/18/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class HUDManager {
    public static let shared = HUDManager()

    public let progress = BehaviorSubject<Float?>(value: nil)
    public let loadingIndicator = PublishSubject<HUDType?>.init()

    public func manageHUD(_ hud: HUDType) {
        loadingIndicator.onNext(hud)
    }

    public func setProgress(_ value: Float?) {
        progress.onNext(value)
    }
}

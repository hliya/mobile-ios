//
//  Enums.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/9/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public struct Enums {
    public enum eMasterSync: Int, CaseIterable {
        case Assigment = 1
        case Attachment = 2
        case Badge = 3
        case Config = 4
        case Device = 5
        case Feedback = 6
        case Notification = 7
        case Program = 8
        case Tempate = 9
        case Unit = 10
        case User = 11
        case Status = 12
    }
    
    public enum eActions: Int {
        case Add
        case Remove
    }
    
    public enum eStatus: Int {
        case InProgress = 1
        case OnHOld
        case Completed
        case NotStarted
        
        public var description: String {
        switch(self) {
        case .InProgress:
            return "In Progress"
        case .OnHOld:
            return "On Hold"
        case .Completed:
            return "Completed"
        case .NotStarted:
            return "Not Started"
        }
        }
    }
    
    public enum eUserType: Int {
        case Admin = 1
        case Parent
        case Child
        case HeadCoach
        case AssistantCoach
        
        public var description: String {
        switch(self) {
        case .Admin:
            return "Administrator"
        case .Parent:
            return "Parent"
        case .Child:
            return "Child"
        case .HeadCoach:
            return "Head Coach"
        case .AssistantCoach:
            return "Assistant Coach"
        }
        }
    }
    
    public enum eChatUserState:Int {
        case Joined = 1;
        case Owner = 2;
        case None = 3
    }
    
    public enum eAttachmentFileTypes:Int {
        case Images = 1
        case Videos = 2
        
        static func fileType(for mimeType: String) -> eAttachmentFileTypes {
            let type = mimeType.split(separator: "/").first?.lowercased()
            if(type == "image"){
                return .Images
            }else{
                return .Videos
            }
        }
    }
    
    public enum eCellPossition: Int {
        case first
        case middle
        case last
    }
}

public enum eUpdateType: Int {
    case attendance = 1
    case statistics
}


public enum eMultipartyChatSelection: Int{
    case users = 1
    case conversations = 2
}


public enum eConversationSubType:Int{
    case program = 1
    case global = 2
    
    public var description : String {
        switch self {
        case .program: return "program"
        case .global: return "global"
        }
    }
}

public enum eDeviceType: Int {
    case iOS = 1
    case Android = 2
}

public enum eUserFilter: Int {
    case parent
    case coach
}

public enum eProfileHeader: Int {
    case Experience = 1
    case Qualification
    case CoachingExp
    
    public var description: String {
    switch(self) {
    case .Experience:
        return "Experience"
    case .Qualification:
        return "Education"
    case .CoachingExp:
        return "Other"
    }
    }
}

public enum eNotificationTemplates: Int {
    case ProgramScheduledOnDay = 1
    case ProgramScheduledPriorDay
    case ProgramScheduledPriorTime
    case ProgramIsCancelded
    case ProgramIsStarted
    case AttendanceRecord
    case NotParticipated
    case NotOnTime
    case ProgramSchedulCompletionPriorTime
    case Chat
}

public enum DataLoadingStatus {
    case initial
    case pullHeader
    case pullFooter
    case endRecord
    case noData
}

public enum HUDType {
    case loading(String = "loading")
    case success(String = "success")
    case downloading(String = "downloading")
    case error(String = "error")
    case dismiss

    public init() {
        self = .loading()
    }

    public func string() -> String {
        switch (self) {
            case .loading(let message), .success(let message), .downloading(let message), .error(let message):
                return message.localized
            default:
                return ""
        }
    }
}

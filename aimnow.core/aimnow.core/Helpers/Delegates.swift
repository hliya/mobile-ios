//
//  Delegates.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/9/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public protocol MasterSyncDelegate {
    func onStart ()
    func onNext (api:String, progress:String)
    func onComplete ()
}

public protocol CaseIterable {
    associatedtype AllCases: Collection where AllCases.Element == Self
    static var allCases: AllCases { get }
}
extension CaseIterable where Self: Hashable {
    public static var allCases: [Self] {
        return [Self](AnySequence { () -> AnyIterator<Self> in
            var raw = 0
            var first: Self?
            return AnyIterator {
                let current = withUnsafeBytes(of: &raw) { $0.load(as: Self.self) }
                if raw == 0 {
                    first = current
                } else if current == first {
                    return nil
                }
                raw += 1
                return current
            }
        })
    }
}

public protocol ChatUserDelegate {
    func onAddUser(chatUser:ChatUsers)
    func onRemoveUser(chatUser:ChatUsers)
    func onLeaveUser(chatUser:ChatUsers)
    func onLeaveConversation(conversation: ChatModel)
}

public extension ChatUserDelegate {
    func onAddUser(chatUser:ChatUsers){}
    func onRemoveUser(chatUser:ChatUsers){}
    func onLeaveUser(chatUser:ChatUsers){}
    func onLeaveConversation(conversation: ChatModel){}
}


public protocol ManageUserDelegate {
    func onCreateGroupChat(conversation: ChatModel)
    func onUserLeaveGroup()
    func updateUserCount(user: [ChatUsers])
}

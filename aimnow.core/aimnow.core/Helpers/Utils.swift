//
//  Utils.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/22/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

public class Utils: NSObject {
    
    static func presentUniversalAlertController(controller: UIViewController?, alertController: UIAlertController, sourceView: UIView? = nil, animated: Bool = true, completion: (() -> Void)? = nil){
        if UIDevice.current.userInterfaceIdiom == .pad {
            if sourceView != nil {
                alertController.popoverPresentationController?.sourceView = sourceView
                alertController.popoverPresentationController?.permittedArrowDirections = .any
            }
        }
        controller?.present(alertController, animated: animated, completion: completion)
    }
    
    static func documentDirectory() -> String {
        let docDir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        return docDir
    }
    
    static func findDateDiff(time1Str: String, time2Str: String, completion: @escaping(_ readable: String,_ minutes: Double) ->()) {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "HH:mm:ss"

        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return}

        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        let intervalInt = Int(interval)
        let readable = "\(intervalInt < 0 ? "-" : "+") \(Int(hour)) Hours \(Int(minute)) Minutes"
        completion(readable, interval/60)
    }
    
    static func getCurrentTimeStamp() -> Int {
        return Int(Date().timeIntervalSince1970)
    }
    
    static func getThumbForVideo(videoPath: URL, completion: @escaping(_ image: UIImage) -> ()){
        DispatchQueue.global(qos: .background).async {
            do {
                let asset = AVURLAsset(url: videoPath , options: nil)
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                let thumbnail = UIImage(cgImage: cgImage)
                DispatchQueue.main.async {
                    completion(thumbnail)
                }
            } catch let error {
                print("Error generating thumbnail: \(error.localizedDescription)")
            }
        }
    }
    
    static func getRelativeUrlPath(path:String) -> String {
        return Constants.BASE_API_URL + path.replacingOccurrences(of: "\\", with: "/")
    }
}


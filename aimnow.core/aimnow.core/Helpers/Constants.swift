//
//  Constants.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/5/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation


public struct Constants {

    public static let MAXIMUM_CONNECTION                   = 20
    public static let TIME_OUT_INTERVAL                    = 360.0
    public static let RETRY_COUNT                          = 4
    public static let DATE_FORMAT                          = "yyyy-MM-dd"
    public static let TIME_FORMAT                          = "hh:mm a"
    public static let TIME_FORMAT_WITHOUT_AMPM             = "HH:mm"
    public static let SERVER_DATE_FORMAT                   = "yyyy-MM-dd'T'HH:mm:ss"
    public static let NULL_DATE                            = "01/01/0001 00:00:00"
    public static let CHAT_DATE_DISPLAY_FORMAT             = "dd/MM/yyyy HH:mm a"
    public static let SERVER_DATE_FORMAT_WITH_ZONE         = "yyyy-MM-dd'T'HH:mm:ss.SSSS"

    public static let BASE_API_URL                         = "https://anow.azurewebsites.net/"
    public static let PAGE_SIZE                            = 200
    public static let IMAGE_EXTENTION                      = ".png"
    public static let VIDEO_EXTENTION                      = ".mp4"
    
    
    public static let authentication                       = "api/account/authenticate"
    public static let signUp                               = "api/account/register"
    public static let getAllPrograms                       = "api/program/all"
    public static let getAssignedPrograms                  = "api/program/assigned"
    public static let getProgramStats                      = "api/program/stats"
    public static let getAllDependentUsers                 = "api/user/dependant/%@/%@"
    public static let getAllDependentUser                  = "api/user/dependant/%@/%@/%@"
    public static let getMasterSync                        = "api/master/type"
    public static let requestNewProgram                    = "api/program/request"
    public static let getDetailedAssignedPrograms          = "api/program/assigned/pool"
    public static let playerAttendanceRecording            = "api/program/assigned/upsert/attendace"
    public static let updateAssesmentStats                 = "api/Program/assigned/upsert/statistics"
    public static let upsertFeedBack                       = "api/Program/assigned/upsert/feedback"
    public static let upsertAttachment                     = "api/Program/assigned/upsert/attachment"
    public static let getFeedbackDetails                   = "api/Program/feedbacks"
    public static let getProgramOverallSummary             = "api/Program/stats/agg"
    public static let registerDevice                       = "api/Account/registerdevice"
    public static let deleteDeviceRegistration             = "api/Account/unRegisterdevice"
    public static let programUsers                         = "api/Program/users"
    public static let profile                              = "api/User/profile/%@"
    public static let notifications                        = "api/Notification/all"
    public static let updateNotifications                  = "api/Notification/noti/upsert/hasseen"
    public static let postNotifications                    = "api/Notification/noti/upsert"
    public static let notificationSummary                  = "/api/notification/all/summary"
    public static let getFeedbackStatistics                = "api/Program/feedbacks/stats"
    public static let updateUserProfile                    = "api/user/profile/update"

    // Master sync
    public static let getMasterTypes                       = "api/master/type"
    public static let getLocation                          = "api/master/location"
    public static let getMasterSyncLog                     = "api/master/synclog"
    
    public struct Notifications {
        public static let chatReceived = "%@_chatReceived"
        public static let chatUpdated = "%@_chatUpdated"
        public static let chatDownloaded = "%@_chatDownloaded"
        public static let ProviderLocationChanged = "%@_Changed"
        public static let ChangeProfilePicture = "ChangeProfilePicture"
        public static let notificationCountChanged = "notificationCountChanged"
        public static let multipartyChatDownloadCompleted = "mulitpartyChatDownloadCompleted"
        public static let multipartyChatReceived = "multipartyChatReceived"
        public static let multipartyChatModified = "multipartyChatModified"
        public static let multipartyChatMessageReceived = "%@_multipartyMessageReceived"
        public static let multipartyChatMessageModified = "%@_multipartyChatMessageModified"
        public static let multipartyChatUserReceived = "%@_multipartyUserReceived"
        public static let userAvailable = "userAvailable"
        public static let notificationsAvailable = "notificationsAvailable"
    }
    
    public static let scaleMultiplier:CGFloat = 0.25
    public static let minScale:CGFloat = 0.55
    public static let maxScale:CGFloat = 1.08
    public static let minFade:CGFloat = -2.0
    public static let maxFade:CGFloat = 2.0
    
    public struct BundleIdentifiers {
        public static let CORE = "com.eagle.dev.anow.core"
        public static let PARENT = "com.eagle.dev.anow.parent"
        public static let COACH = "com.eagle.dev.anow.coach"
    }
    
    public static let NOTIFICATION_INTERVAL:Int = 30
}

//
//  Structs.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 5/2/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public struct ResultObject<T> {
    public init(hasMoreRecords: Bool, result: [T]?, error: errorMessage?) {
        self.hasMoreRecords = hasMoreRecords
        self.result = result
        self.error = error
    }
    
    public var hasMoreRecords: Bool
    public var result:[T]?
    public var error: errorMessage?
}

public struct AttachmentModel {
    public var requesterId:String
    public var assignmentId:Int
    public var unitId:Int
    public var typeId:Int
    
    public init(requesterId:String, assignmentId:Int, unitId:Int, typeId:Int){
        self.requesterId = requesterId
        self.assignmentId = assignmentId
        self.unitId = unitId
        self.typeId = typeId
    }
}

public struct requestSendStatus {
    public var isSuccess: Bool?
    public var message: String?
}

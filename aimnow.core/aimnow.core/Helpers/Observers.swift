//
//  Observers.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/6/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import RxSwift

public struct errorMessage {
    public var code: Int?
    public var error: String?
    public var title: String?
    
    public init(code:Int? = nil, error: String? = nil, title: String? = nil){
        self.code = code
        self.error = error
        self.title = title
    }
}

public class Observers {
    public static var instance = Observers()
    public let errorMessageObserver = PublishSubject<errorMessage?>()
    public let playerSelectObsrver = PublishSubject<DependantModel?>()
    public let programSelectObserver = PublishSubject<ProgramModel?>()
    public var fcmTokenUpdate = BehaviorSubject<String?>(value: "")
    public var logoutObserver = PublishSubject<Bool>()
    public var notificationSummaryObserver = PublishSubject<NotificationSummary?>()
    public var updateObserver = PublishSubject<UpdateModel>()
    public var onUserProfileUpdating = PublishSubject<UserModel>()
    public var onUserProfileUpdated = PublishSubject<UserModel>()
}

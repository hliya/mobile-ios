//
//  SharedAppResouces.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/16/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import JGProgressHUD

open class SharedAppResource : NSObject {
    public static let instance = SharedAppResource.init()
    public var selectedPlayer: DependantModel?
    public var selectedProgram: ProgramModel?
    public var openChatIds: [Int] = []
    public var hud: JGProgressHUD = JGProgressHUD(style: .dark)

    public var loggedInUser: UserModel?
    public var connectedToInternet = PublishSubject<Bool?>()
    public var notificationCount = PublishSubject<Int?>()
    public var chatCount = PublishSubject<Int?>()
    
    public var multipartyChats: [ChatModel] = []
    public var conversationUsers: [ConversationUsers] = []
    public var onlineUsers: BehaviorRelay<[OnlineUserModel]> = BehaviorRelay<[OnlineUserModel]>.init(value: [])
    public var chatObserver: BehaviorRelay<[ConversationMessage]> = BehaviorRelay<[ConversationMessage]>.init(value: [])

    
    public var conversationChats: [ConversationMessage] = []
    public var eligibleChatUsers: [ChatUsers] = []
    
    public var isCoachAttend: Bool = false
    
    public var appId: String = "";
    public var totalUnseenNotifications: Int = 0;
    
}



public struct OnlineUserModel {
    public var userId: String
    public var isOnline: Bool
}

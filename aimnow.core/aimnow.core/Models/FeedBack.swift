//
//  FeedBacks.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 15, 2020
//
import Foundation
import Realm
import RealmSwift
import SwiftyJSON

@objcMembers public class FeedBack: Object, ParamEncoder {
    dynamic public var id: Int = 0
    dynamic public var assignmentId = RealmOptional<Int>()
    dynamic public var statisticsId = RealmOptional<Int>()
    dynamic public var userId: String? = nil
    dynamic public var typeId = RealmOptional<Int>()
    dynamic public var comment: String? = nil
    dynamic public var rating = RealmOptional<Int>()
    dynamic public var attachment: Attachment?
    dynamic public var isActive = RealmOptional<Bool>()
    dynamic public var createdOn: String? = nil
    dynamic public var createdBy: String? = nil
    dynamic public var updatedOn: String? = nil
    dynamic public var updatedBy: String? = nil
    
    private struct SerializationKeys {
        static let id = "id"
        static let assignmentId = "assignmentId"
        static let statisticsId = "statisticsId"
        static let userId = "userId"
        static let typeId = "typeId"
        static let comment = "comment"
        static let rating = "rating"
        static let attachment = "attachment"
        static let isActive = "isActive"
        static let createdOn = "createdOn"
        static let createdBy = "createdBy"
        static let updatedOn = "updatedOn"
        static let updatedBy = "updatedBy"
    }

    override public static func primaryKey() -> String?
    {
        return "id"
    }
    
    required override init() {
        super.init()
    }
    
    convenience init(json: JSON) {
        self.init()
        assignmentId.value = json[SerializationKeys.assignmentId].int
        statisticsId.value = json[SerializationKeys.statisticsId].int
        isActive.value = json[SerializationKeys.isActive].boolValue
        userId = json[SerializationKeys.userId].string
        updatedOn = json[SerializationKeys.updatedOn].string
        updatedBy = json[SerializationKeys.updatedBy].string
        id = json[SerializationKeys.id].int ?? 0
        createdOn = json[SerializationKeys.createdOn].string
        typeId.value = json[SerializationKeys.typeId].int
        comment = json[SerializationKeys.comment].string
        rating.value = json[SerializationKeys.rating].int
        attachment = Attachment.init(json: json[SerializationKeys.attachment])
    }
    
}

//
//  ProgramDetail.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 5/15/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import Realm
import RealmSwift

@objcMembers public class ProgramDetail: AssignedProgramModel {

    dynamic public var coaches : List<DependantModel> = List<DependantModel>()
    dynamic public var players : List<DependantModel> = List<DependantModel>()
    
    required init()
    {
        super.init()
    }
    
    required public init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    public override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
    }
}

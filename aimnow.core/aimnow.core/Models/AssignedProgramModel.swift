//
//  AssignedProgramModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 15, 2020
//
import Foundation
import Realm
import RealmSwift

@objcMembers public class AssignedProgramModel: ProgramModel {

    dynamic public var referenceId = RealmOptional<Int>()
    dynamic public var assignmentTypeId = RealmOptional<Int>()
    dynamic public var on: String? = nil
    dynamic public var startTime: String? = nil
    dynamic public var endTime: String? = nil
    dynamic public var userId: String? = nil
    dynamic public var reference: Reference? = nil
    dynamic public var location: Location? = nil
    dynamic public var isRecurring = RealmOptional<Int>()
    dynamic public var locationId = RealmOptional<Int>()
    dynamic public var statusTypeId = RealmOptional<Int>()
    dynamic public var programTypeId = RealmOptional<Int>()
    dynamic public var attendance: List<Attendance> = List<Attendance>()
    dynamic public var feedBacks: List<FeedBack> = List<FeedBack>()
    dynamic public var statistics: List<Statistic> = List<Statistic>()
    dynamic public var createdOn: String? = nil
    dynamic public var createdBy: String? = nil
    dynamic public var updatedOn: String? = nil
    dynamic public var updatedBy: String? = nil
    dynamic public var totalCoaches: Int = 0
    dynamic public var totalPlayers: Int = 0
    dynamic public var remainingSlots : Int = 0
    
    private enum CodingKeys: String, CodingKey {
        case referenceId = "referenceId"
        case userId = "userId"
        case assignmentTypeId = "assignmentTypeId"
        case on = "on"
        case startTime = "startTime"
        case endTime = "endTime"
        case isRecurring = "isRecurring"
        case locationId = "locationId"
        case statusTypeId = "statusTypeId"
        case reference = "reference"
        case location = "location"
        case attendance = "attendance"
        case feedBacks = "feedBacks"
        case statistics = "statistics"
        case id = "id"
        case name = "name"
        case description = "description"
        case programTypeId = "programTypeId"
        case unitId = "unitId"
        case parentId = "parentId"
        case configId = "configId"
        case isActive = "isActive"
        case createdOn = "createdOn"
        case createdBy = "createdBy"
        case updatedOn = "updatedOn"
        case updatedBy = "updatedBy"
        case summary = "programSummary"
        enum NestedCodingKeys: String, CodingKey {
            case totalCoaches = "totalCoaches"
            case totalPlayers = "totalPlayers"
            case remainingSlots = "remainingSlots"
        }
        
    }
    
    required init()
    {
        super.init()
    }
    
    override public static func primaryKey() -> String?
    {
        return "id"
    }
    
    required public init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        referenceId.value = try (values.decodeIfPresent(Int.self, forKey: .referenceId) ?? 0)
        userId = try (values.decodeIfPresent(String.self, forKey: .userId) ?? "")
        assignmentTypeId.value = try values.decodeIfPresent(Int.self, forKey: .assignmentTypeId)
        on = try values.decodeIfPresent(String.self, forKey: .on)
        startTime = try values.decodeIfPresent(String.self, forKey: .startTime)
        endTime = try values.decodeIfPresent(String.self, forKey: .endTime)
        isRecurring.value = try values.decodeIfPresent(Int.self, forKey: .isRecurring)
        locationId.value = try values.decodeIfPresent(Int.self, forKey: .locationId)
        statusTypeId.value = try values.decodeIfPresent(Int.self, forKey: .statusTypeId)
        reference = try values.decodeIfPresent(Reference.self, forKey: .reference)
        location = try values.decodeIfPresent(Location.self, forKey: .location)
        attendance.append(objectsIn: try (values.decodeIfPresent([Attendance].self, forKey: .attendance) ?? []))
        feedBacks.append(objectsIn: try (values.decodeIfPresent([FeedBack].self, forKey: .feedBacks) ?? []))
        statistics.append(objectsIn: try (values.decodeIfPresent([Statistic].self, forKey: .statistics) ?? []))
        id = try (values.decodeIfPresent(Int.self, forKey: .id) ?? 0)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        descriptionValue = try values.decodeIfPresent(String.self, forKey: .description)
        programTypeId.value = try values.decodeIfPresent(Int.self, forKey: .programTypeId)
        unitId.value = try values.decodeIfPresent(Int.self, forKey: .unitId)
        parentId.value = try values.decodeIfPresent(Int.self, forKey: .parentId)
        configId.value = try values.decodeIfPresent(Int.self, forKey: .configId)
        isActive.value = try values.decodeIfPresent(Bool.self, forKey: .isActive)
        createdOn = try values.decodeIfPresent(String.self, forKey: .createdOn)
        createdBy = try values.decodeIfPresent(String.self, forKey: .createdBy)
        updatedOn = try values.decodeIfPresent(String.self, forKey: .updatedOn)
        updatedBy = try values.decodeIfPresent(String.self, forKey: .updatedBy)
        let summary = try? values.nestedContainer(keyedBy: CodingKeys.NestedCodingKeys.self, forKey: .summary)
        totalCoaches = try (summary?.decodeIfPresent(Int.self, forKey: .totalCoaches) ?? 0)
        totalPlayers = try (summary?.decodeIfPresent(Int.self, forKey: .totalPlayers) ?? 0)
        remainingSlots = try (summary?.decodeIfPresent(Int.self, forKey: .remainingSlots) ?? 0)
    }

    public override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(referenceId, forKey: .referenceId)
        try container.encodeIfPresent(userId, forKey: .userId)
        try container.encodeIfPresent(assignmentTypeId, forKey: .assignmentTypeId)
        try container.encodeIfPresent(on, forKey: .on)
        try container.encodeIfPresent(startTime, forKey: .startTime)
        try container.encodeIfPresent(endTime, forKey: .endTime)
        try container.encodeIfPresent(isRecurring, forKey: .isRecurring)
        try container.encodeIfPresent(locationId, forKey: .locationId)
        try container.encodeIfPresent(statusTypeId, forKey: .statusTypeId)
        try container.encodeIfPresent(reference, forKey: .reference)
        try container.encodeIfPresent(location, forKey: .location)
        try container.encodeIfPresent(attendance, forKey: .attendance)
        try container.encodeIfPresent(feedBacks, forKey: .feedBacks)
        try container.encodeIfPresent(statistics, forKey: .statistics)
        try container.encodeIfPresent(id, forKey: .id)
        try container.encodeIfPresent(name, forKey: .name)
        try container.encodeIfPresent(descriptionValue, forKey: .description)
        try container.encodeIfPresent(programTypeId, forKey: .programTypeId)
        try container.encodeIfPresent(unitId, forKey: .unitId)
        try container.encodeIfPresent(parentId, forKey: .parentId)
        try container.encodeIfPresent(configId, forKey: .configId)
        try container.encodeIfPresent(isActive, forKey: .isActive)
        try container.encodeIfPresent(createdOn, forKey: .createdOn)
        try container.encodeIfPresent(createdBy, forKey: .createdBy)
        try container.encodeIfPresent(updatedOn, forKey: .updatedOn)
        try container.encodeIfPresent(updatedBy, forKey: .updatedBy)
    }
    
}

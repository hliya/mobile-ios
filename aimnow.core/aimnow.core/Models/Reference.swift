//
//  Reference.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 15, 2020
//
import Foundation
import Realm
import RealmSwift
import SwiftyJSON

@objcMembers public class Reference: Object, ParamEncoder {
    
    dynamic public var id: Int = 0
    dynamic public var name: String? = nil
    dynamic public var descriptionValue: String? = nil
    dynamic public var typeId = RealmOptional<Int>()
    dynamic public var unitId =  RealmOptional<Int>()
    dynamic public var parentId = RealmOptional<Int>()
    dynamic public var configId = RealmOptional<Int>()
    dynamic public var statusTypeId = RealmOptional<Int>()
    dynamic public var isActive = RealmOptional<Bool>()
    dynamic public var createdOn: String?  = nil
    dynamic public var createdBy: String? = nil
    dynamic public var updatedOn: String? = nil
    dynamic public var updatedBy: String? = nil
    
    private struct SerializationKeys {
        static let createdBy = "createdBy"
        static let unitId = "unitId"
        static let name = "name"
        static let isActive = "isActive"
        static let configId = "configId"
        static let statusTypeId = "statusTypeId"
        static let typeId = "typeId"
        static let updatedOn = "updatedOn"
        static let updatedBy = "updatedBy"
        static let id = "id"
        static let createdOn = "createdOn"
        static let parentId = "parentId"
        static let description = "description"
    }
    
    override public static func primaryKey() -> String?
    {
        return "id"
    }
    
    required override init() {
        super.init()
    }
    
    convenience init(json: JSON) {
        self.init()
        createdBy = json[SerializationKeys.createdBy].string
        unitId.value = json[SerializationKeys.unitId].int
        name = json[SerializationKeys.name].string
        isActive.value = json[SerializationKeys.isActive].boolValue
        configId.value = json[SerializationKeys.configId].int
        statusTypeId.value = json[SerializationKeys.statusTypeId].int
        typeId.value = json[SerializationKeys.typeId].int
        updatedOn = json[SerializationKeys.updatedOn].string
        updatedBy = json[SerializationKeys.updatedBy].string
        id = json[SerializationKeys.id].int ?? 0
        createdOn = json[SerializationKeys.createdOn].string
        parentId.value = json[SerializationKeys.parentId].int
        descriptionValue = json[SerializationKeys.description].string
    }
    
}


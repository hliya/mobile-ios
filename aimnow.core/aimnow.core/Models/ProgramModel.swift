//
//  ProgramModel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/6/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

@objcMembers public class ProgramModel: Object, ParamEncoder {
    private struct SerializationKeys {
        static var id = "id"
        static var name = "name"
        static var descriptionValue = "description"
        static var typeId = "typeId"
        static var unitId = "unitId"
        static var parentId = "parentId"
        static var configId = "configId"
        static var configModel = "configModel"
        static var isActive = "isActive"
        static var masterId = "masterId"
    }
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case description = "description"
        case typeId = "typeId"
        case unitId = "unitId"
        case parentId = "parentId"
        case isActive = "isActive"
        case configId = "configId"
        case config = "config"
        case masterId = "masterId"
    }
    dynamic public var id: Int = 0
    dynamic public var name: String?
    dynamic public var descriptionValue: String?
    dynamic public var typeId = RealmOptional<Int>()
    dynamic public var unitId = RealmOptional<Int>()
    dynamic public var parentId = RealmOptional<Int>()
    dynamic public var configId = RealmOptional<Int>()
    dynamic public var isActive = RealmOptional<Bool>()
    dynamic public var config: ConfigModel? = nil
    public var isEnrolled: Bool = false
    dynamic public var masterId : Int = 0

    public var hasChatNotifications: Bool = false
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    public override static func ignoredProperties() -> [String] {
      return ["hasChatNotifications"]
    }
    
    convenience init(json: JSON) {
        self.init()
        descriptionValue = json[SerializationKeys.descriptionValue].string
        isActive.value = json[SerializationKeys.isActive].bool ?? true
        id = json[SerializationKeys.id].int ?? 0
        name = json[SerializationKeys.name].string
        typeId.value = json[SerializationKeys.typeId].int ?? 0
        unitId.value = json[SerializationKeys.unitId].int ?? 0
        configId.value = json[SerializationKeys.configId].int ?? 0
        parentId.value = json[SerializationKeys.parentId].int
        masterId = json[SerializationKeys.masterId].int ?? 0
    }
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try (values.decodeIfPresent(Int.self, forKey: .id) ?? 0)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        descriptionValue = try values.decodeIfPresent(String.self, forKey: .description)
        typeId.value = try values.decodeIfPresent(Int.self, forKey: .typeId)
        unitId.value = try values.decodeIfPresent(Int.self, forKey: .unitId)
        parentId.value = try values.decodeIfPresent(Int.self, forKey: .parentId)
        configId.value = try values.decodeIfPresent(Int.self, forKey: .configId)
        isActive.value = try values.decodeIfPresent(Bool.self, forKey: .isActive)
        config = try values.decodeIfPresent(ConfigModel.self, forKey: .config)
        masterId = try (values.decodeIfPresent(Int.self, forKey: .masterId) ?? 0)
    }
    
    required override init() {
        descriptionValue = nil
        id = 0
        name = nil
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(description, forKey: .description)
        try container.encode(typeId, forKey: .typeId)
        try container.encode(unitId, forKey: .unitId)
        try container.encode(parentId, forKey: .parentId)
        try container.encode(configId, forKey: .configId)
        try container.encode(isActive, forKey: .isActive)
        try container.encode(config, forKey: .config)
        try container.encode(masterId, forKey: .masterId)
    }
}

@objcMembers public class ConfigModel: Object, ParamEncoder {

    dynamic public var id: Int
    dynamic public var typeId = RealmOptional<Int>()
    dynamic public var minScore = RealmOptional<Int>()
    dynamic public var maxScore = RealmOptional<Int>()
    dynamic public var stages = RealmOptional<Int>()
    dynamic public var isActive = RealmOptional<Bool>()

    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case typeId = "typeId"
        case minScore = "minScore"
        case maxScore = "maxScore"
        case stages = "stages"
        case isActive = "isActive"
    }

    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        typeId.value = try values.decodeIfPresent(Int.self, forKey: .typeId)
        minScore.value = try values.decodeIfPresent(Int.self, forKey: .minScore)
        maxScore.value = try values.decodeIfPresent(Int.self, forKey: .maxScore)
        isActive.value = try values.decodeIfPresent(Bool.self, forKey: .isActive)
        stages.value = try values.decodeIfPresent(Int.self, forKey: .stages)
    }
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    required override init() {
        id = 0
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(id, forKey: .id)
        try container.encodeIfPresent(typeId, forKey: .typeId)
        try container.encodeIfPresent(minScore, forKey: .minScore)
        try container.encodeIfPresent(maxScore, forKey: .maxScore)
        try container.encodeIfPresent(stages, forKey: .stages)
        try container.encodeIfPresent(isActive, forKey: .isActive)
    }
}

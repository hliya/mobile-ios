//
//  Attendance.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 15, 2020
//
import Foundation
import Realm
import RealmSwift
import SwiftyJSON

@objcMembers public class Attendance: Object, ParamEncoder {
    
    dynamic public var id: Int = 0
    dynamic public var deviceTypeId = RealmOptional<Int>()
    dynamic public var assigmentId = RealmOptional<Int>()
    dynamic public var userId: String?
    dynamic public var on: String?
    dynamic public var inTime: String?
    dynamic public var outTime: String?
    dynamic public var createdOn: String?
    dynamic public var createdBy: String?
    dynamic public var updatedOn: String?
    dynamic public var updatedBy: String?
    
    private struct SerializationKeys {
        static let createdBy = "createdBy"
        static let userId = "userId"
        static let on = "on"
        static let inTime = "inTime"
        static let assigmentId = "assigmentId"
        static let out = "outTime"
        static let deviceTypeId = "deviceTypeId"
        static let updatedOn = "updatedOn"
        static let id = "id"
        static let createdOn = "createdOn"
        static let updatedBy = "updatedBy"
    }
    
    override public static func primaryKey() -> String?
    {
        return "id"
    }
    
    required override init() {
        super.init()
    }
    
    convenience init(json: JSON) {
        self.init()
        createdBy = json[SerializationKeys.createdBy].string
        userId = json[SerializationKeys.userId].string
        on = json[SerializationKeys.on].string
        inTime = json[SerializationKeys.inTime].string
        assigmentId.value = json[SerializationKeys.assigmentId].int
        outTime = json[SerializationKeys.out].string
        deviceTypeId.value = json[SerializationKeys.deviceTypeId].int
        updatedOn = json[SerializationKeys.updatedOn].string
        id = json[SerializationKeys.id].int ?? 0
        createdOn = json[SerializationKeys.createdOn].string
        updatedBy = json[SerializationKeys.updatedBy].string
    }
    
}

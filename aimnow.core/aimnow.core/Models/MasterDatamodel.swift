//
//  MasterDatamodel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/9/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

@objcMembers class MasterData: Object, ParamEncoder {
    private struct SerializationKeys {
        static let descriptionValue = "description"
        static let isActive = "isActive"
        static let id = "id"
        static let type = "type"
    }
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case description = "description"
        case isActive = "isActive"
    }
    
    dynamic public var descriptionValue: String?
    dynamic public var type: String?
    dynamic public var isActive = RealmOptional<Bool>()
    dynamic public var id: Int = 0
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(json: JSON) {
        self.init()
        descriptionValue = json[SerializationKeys.descriptionValue].string
        isActive.value = json[SerializationKeys.isActive].bool ?? true
        id = json[SerializationKeys.id].int ?? 0
        type = json[SerializationKeys.type].string
    }
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try (values.decodeIfPresent(Int.self, forKey: .id) ?? 0)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        descriptionValue = try values.decodeIfPresent(String.self, forKey: .description)
        isActive.value = try values.decodeIfPresent(Bool.self, forKey: .isActive)
    }
    
    required override init() {
        descriptionValue = nil
        type = nil
        isActive.value = nil
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(type, forKey: .type)
        try container.encode(description, forKey: .description)
        try container.encode(isActive, forKey: .isActive)
    }
}


@objcMembers class MasterAssigment: MasterData {}
@objcMembers class MasterAttachment: MasterData {}
@objcMembers class MasterBadge: MasterData {}
@objcMembers class MasterConfig: MasterData {}
@objcMembers class MasterDevice: MasterData {}
@objcMembers class MasterFeedback: MasterData {}
@objcMembers class MasterNotification: MasterData {}
@objcMembers class MasterProgram: MasterData {}
@objcMembers class MasterTempate: MasterData {}
@objcMembers class MasterUnit: MasterData {}
@objcMembers class MasterUser: MasterData {}
@objcMembers class MasterStatus: MasterData {}

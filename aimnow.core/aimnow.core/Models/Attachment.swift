//
//  Attachments.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 15, 2020
//
import Foundation
import Realm
import RealmSwift
import SwiftyJSON

@objcMembers public class Attachment: Object, ParamEncoder {
    
    dynamic public var id: Int = 0
    dynamic public var typeId = RealmOptional<Int>()
    dynamic public var path: String? = nil
    dynamic public var isActive = RealmOptional<Bool>()
    dynamic public var updatedOn: String? = nil
    
    private struct SerializationKeys {
        static let id = "id"
        static let typeId = "typeId"
        static let path = "path"
        static let isActive = "isActive"
        static let updatedOn = "updatedOn"
    }
    
    override public static func primaryKey() -> String?
    {
        return "id"
    }
    
    public required override init() {
        super.init()
    }
    
    convenience init(json: JSON) {
        self.init()
        typeId.value = json[SerializationKeys.typeId].int
        isActive.value = json[SerializationKeys.isActive].bool
        path = json[SerializationKeys.path].string
        updatedOn = json[SerializationKeys.updatedOn].string
        id = json[SerializationKeys.id].int ?? 0
        updatedOn = json[SerializationKeys.updatedOn].string 
    }
    
}

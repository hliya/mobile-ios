//
//  UserModel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/5/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import SwiftyUserDefaults

public struct UserModel: Codable, DefaultsSerializable {

    public let id: String?
    public let typeId: Int?
    public let title: String?
    public let firstName: String?
    public let middleName: String?
    public let lastName: String?
    public let dob: String?
    public let age: Int?
    public let phoneNumber: String?
    public let nicnumber: String?
    public let passportNumber: String?
    public let email: String?
    public var profileImage: String?
    public let parentId: Int?

    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case typeId = "typeId"
        case title = "title"
        case firstName = "firstName"
        case middleName = "middleName"
        case lastName = "lastName"
        case dob = "dob"
        case age = "age"
        case phoneNumber = "phoneNumber"
        case nicnumber = "nicnumber"
        case passportNumber = "passportNumber"
        case email = "email"
        case profileImage = "profileImage"
        case parentId = "parentId"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        typeId = try values.decodeIfPresent(Int.self, forKey: .typeId)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
        middleName = try values.decodeIfPresent(String.self, forKey: .middleName)
        lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        age = try values.decodeIfPresent(Int.self, forKey: .age)
        phoneNumber = try values.decodeIfPresent(String.self, forKey: .phoneNumber)
        nicnumber = try values.decodeIfPresent(String.self, forKey: .nicnumber)
        passportNumber = try values.decodeIfPresent(String.self, forKey: .passportNumber)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        profileImage = try values.decodeIfPresent(String.self, forKey: .profileImage)
        parentId = try values.decodeIfPresent(Int.self, forKey: .parentId)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(typeId, forKey: .typeId)
        try container.encode(title, forKey: .title)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(middleName, forKey: .middleName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(dob, forKey: .dob)
        try container.encode(age, forKey: .age)
        try container.encode(phoneNumber, forKey: .phoneNumber)
        try container.encode(nicnumber, forKey: .nicnumber)
        try container.encode(passportNumber, forKey: .passportNumber)
        try container.encode(profileImage, forKey: .profileImage)
        try container.encode(parentId, forKey: .parentId)
        try container.encode(email, forKey: .email)
    }
    
    public var name: String {
        get{
            return (self.firstName ?? "") + " " + (self.lastName ?? "")
        }
    }

}

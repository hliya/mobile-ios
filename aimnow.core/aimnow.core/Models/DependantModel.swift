//
//  DependantModel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/6/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import Realm
import RealmSwift

@objcMembers public class DependantModel: Object, ParamEncoder, DefaultsSerializable{

    dynamic public var id: String? = nil
    dynamic public var typeId = RealmOptional<Int>()
    dynamic public var title: String? = nil
    dynamic public var firstName: String? = nil
    dynamic public var middleName: String? = nil
    dynamic public var lastName: String? = nil
    dynamic public var dob: String? = nil
    dynamic public var address: String? = nil
    dynamic public var age = RealmOptional<Int>()
    dynamic public var phoneNumber: String? = nil
    dynamic public var nicnumber: String? = nil
    dynamic public var passportNumber: String?
    dynamic public var email: String? = nil
    dynamic public var profileImage: String? = nil
    dynamic public var parentId: String?
    dynamic public var assigments : List<Assigment> = List<Assigment>()
    dynamic public var currentAssigments : List<Assigment> = List<Assigment>()
    dynamic public var currentStage: Assigment?
    dynamic public var isPresentToday = RealmOptional<Bool>()
    dynamic public var referenceId: String? = nil
    
    // TODO: This need to check
//    var stage: Int = 1 // This need to calculate
    public var ballTypeColor: String = Ui.ColorCodesHex.Gray
    var isEnrolled: Bool? = true

    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case typeId = "typeId"
        case title = "title"
        case firstName = "firstName"
        case middleName = "middleName"
        case lastName = "lastName"
        case dob = "dob"
        case age = "age"
        case phoneNumber = "phoneNumber"
        case nicnumber = "nicnumber"
        case passportNumber = "passportNumber"
        case email = "email"
        case profileImage = "profileImage"
        case parentId = "parentId"
        case assigments = "assigments"
        case currentAssigments = "currentAssigments"
        case currentStage = "currentStage"
        case isPresentToday = "isPresentToday"
        case address = "address"
        case referenceId = "referenceId"
    }
    
    override public static func primaryKey() -> String?
    {
        return "id"
    }
    
    required override init() {
        super.init()
    }

    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        typeId.value = try values.decodeIfPresent(Int.self, forKey: .typeId)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
        middleName = try values.decodeIfPresent(String.self, forKey: .middleName)
        lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        age.value = try values.decodeIfPresent(Int.self, forKey: .age)
        phoneNumber = try values.decodeIfPresent(String.self, forKey: .phoneNumber)
        nicnumber = try values.decodeIfPresent(String.self, forKey: .nicnumber)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        passportNumber = try values.decodeIfPresent(String.self, forKey: .passportNumber)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        profileImage = try values.decodeIfPresent(String.self, forKey: .profileImage)
        parentId = try values.decodeIfPresent(String.self, forKey: .parentId)
        assigments.append(objectsIn: try (values.decodeIfPresent([Assigment].self, forKey: .assigments) ?? []))
        currentAssigments.append(objectsIn: try (values.decodeIfPresent([Assigment].self, forKey: .currentAssigments) ?? []))
        currentStage = try values.decodeIfPresent(Assigment.self, forKey: .currentStage)
        isPresentToday.value = try values.decodeIfPresent(Bool.self, forKey: .isPresentToday)
        referenceId = try values.decodeIfPresent(String.self, forKey: .referenceId)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(typeId, forKey: .typeId)
        try container.encode(title, forKey: .title)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(middleName, forKey: .middleName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(dob, forKey: .dob)
        try container.encode(age, forKey: .age)
        try container.encode(phoneNumber, forKey: .phoneNumber)
        try container.encode(nicnumber, forKey: .nicnumber)
        try container.encode(passportNumber, forKey: .passportNumber)
        try container.encode(email, forKey: .email)
        try container.encode(profileImage, forKey: .profileImage)
        try container.encode(parentId, forKey: .parentId)
        try container.encode(currentStage, forKey: .currentStage)
        try container.encode(isPresentToday, forKey: .isPresentToday)
        try container.encode(address, forKey: .address)
        try container.encode(address, forKey: .referenceId)
    }

    public var name: String {
        get{
            return (self.firstName ?? "") + " " + (self.lastName ?? "")
        }
    }
}

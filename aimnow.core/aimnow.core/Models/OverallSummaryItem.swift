//
//  OverallSummaryItem.swift
//
//  Created by Isuru Ranasinghe on 6/7/20
//  Copyright (c) . All rights reserved.
//

import Foundation

public struct OverallSummaryItem: ParamEncoder {
    
    enum CodingKeys: String, CodingKey {
        case type
        case color
        case data
        case yAxis
        case name
        case typeId
    }
    
    public var type: String!
    public var color: String!
    public var data: [Int]!
    public var yAxis: Int!
    public var name: String!
    public var typeId: Int!
    
    public init(type: String, color: String, data: [Int], yAxis: Int, name: String, typeId: Int) {
        self.type = type
        self.color = color
        self.data = data
        self.yAxis = yAxis
        self.name = name
        self.typeId = typeId
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        type = try container.decode(String.self, forKey: .type)
        color = try container.decode(String.self, forKey: .color)
        data = try container.decode([Int].self, forKey: .data)
        yAxis = try container.decode(Int.self, forKey: .yAxis)
        name = try container.decode(String.self, forKey: .name)
        typeId = try container.decode(Int.self, forKey: .typeId)
    }
    
}

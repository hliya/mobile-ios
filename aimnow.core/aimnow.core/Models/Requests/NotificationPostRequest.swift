import Foundation

public struct NotificationPostRequest : ParamEncoder {
	public var format : String?
	public var friday : Bool?
	public var hasSeen : Bool?
	public var id : Int?
	public var isRecurring : Bool?
	public var lastUpdatedOn : String?
	public var message : String?
	public var monday : Bool?
	public var pool : [String]?
	public var receiverId : String?
	public var recurringFrom : String?
	public var recurringTo : String?
	public var referenceId : Int?
	public var requesterId : String?
	public var saturday : Bool?
	public var senderId : String?
	public var sunday : Bool?
	public var templateId : Int?
	public var thursday : Bool?
	public var tuesday : Bool?
	public var typeId : Int?
	public var unitId : Int? = 1
	public var wendsday : Bool?

	enum CodingKeys: String, CodingKey {

		case format = "format"
		case friday = "friday"
		case hasSeen = "hasSeen"
		case id = "id"
		case isRecurring = "isRecurring"
		case lastUpdatedOn = "lastUpdatedOn"
		case message = "message"
		case monday = "monday"
		case pool = "pool"
		case receiverId = "receiverId"
		case recurringFrom = "recurringFrom"
		case recurringTo = "recurringTo"
		case referenceId = "referenceId"
		case requesterId = "requesterId"
		case saturday = "saturday"
		case senderId = "senderId"
		case sunday = "sunday"
		case templateId = "templateId"
		case thursday = "thursday"
		case tuesday = "tuesday"
		case typeId = "typeId"
		case unitId = "unitId"
		case wendsday = "wendsday"
	}

    public init(){}
    
	public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		format = try values.decodeIfPresent(String.self, forKey: .format)
		friday = try values.decodeIfPresent(Bool.self, forKey: .friday)
		hasSeen = try values.decodeIfPresent(Bool.self, forKey: .hasSeen)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		isRecurring = try values.decodeIfPresent(Bool.self, forKey: .isRecurring)
		lastUpdatedOn = try values.decodeIfPresent(String.self, forKey: .lastUpdatedOn)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		monday = try values.decodeIfPresent(Bool.self, forKey: .monday)
		pool = try values.decodeIfPresent([String].self, forKey: .pool)
		receiverId = try values.decodeIfPresent(String.self, forKey: .receiverId)
		recurringFrom = try values.decodeIfPresent(String.self, forKey: .recurringFrom)
		recurringTo = try values.decodeIfPresent(String.self, forKey: .recurringTo)
		referenceId = try values.decodeIfPresent(Int.self, forKey: .referenceId)
		requesterId = try values.decodeIfPresent(String.self, forKey: .requesterId)
		saturday = try values.decodeIfPresent(Bool.self, forKey: .saturday)
		senderId = try values.decodeIfPresent(String.self, forKey: .senderId)
		sunday = try values.decodeIfPresent(Bool.self, forKey: .sunday)
		templateId = try values.decodeIfPresent(Int.self, forKey: .templateId)
		thursday = try values.decodeIfPresent(Bool.self, forKey: .thursday)
		tuesday = try values.decodeIfPresent(Bool.self, forKey: .tuesday)
		typeId = try values.decodeIfPresent(Int.self, forKey: .typeId)
		unitId = try values.decodeIfPresent(Int.self, forKey: .unitId)
		wendsday = try values.decodeIfPresent(Bool.self, forKey: .wendsday)
	}

}

//
//  MasterDataRequestmodel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/10/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public struct MasterDataRequestmodel: ParamEncoder {

    let type: Int?
    let id: Int?
    let requesterId: String?
    let unitId: Int?
    let pageIndex: Int?
    let pageSize: Int?
    let orderBy: [String]?

    private enum CodingKeys: String, CodingKey {
        case type = "type"
        case id = "id"
        case requesterId = "requesterId"
        case unitId = "unitId"
        case pageIndex = "pageIndex"
        case pageSize = "pageSize"
        case orderBy = "orderBy"
    }

    public init(type: Int? = nil, id: Int? = nil, requesterId: String? = nil, unitId: Int = 1, pageIndex: Int = 1, pageSize: Int = 20){
        self.type = type
        self.id = id
        self.requesterId = requesterId
        self.unitId = unitId
        self.pageIndex = pageIndex
        self.pageSize = pageSize
        self.orderBy = []
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(Int.self, forKey: .type)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        requesterId = try values.decodeIfPresent(String.self, forKey: .requesterId)
        unitId = try values.decodeIfPresent(Int.self, forKey: .unitId)
        pageIndex = try values.decodeIfPresent(Int.self, forKey: .pageIndex)
        pageSize = try values.decodeIfPresent(Int.self, forKey: .pageSize)
        orderBy = try values.decodeIfPresent([String].self, forKey: .orderBy)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(type, forKey: .type)
        try container.encodeIfPresent(id, forKey: .id)
        try container.encodeIfPresent(requesterId, forKey: .requesterId)
        try container.encodeIfPresent(unitId, forKey: .unitId)
        try container.encodeIfPresent(pageIndex, forKey: .pageIndex)
        try container.encodeIfPresent(pageSize, forKey: .pageSize)
        try container.encodeIfPresent(orderBy, forKey: .orderBy)
    }

}

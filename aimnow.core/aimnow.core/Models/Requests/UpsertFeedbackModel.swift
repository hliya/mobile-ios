//
//  UpsertFeedbackModel.swift
//
//  Created by Isuru Ranasinghe on 5/30/20
//  Copyright (c) . All rights reserved.
//

import Foundation

public struct UpsertFeedbackModel: ParamEncoder {
    
    enum CodingKeys: String, CodingKey {
        case unitId
        case requesterId
        case ownerId
        case feedbackTypeId
        case comment
        case rating
        case assignmentId
        case id
        case statisticsId
        case attachmentId
    }
    
    public var unitId: Int?
    public var requesterId: String?
    public var ownerId: String?
    public var feedbackTypeId: Int?
    public var comment: String?
    public var rating: Int?
    public var assignmentId: Int?
    public var id: Int?
    public var statisticsId: Int?
    public var attachmentId: Int?
    
    public init() {
           
       }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        unitId = try container.decodeIfPresent(Int.self, forKey: .unitId)
        requesterId = try container.decodeIfPresent(String.self, forKey: .requesterId)
        ownerId = try container.decodeIfPresent(String.self, forKey: .ownerId)
        feedbackTypeId = try container.decodeIfPresent(Int.self, forKey: .feedbackTypeId)
        comment = try container.decodeIfPresent(String.self, forKey: .comment)
        rating = try container.decodeIfPresent(Int.self, forKey: .rating)
        assignmentId = try container.decodeIfPresent(Int.self, forKey: .assignmentId)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        statisticsId = try container.decodeIfPresent(Int.self, forKey: .statisticsId)
        attachmentId = try container.decodeIfPresent(Int.self, forKey: .attachmentId)
    }
    
}

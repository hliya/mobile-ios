//
//  OverallSummaryRequest.swift
//
//  Created by Isuru Ranasinghe on 6/7/20
//  Copyright (c) . All rights reserved.
//

import Foundation

public struct OverallSummaryRequest: ParamEncoder {
    
    enum CodingKeys: String, CodingKey {
        case from
        case to
        case unitId
        case assignmentTypeId
        case id
        case requesterId
        case isSummaryMaster
        case isSummary
    }
    
    public var from: String?
    public var to: String?
    public var unitId: Int?
    public var assignmentTypeId: Int?
    public var id: Int?
    public var requesterId: String?
    public var isSummaryMaster: Bool?
    public var isSummary: Bool?
    
    
    public init(
            from: String?,
            to: String?,
            unitId: Int?,
            assignmentTypeId: Int?,
            id: Int?,
            requesterId: String?,
            isSummaryMaster: Bool?,
            isSummary: Bool?
            ){
        
        self.from = from
        self.to = to
        self.unitId = unitId
        self.assignmentTypeId = assignmentTypeId
        self.id = id
        self.requesterId = requesterId
        self.isSummaryMaster = isSummaryMaster
        self.isSummary = isSummary
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        from = try container.decodeIfPresent(String.self, forKey: .from)
        to = try container.decodeIfPresent(String.self, forKey: .to)
        unitId = try container.decodeIfPresent(Int.self, forKey: .unitId)
        assignmentTypeId = try container.decodeIfPresent(Int.self, forKey: .assignmentTypeId)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        requesterId = try container.decodeIfPresent(String.self, forKey: .requesterId)
        isSummaryMaster = try container.decodeIfPresent(Bool.self, forKey: .isSummaryMaster)
        isSummary = try container.decodeIfPresent(Bool.self, forKey: .isSummary)
    }
    
}

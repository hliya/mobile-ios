//
//  StatisticRequestModel.swift
//  aimnow.core
//
//  Created by Hasitha Liyanage  on 11/25/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public struct StatisticRequestModel: ParamEncoder {
    
    enum CodingKeys: String, CodingKey {
        case id
        case assignmentId
        case unitId
        case ascending
        case orderBy
        case pageIndex
        case to
        case requesterId
        case ownerId
        case assignmentTypeId
        case on
        case pageSize
        case isActive
    }
    
    public var id: Int?
    public var assignmentId: Int?
    public var unitId: Int?
    public var ascending: Bool?
    public var orderBy: [String]?
    public var pageIndex: Int?
    public var to: String?
    public var requesterId: String?
    public var assignmentTypeId: Int?
    public var on: String?
    public var pageSize: Int?
    public var isActive: Bool?
    public var ownerId: String?
    
    public init(){
        
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        assignmentId = try container.decodeIfPresent(Int.self, forKey: .assignmentId)
        unitId = try container.decodeIfPresent(Int.self, forKey: .unitId)
        ascending = try container.decodeIfPresent(Bool.self, forKey: .ascending)
        orderBy = try container.decodeIfPresent([String].self, forKey: .orderBy)
        pageIndex = try container.decodeIfPresent(Int.self, forKey: .pageIndex) 
        to = try container.decodeIfPresent(String.self, forKey: .to)
        ownerId = try container.decodeIfPresent(String.self, forKey: .ownerId)
        requesterId = try container.decodeIfPresent(String.self, forKey: .requesterId)
        assignmentTypeId = try container.decodeIfPresent(Int.self, forKey: .assignmentTypeId)
        on = try container.decodeIfPresent(String.self, forKey: .on)
        pageSize = try container.decodeIfPresent(Int.self, forKey: .pageSize)
        isActive = try container.decodeIfPresent(Bool.self, forKey: .isActive)
    }
    
}

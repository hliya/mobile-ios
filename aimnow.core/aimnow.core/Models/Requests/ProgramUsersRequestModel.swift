//
//  ProgramUsersRequestModel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 7/1/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public struct ProgramUsersRequestModel: ParamEncoder {
    var id: Int?
    var unitId: Int!
    var assignmentTypeId: Int?
    var requesterId: String?
    
    public init(id: Int? = nil,unitId: Int = 1, assignmentTypeId: Int? = 1, requesterId: String = SharedAppResource.instance.loggedInUser?.id ?? "") {
        self.unitId = unitId
        self.id = id
        self.assignmentTypeId = assignmentTypeId
        self.requesterId = requesterId
    }
}

//
//  AssignedProgramRequestModel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/6/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public struct AssignedProgramRequestModel: ParamEncoder {
    var on : String?
    var id: Int?
    var requesterId: String?
    var AssignmentTypeId: Int?
    var unitId: Int?
    var pageIndex: Int?
    var pageSize: Int?
    
    
    public init(on: String? = nil, id: Int? = nil, requesterId: String, assignmentTypeId: Int,unitId: Int, pageIndex: Int? = 1, pageSize: Int? = Constants.PAGE_SIZE) {
        self.unitId = unitId
        self.on = on
        self.id = id
        self.requesterId = requesterId
        self.AssignmentTypeId = assignmentTypeId
        self.pageIndex = pageIndex
        self.pageSize = pageSize
    }
}

public struct AssignedProgramDetailRequestModel: ParamEncoder {
    var on : String?
    var id: Int?
    var unitId: Int?
    var assignmentTypeId: Int?
    var requesterId: String?
    
    public init(on: String? = nil, id: Int? = nil,unitId: Int, assignmentTypeId: Int? = 1) {
        self.unitId = unitId
        self.on = on
        self.id = id
        self.assignmentTypeId = assignmentTypeId
        self.requesterId = SharedAppResource.instance.loggedInUser?.id
    }
}

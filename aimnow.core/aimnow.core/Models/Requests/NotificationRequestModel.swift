import Foundation
public struct NotificationRequestModel : ParamEncoder {
	public var ascending : Bool? = true
	public var from : String?
	public var id : Int?
	public var isActive : Bool? = true
	public var lastUpdatedOn : String?
	public var orderBy : [String]?
	public var pageIndex : Int? = 0
    public var pageSize : Int? = Constants.PAGE_SIZE
	public var requesterId : String?
	public var to : String?
	public var unitId : Int?

	enum CodingKeys: String, CodingKey {

		case ascending = "ascending"
		case from = "from"
		case id = "id"
		case isActive = "isActive"
		case lastUpdatedOn = "lastUpdatedOn"
		case orderBy = "orderBy"
		case pageIndex = "pageIndex"
		case pageSize = "pageSize"
		case requesterId = "requesterId"
		case to = "to"
		case unitId = "unitId"
	}

    public init( from: String?, to: String?, unitId: Int? = 1, id: Int?, requesterId: String?, pageIndex: Int){
        self.unitId = unitId
        self.id = id
        self.requesterId = requesterId
        self.pageIndex = pageIndex
    }
    
	public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		ascending = try values.decodeIfPresent(Bool.self, forKey: .ascending)
		from = try values.decodeIfPresent(String.self, forKey: .from)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
		lastUpdatedOn = try values.decodeIfPresent(String.self, forKey: .lastUpdatedOn)
		orderBy = try values.decodeIfPresent([String].self, forKey: .orderBy)
		pageIndex = try values.decodeIfPresent(Int.self, forKey: .pageIndex)
		pageSize = try values.decodeIfPresent(Int.self, forKey: .pageSize)
		requesterId = try values.decodeIfPresent(String.self, forKey: .requesterId)
		to = try values.decodeIfPresent(String.self, forKey: .to)
		unitId = try values.decodeIfPresent(Int.self, forKey: .unitId)
	}

}

//
//  AttendanceRequest.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 5/17/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public struct AttendanceRequest: ParamEncoder {
    var on : String?
    var id: Int?
    var unitId: Int?
    var assignmentTypeId: Int?
    var deviceTypeId: Int? = 1
    var ownerId: String?
    var requesterId: String?
    var time: String?
    
    public init(on: String? = nil, id: Int? = nil,unitId: Int, assignmentTypeId: Int? , deviceTypeId: Int?, ownerId: String?, requesterId: String?, time: String?) {
        self.unitId = unitId
        self.on = on
        self.id = id
        self.assignmentTypeId = assignmentTypeId
        self.deviceTypeId = deviceTypeId
        self.ownerId = ownerId
        self.requesterId = requesterId
        self.time = time
    }
}

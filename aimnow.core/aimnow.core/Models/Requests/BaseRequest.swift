//
//  BaseRequest.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 10/31/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public struct BaseRequest: ParamEncoder {
    public var unitId: Int?
    public var requesterId: String?

    private enum CodingKeys: String, CodingKey {
        case requesterId = "requesterId"
        case unitId = "unitId"
    }

    public init() {
        unitId = 1
        requesterId = SharedAppResource.instance.loggedInUser?.id
    }
}

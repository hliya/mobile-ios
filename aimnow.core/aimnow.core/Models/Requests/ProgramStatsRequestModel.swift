//
//  ProgramStatsRequestModel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/6/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation


public struct ProgramStatsRequestModel: ParamEncoder {
    var on : String
    var id: Int
    var requesterId: String
    var AssignmentTypeId: Int
    var unitId: Int
    var IsSummary: Bool
    
    public init(on: String, id: Int, requestId: String, assignmentTypeId: Int,unitId: Int, isSummary: Bool =  true) {
        self.unitId = unitId
        self.on = on
        self.id = id
        self.requesterId = requestId
        self.AssignmentTypeId = assignmentTypeId
        self.IsSummary = isSummary
    }
}


//
//  NewProgramRequestModel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 4/26/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public struct NewProgramRequestModel: ParamEncoder {
    var programID, unitId: Int
    var requesterId, requestedTo, date, time: String?

    enum CodingKeys: String, CodingKey {
        case programID = "id"
        case requesterId, requestedTo, date, time, unitId
    }
    
    public init(programID: Int, requesterId: String, requestedTo: String, date: String, time: String, unitId: Int? = 1) {
        self.programID = programID
        self.requesterId = requesterId
        self.requestedTo = requestedTo
        self.date = date
        self.time = time
        self.unitId = unitId!
    }
}

//
//  ProgramsRequestModel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/6/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public struct ProgramsRequestModel: ParamEncoder {
    var unitId: Int
    init?(unitId: Int) {
        self.unitId = unitId
    }
}

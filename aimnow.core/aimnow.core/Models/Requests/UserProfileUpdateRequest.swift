//
//  UserProfileUpdateRequest.swift
//  aimnow.core
//
//  Created by Hasitha Liyanage  on 12/3/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

import SwiftyUserDefaults

public struct UserProfileUpdateRequest {

    public var id: String?
    public var typeId: Int?
    public var title: String?
    public var firstName: String?
    public var middleName: String?
    public var lastName: String?
    public var dob: String?
    public var age: Int?
    public var phoneNumber: String?
    public var nicnumber: String?
    public var passportNumber: String?
    public var email: String?
    public var profileImage: UIImage?
    public var parentId: Int?

    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case typeId = "typeId"
        case title = "title"
        case firstName = "firstName"
        case middleName = "middleName"
        case lastName = "lastName"
        case dob = "dob"
        case age = "age"
        case phoneNumber = "phoneNumber"
        case nicnumber = "nicnumber"
        case passportNumber = "passportNumber"
        case email = "email"
        case profileImage = "profileImage"
        case parentId = "parentId"
    }
    public init(){
        
    }

    public func encode() -> [String: String] {
        
        var params = [String: String]()
        params =
            [
             "requesterId"      : "\(id!)",
             "unitId"           : "\(1)",
             "typeId"           : "\(Enums.eAttachmentFileTypes.Images.rawValue)",
             "title"            : "\(title ?? "")",
             "firstName"        : "\(firstName ?? "")",
             "middleName"       : "\(middleName ?? "")",
             "lastName"         : "\(lastName ?? "")",
             //"dob"              : "\(dob ?? "")",
             "age"              : "\(age ?? 0)",
             "nicnumber"        : "\(nicnumber ?? "")",
             "passportNumber"   : "\(passportNumber ?? "")",
             "parentId"         : "\(parentId ?? 0)",
            ]
        
        return params;
    }
}

//
//  Location.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 15, 2020
//
import Foundation
import Realm
import RealmSwift
import SwiftyJSON

@objcMembers public class Location: Object, ParamEncoder {
    
    dynamic public var id: Int = 0
    dynamic public var name: String? = nil
    dynamic public var address1: String? = nil
    dynamic public var address2: String? = nil
    dynamic public var city: String? = nil
    dynamic public var state: String? = nil
    dynamic public var zipPostalCode: String? = nil
    dynamic public var latitude: String? = nil
    dynamic public var longitude: String? = nil
    dynamic public var isActive = RealmOptional<Bool>()
    dynamic public var createdOn: String? = nil
    dynamic public var createdBy: String? = nil
    dynamic public var updatedOn: String? = nil
    dynamic public var updatedBy: String? = nil
    
    private struct SerializationKeys {
        static let id = "id"
        static let name = "name"
        static let address1 = "address1"
        static let address2 = "address2"
        static let city = "city"
        static let state = "state"
        static let zipPostalCode = "zipPostalCode"
        static let latitude = "latitude"
        static let longitude = "longitude"
        static let isActive = "isActive"
        static let createdOn = "createdOn"
        static let createdBy = "createdBy"
        static let updatedOn = "updatedOn"
        static let updatedBy = "updatedBy"
    }
    
    required override init() {
        super.init()
    }
    
    override public static func primaryKey() -> String?
    {
        return "id"
    }
    
    convenience init(json: JSON) {
        self.init()
        zipPostalCode = json[SerializationKeys.zipPostalCode].string
        createdBy = json[SerializationKeys.createdBy].string
        city = json[SerializationKeys.city].string
        address1 = json[SerializationKeys.address1].string
        state = json[SerializationKeys.state].string
        isActive.value = json[SerializationKeys.isActive].boolValue
        address2 = json[SerializationKeys.address2].string
        updatedOn = json[SerializationKeys.updatedOn].string
        updatedBy = json[SerializationKeys.updatedBy].string
        id = json[SerializationKeys.id].int ?? 0
        createdOn = json[SerializationKeys.createdOn].string
        latitude = json[SerializationKeys.latitude].string
        latitude = json[SerializationKeys.latitude].string
        name = json[SerializationKeys.name].string
    }
    
    open func getFullAddress() -> String{
        return String.init(format: "%@, %@, %@", address1 ?? "", address2 ?? "", city ?? "")
    }
}

//
//  DeviceModel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 6/30/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

public struct DeviceModel: ParamEncoder {

    public var code: String?
    public var userId: String?
    public var typeId: Int? = eDeviceType.iOS.rawValue

    private enum CodingKeys: String, CodingKey {
        case code = "code"
        case userId = "userId"
        case typeId = "typeId"
    }

    public init() {
        code = nil
        userId = nil
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
        typeId = try values.decodeIfPresent(Int.self, forKey: .typeId)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(code, forKey: .code)
        try container.encode(userId, forKey: .userId)
        try container.encode(typeId, forKey: .typeId)
    }

}

//
//  Profile.swift
//
//  Created by Isuru Ranasinghe on 9/27/20
//  Copyright (c) . All rights reserved.
//

import Foundation

public struct Profile: Codable {
    
    enum CodingKeys: String, CodingKey {
        case isActive
        case userId
        case updatedOn
        case content
        case id
        case type
    }
    
    public var isActive: Bool?
    public var userId: String?
    public var updatedOn: String?
    public var content: String?
    public var id: Int?
    public var type: Int?
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        isActive = try container.decodeIfPresent(Bool.self, forKey: .isActive)
        userId = try container.decodeIfPresent(String.self, forKey: .userId)
        updatedOn = try container.decodeIfPresent(String.self, forKey: .updatedOn)
        content = try container.decodeIfPresent(String.self, forKey: .content)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        type = try container.decodeIfPresent(Int.self, forKey: .type)
    }
    
}

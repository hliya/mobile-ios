//
//  Statistics.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 15, 2020
//
import Foundation
import Realm
import RealmSwift
import SwiftyJSON

@objcMembers public class Statistic:Object, ParamEncoder {
    
    dynamic public var id: Int = 0
    dynamic public var userId: String? = nil
    dynamic public var assigmentId = RealmOptional<Int>()
    dynamic public var score = RealmOptional<Int>()
    dynamic public var badgeTypeId = RealmOptional<Int>()
    dynamic public var isActive = RealmOptional<Bool>()
    dynamic public var createdOn: String? = nil
    dynamic public var createdBy: String? = nil
    dynamic public var updatedOn: String? = nil
    dynamic public var updatedBy: String? = nil
    
    private struct SerializationKeys {
        static let id = "id"
        static let userId = "userId"
        static let assigmentId = "assigmentId"
        static let score = "score"
        static let badgeTypeId = "badgeTypeId"
        static let isActive = "isActive"
        static let createdOn = "createdOn"
        static let createdBy = "createdBy"
        static let updatedOn = "updatedOn"
        static let updatedBy = "updatedBy"
    }
    
    required override init() {
        super.init()
    }
    
    public init(id: Int? = 0, assignmentId: Int,score: Int, userId: String) {
        self.assigmentId.value = assignmentId
        self.score.value = score
        self.id = id!
        self.userId = userId
    }
    
    override public static func primaryKey() -> String?
    {
        return "id"
    }
    
    convenience init(json: JSON) {
        self.init()
        createdBy = json[SerializationKeys.createdBy].string
        userId = json[SerializationKeys.userId].string
        badgeTypeId.value = json[SerializationKeys.badgeTypeId].int
        score.value = json[SerializationKeys.score].int
        isActive.value = json[SerializationKeys.isActive].boolValue
        assigmentId.value = json[SerializationKeys.assigmentId].int
        updatedOn = json[SerializationKeys.updatedOn].string
        id = json[SerializationKeys.id].int ?? 0
        createdOn = json[SerializationKeys.createdOn].string
        updatedBy = json[SerializationKeys.updatedBy].string
    }
    
    
    
}

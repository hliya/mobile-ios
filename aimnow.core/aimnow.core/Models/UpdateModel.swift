//
//  UpdateModel.swift
//  parent
//
//  Created by Isuru Ranasinghe on 11/21/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import SwiftyJSON

public class UpdateModel : ParamEncoder {
    
    private struct SerializationKeys {
        static let sender = "sender"
        static let receiver = "receiver"
        static let updateType = "updateType"
    }
    
    public var sender: String?
    public var receiver: String?
    public var updateType: Int?
    
    public init() {
    }
    
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        sender = json[SerializationKeys.sender].string
        receiver = json[SerializationKeys.receiver].string
        updateType = json[SerializationKeys.updateType].int
    }
}


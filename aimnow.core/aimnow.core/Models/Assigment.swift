import Foundation
import Realm
import RealmSwift

@objcMembers public class Assigment : Object, ParamEncoder {
    dynamic public var referenceId = RealmOptional<Int>()
    dynamic public var userId : String? = nil
    dynamic public var assignmentTypeId = RealmOptional<Int>()
    dynamic public var on : String?
    dynamic public var startTime : String? = nil
    dynamic public var endTime : String? = nil
    dynamic public var isRecurring = RealmOptional<Int>()
    dynamic public var locationId = RealmOptional<Int>()
    dynamic public var programTypeId = RealmOptional<Int>()
    dynamic public var reference : String? = nil
    dynamic public var location : Location?
    dynamic public var attendance : List<Attendance> = List<Attendance>()
    dynamic public var feedBacks :  List<FeedBack> = List<FeedBack>()
    dynamic public var statistics : List<Statistic> = List<Statistic>()
    dynamic public var programSummary : String?
    dynamic public var id = RealmOptional<Int>()
    dynamic public var name : String? = nil
    dynamic public var descriptionValue : String?
    dynamic public var unitId = RealmOptional<Int>()
    dynamic public var parentId = RealmOptional<Int>()
    dynamic public var masterId = RealmOptional<Int>()
    dynamic public var configId = RealmOptional<Int>()
    dynamic public var statusTypeId = RealmOptional<Int>()
    dynamic public var config : ConfigModel?
    
    enum CodingKeys: String, CodingKey {
        
        case referenceId = "referenceId"
        case userId = "userId"
        case assignmentTypeId = "assignmentTypeId"
        case on = "on"
        case startTime = "startTime"
        case endTime = "endTime"
        case isRecurring = "isRecurring"
        case locationId = "locationId"
        case programTypeId = "programTypeId"
        case reference = "reference"
        case location = "location"
        case attendance = "attendance"
        case feedBacks = "feedBacks"
        case statistics = "statistics"
        case programSummary = "programSummary"
        case id = "id"
        case name = "name"
        case description = "description"
        case unitId = "unitId"
        case parentId = "parentId"
        case masterId = "masterId"
        case configId = "configId"
        case statusTypeId = "statusTypeId"
        case config = "config"
    }
    
    override public static func primaryKey() -> String?
    {
        return "id"
    }
    
    required override init() {
        super.init()
    }
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        referenceId.value = try values.decodeIfPresent(Int.self, forKey: .referenceId)
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
        assignmentTypeId.value = try values.decodeIfPresent(Int.self, forKey: .assignmentTypeId)
        on = try values.decodeIfPresent(String.self, forKey: .on)
        startTime = try values.decodeIfPresent(String.self, forKey: .startTime)
        endTime = try values.decodeIfPresent(String.self, forKey: .endTime)
        isRecurring.value = try values.decodeIfPresent(Int.self, forKey: .isRecurring)
        locationId.value = try values.decodeIfPresent(Int.self, forKey: .locationId)
        programTypeId.value = try values.decodeIfPresent(Int.self, forKey: .programTypeId)
        reference = try values.decodeIfPresent(String.self, forKey: .reference)
        location = try values.decodeIfPresent(Location.self, forKey: .location)
        attendance = try values.decodeIfPresent(List<Attendance>.self, forKey: .attendance) ?? List<Attendance>()
        feedBacks = try values.decodeIfPresent(List<FeedBack>.self, forKey: .feedBacks) ?? List<FeedBack>()
        statistics = try values.decodeIfPresent(List<Statistic>.self, forKey: .statistics) ?? List<Statistic>()
        programSummary = try values.decodeIfPresent(String.self, forKey: .programSummary)
        id.value = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        descriptionValue = try values.decodeIfPresent(String.self, forKey: .description)
        unitId.value = try values.decodeIfPresent(Int.self, forKey: .unitId)
        parentId.value = try values.decodeIfPresent(Int.self, forKey: .parentId)
        masterId.value = try values.decodeIfPresent(Int.self, forKey: .masterId)
        configId.value = try values.decodeIfPresent(Int.self, forKey: .configId)
        statusTypeId.value = try values.decodeIfPresent(Int.self, forKey: .statusTypeId)
        config = try values.decodeIfPresent(ConfigModel.self, forKey: .config)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(location, forKey: .location)
        try container.encode(attendance, forKey: .attendance)
        try container.encode(feedBacks, forKey: .feedBacks)
        try container.encode(statistics, forKey: .statistics)
        try container.encode(programSummary, forKey: .programSummary)
        try container.encode(referenceId, forKey: .referenceId)
        try container.encode(reference, forKey: .reference)
        try container.encode(programTypeId, forKey: .programTypeId)
        try container.encode(locationId, forKey: .locationId)
        try container.encode(isRecurring, forKey: .isRecurring)
        try container.encode(endTime, forKey: .endTime)
        try container.encode(startTime, forKey: .startTime)
        try container.encode(on, forKey: .on)
        try container.encode(assignmentTypeId, forKey: .assignmentTypeId)
        try container.encode(userId, forKey: .userId)
        try container.encode(name, forKey: .name)
        try container.encode(descriptionValue, forKey: .description)
        try container.encode(unitId, forKey: .unitId)
        try container.encode(parentId, forKey: .parentId)
        try container.encode(masterId, forKey: .masterId)
        try container.encode(statusTypeId, forKey: .statusTypeId)
        try container.encode(config, forKey: .config)
        try container.encode(parentId, forKey: .parentId)
    }
}

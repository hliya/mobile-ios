import Foundation

public struct PostStatisticModel : ParamEncoder {
    let badgeTypeId : Int?
    let id : Int?
    let ownerId : String?
    let requesterId : String?
    let score : Int?
    let unitId : Int?
    let assignmentId: Int?
    var on : String?

    enum CodingKeys: String, CodingKey {
        
        case badgeTypeId = "badgeTypeId"
        case id = "id"
        case ownerId = "ownerId"
        case requesterId = "requesterId"
        case score = "score"
        case unitId = "unitId"
        case assignmentId = "assignmentId"
        case on = "on"
    }
    
    public init(id: Int?, requesterId: String, ownerId: String,score: Int, unitId: Int? = 1, badgeTypeId: Int? = nil, assignmentId: Int) {
        self.badgeTypeId = badgeTypeId
        self.id = id
        self.ownerId = ownerId
        self.requesterId = requesterId
        self.score = score
        self.unitId = unitId
        self.assignmentId = assignmentId
        self.on = Date().apiDateString(format: Constants.SERVER_DATE_FORMAT)
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        badgeTypeId = try values.decodeIfPresent(Int.self, forKey: .badgeTypeId)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        ownerId = try values.decodeIfPresent(String.self, forKey: .ownerId)
        requesterId = try values.decodeIfPresent(String.self, forKey: .requesterId)
        score = try values.decodeIfPresent(Int.self, forKey: .score)
        unitId = try values.decodeIfPresent(Int.self, forKey: .unitId)
        assignmentId = try values.decodeIfPresent(Int.self, forKey: .assignmentId)
        on = try values.decodeIfPresent(String.self, forKey: .on)
    }
    
}

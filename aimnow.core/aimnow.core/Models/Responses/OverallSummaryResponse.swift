//
//  OverallSummaryResponse.swift
//
//  Created by Isuru Ranasinghe on 6/7/20
//  Copyright (c) . All rights reserved.
//

import Foundation

public struct OverallSummaryResponse: ParamEncoder {

  enum CodingKeys: String, CodingKey {
    case month
    case programType
    case year
    case score
    case programTypeId
  }

  public var month: Int?
  public var programType: String?
  public var year: Int?
  public var score: Int?
  public var programTypeId: Int?

  public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    month = try container.decodeIfPresent(Int.self, forKey: .month)
    programType = try container.decodeIfPresent(String.self, forKey: .programType)
    year = try container.decodeIfPresent(Int.self, forKey: .year)
    score = try container.decodeIfPresent(Int.self, forKey: .score)
    programTypeId = try container.decodeIfPresent(Int.self, forKey: .programTypeId)
  }

}

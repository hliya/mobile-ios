
import Foundation

public class ProgramDetailResponse : Codable {
	
    public var program : ProgramDetail?
   
	enum CodingKeys: String, CodingKey {

		case program = "program"
		case coaches = "coaches"
		case players = "players"
	}
    
    required public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		program = try values.decodeIfPresent(ProgramDetail.self, forKey: .program)
        program?.coaches.append(objectsIn: try (values.decodeIfPresent([DependantModel].self, forKey: .coaches) ?? []))
        program?.players.append(objectsIn: try (values.decodeIfPresent([DependantModel].self, forKey: .players) ?? []))
	}

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(program, forKey: .program)
    }
}

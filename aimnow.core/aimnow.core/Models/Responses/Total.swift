
import Foundation
public struct Total : Codable {
    public let score : Double?
    public let totalCompletedPrograms : Int?
    public let totalAssignedPrograms : Int?
    public let totalProgramInMinute : Double?
    public let totalAttendaceInMinute : Double?
    public let totalLostInMinute : Double?
    public let totalBreaksInMinute : Double?
    public let precCompleted : Double?
    public let precTennisTime : Double?
    public let precAttendace : Double?

    public enum CodingKeys: String, CodingKey {

        case score = "score"
        case totalCompletedPrograms = "totalCompletedPrograms"
        case totalAssignedPrograms = "totalAssignedPrograms"
        case totalProgramInMinute = "totalProgramInMinute"
        case totalAttendaceInMinute = "totalAttendaceInMinute"
        case totalLostInMinute = "totalLostInMinute"
        case totalBreaksInMinute = "totalBreaksInMinute"
        case precCompleted = "precCompleted"
        case precTennisTime = "precTennisTime"
        case precAttendace = "precAttendace"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        score = try values.decodeIfPresent(Double.self, forKey: .score)
        totalCompletedPrograms = try values.decodeIfPresent(Int.self, forKey: .totalCompletedPrograms)
        totalAssignedPrograms = try values.decodeIfPresent(Int.self, forKey: .totalAssignedPrograms)
        totalProgramInMinute = try values.decodeIfPresent(Double.self, forKey: .totalProgramInMinute)
        totalAttendaceInMinute = try values.decodeIfPresent(Double.self, forKey: .totalAttendaceInMinute)
        totalLostInMinute = try values.decodeIfPresent(Double.self, forKey: .totalLostInMinute)
        totalBreaksInMinute = try values.decodeIfPresent(Double.self, forKey: .totalBreaksInMinute)
        precCompleted = try values.decodeIfPresent(Double.self, forKey: .precCompleted)
        precTennisTime = try values.decodeIfPresent(Double.self, forKey: .precTennisTime)
        precAttendace = try values.decodeIfPresent(Double.self, forKey: .precAttendace)
    }

}

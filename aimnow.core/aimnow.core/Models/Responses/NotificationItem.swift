//
//  PagedItems.swift
//
//  Created by Isuru Ranasinghe on 11/17/20
//  Copyright (c) . All rights reserved.
//

import Foundation

public struct NotificationItem: Codable {

  enum CodingKeys: String, CodingKey {
    case recurringTo
    case typeId
    case referenceId
    case templateId
    case updatedOn
    case message
    case isSent
    case id
    case isActive
    case sendOn
    case receiverId
    case recurringFrom
    case isRecurring
    case senderId
    case hasSeen
    case sender
  }

    public var recurringTo: String?
    public var typeId: Int?
    public var referenceId: Int?
    public var templateId: Int?
    public var updatedOn: String?
    public var message: String?
    public var isSent: Bool?
    public var id: Int?
    public var isActive: Bool?
    public var sendOn: String?
    public var receiverId: String?
    public var recurringFrom: String?
    public var isRecurring: Bool?
    public var senderId: String?
    public var hasSeen: Bool?
    public var sender: UserModel?



    public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    recurringTo = try container.decodeIfPresent(String.self, forKey: .recurringTo)
    typeId = try container.decodeIfPresent(Int.self, forKey: .typeId)
    referenceId = try container.decodeIfPresent(Int.self, forKey: .referenceId)
    templateId = try container.decodeIfPresent(Int.self, forKey: .templateId)
    updatedOn = try container.decodeIfPresent(String.self, forKey: .updatedOn)
    message = try container.decodeIfPresent(String.self, forKey: .message)
    isSent = try container.decodeIfPresent(Bool.self, forKey: .isSent)
    id = try container.decodeIfPresent(Int.self, forKey: .id)
    isActive = try container.decodeIfPresent(Bool.self, forKey: .isActive)
    sendOn = try container.decodeIfPresent(String.self, forKey: .sendOn)
    receiverId = try container.decodeIfPresent(String.self, forKey: .receiverId)
    recurringFrom = try container.decodeIfPresent(String.self, forKey: .recurringFrom)
    isRecurring = try container.decodeIfPresent(Bool.self, forKey: .isRecurring)
    senderId = try container.decodeIfPresent(String.self, forKey: .senderId)
    hasSeen = try container.decodeIfPresent(Bool.self, forKey: .hasSeen)
    sender = try container.decodeIfPresent(UserModel.self, forKey: .sender)
    }
}

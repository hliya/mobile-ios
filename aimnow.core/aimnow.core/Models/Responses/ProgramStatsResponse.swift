
import Foundation
public struct ProgramStatsResponse : Codable {
    public let total : Total?
    public let summary : [Summary]?

    enum CodingKeys: String, CodingKey {

        case total = "total"
        case summary = "summary"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        total = try values.decodeIfPresent(Total.self, forKey: .total)
        summary = try values.decodeIfPresent([Summary].self, forKey: .summary)
    }

}

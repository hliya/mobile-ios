//
//  ProgramUsersModel.swift
//
//  Created by Isuru Ranasinghe on 7/1/20
//  Copyright (c) . All rights reserved.
//

import Foundation

public struct ProgramUsersModel: Codable {

  public enum CodingKeys: String, CodingKey {
    case email
    case passportNumber
    case phoneNumber
    case parentId
    case typeId
    case firstName
    case updatedOn
    case nicnumber
    case isPresentToday
    case profileImage
    case middleName
    case title
    case isActive
    case age
    case lastName
    case dob
    case id
  }

  public var email: String?
  public var passportNumber: String?
  public var phoneNumber: String?
  public var parentId: String?
  public var typeId: Int?
  public var firstName: String?
  public var updatedOn: String?
  public var nicnumber: String?
  public var isPresentToday: Bool?
  public var profileImage: String?
  public var middleName: String?
  public var title: String?
  public var isActive: Bool?
  public var age: Int?
  public var lastName: String?
  public var dob: String?
  public var id: String?



  public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    email = try container.decodeIfPresent(String.self, forKey: .email)
    passportNumber = try container.decodeIfPresent(String.self, forKey: .passportNumber)
    phoneNumber = try container.decodeIfPresent(String.self, forKey: .phoneNumber)
    parentId = try container.decodeIfPresent(String.self, forKey: .parentId)
    typeId = try container.decodeIfPresent(Int.self, forKey: .typeId)
    firstName = try container.decodeIfPresent(String.self, forKey: .firstName)
    updatedOn = try container.decodeIfPresent(String.self, forKey: .updatedOn)
    nicnumber = try container.decodeIfPresent(String.self, forKey: .nicnumber)
    isPresentToday = try container.decodeIfPresent(Bool.self, forKey: .isPresentToday)
    profileImage = try container.decodeIfPresent(String.self, forKey: .profileImage)
    middleName = try container.decodeIfPresent(String.self, forKey: .middleName)
    title = try container.decodeIfPresent(String.self, forKey: .title)
    isActive = try container.decodeIfPresent(Bool.self, forKey: .isActive)
    age = try container.decodeIfPresent(Int.self, forKey: .age)
    lastName = try container.decodeIfPresent(String.self, forKey: .lastName)
    dob = try container.decodeIfPresent(String.self, forKey: .dob)
    id = try container.decodeIfPresent(String.self, forKey: .id)
  }
    
    public func getDisplayName() -> String {
        return String.init(format: "%@ %@", self.firstName ?? "", self.lastName ?? "")
    }
    
    public func getChatUser() -> ChatUsers {
        return ChatUsers.init(userId: id!, displayName: getDisplayName(), profileImage: profileImage ?? "", phoneNumber: phoneNumber ?? "", firstName: firstName ?? "", lastName: lastName ?? "", typeId: typeId ?? 0)
    }

}

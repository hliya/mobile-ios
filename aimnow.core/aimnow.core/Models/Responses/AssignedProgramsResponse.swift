//
//  AssignedProgramsResponse.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/6/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public struct AssignedProgramsResponse: Codable {
    public let pageIndex: Int?
    public let pageSize: Int?
    public let total: Int?
    public let assignedPrograms: [AssignedProgramModel]?

    private enum CodingKeys: String, CodingKey {
        case pageIndex = "pageIndex"
        case pageSize = "pageSize"
        case total = "total"
        case programs = "pagedItems"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pageIndex = try values.decodeIfPresent(Int.self, forKey: .pageIndex)
        pageSize = try values.decodeIfPresent(Int.self, forKey: .pageSize)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
        assignedPrograms = try values.decodeIfPresent([AssignedProgramModel].self, forKey: .programs)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(pageIndex, forKey: .pageIndex)
        try container.encode(pageSize, forKey: .pageSize)
        try container.encode(total, forKey: .total)
        try container.encode(assignedPrograms, forKey: .programs)
    }

}

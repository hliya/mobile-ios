//
//  AttachmentResponse.swift
//
//  Created by Isuru Ranasinghe on 6/4/20
//  Copyright (c) . All rights reserved.
//

import Foundation

public struct AttachmentResponse: ParamEncoder {

  enum CodingKeys: String, CodingKey {
    case typeId
    case updatedOn
    case id 
    case path
    case isActive
  }

  public var typeId: Int?
  public var updatedOn: String?
  public var id: Int?
  public var path: String?
  public var isActive: Bool?



  public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    typeId = try container.decodeIfPresent(Int.self, forKey: .typeId)
    updatedOn = try container.decodeIfPresent(String.self, forKey: .updatedOn)
    id = try container.decodeIfPresent(Int.self, forKey: .id) 
    path = try container.decodeIfPresent(String.self, forKey: .path)
    isActive = try container.decodeIfPresent(Bool.self, forKey: .isActive)
  }

}

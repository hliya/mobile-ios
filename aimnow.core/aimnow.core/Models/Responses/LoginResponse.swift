//
//  LoginResponse.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/6/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public struct LoginResponse: Codable {

    let token: String?
    let userModel: UserModel?

    private enum CodingKeys: String, CodingKey {
        case token = "token"
        case user = "user"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        token = try values.decodeIfPresent(String.self, forKey: .token)
        userModel = try values.decodeIfPresent(UserModel.self, forKey: .user)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(token, forKey: .token)
        try container.encode(userModel, forKey: .user)
    }
}


import Foundation
public struct NotificatoinResponse : Codable {
	public let pagedItems : [NotificationItem]?
	public let pageIndex : Int?
	public let pageSize : Int?
	public let total : Int?

	enum CodingKeys: String, CodingKey {

		case pagedItems = "pagedItems"
		case pageIndex = "pageIndex"
		case pageSize = "pageSize"
		case total = "total"
	}

	public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		pagedItems = try values.decodeIfPresent([NotificationItem].self, forKey: .pagedItems)
		pageIndex = try values.decodeIfPresent(Int.self, forKey: .pageIndex)
		pageSize = try values.decodeIfPresent(Int.self, forKey: .pageSize)
		total = try values.decodeIfPresent(Int.self, forKey: .total)
	}

}

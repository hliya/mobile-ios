//
//  FeedbackListResponse.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 6/4/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import SwiftyJSON

struct FeedbackListResponse: Codable {

    let pagedItems: [FeedBack]?

    private enum CodingKeys: String, CodingKey {
        case pagedItems = "pagedItems"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pagedItems = try values.decodeIfPresent([FeedBack].self, forKey: .pagedItems)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(pagedItems, forKey: .pagedItems)
    }
}

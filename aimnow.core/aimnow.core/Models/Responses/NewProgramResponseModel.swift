//
//  NewProgramResponseModel.swift
//
//  Created by Isuru Ranasinghe on 6/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

public struct NewProgramResponseModel: Codable {

  enum CodingKeys: String, CodingKey {
    case requestedBy
    case updatedOn
    case time
    case programId
    case isActive
    case id
    case statusTypeId
    case date
    case requestedTo
  }

  public var requestedBy: String?
  public var updatedOn: String?
  public var time: String?
  public var programId: Int?
  public var isActive: Bool?
  public var id: Int?
  public var statusTypeId: Int?
  public var date: String?
  public var requestedTo: String?



  public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    requestedBy = try container.decodeIfPresent(String.self, forKey: .requestedBy)
    updatedOn = try container.decodeIfPresent(String.self, forKey: .updatedOn)
    time = try container.decodeIfPresent(String.self, forKey: .time)
    programId = try container.decodeIfPresent(Int.self, forKey: .programId)
    isActive = try container.decodeIfPresent(Bool.self, forKey: .isActive)
    id = try container.decodeIfPresent(Int.self, forKey: .id)
    statusTypeId = try container.decodeIfPresent(Int.self, forKey: .statusTypeId)
    date = try container.decodeIfPresent(String.self, forKey: .date)
    requestedTo = try container.decodeIfPresent(String.self, forKey: .requestedTo)
  }

}

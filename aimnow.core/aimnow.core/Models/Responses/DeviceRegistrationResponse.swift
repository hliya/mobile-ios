//
//  DeviceRegistrationResponse.swift
//
//  Created by Isuru Ranasinghe on 7/3/20
//  Copyright (c) . All rights reserved.
//

import Foundation

public struct DeviceRegistrationResponse: Codable {

  public enum CodingKeys: String, CodingKey {
    case isActive
    case userId
    case typeId
    case id
    case code
    case createdOn
    case updatedBy
    case updatedOn
    case createdBy
  }

  public var isActive: Bool?
  public var userId: String?
  public var typeId: Int?
  public var id: Int?
  public var code: String?
  public var createdOn: String?
  public var updatedBy: String?
  public var updatedOn: String?
  public var createdBy: String?

  public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    isActive = try container.decodeIfPresent(Bool.self, forKey: .isActive)
    userId = try container.decodeIfPresent(String.self, forKey: .userId)
    typeId = try container.decodeIfPresent(Int.self, forKey: .typeId)
    id = try container.decodeIfPresent(Int.self, forKey: .id)
    code = try container.decodeIfPresent(String.self, forKey: .code)
    createdOn = try container.decodeIfPresent(String.self, forKey: .createdOn)
    updatedBy = try container.decodeIfPresent(String.self, forKey: .updatedBy)
    updatedOn = try container.decodeIfPresent(String.self, forKey: .updatedOn)
    createdBy = try container.decodeIfPresent(String.self, forKey: .createdBy)
  }
}

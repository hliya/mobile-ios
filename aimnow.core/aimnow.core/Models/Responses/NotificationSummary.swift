//
//  NotificationSummary.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 10/31/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public class NotificationSummary: ParamEncoder {
    public var total, totalUnSeen: Int?

    public init(total: Int?, totalUnSeen: Int?) {
        self.total = total
        self.totalUnSeen = totalUnSeen
    }
}


import Foundation
public struct Summary : Codable {
    public let id : Int?
    public let name : String?
    public let on : String?
    public let totalCompletedPrograms : Int?
    public let totalAssignedPrograms : Int?
    public let totalProgramInMinute : Double?
    public let totalAttendaceInMinute : Double?
    public let totalLostInMinute : Double?
    public let totalBreaksInMinute : Double?
    public let score : Double?
    public let precCompleted : Float?
    public let precTennisTime : Float?
    public let precAttendace : Float?

    public enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case on = "on"
        case totalCompletedPrograms = "totalCompletedPrograms"
        case totalAssignedPrograms = "totalAssignedPrograms"
        case totalProgramInMinute = "totalProgramInMinute"
        case totalAttendaceInMinute = "totalAttendaceInMinute"
        case totalLostInMinute = "totalLostInMinute"
        case totalBreaksInMinute = "totalBreaksInMinute"
        case score = "score"
        case precCompleted = "precCompleted"
        case precTennisTime = "precTennisTime"
        case precAttendace = "precAttendace"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        on = try values.decodeIfPresent(String.self, forKey: .on)
        totalCompletedPrograms = try values.decodeIfPresent(Int.self, forKey: .totalCompletedPrograms)
        totalAssignedPrograms = try values.decodeIfPresent(Int.self, forKey: .totalAssignedPrograms)
        totalProgramInMinute = try values.decodeIfPresent(Double.self, forKey: .totalProgramInMinute)
        totalAttendaceInMinute = try values.decodeIfPresent(Double.self, forKey: .totalAttendaceInMinute)
        totalLostInMinute = try values.decodeIfPresent(Double.self, forKey: .totalLostInMinute)
        totalBreaksInMinute = try values.decodeIfPresent(Double.self, forKey: .totalBreaksInMinute)
        score = try values.decodeIfPresent(Double.self, forKey: .score)
        precCompleted = try values.decodeIfPresent(Float.self, forKey: .precCompleted)
        precTennisTime = try values.decodeIfPresent(Float.self, forKey: .precTennisTime)
        precAttendace = try values.decodeIfPresent(Float.self, forKey: .precAttendace)
    }

}

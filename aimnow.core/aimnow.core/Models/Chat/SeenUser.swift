//
//  SeenUser.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 6/1/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class SeenUser: ParamEncoder {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let userImage = "userImage"
        static let seenTime = "seenTime"
        static let userId = "userId"
        static let firstName = "firstName"
    }
    
    // MARK: Properties
    public var userImage: String?
    public var seenTime: Date?
    public var userId: String?
    public var firstName: String?

    init() {
    }
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        userImage = json[SerializationKeys.userImage].string
        seenTime = json[SerializationKeys.seenTime].string?.apiDate()
        userId = json[SerializationKeys.userId].string
        firstName = json[SerializationKeys.firstName].string

    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = userImage { dictionary[SerializationKeys.userImage] = value }
        if let value = seenTime { dictionary[SerializationKeys.seenTime] = value.apiDateString() }
        if let value = userId { dictionary[SerializationKeys.userId] = value }
        if let value = firstName { dictionary[SerializationKeys.firstName] = value }

        return dictionary
    }
}

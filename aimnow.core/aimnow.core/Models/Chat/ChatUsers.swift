//
//  ChatUsers.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 6/1/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ChatUsers: NSObject, ParamEncoder {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let status = "status"
        static let email = "email"
        static let lastName = "lastName"
        static let displayName = "displayName"
        static let userId = "userId"
        static let firstName = "firstName"
        static let profileImageUrl = "profileImageUrl"
        static let unreadMessageCount = "unreadMessageCount"
        static let isRead = "isRead"
        static let phoneNumber = "phoneNumber"
        static let programType = "programType"
        static let typeId = "typeId"
    }
    
    // MARK: Properties
    public var status: Int?
    public var email: String?
    public var lastName: String?
    public var displayName: String?
    public var userId: String?
    public var firstName: String?
    public var profileImageUrl: String?
    public var notificationCount = 0
    public var unreadMessageCount: Int?
    public var isRead: Bool? = false
    public var isAvailable: Bool = false
    public var userState: Int = Enums.eChatUserState.None.rawValue
    public var isBot: Bool = false
    public var phoneNumber: String?
    public var programType: String?
    public var typeId: Int?
    
    public override init() {
    }
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    public init(userId: String, displayName: String, profileImage: String, phoneNumber: String, programType:String? = nil, firstName: String, lastName: String, typeId: Int) {
        self.userId = userId
        self.displayName = displayName
        self.profileImageUrl = profileImage
        self.phoneNumber = phoneNumber
        self.programType = programType
        self.firstName = firstName
        self.lastName = lastName
        self.typeId = typeId
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        status = json[SerializationKeys.status].int
        email = json[SerializationKeys.email].string
        lastName = json[SerializationKeys.lastName].string
        displayName = json[SerializationKeys.displayName].string
        userId = json[SerializationKeys.userId].string
        firstName = json[SerializationKeys.firstName].string
        profileImageUrl = json[SerializationKeys.profileImageUrl].string
        isRead = json[SerializationKeys.isRead].boolValue
        unreadMessageCount = json[SerializationKeys.unreadMessageCount].int
        phoneNumber = json[SerializationKeys.phoneNumber].string
        programType = json[SerializationKeys.programType].string
        typeId = json[SerializationKeys.typeId].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = lastName { dictionary[SerializationKeys.lastName] = value }
        if let value = displayName { dictionary[SerializationKeys.displayName] = value }
        if let value = userId { dictionary[SerializationKeys.userId] = value }
        if let value = firstName { dictionary[SerializationKeys.firstName] = value }
        if let value = profileImageUrl { dictionary[SerializationKeys.profileImageUrl] = value }
        dictionary[SerializationKeys.isRead] = isRead
        if let value = unreadMessageCount { dictionary[SerializationKeys.unreadMessageCount] = value }
        if let value = phoneNumber { dictionary[SerializationKeys.phoneNumber] = value }
        if let value = programType { dictionary[SerializationKeys.programType] = value }
        if let value = typeId { dictionary[SerializationKeys.typeId] = value }
        return dictionary
        
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? Int
        self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
        self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
        self.displayName = aDecoder.decodeObject(forKey: SerializationKeys.displayName) as? String
        self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? String
        self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
        self.profileImageUrl = aDecoder.decodeObject(forKey: SerializationKeys.profileImageUrl) as? String
        self.isRead = aDecoder.decodeBool(forKey: SerializationKeys.isRead)
        self.unreadMessageCount = aDecoder.decodeObject(forKey: SerializationKeys.unreadMessageCount) as? Int
        self.phoneNumber = aDecoder.decodeObject(forKey: SerializationKeys.phoneNumber) as? String
        self.programType = aDecoder.decodeObject(forKey: SerializationKeys.programType) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(status, forKey: SerializationKeys.status)
        aCoder.encode(email, forKey: SerializationKeys.email)
        aCoder.encode(lastName, forKey: SerializationKeys.lastName)
        aCoder.encode(displayName, forKey: SerializationKeys.displayName)
        aCoder.encode(userId, forKey: SerializationKeys.userId)
        aCoder.encode(firstName, forKey: SerializationKeys.firstName)
        aCoder.encode(profileImageUrl, forKey: SerializationKeys.profileImageUrl)
        aCoder.encode(isRead, forKey: SerializationKeys.isRead)
        aCoder.encode(unreadMessageCount, forKey: SerializationKeys.unreadMessageCount)
        aCoder.encode(phoneNumber, forKey: SerializationKeys.phoneNumber)
        aCoder.encode(programType, forKey: SerializationKeys.programType)
    }
    
    public func getDisplayName() -> String {
        return String.init(format: "%@ %@", self.firstName ?? "", self.lastName ?? "")
    }
    
}

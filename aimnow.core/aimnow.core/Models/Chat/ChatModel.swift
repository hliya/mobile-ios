//
//  ChatModel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 6/1/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ChatModel: ParamEncoder {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let conversationId = "conversationId"
        static let receiverImageUrl = "receiverImageUrl"
        static let senderImageUrl = "senderImageUrl"
        static let unReadMessageCount = "unReadMessageCount"
        static let isPrivate = "isPrivate"
        static let senderLastName = "senderLastName"
        static let senderName = "senderName"
        static let receiverName = "receiverName"
        static let conversationName = "conversationName"
        static let receiverLastName = "receiverLastName"
        static let receiver = "receiver"
        static let isRead = "isRead"
        static let sender = "sender"
        static let senderFirstName = "senderFirstName"
        static let receiverFirstName = "receiverFirstName"
        static let lastMessage = "lastMessage"
        static let lastMessageSentAt = "lastMessageSentAt"
        static let subTypeData = "subTypeData"
    }
    
    // MARK: Properties
    public var conversationId: String?
    public var receiverImageUrl: String?
    public var senderImageUrl: String?
    public var unReadMessageCount: Int?
    public var isPrivate: Bool? = false
    public var senderLastName: String?
    public var senderName: String?
    public var receiverName: String?
    public var conversationName: String?
    public var receiverLastName: String?
    public var receiver: String?
    public var isRead: Bool? = false
    public var sender: String?
    public var senderFirstName: String?
    public var receiverFirstName: String?
    public var lastMessage: String?
    public var lastMessageSentAt: Date?
    public var subTypeData: CurrentSubTypeData!

    // Default constructor
    public init() {
    }
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        conversationId = json[SerializationKeys.conversationId].string
        receiverImageUrl = json[SerializationKeys.receiverImageUrl].string
        senderImageUrl = json[SerializationKeys.senderImageUrl].string
        unReadMessageCount = json[SerializationKeys.unReadMessageCount].int
        isPrivate = json[SerializationKeys.isPrivate].boolValue
        senderLastName = json[SerializationKeys.senderLastName].string
        senderName = json[SerializationKeys.senderName].string
        receiverName = json[SerializationKeys.receiverName].string
        conversationName = json[SerializationKeys.conversationName].string
        receiverLastName = json[SerializationKeys.receiverLastName].string
        receiver = json[SerializationKeys.receiver].string
        isRead = json[SerializationKeys.isRead].boolValue
        sender = json[SerializationKeys.sender].string
        senderFirstName = json[SerializationKeys.senderFirstName].string
        receiverFirstName = json[SerializationKeys.receiverFirstName].string
        lastMessage = json[SerializationKeys.lastMessage].string
        lastMessageSentAt = json[SerializationKeys.lastMessageSentAt].string?.apiDate()
        subTypeData = CurrentSubTypeData.init(json: json[SerializationKeys.subTypeData])
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = conversationId { dictionary[SerializationKeys.conversationId] = value }
        if let value = receiverImageUrl { dictionary[SerializationKeys.receiverImageUrl] = value }
        if let value = senderImageUrl { dictionary[SerializationKeys.senderImageUrl] = value }
        if let value = unReadMessageCount { dictionary[SerializationKeys.unReadMessageCount] = value }
        dictionary[SerializationKeys.isPrivate] = isPrivate
        if let value = senderLastName { dictionary[SerializationKeys.senderLastName] = value }
        if let value = senderName { dictionary[SerializationKeys.senderName] = value }
        if let value = receiverName { dictionary[SerializationKeys.receiverName] = value }
        if let value = conversationName { dictionary[SerializationKeys.conversationName] = value }
        if let value = receiverLastName { dictionary[SerializationKeys.receiverLastName] = value }
        if let value = receiver { dictionary[SerializationKeys.receiver] = value }
        dictionary[SerializationKeys.isRead] = isRead
        if let value = sender { dictionary[SerializationKeys.sender] = value }
        if let value = senderFirstName { dictionary[SerializationKeys.senderFirstName] = value }
        if let value = receiverFirstName { dictionary[SerializationKeys.receiverFirstName] = value }
        if let value = lastMessage { dictionary[SerializationKeys.lastMessage] = value }
        if let value = lastMessageSentAt { dictionary[SerializationKeys.lastMessageSentAt] = value.apiDateString() }
        if let value = subTypeData { dictionary[SerializationKeys.subTypeData] = value }
        return dictionary
    }
    
    public func getReceiverDisplayName() -> String {
        return String.init(format: "%@ %@", self.receiverFirstName ?? "", self.receiverLastName ?? "")
    }
}



public class ConversationMessage {
    public init(conversationId: String,messages: [ChatMessageModel]) {
        self.conversationId = conversationId
        self.messages = messages
    }
    public var conversationId: String!
    public var messages: [ChatMessageModel] = []
}

public class ConversationUsers {
    public init(conversationId: String,users: [ChatUsers], createdBy:String) {
        self.conversationId = conversationId
        self.users = users
        self.createdBy = createdBy
    }
    public var conversationId: String!
    public var users: [ChatUsers] = []
    public var createdBy: String!
}


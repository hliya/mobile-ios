//
//  ChatMessageModel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 6/1/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ChatMessageModel: ParamEncoder {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let senderId = "senderId"
        static let sentDate = "sentDate"
        static let senderImage = "senderImage"
        static let message = "message"
        static let senderFirstName = "senderFirstName"
        static let senderName = "senderName"
        static let seenBy = "seenBy"
        static let isEmoteText = "isEmoteText"
        static let timestamp = "timestamp"
        static let subType = "subType"
        static let subTypeId = "subTypeId"
    }
    
    // MARK: Properties
    public var conversationMessageId: String?
    public var senderId: String?
    public var sentDate: Date?
    public var senderImage: String?
    public var message: String?
    public var senderFirstName: String?
    public var senderName: String?
    public var seenBy: [SeenUser]? = []
    public var subType: Int?
    public var timestamp: String?
    public var isEmoteText: Bool?
    public var subTypeId: String?
    public var isModifiedMessge: Bool? = false
    
    // Dfault initialization
    public init() {
        
    }
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        senderId = json[SerializationKeys.senderId].string
            sentDate = json[SerializationKeys.sentDate].string?.apiDate(applyTimeZone: true)
        senderImage = json[SerializationKeys.senderImage].string
        message = json[SerializationKeys.message].string
        senderFirstName = json[SerializationKeys.senderFirstName].string
        senderName = json[SerializationKeys.senderName].string
        seenBy = [SeenUser]()
        if let seenUsers = json[SerializationKeys.seenBy].dictionary{
            for user in seenUsers{
                seenBy?.append(SeenUser.init(json: user.value))
            }
        }
        isEmoteText = json[SerializationKeys.isEmoteText].bool
        timestamp = json[SerializationKeys.timestamp].string
        subType = json[SerializationKeys.subType].int
        subTypeId = json[SerializationKeys.subTypeId].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = senderId { dictionary[SerializationKeys.senderId] = value }
            if let value = sentDate { dictionary[SerializationKeys.sentDate] = value.apiDateString() }
        if let value = senderImage { dictionary[SerializationKeys.senderImage] = value }
        if let value = message { dictionary[SerializationKeys.message] = value }
        if let value = senderFirstName { dictionary[SerializationKeys.senderFirstName] = value }
        if let value = senderName { dictionary[SerializationKeys.senderName] = value }
        if let value = seenBy { dictionary[SerializationKeys.seenBy] = value.map { $0.dictionaryRepresentation() } }
        if let value = isEmoteText { dictionary[SerializationKeys.isEmoteText] = value }
        if let value = timestamp { dictionary[SerializationKeys.timestamp] = value }
        if let value = subType { dictionary[SerializationKeys.subType] = value }
        if let value = subTypeId { dictionary[SerializationKeys.subTypeId] = value }
        return dictionary
    }
}

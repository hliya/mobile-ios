//
//  TTConstants.swift
//  aimnow.core
//
//  Created by Hasitha Liyanage  on 7/24/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import Foundation


public struct K {
    public struct NotificationKey {
        public static let ForLoggedInUser = "kForLoggedInUser"
        public static let OnPlayerSelected = "kOnPlayerSelected"
        public static let OnProgramSelected = "kOnProgramSelected"
        public static let OnCoachSelected = "kOnCoachSelected"
        public static let OnItemSelected = "kOnItemSelected"
        public static let OnItemRemoved = "kOnItemRemoved"
    }
    
    public struct Path {
        static let Documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        static let Tmp = NSTemporaryDirectory()
    }
}

public enum E {
    public enum FilterMode : Int {
        case day = 0
        case week = 1
        case month = 2
    }
    
    public enum FilterFeedMode : Int {
        case stores = 0
        case feed = 1
    }
    
    public enum ViewMode : Int {
        case palyer = 0
        case coach = 1
    }
    
    public enum ViewType : Int {
        case none = 0
        case coach = 1
        case profile = 2
        case player = 3
        case dependant = 4
    }
    
    public enum NotificationMode : Int {
        case approval = 0
        case general = 1
    }
    
    public enum ApprovalStatus : Int {
        case none = 0
        case pending = 1
        case approved = 2
    }
}

public enum Ui {
    public struct ColorCodesHex {
        public static let Green = "#8BD250"
        public static let Gray = "#868989"
        public static let Red = "#FC3768"
        public static let Orange = "#ffa500"
        public static let Yellow = "#D4AF1B"
        public static let Blue = "#1B97D4"
        public static let Purple = "#6C28E4"
        public static let Pink = "#f400f0"
        public static let DarkGray = "#474547"
    }
}

//
//  TTNavigationBar.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/10/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit

public class TTNavigationBar: UINavigationBar {
    
    //set NavigationBar's height
    var customHeight : CGFloat = 200
    
    override public func sizeThatFits(_ size: CGSize) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width, height: customHeight)
        
    }
}

//
//  NotificationCollectionViewCell.swift
//  aimnow.core
//
//  Created by Hasitha Liyanage  on 9/3/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit 

public class NotificationCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var imgCollectionView: UICollectionView!
    @IBOutlet weak var post: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var btnPlayer: UIButton!
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var vwStatus: UIView!
    
     var item: [String:Any]? = nil
    
    public func set(item : [String: Any], mode : E.ViewMode) -> Void {
       
        //self.post.text = String(format: "%dK", (item["posts"] as? Int)!)
        //self.followers.text = String(format: "%dK", (item["followers"] as? Int)!)
        
        self.item = item;
        self.btnPlayer.isHidden = true;
        self.imgPlayer.isHidden = true;
        switch mode {
        case .coach:
            let status = E.ApprovalStatus(rawValue: (item["status"] as! Int?)!)
            if self.vwStatus != nil{
                switch(status){
                case .pending?:
                    self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Yellow)//FFCE09
                    self.lblStatus.textColor = UIColor.init(hexString: Ui.ColorCodesHex.Yellow);
                    self.lblStatus.text = "PENDING FOR APPROVAL"
                    break;
                case .approved?:
                    self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Green);
                     self.lblStatus.textColor = UIColor.init(hexString: Ui.ColorCodesHex.Green);
                     self.lblStatus.text = "APPROVED"
                    break;
                default:
                    self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Gray)
                    self.lblStatus.textColor = UIColor.init(hexString: Ui.ColorCodesHex.Gray);
                    //self.lblStatus.isHidden = true;
                    self.lblStatus.text = (item["summary"] as? String)?.uppercased()
                    self.btnPlayer.isHidden = false;
                    self.imgPlayer.isHidden = false;
                }
            }
            
            self.title.text = item["description"] as? String
            self.btnDetail.isHidden = true;
            
        case .palyer:
            var x = 0;
        }
        
        //self.btnPlayer.downloaded(from: player["avatar"] as! String);
        //self.btnPlayer.layer.borderWidth = 2.0;
        //self.btnPlayer.layer.borderColor = UIColor.init(hexString: (player["ballTypeColor"] as? String)!).cgColor;
    }
    
    @IBAction func onPlayerSelected(_ sender: UIButton) {
        
        // TODO: Is this correct?
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: K.NotificationKey.OnPlayerSelected), object: nil, userInfo: self.item);
        
    }
}

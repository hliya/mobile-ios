//
//  SearchUserCell.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 6/28/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public class SearchUserCell: UICollectionViewCell {

    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet weak var viewAvailable: UIView!
    @IBOutlet var seperatorView: UIView!
    @IBOutlet var btnChat: UIButton!
    
    public var chatUser: ChatUsers!
    
    public var disposeBag = DisposeBag()
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public var onChatUserChatObservr: PublishSubject<ChatUsers> = PublishSubject.init()

    public func setChatUser(user: ChatUsers) {
        self.chatUser = user
        self.lblName.text = user.displayName
        self.imgIcon.setRound()
        if (user.isBot) {
            let placeHolderImage = UIImage(named: "Icon_bot")!
            self.imgIcon.image = placeHolderImage
            self.viewAvailable.isHidden = true
        } else {
            let placeHolderImage = UIImage(named: "profile")!
            self.imgIcon.image = placeHolderImage
            
            if let profile = user.profileImageUrl{
                self.imgIcon.downloaded(from:  Utils.getRelativeUrlPath(path: profile));
            }else{
                self.imgIcon.image = UIImage.init(named: "profile")
            }
            if(user.isAvailable){
                viewAvailable.backgroundColor = UIColor.green
            }else{
                viewAvailable.backgroundColor = UIColor.darkGray
            }
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onUserAvailabilityChanged(_:)),
                                               name: NSNotification.Name(rawValue: Constants.Notifications.userAvailable),
                                               object: nil)
    }
    
    @objc func onUserAvailabilityChanged(_ notification:Notification) {
        guard let availability = notification.object as? Bool else {
            return
        }
        if let userId = (notification.userInfo)!["userId"] as? String, let chatUserId = self.chatUser.userId, userId == chatUserId{
            if(availability){
                self.viewAvailable.backgroundColor = UIColor.green
            }else{
                self.viewAvailable.backgroundColor = UIColor.darkGray
            }
        }
    }
    
    @IBAction func onClickChat(_ sender: UIButton) {
        self.onChatUserChatObservr.onNext(self.chatUser)
    }
    
    public override func prepareForReuse() {
        disposeBag = DisposeBag()
    }
}

//
//  GroupHeaderCollectionViewCell.swift
//  aimnow.core
//
//  Created by Hasitha Liyanage  on 8/2/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit

public class GroupHeaderCollectionViewCell : UICollectionReusableView{
    @IBOutlet public weak var lblDate: UILabel!
    @IBOutlet public weak var lblName: UILabel!
    @IBOutlet public weak var lblStat1: UILabel!
    @IBOutlet public weak var lblStat2: UILabel!
}

//
//  ScheduleCollectionViewCell.swift
//  aimnow.core
//
//  Created by Hasitha Liyanage  on 7/25/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit

public class
ScheduleCollectionViewCell : UICollectionViewCell{
    var schedule : [String:Any] = [:]
    var program: ProgramModel!
    var assignedProgram: AssignedProgramModel!
    
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var imgCoach: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime1: UILabel!
    @IBOutlet weak var lblTime2: UILabel!
    @IBOutlet weak var lblMore: UILabel!
    @IBOutlet weak var vwStatus: UIView!
    
    @IBOutlet weak var vwChatNotification: UIView?
    @IBOutlet weak var imgChatNotification: UIImageView!

    @IBOutlet weak var lblCountCoaches: UILabel!
    @IBOutlet weak var lblCountPlayers: UILabel!
    public var onRequestNewProgramClosure : (() -> ())?
    public var onChatClickClosure : ((_ program: ProgramModel) -> ())?
    
    public func set(program : ProgramModel) -> Void {
        self.program = program;
        self.lblName.text = program.name
        self.lblTime1.text = program.descriptionValue?.split(separator: "|").first?.uppercased().trim()
        self.lblTime2.text = program.descriptionValue?.split(separator: "|").last?.uppercased().trim()
        
        let enrolled = program.isEnrolled 
        self.btnRequest.isHidden = enrolled
        self.imgCoach.isHidden = !self.btnRequest.isHidden
        
        if(program.typeId.value == 7){
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Red)
        }else if(program.typeId.value == 9){
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Green)
        }else if(program.typeId.value == 8){
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Orange)
        }else{
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Gray)
        }
        if(program.hasChatNotifications){
            self.vwChatNotification?.isHidden = false
            self.imgChatNotification?.blink()
        }else{
            self.vwChatNotification?.isHidden = true
        }
        self.layoutIfNeeded()
        self.imgCoach.isHidden = true;
    }
    
    @IBAction func onClickChat(_ sender: UIButton) {
        if let closure = self.onChatClickClosure{
            closure(self.program)
        }
    }
    
    public func setForSchedule(assignedProgram : AssignedProgramModel) -> Void {
        self.assignedProgram = assignedProgram;
        let location : Location = assignedProgram.location!
        self.lblName.text = location.name
        self.lblTime1.text = assignedProgram.startTime?.date(format: "HH:mm:ss", includeUTC: false)?.toString(format: Constants.TIME_FORMAT)
        self.lblMore.text = location.getFullAddress()
        
        let canceled = location.isActive.value ?? true;
        self.lblTime2.isHidden = canceled;
        self.lblTime2.alpha = canceled ? 0.3 : 1;
        
        if(assignedProgram.programTypeId.value == 7){
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Red)
        }else if(assignedProgram.programTypeId.value == 9){
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Green)
        }else if(assignedProgram.programTypeId.value == 8){
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Orange)
        }
        if canceled {
            self.contentView.alpha = 0.2;
        }else{
            self.contentView.alpha = 1;
        }
    }
    
    public func setForSchedule(schedule : [String: Any]) -> Void {
        let location : [String:Any] = schedule["location"] as! [String : Any]
        self.lblName.text = location["name"] as? String;
        self.lblTime1.text = location["time"] as? String;
        self.lblMore.text = location["venue"] as? String;
        
        let canceled = location["canceled"] as! Bool;
        self.lblTime2.isHidden = canceled;
        self.lblTime2.alpha = canceled ? 0.3 : 1;
        
        if canceled {
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Green)
        }else {
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Gray)
            self.contentView.alpha = 0.3;
        }
    }
    
    public func setForSchedule(schedule : [String: Any], mode : E.ViewMode) -> Void {
        let location : [String:Any] = schedule["location"] as! [String : Any]
        let program : [String:Any] = schedule["program"] as! [String : Any]
        
        self.lblName.text = program["name"] as? String;
        self.lblTime1.text = location["time"] as? String;
        
        let canceled = location["canceled"] as! Bool;
        let coaches = schedule["coaches"] as! Array<[String:Any]>
        let players = schedule["players"] as! Array<[String:Any]>
        
        if !canceled {
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Green)
        }else {
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Gray)
            self.contentView.alpha = 0.3;
        }
        
        switch (mode){
        case E.ViewMode.coach:
            self.lblCountCoaches.text = String(format: "%d", coaches.count)
            self.lblCountPlayers.text = String(format: "%d", players.count)
        default:
            self.lblTime2.isHidden = canceled;
            self.lblMore.text = location["venue"] as? String;
        }
    }
    
    public func setCoach(program : AssignedProgramModel) -> Void {
        self.program = program;
        let location : Location = program.location!
        self.contentView.alpha = 1.0
        
        self.lblName.text = program.name
        //self.lblTime1.text = location.name
        self.lblTime1.text =
            (program.startTime?.date(format: "HH:mm:ss", includeUTC: false)?.toString(format: Constants.TIME_FORMAT))! + " - " +
            (program.endTime?.date(format: "HH:mm:ss", includeUTC: false)?.toString(format: Constants.TIME_FORMAT))!
        
        let canceled = program.statusTypeId.value == Enums.eStatus.Completed.rawValue
        let coaches = program.totalCoaches
        let players = program.totalPlayers
        
        if !canceled {
            if(program.programTypeId.value == 7){
                self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Red)
            }else if(program.programTypeId.value == 9){
                self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Green)
            }else if(program.programTypeId.value == 8){
                self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Orange)
            }
        }else {
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Gray)
            self.contentView.alpha = 0.3
        }
        self.lblCountCoaches.text = String(format: "%d", coaches)
        self.lblCountPlayers.text = String(format: "%d", players)
    }
    
    public func setCoachWeekView(program : AssignedProgramModel) -> Void {
        self.program = program;
        let location : Location = program.location!
        self.contentView.alpha = 1.0

        self.lblName.text = program.name
        self.lblTime1.text = program.startTime?.date(format: "HH:mm:ss", includeUTC: false)?.toString(format: Constants.TIME_FORMAT)
        
        let canceled = program.statusTypeId.value == Enums.eStatus.Completed.rawValue
        let coaches = program.totalCoaches
        let players = program.totalPlayers
        
        if !canceled {
            if(program.programTypeId.value == 7){
                self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Red)
            }else if(program.programTypeId.value == 9){
                self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Green)
            }else if(program.programTypeId.value == 8){
                self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Orange)
            }
        }else {
            self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Gray)
            self.contentView.alpha = 0.3
        }
        self.lblCountCoaches.text = String(format: "%d", coaches)
        self.lblCountPlayers.text = String(format: "%d", players)
    }
    
    
    @IBAction func onRequestNewProgram(){
        if let closure = self.onRequestNewProgramClosure{
            closure()
        }
    }
}

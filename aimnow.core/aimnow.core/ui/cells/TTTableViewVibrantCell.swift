//
//  TTTableViewVibrantCell.swift
//  aimnow.core
//
//  Created by Hasitha Liyanage  on 11/24/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import SideMenu

public class TTTableViewVibrantCell : UITableViewVibrantCell
{
    @IBOutlet weak var badge: UILabel!
    
    
    public func setBadge(badge:Int){
        if(self.badge != nil){
            self.badge.text = "\(badge)";
        }
    }
}

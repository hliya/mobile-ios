//
//  PlayerCollectionViewCell.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/25/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit 
import Nuke
import AlamofireImage

public class PlayerCollectionViewCell : UICollectionViewCell{
    @IBOutlet weak var btnPlayer: UIButton!
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblStage: UILabel!
    @IBOutlet weak var vwStatus: UIView!
    let downloader = ImageDownloader()
    
    var player: DependantModel?
    
    public func set(player : DependantModel , selected : Bool) -> Void {
        
        self.player = player;
        if let profile = player.profileImage{
            let urlRequest = URLRequest(url: URL(string: Utils.getRelativeUrlPath(path: profile))!)
            downloader.download(urlRequest, completion:  { response in
                if case .success(let image) = response.result {
                    self.imgPlayer.image = image.af.imageRoundedIntoCircle()
                }else{
                    print(response.error?.localizedDescription ?? "Something went wrong")
                }
            })
            
            self.imgPlayer.af.setImage(withURL: URL.init(string: Utils.getRelativeUrlPath(path: profile))!)
        }else{
            self.imgPlayer.image = UIImage.init(named: "profile")?.af.imageRoundedIntoCircle()
        }
        self.lblName.text = player.firstName
        self.imgPlayer.layer.cornerRadius = self.imgPlayer.bounds.width / 2
        self.imgPlayer.layer.borderWidth = 2.0;
        self.imgPlayer.layer.borderColor = UIColor.init(hexString: player.ballTypeColor).cgColor;
        self.btnPlayer.layer.borderColor = UIColor.clear.cgColor
    }
    
    public func set(player : DependantModel, mode : E.ViewMode) -> Void {
        self.player = player
        self.lblName.text = player.firstName
        let enrolled = player.isEnrolled 
        if self.vwStatus != nil{
            if enrolled! {
                self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Green)
            }else {
                self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Gray)
            }
        }
        
        self.lblName.text = player.firstName! + " " + player.lastName!
        if self.lblAge != nil, let age = player.age.value {
            self.lblAge.text = "\(age)"
        }
        
        if self.lblStage != nil{
            if let stageName = player.currentStage?.name, let stage = stageName.split(separator: " ").last {
                self.lblStage.text =  "\(stage)"
            }
        }
        
        if let profile = player.profileImage{
            self.btnPlayer.downloaded(from: Utils.getRelativeUrlPath(path: profile));
        }else{
            self.btnPlayer.setImage(UIImage.init(named: "profile"), for: .normal)
        }
        self.btnPlayer.layer.borderWidth = 2.0;
        self.btnPlayer.layer.borderColor = UIColor.init(hexString: player.ballTypeColor).cgColor;
    }
    
    public func setOverViewFor(player : DependantModel, mode : E.ViewMode) -> Void {
        
        self.player = player
        self.lblName.text = player.firstName
        let enrolled = player.isEnrolled
        if self.vwStatus != nil{
            if enrolled! {
                self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Green)
            }else {
                self.vwStatus.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Gray)
            }
        }
        
        if (!(player.isPresentToday.value ?? false)){
            self.contentView.alpha = 0.3
            self.contentView.isUserInteractionEnabled = false
        }else{
            self.contentView.alpha = 1.0
            self.contentView.isUserInteractionEnabled = true
        }
        
        self.lblName.text = player.firstName
        if self.lblAge != nil, let age = player.age.value {
            self.lblAge.text = "\(age)"
        }
        
        if self.lblStage != nil{
            if let stageName = player.currentStage?.name, let stage = stageName.split(separator: " ").last {
                self.lblStage.text =  "\(stage)"
            }
        }
        
        if let profile = player.profileImage{
            self.btnPlayer.downloaded(from: Utils.getRelativeUrlPath(path: profile));
        }else{
            self.btnPlayer.setImage(UIImage.init(named: "profile"), for: .normal)
        }
        self.btnPlayer.layer.borderWidth = 2.0;
        self.btnPlayer.layer.borderColor = UIColor.init(hexString: player.ballTypeColor).cgColor;
    }
    
    
    public func set(player : [String : Any], mode : E.ViewMode) -> Void {
        
        
    }
    
    
    @IBAction func onPlayerSelected(_ sender: UIButton) {
        Observers.instance.playerSelectObsrver.onNext(self.player)
    }
}

//
//  GlobleChatUserViewCell.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 6/28/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import RxSwift

public class GlobleChatUserViewCell: UICollectionViewCell {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewAvailable: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var chatCount: UIView!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet var seperatorView: UIView!
    
    public var disposeBag:DisposeBag = DisposeBag()
    var userId : String?
    public func setChatUserDetails(user: ChatUsers) {
        self.lblName.text = user.getDisplayName()
        self.imgProfile.setRound()
        self.userId = user.userId
        let placeHolderImage = UIImage(named: "profile")!
        self.imgProfile.image = placeHolderImage
        if let profile = user.profileImageUrl{
            self.imgProfile.downloaded(from: Utils.getRelativeUrlPath(path: profile));
        }else{
            self.imgProfile.image = UIImage.init(named: "profile")
        }
        self.lblNote.isHidden = true
        FirebaseHelper.instance.initiateUserAvailability(userId: self.userId!)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onUserAvailabilityChanged(_:)),
                                               name: NSNotification.Name(rawValue: Constants.Notifications.userAvailable),
                                               object: nil)
    }
    
    public func setConversationDetails(conversation: ChatModel) {
        self.lblTime.isHidden = true
        if let date = conversation.lastMessageSentAt {
            self.lblTime.isHidden = false
            self.lblTime.text = date.toString(format: Constants.CHAT_DATE_DISPLAY_FORMAT)
        }
        
        self.lblName.text = conversation.conversationName ?? ""
        self.imgProfile.setRound()
        if(!(conversation.isPrivate ?? true)){
            let placeHolderImage = UIImage.init(named: "icon_default_group_image")
            self.imgProfile.image = placeHolderImage
            self.viewAvailable.isHidden = true
        }else{
            self.imgProfile.image = UIImage.init(named: "profile")
            if let profile = conversation.receiverImageUrl{
                self.imgProfile.downloaded(from: Utils.getRelativeUrlPath(path: profile));
            }
            self.viewAvailable.isHidden = false
        }
        if let count = conversation.unReadMessageCount, count > 0 {
            self.chatCount.isHidden = false
            self.lblCount.text = "\(count)"
        } else {
            self.chatCount.isHidden = true
        }
        
        if let lastMessage = conversation.lastMessage{
            self.lblNote.text = lastMessage
            self.lblNote.isHidden = false
        } else {
            self.lblNote.isHidden = true
        }
        
        if let receiverId = conversation.receiver {
            self.userId = receiverId
            FirebaseHelper.instance.initiateUserAvailability(userId: self.userId!)
        } else{
            self.viewAvailable.backgroundColor = UIColor.darkGray
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onUserAvailabilityChanged(_:)),
                                               name: NSNotification.Name(rawValue: Constants.Notifications.userAvailable),
                                               object: nil)
    }
    
    @objc func onUserAvailabilityChanged(_ notification:Notification) {
        guard let availability = notification.object as? Bool else {
            return
        }
        if let userId = (notification.userInfo)!["userId"] as? String, let chatUserId = self.userId, userId == chatUserId{
            if(availability){
                self.viewAvailable.backgroundColor = UIColor.green
            }else{
                self.viewAvailable.backgroundColor = UIColor.darkGray
            }
        }
    }
    
    public override func prepareForReuse() {
        self.disposeBag = DisposeBag()
    }
}

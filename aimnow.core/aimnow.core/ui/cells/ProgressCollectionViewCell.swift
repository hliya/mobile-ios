//
//  ProgressCollectionViewCell.swift
//  aimnow.core
//
//  Created by Hasitha Liyanage  on 8/4/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit

public class ProgressCollectionViewCell : UICollectionViewCell{
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStat1: UILabel!
    @IBOutlet weak var btnProgress: UIButton!
    @IBOutlet weak var progressSlider: UISlider!
    
    var onProgressChanged : ((_ progressValue: Float) -> ())?

    
    var stat: [String:Any]? = nil
    
    public func set(stat : [String: Any]) -> Void {
        self.lblName.text = stat["name"] as? String
        
        let precentage : Int = stat["stat"] as! Int;
        self.stageCloours(precentage: precentage)
    }
    
    
    public func set(stat : ProgramProgressDetail, onProgressChanged : ((_ progressValue: Float) -> ())? = nil) -> Void {
        self.lblName.text = stat.name
        self.onProgressChanged = onProgressChanged
        let precentage : Int = stat.stat ?? 0
        self.lblStat1.textColor = UIColor.black
        
        self.stageCloours(precentage: precentage)
        
        self.progressSlider?.maximumValue = Float(stat.maxValue ?? 100) * 0.1
        self.progressSlider?.value = Float(precentage) * 0.1
    }
    
    @IBAction func sliderValueChanged(_ slider: UISlider, _ event: UIEvent) {
        guard let touch = event.allTouches?.first, touch.phase == .ended else {
            return
        }
        if let progressClosure = self.onProgressChanged {
            progressClosure(slider.value)
        }
        slider.value = 0
//        self.stageCloours(precentage: Int(slider.value * 10))
    }
    
    fileprivate func stageCloours(precentage: Int) {
        if precentage >= 0 && precentage <= 25 {
            self.lblStat1.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Yellow)
        } else if precentage > 25 && precentage <= 50 {
            self.lblStat1.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Orange)
        } else if precentage > 50 && precentage <= 75 {
            self.lblStat1.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Blue)
        }else{
            self.lblStat1.backgroundColor = UIColor.init(hexString: Ui.ColorCodesHex.Green)
        }
        self.lblStat1.text =  "\(Int(Double(precentage) * 0.1))";
    }
    
    public func setStage(stat : [String: Any], index :  Int) -> Void {
        
        let current: Int = stat["current_stage"] as! Int
        
        if  current > (index + 1) {
            self.btnProgress.setImage(UIImage(named: "pro-green"), for: UIControl.State.normal)
        } else if (index + 1) == current{
            self.btnProgress.setImage(UIImage(named: "pro-inprogress"), for: UIControl.State.normal)
        }else {
            self.btnProgress.setImage(UIImage(named: "pro-prending"), for: UIControl.State.normal)
        }
    }
    
    public func setStage(programStagesInfo : ProgramStagesInfo, index :  Int) -> Void {
        let current: Int = programStagesInfo.currentStage
        if  current > (index + 1) {
            self.btnProgress.setImage(UIImage(named: "pro-green"), for: UIControl.State.normal)
        } else if (index + 1) == current{
            self.btnProgress.setImage(UIImage(named: "pro-inprogress"), for: UIControl.State.normal)
        }else {
            self.btnProgress.setImage(UIImage(named: "pro-prending"), for: UIControl.State.normal)
        }
    }
    
    public override func prepareForReuse() {
        self.progressSlider?.value = 0
    }
}

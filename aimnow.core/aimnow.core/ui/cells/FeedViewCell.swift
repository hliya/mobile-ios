//
//  FeedViewCell.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/15/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit

public class FeedViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {
    

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var imgCollectionView: UICollectionView!
    @IBOutlet weak var post: UILabel!
    @IBOutlet weak var followers: UILabel!
    
    public var items: Array<[String:Any]> = []
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:ImageViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! ImageViewCell
      
        cell.set(item: self.items[indexPath.row])
        
        return cell
    }
    
    public func set(item : [String: Any]) -> Void {
        self.title.text = item["name"] as? String
        self.subTitle.text = item["last_updated"] as? String
        self.post.text = String(format: "%dK", (item["posts"] as? Int)!)
        self.followers.text = String(format: "%dK", (item["followers"] as? Int)!)
        self.items = (item["items"] as? Array<[String:Any]>)!
        self.imgCollectionView.reloadData()
    }
}

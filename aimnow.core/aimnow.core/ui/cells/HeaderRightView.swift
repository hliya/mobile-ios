//
//  HeaderRightView.swift
//  aimnow
//
//  Created by Hasitha Liyanage  on 7/25/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit

public protocol HeaderViewDelegate {
    func onChatClick()
}

public class HeaderRightView : UIView {
    @IBOutlet weak var lblPlayer: UILabel!
    @IBOutlet weak var lblSchedule: UILabel!
    @IBOutlet weak var imgPlayer: UIImageView!
    
    public var delegate: HeaderViewDelegate?
    var player: DependantModel? = nil
    var selectedProgram: ProgramModel? = nil
    
    public func set(player : DependantModel) -> Void {
        self.player = player;
        self.lblPlayer?.text = player.firstName
        if let profile = player.profileImage{
            self.imgPlayer?.downloaded(from: Utils.getRelativeUrlPath(path: profile));
        }else{
            self.imgPlayer?.image = UIImage.init(named: "profile")
        }
        self.imgPlayer?.layer.borderWidth = 2.0;
        self.imgPlayer?.layer.borderColor = UIColor.init(hexString: player.ballTypeColor).cgColor;
    }

    public func setProgram(selectedProgram : ProgramModel?) -> Void {
        if let lblCurrentSchedul = self.lblSchedule{
            print(selectedProgram?.typeId.value)
            if let selectedProgram = selectedProgram {
                self.selectedProgram = selectedProgram;
                if let assignedProgram = selectedProgram as? AssignedProgramModel, let programTypeId = assignedProgram.programTypeId.value{
                    if(programTypeId == 7){
                        self.imgPlayer?.layer.borderColor = UIColor.init(hexString: Ui.ColorCodesHex.Red).cgColor;
                    }else if(programTypeId == 8){
                        self.imgPlayer?.layer.borderColor = UIColor.init(hexString: Ui.ColorCodesHex.Orange).cgColor;
                    }else if(programTypeId == 9){
                        self.imgPlayer?.layer.borderColor = UIColor.init(hexString: Ui.ColorCodesHex.Green).cgColor;
                    }
                }else if let programTypeId = selectedProgram.typeId.value{
                    if(programTypeId == 7){
                        self.imgPlayer?.layer.borderColor = UIColor.init(hexString: Ui.ColorCodesHex.Red).cgColor;
                    }else if(programTypeId == 8){
                        self.imgPlayer?.layer.borderColor = UIColor.init(hexString: Ui.ColorCodesHex.Orange).cgColor;
                    }else if(programTypeId == 9){
                        self.imgPlayer?.layer.borderColor = UIColor.init(hexString: Ui.ColorCodesHex.Green).cgColor;
                    }
                }
                lblCurrentSchedul.text = selectedProgram.name
            }else{
                lblCurrentSchedul.text = ""
            }
        }
    }
    
    public func setSchedule(schedule : [String: Any]) -> Void {
        if let lblCurrentSchedul = self.lblSchedule{
            lblCurrentSchedul.text = schedule["name"] as? String
        }
    }
    
    @IBAction func onChatSelect(_ sender: UIButton) {
        if let delegate = self.delegate{
            delegate.onChatClick()
        }
    }
}

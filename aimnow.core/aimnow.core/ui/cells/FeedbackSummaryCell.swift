//
//  carouselView.swift
//  coach
//
//  Created by Isuru Ranasinghe on 6/3/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import AlamofireImage
import ImageViewer

public class FeedbackSummaryCell: UICollectionViewCell {

    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtComment: UITextView!
    
    var feedback: FeedBack!
    public var onFeedbackVideoSelect: ((_ feedBack: FeedBack) ->())?
    public var onFeedbackImageSelect: ((_ imageView: UIImageView) ->())?

    public func setFeedbackInfo(feedback: FeedBack) {
        self.feedback = feedback
        self.txtComment.textContainer.lineFragmentPadding = 0
        self.txtComment.textContainerInset = .init(top: -2, left: 0, bottom: 0, right: 0)
        self.imgVideo.image = UIImage.init(named: "dummy")
        self.txtComment.text = feedback.comment
        if let attachment = feedback.attachment{
            self.imageWidthConstraint?.constant = 100
            if let attachmentPath = feedback.attachment?.path {
                if(attachment.typeId.value == Enums.eAttachmentFileTypes.Images.rawValue){
                    self.imgVideo.downloaded(from: Utils.getRelativeUrlPath(path: attachmentPath))
                }else{
                    Utils.getThumbForVideo(videoPath: URL.init(string: Utils.getRelativeUrlPath(path: attachmentPath))!, completion: { image in
                        self.imgVideo.image = image
                    })
                }
            }else{
//                 self.imageWidthConstraint?.constant = 0
            }
        }else{
//            self.imageWidthConstraint?.constant = 0
        }
    }
    @IBAction func onClickVideo(_ sender: UIButton) {
        if(feedback.attachment?.typeId.value == Enums.eAttachmentFileTypes.Images.rawValue){
            if let imageClosure = self.onFeedbackImageSelect{
                imageClosure(imgVideo)
            }
        }else{
            if let videoClasure = self.onFeedbackVideoSelect{
                videoClasure(feedback)
            }
        }
    }
    
    public override func prepareForReuse() {
        self.txtComment.text = ""
        self.imgVideo.image = nil
    }
}

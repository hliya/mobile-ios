//
//  ImageViewCell.swift
//  aimnow.core
//
//  Created by Hasitha Liyanage  on 8/14/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//
import UIKit
import AlamofireImage


public class ImageViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    public func set(item : [String: Any]) -> Void {
        self.img.downloaded(from: item["url"] as! String)
    }
}

public class ButtonViewCell: UICollectionViewCell {
    
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var lblName: UILabel!
    var onActionPerformed : ((Enums.eActions) -> ())?

    var item : [String: Any] = [String: Any]()
    var user: DependantModel!
    let downloader = ImageDownloader()

    public func set(item : [String: Any], mode : E.ViewMode) -> Void {
        self.item = item
        self.lblName.text = item["name"] as? String
        self.btnImage.downloaded(from: item["avatar"] as! String);
    }
    
    public func set(user : DependantModel, mode : E.ViewMode, onActionPerformed : @escaping ((Enums.eActions) -> ())) -> Void {
        self.onActionPerformed = onActionPerformed
        self.user = user
        if(user.id == SharedAppResource.instance.loggedInUser?.id){
            self.lblName.text = "Me"
        }else{
            self.lblName.text = user.firstName
        }
        if let user = user.profileImage{
            let urlRequest = URLRequest(url: URL(string: Utils.getRelativeUrlPath(path: user))!)
            downloader.download(urlRequest, completion:  { response in
                if case .success(let image) = response.result {
                    let image = image.resizeWith(width: self.btnImage.bounds.width)
                    self.btnImage.setImage(image, for: .normal)
                }else{
                    print(response.error?.localizedDescription as Any)
                }
            })
        }else{
            self.btnImage.setImage(UIImage.init(named: "profile"), for: .normal)
        }
    }
    
    @IBAction func onItemSelected(_ sender: UIButton) {
        if let closure = self.onActionPerformed{
            closure(Enums.eActions.Add)
        }
    }
    
    @IBAction func onRemoveSelected(_ sender: UIButton) {
        if let closure = self.onActionPerformed{
            closure(Enums.eActions.Remove)
        }
    }
}

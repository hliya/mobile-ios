//
//  ChatViewTableViewCell.swift
//  aimnow.core
//
//  Created by Hasitha Liyanage  on 8/9/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import UIKit

public class ChatViewTableViewCell : UITableViewCell{
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var imgSender: UIImageView!
    
    public func set(stat : [String: Any]) -> Void {
        lblMessage.text = stat["message"] as? String
        imgSender.downloaded(from: (stat["avatar"] as? String)!)
        
        let status = stat["status"] as? Bool
         imgStatus.image = UIImage(named: status! ? "small-circle-green" : "")
    }
    
    public func setCoaches(stat : [String: Any]) -> Void {
        lblMessage.text = stat["name"] as? String
        imgSender.downloaded(from: (stat["avatar"] as? String)!)
    }
}

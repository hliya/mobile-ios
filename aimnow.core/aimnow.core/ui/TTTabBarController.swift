//
//  TTTabBarController.swift
//  aimnow.core
//
//  Created by Hasitha Liyanage  on 11/24/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public class TTTabBarController: UITabBarController {

    public override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onConversationModified(_:)),
                                               name: NSNotification.Name(rawValue: Constants.Notifications.multipartyChatModified),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onNotificationModified(_:)),
                                               name: NSNotification.Name(rawValue: Constants.Notifications.notificationsAvailable),
                                               object: nil)
        
        self.setBadgeForMessageNotifications(value: "0");
        self.setBadgeForCommonNotifications(value: "0");
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self,name: NSNotification.Name(rawValue: Constants.Notifications.multipartyChatModified), object: nil)
        NotificationCenter.default.removeObserver(self,name: NSNotification.Name(rawValue: Constants.Notifications.notificationsAvailable), object: nil)
    }


    public func setBadgeForMessageNotifications(value:String) {
        //guard let item = tabBar.selectedItem else { return }
        guard let items = tabBar.items else { return }
        if(items.count == 4){
            items[2].badgeValue = value
        }else{
            items[1].badgeValue = value
        }
    }
    
    public func setBadgeForCommonNotifications(value:String) {
        //guard let item = tabBar.selectedItem else { return }
        guard let items = tabBar.items else { return }
        if(items.count == 4){
            items[3].badgeValue = value
        }
    }

    @objc func onConversationModified(_ notification:Notification) {
        guard let conversation = notification.object as? ChatModel else {
            return
        }
        self.setBadgeForMessageNotifications(value: "\(conversation.unReadMessageCount ?? 0)")
    };
    
    @objc func onNotificationModified(_ notification:Notification) {
        guard let conversation = notification.object as? NotificationSummary else {
            return
        }
        self.setBadgeForCommonNotifications(value: "\(conversation.totalUnSeen ?? 0)")
    };
}

//
//  AnowRefresher.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 10/18/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import UIKit
import PullToRefreshKit

public class AnowRefresher: UIView {
    public static func header() -> DefaultRefreshHeader {
        let header = DefaultRefreshHeader.header()
        header.setText("Pull to refresh", mode: .pullToRefresh)
        header.setText("Release to refresh", mode: .releaseToRefresh)
        header.setText("Success", mode: .refreshSuccess)
        header.setText("Refreshing...", mode: .refreshing)
        header.setText("Failed", mode: .refreshFailure)
        header.tintColor = UIColor(named: "FontDarkBlue")
        header.imageRenderingWithTintColor = true
        header.durationWhenHide = 0.4
        return header
    }

    public static func footer() -> DefaultRefreshFooter {
        let footer = DefaultRefreshFooter.footer()
        footer.setText("Pull up to refresh", mode: .pullToRefresh)
        footer.setText("No data any more", mode: .noMoreData)
        footer.setText("Refreshing...", mode: .refreshing)
        footer.setText("Tap to load more", mode: .tapToRefresh)
        footer.textLabel.textColor  = UIColor(named: "FontDarkBlue")
        footer.refreshMode = .scroll
        return footer
    }
}

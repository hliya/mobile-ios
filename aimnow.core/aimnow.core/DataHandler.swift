//
//  DataHandler.swift
//  aimnow.core
//
//  Created by Hasitha Liyanage  on 7/24/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

import Foundation
import SwiftyUserDefaults

public class DataHandler {
    
    public static let shared = DataHandler()
    public var programs = [[String:Any]]()
    //public var grouped = [Date? :[[String:Any]]]()
    public var grouped = [(key: Date?, value: [[String : Any]])]()
        
    public func getProgramsStatistics(player:[String:Any],
                               onbefore: @escaping(Bool) -> Void,
                               completion: @escaping(_ result: [String:Any]?, _ error: Error?) -> Void) {
        
        onbefore(true)
        
        var arr = [String:Any]()
        arr["tenisTime"] = Int.random(in: 10 ..< 100)
        arr["completed"] = Int.random(in: 10 ..< 100)
        arr["attendace"] = Int.random(in: 10 ..< 100)
        
        completion(arr,nil);
    }
    
    public func getProgramProgress(player:[String:Any],
                                   program:[String:Any],
                                   filterBy : E.FilterMode,
                                   onbefore: @escaping(Bool) -> Void,
                                   completion: @escaping(_ result: [(key: String?, value: [[String : Any]])]?,_ summary: Array<[String:Any]>?, _ error: Error?) -> Void) {
        
        onbefore(true)
        
        var cprogram = [String:Any]();
        for (_, element) in programs.enumerated() {
            if (element["id"] as? String) == (program["id"] as? String) {
                cprogram = element
                break;
            }
        }
        
        let categories = ["Skills", "Tactics"];
        let scategories =  ["Tracking (watch & judge)", "Balancing",  "Skipping"];
        let svedios =  ["https://www.youtube.com/watch?v=KwxPQgyAzhI", "https://www.youtube.com/watch?v=IsHBVdZeWOE",  "https://www.youtube.com/watch?v=mJJ0EXrs2yc"];
        let tcategories =  ["Having racquet control on forehand, backhand & serve"];
        let tvedios = ["https://www.youtube.com/watch?v=aZj7DIEftPg"]
        var arrProgress = Array<[String:Any]>()
        
        var summary =  Array<[String:Any]>()
        
        for (index, element) in categories.enumerated() {
            if index == 0 {
                var total = 0;
                var s = [String:Any]()
                for (i, el2) in scategories.enumerated() {
                    var v = [String:Any]()
                    v["type"] = element
                    v["name"] = el2
                    v["stat"] = Int.random(in: 10 ..< 100)
                    v["vedio"] = svedios[i]
                        
                    total = total +  ((v["stat"] as? Int)!)
                    arrProgress.append(v)
                }
                s["name"] = element
                s["total"] = total
                s["precentage"] =  Float((((scategories.count * 100) - total) * 100 / (scategories.count * 100) ))
                summary.append(s)
                
            } else  if index == 1 {
                var total = 0;
                var s = [String:Any]()
                for (i, el2) in tcategories.enumerated() {
                    var v = [String:Any]()
                    v["type"] = element
                    v["name"] = el2
                    v["stat"] = Int.random(in: 10 ..< 100)
                    v["vedio"] = tvedios[i]
                    total = total +  ((v["stat"] as? Int)!)
                    arrProgress.append(v)
                }
                s["name"] = element
                s["total"] = total
                s["precentage"] = Float((((tcategories.count * 100) - total) * 100 / (tcategories.count * 100) ))
                summary.append(s)
            }
        }
        
        let grouped = Dictionary(grouping: arrProgress, by: { $0["type"] as? String } )
        let ordered = grouped.sorted { $0.0! < $1.0! }
        
        completion(ordered, summary, nil);
    }
    
    public func getRecentChatsForProgram(player:[String:Any],
                                     program:[String:Any],
                                     onbefore: @escaping(Bool) -> Void,
                                     completion: @escaping(_ result: Array<[String:Any]>?, _ error: Error?) -> Void) {
        
        onbefore(true)
        
        var arr = Array<[String:Any]>();
        
        let items =  [1,2];
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        for (i, el2) in items.enumerated() {
            var v = [String:Any]()
            v["id"] = el2
            v["message"] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur"
            v["date"] = dateFormatterGet.string(from:  _generateRandomDate(daysBack: i)!)
            v["time"] = el2
            switch(Int.random(in:0 ..< 3)){
            case 0 : v["avatar"] = "https://tinyfac.es/data/avatars/BA0CB1F2-8C79-4376-B13B-DD5FB8772537-200w.jpeg"
            case 1 : v["avatar"] = "https://tinyfac.es/data/avatars/AEF44435-B547-4B84-A2AE-887DFAEE6DDF-200w.jpeg"
            case 2 : v["avatar"] = "https://tinyfac.es/data/avatars/03F55412-DE8A-4F83-AAA6-D67EE5CE48DA-200w.jpeg"
            default:
                v["avatar"] = "https://tinyfac.es/data/avatars/FBEBF655-4886-455A-A4A4-D62B77DD419B-200w.jpeg"
            }
            v["status"] = Bool.random()
            
            arr.append(v)
        }

       
        
        completion(arr,nil);
    }
    
    public func getChatDetailsForProgram(player:[String:Any],
                                         program:[String:Any],
                                         onbefore: @escaping(Bool) -> Void,
                                         completion: @escaping(_ result: Array<[String:Any]>?, _ error: Error?) -> Void) {
        
        onbefore(true)
        
        var arr = Array<[String:Any]>();
        
        
        completion(arr,nil);
    }
    
    public func getStores(
                        filterBy: E.FilterFeedMode,
                        onbefore: @escaping(Bool) -> Void,
                        completion: @escaping(_ result: Array<[String:Any]>?, _ error: Error?) -> Void) {
        
        onbefore(true)
        
        var arr = Array<[String:Any]>();
        let stores = ["United Stores","Tennis Power", "Sport Mart"]
        for (index, element) in stores.enumerated() {
            var data = [String:Any]();
            data["id"] = index
            data["name"] = element
            data["last_updated"] = _generateRandomDate(daysBack: 10);
            data["posts"] = Int.random(in: 10 ..< 500)
            data["followers"] = Int.random(in: 10 ..< 100)
            
            let accu = ["https://www.zoealexanderuk.com/wp-content/uploads/2017/08/85367-609.00-ZAUK.png",
                "https://www.perfect-tennis.com/wp-content/uploads/2018/02/babolat-pure-drive-110-racket.png",
                "https://atennisoutlet.com/wp-content/uploads/2017/01/WRS324750_W_Rush_Pro_3_FieryCoral_White_CashmereBlue_Side-copy.png",
                "https://cdn.shopify.com/s/files/1/1248/6269/products/30S18529_SFX_III_AC_men_2007_Black_Silver_externe_HD_599x400.png?v=1535207457",
                "https://www.tennisranch.com.au/media/catalog/product/cache/1/image/1500x1500/9df78eab33525d08d6e5fb8d27136e95/1/0/101388_pure_drive_team_3-4face.png",
                "https://www.icons.com/media/catalog/product/cache/1/image/650x/040ec09b1e35df139433887a97daa66f/i/c/icswtr1-_-serena-williams-signed-black-and-green-wilson-tennis-racket-_main__1.png",
                "https://racquetnetwork.com/wp-content/uploads/BABOLAT-PURE-AERO-JR.png",
                "https://www.team-colours.co.uk/images/subcategories/teamwear/ladies-custom-polo-shirts.png",
                "https://custom-made-uniforms.com/wp-content/uploads/2017/12/PS-005-Thumb.jpg",
            "https://www.teamshield.com/wp-content/uploads/2018/07/Teamshield-Essential-Women-Sublimation-Polo-Shirt-Jersey-Custom-Print-Logo-1170x1170.jpg",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSR7ffB1KyL0ZQUYbqBn6VFZy9_fn8YZquAy1f2D2faya-f8JQ4vA",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTDTDguE5JjaVOY99Yqh0S3vgT1VDKw9G3UoZgBggPBjqqqIhcs",
                "https://image.keller-sports.com/storage/products/32/32/32329BD003F5E5D6C663870C17550196.png"
            ]
            var arra = Array<[String:Any]>();
            var i = 0;
            while (i<=10){
                var data = [String:Any]();
                data["name"] = element
                data["url"] = accu[Int.random(in: 0 ..< 12)]
                arra.append(data);
                i += 1;
            }
           
            data["items"] = arra
            
            arr.append(data);
        }
        
        completion(arr,nil);
    }
    
    
    public func  getProgramScheduleFor(coach:[String:Any],
            date:Date,
            onbefore: @escaping(Bool) -> Void,
            completion: @escaping(_ result: (key: Date?, value: [[String : Any]])?, _ error: Error?) -> Void){
    
        var dt = Calendar.current.date(byAdding: .day, value: -1, to: date)!
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let calendar = NSCalendar.current
        
        self.getProgramSchedule(
            coach: coach,
            date:  dt,
            onbefore: onbefore,
            completion: {(result, error) in
                let items = self.grouped.filter{calendar.isDateInToday($0.key!)}
                completion(items[0] ,nil);
            })
    }

    public func getProgramSchedule(coach:[String:Any],
                                   date:Date,
                                   onbefore: @escaping(Bool) -> Void,
                                   completion: @escaping(_ result: [(key: Date?, value: [[String : Any]])]?, _ error: Error?) -> Void) {
        onbefore(true)
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        if(!self.grouped.isEmpty){
            
            let items = self.grouped.filter{$0.key == date}
            
            completion(items ,nil);
        }else{
            let cprograms : Array<[String:Any]> = _buildPrograms();
            var arrDays = [[String:Any]]();
            
            let days = [0,0,Int.random(in: 0 ..< 5),Int.random(in: 12 ..< 20),Int.random(in: 10 ..< 20),Int.random(in: 25 ..< 30)];
            var adays = Array<String>();
            
            for (_, element) in days.enumerated() {
                let i =  _generateRandomDate(daysBack: element)!
                //if (adays.count == 0){
                //    adays.append(dateFormatterGet.string(from: Date()))
                //}
                adays.append(dateFormatterGet.string(from: i))
            }
            for (index, element) in adays.enumerated() {
                var d = [String:Any]();
                d["date"] = dateFormatterGet.date(from: element)
                d["location"] = _buildLocation(count: Int.random(in: 0 ..< 3), dayIndex : index)
                d["coaches"] = _buildCoaches(count: Int.random(in: 0 ..< 7))
                d["players"] = _buildPlayers(count: Int.random(in: 0 ..< 7))
                d["program"] = cprograms[Int.random(in: 0 ..< 4)]
                d["free_slots"] = Int.random(in: 0 ..< 4)
                arrDays.append(d)
            }
            
            let grouped = Dictionary(grouping: arrDays, by: { $0["date"] as? Date } )
            let ordered = grouped.sorted { $0.0! > $1.0! }
            
            self.grouped = ordered;
            //let items = self.grouped.filter{$0.key == date}
            completion(ordered ,nil);
        }
    }
    
    public func getNotifications(user:[String:Any],
                                 onbefore: @escaping(Bool) -> Void,
                                 completion: @escaping(_ result: [(key: Date?, value: [[String : Any]])]?, _ error: Error?) -> Void) {
        
        onbefore(true)
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
                    var arrDays = [[String:Any]]();
            let days = [0,1,1,2];
            var adays = Array<String>();
            for (_, element) in days.enumerated() {
                let i =  _generateRandomDate(daysBack: element)!
                adays.append(dateFormatterGet.string(from: i))
            }
            var arr = [
                String(format: "Scheduled Practice Canceld on %@",  dateFormatterGet.string(from: _generateRandomDate(daysBack: 5)!)),
                String(format: "Scheduled Practice Canceld on %@",  dateFormatterGet.string(from: _generateRandomDate(daysBack: 10)!)),
                String(format: "Parents meeting on %@",  dateFormatterGet.string(from: _generateRandomDate(daysBack: 4)!))
               ]
           let list = ["Advanced physical session", "Team selection tournament", "Skill development program", "Health program"]
            for (index, element) in adays.enumerated() {
                var d = [String:Any]();
                d["id"] = index
                d["date"] = dateFormatterGet.date(from: element)
                let t = Int.random(in: 0 ..< 2)
                d["summary"] = ""
                d["status"] = 0
                if(t == 1){
                    d["description"] = "Scheduled practice is canceled"
                    d["summary"] = "Due to unavailability of the courts"
                }else{
                    d["description"] = list[Int.random(in: 0 ..< 4)]
                    d["status"] = Int.random(in: 1 ..< 3)
                }
                d["type"] = t;
                arrDays.append(d)
            }
            
            let grouped = Dictionary(grouping: arrDays, by: { $0["date"] as? Date } )
            let ordered = grouped.sorted { $0.0! < $1.0! }
            
            completion(ordered ,nil);
    
    }
    
    func _generateRandomDate(daysBack: Int)-> Date?{
        /*
        let day = arc4random_uniform(UInt32(daysBack))
        let hour = arc4random_uniform(23)
        let minute = arc4random_uniform(59)
        
        let today = Date(timeIntervalSinceNow: 0)
        let gregorian  = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        var offsetComponents = DateComponents()
        offsetComponents.day = -1 * Int(day)
        offsetComponents.hour = -1 * Int(hour)
        offsetComponents.minute = -1 * Int(minute)
        
        let date = gregorian?.date(byAdding: offsetComponents, to: today, options: .init(rawValue: 0) )
        */
        let date = Calendar.current.date(byAdding: .day, value: daysBack, to: Date())
        
        return date
    }
    
    func _buildLocation(count:Int, dayIndex:Int) -> [String:Any] {
        let list = ["Colombo Tennis Association", "Hit Tennis Accedemy", "Players Tennis Accedemy"]
        let venue = ["Colombo 10", "Duplication Road", "Kolpity Colombo 11"]
        var location = [String: Any]();
        location["name"] = list[count];
        location["venue"] = venue[count];
        switch (dayIndex){
        case 0:
            location["time"] = "9:00 AM";
            location["etime"] = "10:00 AM"; break;
        case 1:
            location["time"] = "10:00 AM";
            location["etime"] = "11:00 AM"; break;
        case 2:
            location["time"] = "12.00 PM";
            location["etime"] = "1.00 PM";  break;
        case 3:
            location["time"] = "2:00 PM";
            location["etime"] = "3:00 PM";  break;
        case 4:
            location["time"] = "3:00 PM";
            location["etime"] = "4:00 PM";   break;
        case 5:
            location["time"] = "12:00 PM";
            location["etime"] = "2:00 PM"; break;
        case 6:
            location["time"] = "3:00 PM";
            location["etime"] = "9:00 PM"; break;
        default :
            location["time"] = "8:00 AM";
            location["etime"] = "9:00 AM";break;
        }
        location["canceled"] = Bool.random()
        
        return location
    }
    
    func _buildPlayers(count:Int) -> Array<[String:Any]>{
        
        var arr = Array<[String:Any]>()
        let list = ["Peter Arnold", "Jack Sheperd", "Donald Jackson","Haywood Zeledon","Jeffry Yahn","Moses Signor","Roscoe Laskowski","Stacey Sasaki","Sidney Machin","Bennett Gowins","Davis Rudasill","Ralph Cao","Valentin Villalva","Olin Balliet","Toney Yokota","Walton Niehaus","Jc Mowrer","Ron Cessna","Waylon Eastin","Thad Pebley","Bruno Purpura","Cody Portis","Sherman Mcguffie","Clifton Low","Marcos Vawter","Carrol Egan","Domingo Rushton","Wilford Landreth","Raul Acord","Fidel Brenton","Jordon Gory","Felton Seip","Boyce Linke",]
        //33
        let ballTypes = ["Green","Orange", "Red"]
        let ballTypeColors = ["#5fd150", "#ffa634", "#ff5733"]
        let type = Int.random(in: 0 ..< 3)
        let ballType = ballTypes[type]
        let ballTypeColor = ballTypeColors[type]
        for (index, element) in list.enumerated() {
            if index == count {
                break;
            }
            //let type = Int.random(in: 0 ..< 3)
            var data = [String:Any]()
            data["id"]  = index
            data["parentId"] = 0
            switch(index){
            case 0 : data["avatar"] = "https://tinyfac.es/data/avatars/BA0CB1F2-8C79-4376-B13B-DD5FB8772537-200w.jpeg"
            case 1 : data["avatar"] = "https://tinyfac.es/data/avatars/AEF44435-B547-4B84-A2AE-887DFAEE6DDF-200w.jpeg"
            case 2 : data["avatar"] = "https://tinyfac.es/data/avatars/03F55412-DE8A-4F83-AAA6-D67EE5CE48DA-200w.jpeg"
            default:
                data["avatar"] = ""
            }
            data["email"] =  String(format:"%@@aimnow.digital",element)
            data["name"]  = String(format:"%@",element)
            data["age"] = Int.random(in: 15 ..< 20)
            data["ballType"] = ballType;
            data["ballTypeColor"] =  ballTypeColor
            data["availalbe"] = Bool.random()
            data["stage"] = Int.random(in: 1 ..< 8)
            arr.append(data);
        }
        
        return arr;
    }
    
    func _buildCoaches(count:Int) -> Array<[String:Any]>{
        var ca = Array<[String:Any]>();
        var c = [String:Any]()
        c["name"] = "Peter Chen"
        c["avatar"] = "https://tinyfac.es/data/avatars/852EC6E1-347C-4187-9D42-DF264CCF17BF-200w.jpeg"
        ca.append(c)
        
        c = [String:Any]()
        c["name"] = "Cheng Chen"
        c["avatar"] = "https://tinyfac.es/data/avatars/B3CF5288-34B0-4A5E-9877-5965522529D6-200w.jpeg"
        ca.append(c)
        
        c = [String:Any]()
        c["name"] = "Rubi Chen"
        c["avatar"] = "https://tinyfac.es/data/avatars/FBEBF655-4886-455A-A4A4-D62B77DD419B-200w.jpeg"
        ca.append(c)
        
        return ca;
    }
    
    func _buildPrograms() -> Array<[String:Any]> {
        var arr = Array<[String:Any]>()
        let list = ["Scheduled Practices", "1 on 1 Induvidual Training", "Advance Training", "Fast Track"]
        let stages = [8,8,8,8]
        for (index, element) in list.enumerated() {
            var data = [String:Any]()
            
            data["id"]  = index
            data["name"] = String(format:"%@",element)
            data["times"] = ["MON 9.00 AM - 10.00 AM","MON 5:00 PM - 6:00 PM", "MON 9:00 AM - 10:00 AM"]
            data["coaches"] = _buildCoaches(count: Int.random(in: 3 ..< 10))
            data["enrolled"] = (index == 0  || index == 1) ? true :false;
            data["stages"] = stages[index]
            data["current_stage"] = Int.random(in: 1 ..< 8)
            data["stage_name"] = String(format: "Stage %i", data["current_stage"] as! Int)
            
            arr.append(data);
        }
        return arr
    }
    
    
}

//
//  ProgramDomain.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/6/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import Realm
import RealmSwift
import SwiftDate

public struct GroupedScheduledPrograms {
    public var date: Date
    public var programs: [AssignedProgramModel]
}

public struct GroupedProgramsForDate {
    public var program: AssignedProgramModel
    public var dates: [Date]
}

public struct ProgramStagesInfo {
    public var name: String
    public var currentStage: Int
    public var currentStageId: Int
    public var totalStages: Int = 0
    public var programTypeId: Int
    
    public init(name: String, currentStage: Int, totalStages: Int, currentStageId: Int, programTypeId: Int) {
        self.name = name
        self.currentStage = currentStage
        self.totalStages = totalStages
        self.currentStageId = currentStageId
        self.programTypeId = programTypeId
    }
}

public struct ProgramProgressSummaryDate{
    public var date: Date?
    public var programName: String?
    public var progressSummary: [ProgramStageInfo] = []
    public var assignmentId: Int?
    public init(){}
}

public struct ProgramStageInfo{
    public var stageName: String?
    public var stage: Int?
    public var date: Date?
    public var progressStageSummary: [ProgramProgressSummary] = []
    public var assignmentId: Int?
    public init(){}
}

public struct ProgramProgressSummary{
    public var name: String?
    public var total:Int? = 0
    public var precentage: Float? = 0.0
    public var categories: [ProgramProgressDetail] = []
    
    public init() {
    }
}

public struct ProgramProgressDetail {
    public var id: Int?
    public var name: String?
    public var descriptionValue: String?
    public var type: String?
    public var stat: Int? = 0
    public var video: [String] = []
    public var maxValue: Int? = 0
    public var minValue: Int? = 0
    public var lastStatId: Int? = 0
    public var referenceId: Int? = 0
    public var assignedProgramRef: AssignedProgramModel?
    
    public init() {
    }
}

public class ProgramDomain: NSObject {
    
    public static let instance = ProgramDomain()
    public var dataManager: DataManager!
    public let observers = Observers.instance
    let realm = try? Realm()
    
    init(dataManager: DataManager = DataManager.sharedInstance) {
        self.dataManager = dataManager
        
    }
    
    public func getAllPrograms(unitId: Int = 1, completion: @escaping(_ progameResponse: ProgramsResponse) ->()) {
        let request = ProgramsRequestModel.init(unitId: unitId)
        let router = NetworkRouter.getAllPrograms(request!)
        self.dataManager.callAPI(router: router) { (result: Swift.Result<ProgramsResponse, NetworkError>) in
            switch(result){
            case .success(let response):
                completion(response)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                break;
            }
        }
    }
    
    public func getAssingedProgramsFromAPI(router: NetworkRouter, completion: @escaping(_ progameResponse: AssignedProgramsResponse) ->()) {
        self.dataManager.callAPI(router: router) { (result: Swift.Result<AssignedProgramsResponse, NetworkError>) in
            switch(result){
            case .success(let response):
                completion(response)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                break;
            }
        }
    }
    
    public func getAssingedProgramDetail(router: NetworkRouter, completion: @escaping(_ progameResponse: ProgramDetailResponse?, _ error: Error?) ->()) {
        self.dataManager.callAPI(router: router) { (result: Swift.Result<ProgramDetailResponse, NetworkError>) in
            switch(result){
            case .success(let response):
                completion(response, nil)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                completion(nil, error)
                break;
            }
        }
    }
    
    public func getProgramStats(router: NetworkRouter, completion: @escaping(_ result: ProgramStatsResponse, _ error: Error?) -> Void) {
        self.dataManager.callAPI(router: router) { (result: Swift.Result<ProgramStatsResponse, NetworkError>) in
            switch(result){
            case .success(let response):
                completion(response, nil)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                break;
            }
        }
    }
    
    public func recordPlayerAttendance(router: NetworkRouter, completion: @escaping(_ result: Bool, _ error: Error?) -> Void) {
        self.dataManager.callAPI(router: router) { (result: Swift.Result<Bool, NetworkError>) in
            switch(result){
            case .success(let response):
                completion(true, nil)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                completion(false, error)
                break;
            }
        }
    }
    
    public func getProgramOverallSummary(router: NetworkRouter, completion: @escaping(_ result: [OverallSummaryResponse], _ error: Error?) -> Void) {
        self.dataManager.callAPI(router: router) { (result: Swift.Result<[OverallSummaryResponse], NetworkError>) in
            switch(result){
            case .success(let response):
                completion(response, nil)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                break;
            }
        }
    }
    
    public func getProgramAggStats(router: NetworkRouter, completion: @escaping(_ result: [AggStatsResponse], _ error: Error?) -> Void) {
        self.dataManager.callAPI(router: router) { (result: Swift.Result<[AggStatsResponse], NetworkError>) in
            switch(result){
            case .success(let response):
                completion(response, nil)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                break;
            }
        }
    }
    
    public func getProgramUsers(router: NetworkRouter, completion: @escaping(_ result: [ProgramUsersModel], _ error: Error?) -> Void) {
        self.dataManager.callAPI(router: router) { (result: Swift.Result<[ProgramUsersModel], NetworkError>) in
            switch(result){
            case .success(let response):
                completion(response, nil)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                break;
            }
        }
    }
    
    public func getCalendarEvents(player: DependantModel, program: ProgramModel) -> [Date]{
        var eventDates: [Date] = []
        if let dateList = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true).filter("userId= \"\(player.id!)\" AND masterId = \(program.masterId)").distinct(by: ["on"]){
            for date in dateList{
                if let scheduledDate = date.on?.date(format: Constants.SERVER_DATE_FORMAT){
                    eventDates.append(scheduledDate)
                }
            }
            return eventDates
        }else{
            return eventDates
        }
    }
    
    public func getProgramScheduleForDate(date selectedDate: Date? = nil, Player: DependantModel, program selectedProgram: ProgramModel? = nil) -> [GroupedScheduledPrograms]{
        var groupedPrograms: [GroupedScheduledPrograms] = []
        
        if let selectedDate = selectedDate{
            if let dateList = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true).filter("on = \"\(selectedDate.apiDateString(format: Constants.SERVER_DATE_FORMAT))\" AND userId= \"\(Player.id!)\"").distinct(by: ["on"]){
                for date in dateList{
                    let scheduledDate = date.on?.date(format: Constants.SERVER_DATE_FORMAT)
                    if let programList = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true).filter({$0.on == date.on}){
                        if let program = selectedProgram{
                            groupedPrograms.append(GroupedScheduledPrograms(date: scheduledDate!, programs: Array(programList.filter({$0.referenceId.value == program.id}))))
                        }else{
                            groupedPrograms.append(GroupedScheduledPrograms(date: scheduledDate!, programs: Array(programList)))
                        }
                        
                    }
                }
                return groupedPrograms
            }else{
                return groupedPrograms
            }
        }else{
            if let dateList = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true).filter("userId= \"\(Player.id!)\"").distinct(by: ["on"]){
                for date in dateList{
                    let scheduledDate = date.on?.date(format: Constants.SERVER_DATE_FORMAT)
                    if let programList = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true).filter({$0.on == date.on}){
                        if let program = selectedProgram{
                            groupedPrograms.append(GroupedScheduledPrograms(date: scheduledDate!, programs: Array(programList.filter({$0.referenceId.value == program.id}))))
                        }else{
                            groupedPrograms.append(GroupedScheduledPrograms(date: scheduledDate!, programs: Array(programList)))
                        }
                    }
                }
                return groupedPrograms
            }else{
                return groupedPrograms
            }
        }
    }
    
    public func getProgramScheduleFor(program selectedProgram: ProgramModel? = nil, Player: DependantModel) -> [GroupedProgramsForDate]{
        var groupedPrograms: [GroupedProgramsForDate] = []
        
        if let programList = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true).filter("userId= \"\(Player.id!)\"").distinct(by: ["referenceId"]){
            for program in programList{
                if let dateList = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true).filter({$0.referenceId.value == program.id}){
                    let dates = Array(dateList.map({$0.on?.date(format: Constants.SERVER_DATE_FORMAT)}))
                    if let selectedProgram = selectedProgram{
                        if(program.id == selectedProgram.id){
                            return [GroupedProgramsForDate(program: program, dates: dates as! [Date])]
                        }
                    }else{
                        groupedPrograms.append(GroupedProgramsForDate(program: program, dates: dates as! [Date]))
                    }
                }
            }
            return groupedPrograms
        }else{
            return groupedPrograms
        }
    }
    
    public func getProgramStatistics(player:DependantModel,
                                     program:ProgramModel,
                                     filterBy : E.FilterMode,
                                     date: Date? = nil,
                                     completion: @escaping(_ result: [String:Any]?, _ error: Error?) -> Void) {
        var arr = [String:Any]()
        
        calculatePracticeTimeFor(player: player, program: program.id, filter: filterBy) { (player, allocated) in
            if(player > 0 && allocated > 0){
                arr["tenisTime"] = Int((player/allocated) * 100)
            }else{
                arr["tenisTime"] = 0
            }
        }
        getAttendanceForProgram(player: player, programId: program.id, filter: filterBy) { (attendace, allocation) in
            if(attendace > 0 && allocation > 0){
                arr["attendace"] = Int((attendace/allocation) * 100)
            }else{
                arr["attendace"] = 0
            }
        }
        switch filterBy {
        case E.FilterMode.day:
            arr["completed"] = Int.random(in: 10 ..< 100)
            break;
        case E.FilterMode.week:
            arr["completed"] = Int.random(in: 10 ..< 50)
            break;
        case E.FilterMode.month:
            arr["completed"] = Int.random(in: 0 ..< 30)
            break;
        }
        
        completion(arr,nil);
    }
    
    fileprivate func getAttendanceForProgram(player: DependantModel, programId: Int, filter: E.FilterMode? = .day, date: Date? = nil, completion: @escaping(_ totalAttended: Int,_ totalAllocated: Int) ->()){
        
        if let scheduldedDates = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true).filter({$0.referenceId.value == programId}){
            
            var attCount = 0
            for assDate in scheduldedDates{
                if(assDate.attendance.count > 0){
                    attCount += 1
                }
            }
            completion(attCount, scheduldedDates.count)
        }
    }
    
    fileprivate func calculatePracticeTimeFor(player dependant: DependantModel, program programId: Int, filter: E.FilterMode? = .day, date: Date? = nil, completion: @escaping(_ totalPractice: Double,_ totalAllocated: Double) ->()){
        var totalPlayingTime = 0.0
        var totalAssignedTime = 0.0
        if let programs = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true).filter({$0.referenceId.value == programId}){
            for program in programs{
                for att in program.attendance{
                    Utils.findDateDiff(time1Str: att.inTime!, time2Str: att.outTime!) { (readable, minutes) in
                        totalPlayingTime += minutes
                    }
                    Utils.findDateDiff(time1Str: program.startTime!, time2Str: program.endTime!) { (readable, minutes) in
                        totalAssignedTime += minutes
                        
                    }
                }
            }
        }
        completion(totalPlayingTime, totalAssignedTime)
    }
    
    public func getProgramProgressStatus(player: DependantModel, program: ProgramModel, completion: @escaping(_ userStages:[ProgramStagesInfo]) ->()){
        var stageInfo:[ProgramStagesInfo] = []
        var parentId = 0
        var masterId = 0
        if let ref = program as? AssignedProgramModel, let referenceId = ref.referenceId.value {
            masterId = ref.masterId
            parentId = referenceId
        }else{
            parentId = program.id
        }
        if let userStages = realm?.objects(Assigment.self).sorted(byKeyPath: "id", ascending: true).filter("userId= \"\(player.id!)\" AND programTypeId = 2 and masterId = \(masterId)"){
            let totalStages = program.config?.stages.value ?? 0
            for stage in userStages {
                let currentStage: Int =  ((stage.name!.split(separator: " ").last)! as NSString).integerValue
                let programStagesInfo = ProgramStagesInfo(name: (stage.name!), currentStage: currentStage, totalStages: totalStages, currentStageId: stage.id.value!, programTypeId: stage.referenceId.value!)
                stageInfo.append(programStagesInfo)
            }
            completion(stageInfo)
        }else{
            completion(stageInfo)
        }
    }
    
    public func getProgramProgress(player:DependantModel,
                                   programId:Int,
                                   filterBy : E.FilterMode,
                                   completion: @escaping(_ programProgress: [ProgramProgressSummaryDate]) -> ()) {
        
        
        var programProgressSummaryDate =  [ProgramProgressSummaryDate]()
        // Getting stages list for date
        if let programList = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true).filter("referenceId = \(programId)  AND userId= \"\(player.id!)\""){
            for program in programList {
                var programProgressSummary = ProgramProgressSummaryDate()
                let date = program.on?.date(format: Constants.SERVER_DATE_FORMAT)
                programProgressSummary.date = date
                programProgressSummary.programName = program.name
                // Getting stages list for program
                var arrProgramStageInfo = [ProgramStageInfo]()
                if let stagesList = realm?.objects(AssignedProgramModel.self).filter("parentId = \(program.id) AND programTypeId = 2"){
                    for stage in stagesList{
                        var programStageInfo = ProgramStageInfo()
                        programStageInfo.stageName = stage.name
                        // Getting sub categoris
                        if let subCategories = realm?.objects(AssignedProgramModel.self).filter("parentId = \(stage.id)"){
                            // Getting main categories
                            let categories = subCategories.distinct(by: ["programTypeId"])
                            var programProgressSummary = [ProgramProgressSummary]()
                            for cat in categories{
                                if let mainCategory = realm?.object(ofType: MasterProgram.self, forPrimaryKey: cat.programTypeId){
                                    let mSubCategory = subCategories.filter("programTypeId = \(mainCategory.id)")
                                    var programProgressDetail = [ProgramProgressDetail]()
                                    var total = 0
                                    var maxScore = 0
                                    var summary = ProgramProgressSummary()
                                    for mSCat in mSubCategory{
                                        var detail = ProgramProgressDetail()
                                        detail.type = mainCategory.type
                                        detail.name = mSCat.name
                                        detail.id = mSCat.id
                                        detail.descriptionValue = mSCat.descriptionValue
                                        if let lastStat = mSCat.statistics.last{
                                            detail.stat = lastStat.score.value ?? 0
                                            let updateOn = lastStat.updatedOn?.date(format: Constants.SERVER_DATE_FORMAT)?.toString(format: Constants.DATE_FORMAT)
                                            if(updateOn == Date().toString(format: Constants.DATE_FORMAT)){
                                                detail.lastStatId = lastStat.id
                                            }
                                        }
                                        
                                        if let config = realm?.object(ofType: ConfigModel.self, forPrimaryKey: mSCat.configId.value ?? 1){
                                            detail.minValue = config.minScore.value
                                            detail.maxValue = config.maxScore.value
                                            maxScore += config.maxScore.value!
                                        }
                                        detail.video = mSCat.feedBacks.map({$0.attachment}).compactMap({$0.map({$0.path!})})
                                        programProgressDetail.append(detail)
                                        
                                        total += detail.stat ?? 0
                                    }
                                    summary.categories = programProgressDetail
                                    summary.name = mainCategory.type
                                    summary.total = Int(Double((total)) * 0.1)
                                    summary.precentage = Float((total * 100)/maxScore)
                                    programProgressSummary.append(summary)
                                }
                                
                            }
                            programStageInfo.progressStageSummary = programProgressSummary
                        }
                        arrProgramStageInfo.append(programStageInfo)
                    }
                    
                }
                programProgressSummary.progressSummary = arrProgramStageInfo
                programProgressSummaryDate.append(programProgressSummary)
            }
        }
        completion(programProgressSummaryDate)
    }
    
    public func getProgramOverview(date selectedDate: Date, player: DependantModel, program programId: Int, completion: @escaping(_ programProgress: [ProgramProgressSummaryDate]) -> ()) {
               var programProgressSummaryDate =  [ProgramProgressSummaryDate]()
               // Getting stages list for date
        if let programList = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true).filter("on = \"\(selectedDate.removeTimeStamp!.apiDateString(format: Constants.SERVER_DATE_FORMAT))\" AND referenceId = \(programId)  AND userId= \"\(player.id!)\""){
                   for program in programList {
                       var programProgressSummary = ProgramProgressSummaryDate()
                       let date = program.on?.date(format: Constants.SERVER_DATE_FORMAT)
                       programProgressSummary.date = date
                       programProgressSummary.programName = program.name
                    programProgressSummary.assignmentId = program.id
                       // Getting stages list for program
                       var arrProgramStageInfo = [ProgramStageInfo]()
                       if let stagesList = realm?.objects(AssignedProgramModel.self).filter("parentId = \(program.id) AND programTypeId = 2"){
                           for stage in stagesList{
                               var programStageInfo = ProgramStageInfo()
                               programStageInfo.stageName = stage.name
                            programStageInfo.assignmentId = stage.id
                               // Getting sub categoris
                               if let subCategories = realm?.objects(AssignedProgramModel.self).filter("parentId = \(stage.id)"){
                                   // Getting main categories
                                   let categories = subCategories.distinct(by: ["programTypeId"])
                                   var programProgressSummary = [ProgramProgressSummary]()
                                   for cat in categories{
                                       if let mainCategory = realm?.object(ofType: MasterProgram.self, forPrimaryKey: cat.programTypeId){
                                           let mSubCategory = subCategories.filter("programTypeId = \(mainCategory.id)")
                                           var programProgressDetail = [ProgramProgressDetail]()
                                           var total = 0
                                           var maxScore = 0
                                           var summary = ProgramProgressSummary()
                                           for mSCat in mSubCategory{
                                               var detail = ProgramProgressDetail()
                                               detail.type = mainCategory.type
                                               detail.name = mSCat.name
                                               detail.id = mSCat.id
                                               detail.descriptionValue = mSCat.descriptionValue
                                               detail.assignedProgramRef = mSCat
                                               detail.referenceId = mSCat.referenceId.value
                                               if let lastStat = mSCat.statistics.last{
                                                   detail.stat = lastStat.score.value ?? 0
                                                   let updateOn = lastStat.updatedOn?.date(format: Constants.SERVER_DATE_FORMAT)?.toString(format: Constants.DATE_FORMAT)
                                                   if(updateOn == Date().toString(format: Constants.DATE_FORMAT)){
                                                       detail.lastStatId = lastStat.id
                                                   }
                                               }
                                               
                                               if let config = realm?.object(ofType: ConfigModel.self, forPrimaryKey: mSCat.configId.value ?? 1){
                                                   detail.minValue = config.minScore.value
                                                   detail.maxValue = config.maxScore.value
                                                   maxScore += config.maxScore.value!
                                               }
                                               detail.video = mSCat.feedBacks.map({$0.attachment}).compactMap({$0.map({$0.path!})})
                                               programProgressDetail.append(detail)
                                               
                                               total += detail.stat ?? 0
                                           }
                                           summary.categories = programProgressDetail
                                           summary.name = mainCategory.type
                                           summary.total = Int(Double((total)) * 0.1)
                                           summary.precentage = Float((total * 100)/maxScore)
                                           programProgressSummary.append(summary)
                                       }
                                       
                                   }
                                   programStageInfo.progressStageSummary = programProgressSummary
                               }
                               arrProgramStageInfo.append(programStageInfo)
                           }
                           
                       }
                       programProgressSummary.progressSummary = arrProgramStageInfo
                       programProgressSummaryDate.append(programProgressSummary)
                   }
               }
               completion(programProgressSummaryDate)
    }
    
    public func getProgramLocations(completion: @escaping(_ locations:[Location]) -> ()){
        if let locations = realm?.objects(Location.self).sorted(byKeyPath: "id", ascending: true){
            completion(Array(locations))
        }else{
            completion([])
        }
    }
    
    
    public func requestNewProgram(router: NetworkRouter, completion: @escaping(_ response: NewProgramResponseModel?, _ errorDescription:String?) ->()) {
        self.dataManager.callAPI(router: router) { (result: Swift.Result<NewProgramResponseModel, NetworkError>) in
            switch(result){
            case .success(let response):
                completion(response, nil)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                completion(nil, error.errorDescription)
                break;
            }
        }
    }
    
    // Coach related methods
    public func getAssignedPrograms(completion: @escaping (_ programs: [AssignedProgramModel]) -> ()){
        if let programs = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true){
            completion(Array(programs))
        }
    }
    
    public func getAssignedProgramsFor(date:Date, completion: @escaping (_ programs: [AssignedProgramModel]) -> ()){
        if let programs = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true).filter("on = \"\(date.removeTimeStamp!.apiDateString(format: Constants.SERVER_DATE_FORMAT))\""){
            completion(Array(programs))
        }
    }
    
    public func getProgramProgressFor(player user:DependantModel,
                                      masterId:Int,
                                      completion: @escaping(_ programProgress: [ProgramStageInfo]) -> ()) {
        var arrProgramStageInfo = [ProgramStageInfo]()
        // getting user stages
        if let stages = realm?.objects(Assigment.self).filter("programTypeId = 2 and userId= \"\(user.id!)\" and masterId = \(masterId)"){
            for stage in stages{
                var programStageInfo1 = ProgramStageInfo()
                programStageInfo1.stageName = stage.name
                let date = stage.on?.date(format: Constants.SERVER_DATE_FORMAT,includeUTC: true)
                programStageInfo1.date = date
                programStageInfo1.stage = ((stage.name!.split(separator: " ").last)! as NSString).integerValue
                let subCategories = realm?.objects(Assigment.self).filter("parentId = \(stage.id.value!)")
                let categories = subCategories!.distinct(by: ["programTypeId"])
                var programProgressSummary = [ProgramProgressSummary]()
                for cat in categories{
                    if let mainCategory = realm?.object(ofType: MasterProgram.self, forPrimaryKey: cat.programTypeId){
                        let mSubCategory = subCategories!.filter("programTypeId = \(mainCategory.id)")
                        var programProgressDetail = [ProgramProgressDetail]()
                        var total = 0
                        var maxScore = 0
                        var summary = ProgramProgressSummary()
                        for mSCat in mSubCategory{
                            var detail = ProgramProgressDetail()
                            detail.type = mainCategory.type
                            detail.name = mSCat.name
                            detail.descriptionValue = mSCat.descriptionValue
                            detail.id = mSCat.id.value
                            detail.referenceId = mSCat.referenceId.value
                            if let lastStat = mSCat.statistics.last{
                                detail.stat = lastStat.score.value ?? 0
                                let updateOn = lastStat.updatedOn?.date(format: Constants.SERVER_DATE_FORMAT, includeUTC: false)?.toString(format: Constants.DATE_FORMAT)
                                if(updateOn == Date().toString(format: Constants.DATE_FORMAT)){
                                    detail.lastStatId = lastStat.id
                                }
                            }
                            
                            if let config = realm?.object(ofType: ConfigModel.self, forPrimaryKey: mSCat.configId.value ?? 1){
                                detail.minValue = config.minScore.value
                                detail.maxValue = config.maxScore.value
                                maxScore += config.maxScore.value!
                            }
                            detail.video = mSCat.feedBacks.map({$0.attachment}).compactMap({$0.map({$0.path!})})
                            programProgressDetail.append(detail)
                            
                            total += detail.stat ?? 0
                        }
                        summary.categories = programProgressDetail
                        summary.name = mainCategory.type
                        summary.total = Int(Double((total)) * 0.1)
                        summary.precentage = Float((total * 100)/maxScore)
                        programProgressSummary.append(summary)
                    }
                }
                programStageInfo1.progressStageSummary = programProgressSummary
                arrProgramStageInfo.append(programStageInfo1)
                
            }
        }
        completion(arrProgramStageInfo)
    }
    
    
    public func getPlayerStatsForProgram(player: DependantModel, programId: Int,filterBy : E.ViewMode = .coach,
                                         completion :@escaping(_ programStats: [[Int:Double]]) -> ()) {
        var programStats = [[Int:Double]]()
        let date = Date()
        var start = date.startOfMonth()
        let end = date.endOfMonth()
        let calendar = Calendar.current
        switch filterBy {
        case .coach:
            
            let prog  = player.currentAssigments.sorted(byKeyPath: "id", ascending: true).filter({assignment in
                return assignment.referenceId.value == programId && assignment.userId == player.id
            })
            
            while start < end {
                let dateValue = calendar.component(.day, from: start)
                for item in prog {
                    if let stat = item.statistics.first(where: {calendar.component(.day, from: ($0.updatedOn?.split(separator: ".").first?.uppercased().date(format: Constants.SERVER_DATE_FORMAT))!) == dateValue }){
                        programStats.append([dateValue: Double(stat.score.value ?? 0)])
                    }else{
                        programStats.append([dateValue: 0.0])
                    }
                }
                start = start.addDays(inDays: 1)
            }
            completion(programStats)
        default:
            guard let prog = realm?.objects(AssignedProgramModel.self).sorted(byKeyPath: "id", ascending: true).filter("referenceId = \(programId)  AND userId= \"\(player.id!)\"") else {
                completion(programStats)
                return
            }
             
            while start < end {
                let dateValue = calendar.component(.day, from: start)
                for item in prog {
                    if let stat = item.statistics.first(where: {calendar.component(.day, from: ($0.updatedOn?.split(separator: ".").first?.uppercased().date(format: Constants.SERVER_DATE_FORMAT))!) == dateValue }){
                        programStats.append([dateValue: Double(stat.score.value ?? 0)])
                    }else{
                        programStats.append([dateValue: 0.0])
                    }
                }
                start = start.addDays(inDays: 1)
            }
            completion(programStats)
        }
    }
    
    
    public func getPlayerStatsForProgram(player: DependantModel, statistics: [Statistic],filterBy : E.ViewMode = .coach,
                                         completion :@escaping(_ programStats: [[Int:Double]]) -> ()) {
        var programStats = [[Int:Double]]()
        let date = Date()
        var start = date.startOfMonth()
        let end = date.endOfMonth()
        let calendar = Calendar.current
        switch filterBy {
        case .coach:
            while start < end {
                let dateValue = calendar.component(.day, from: start)
                if let stat = statistics.first(where: {calendar.component(.day, from: ($0.updatedOn?.split(separator: ".").first?.uppercased().date(format: Constants.SERVER_DATE_FORMAT))!) == dateValue }){
                        programStats.append([dateValue: Double(stat.score.value ?? 0)])
                }else{
                        programStats.append([dateValue: 0.0])
                }
                start = start.addDays(inDays: 1)
            }
            completion(programStats)
        default:
            while start < end {
                let dateValue = calendar.component(.day, from: start)
                if let stat = statistics.first(where: {calendar.component(.day, from: ($0.updatedOn?.split(separator: ".").first?.uppercased().date(format: Constants.SERVER_DATE_FORMAT))!) == dateValue }){
                        programStats.append([dateValue: Double(stat.score.value ?? 0)])
                }else{
                        programStats.append([dateValue: 0.0])
                }
                start = start.addDays(inDays: 1)
            }
            completion(programStats)
        }
    }
}


public struct PlayerProgramStats {
    public var date: Int
    public var score: Double
    
    public init(date: Int, score: Double){
        self.date = date
        self.score = score
    }
}

//
//  UserDomain.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/6/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import FirebaseInstanceID
import RxSwift
import RxCocoa

public class UserDomain: NSObject {
    
    public static let instance = UserDomain()
    public var dataManager: DataManager!
    public let observers = Observers.instance
    var firebaseObserver:PublishSubject<requestSendStatus> = PublishSubject<requestSendStatus>()

    init(dataManager: DataManager = DataManager.sharedInstance) {
        self.dataManager = dataManager
    }
    
    public func authenticateUser(userName: String, password: String, completion: @escaping(_ userModel: UserModel?,_ error: Error?) ->()){
        let router = NetworkRouter.authentication(ParamLoginMail(username: userName, password: password))
        self.dataManager.callAPI(router: router) { (result: Swift.Result<LoginResponse, NetworkError>) in
            switch(result){
            case .success(let response):
                if let accessToken = response.token,let userModel = response.userModel {
                    Defaults[\.token] = accessToken
                    Defaults[\.isLoggedIn] = true
                    Defaults[\.userDetail] = userModel
                    completion(userModel, nil)
                }
                break;
            case .failure(let error):
                completion(nil, error)
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                break;
            }
        }
    }
    
    public func getDependantUsers(completion: @escaping(_ dependants: [DependantModel]) ->()){
        if let logedUser = Defaults[\.userDetail]{
            let router = NetworkRouter.getDependantUsers("1", logedUser.id!)
            self.dataManager.callAPI(router: router) { (result: Swift.Result<[DependantModel], NetworkError>) in
                switch(result){
                case .success(let response):
                    completion(response)
                    break;
                case .failure(let error):
                    self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                    break;
                }
            }
        } else {
            self.observers.errorMessageObserver.onNext(errorMessage(error: "LoggedIn user can not be null"))
        }
    }
    
    public func getDependantUser(dependantId: String, completion: @escaping(_ dependant: DependantModel) ->()){
        if let logedUser = Defaults[\.userDetail]{
            let router = NetworkRouter.getDependantUser("1", logedUser.id!, dependantId)
            self.dataManager.callAPI(router: router) { (result: Swift.Result<DependantModel, NetworkError>) in
                switch(result){
                case .success(let response):
                    completion(response)
                    break;
                case .failure(let error):
                    self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                    break;
                }
            }
        } else {
            self.observers.errorMessageObserver.onNext(errorMessage(error: "LoggedIn user can not be null"))
        }
    }
    
    public func updateFirebaseToken(isDelete: Bool = false, isFromRefresh:Bool = false) {
        if((Defaults[\.isDeviceRegistered] ?? false) && (!isDelete || !isFromRefresh)){
            return
        }
        if let deviceToken = Defaults[\.fcmTokenKey], !deviceToken.isEmpty,let user = SharedAppResource.instance.loggedInUser{
            FirebaseHelper.instance.updateDevice(deviceToken: deviceToken)
            var deviceModel = DeviceModel()
            deviceModel.code = String(describing: deviceToken)
            deviceModel.userId = user.id
            
            let router: NetworkRouter?
            if(isDelete){
                router = NetworkRouter.deleteDeviceRegistration(deviceModel)
            }else{
                router = NetworkRouter.registerDevice(deviceModel)
            }
            self.dataManager.callAPI(router: router!) { (result: Swift.Result<DeviceRegistrationResponse, NetworkError>) in
                switch(result) {
                case .success( _):
                    if(!isDelete){
                        Defaults[\.isDeviceRegistered] = true
                        self.firebaseObserver.onNext(requestSendStatus(isSuccess: true, message: "Device registration successful"))
                    }else{
                        Defaults[\.isDeviceRegistered] = false
                    }
                    break;
                case .failure(let error):
                    self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                    break;
                }

            }
        }
    }
    
    public func getProfile(userId: String?, completion:@escaping(_ profileInfo: [Profile],_ error: Error?) ->()) {
        var id: String!
        
        if let userId = userId{
            id = userId
        }else if let logedUser = Defaults[\.userDetail]{
            id = logedUser.id!
        } else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "UserId can not be null"])
            completion([], error)
            self.observers.errorMessageObserver.onNext(errorMessage(error: "LoggedIn user can not be null"))
            return
        }
        let router = NetworkRouter.getProfile(id)
        self.dataManager.callAPI(router: router) { (result: Swift.Result<[Profile], NetworkError>) in
            switch(result){
            case .success(let response):
                completion(response, nil)
                break;
            case .failure(let error):
                completion([], error)
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                break;
            }
        }
    }
    
    public func getNotifiction(router: NetworkRouter , completion:@escaping (_ notifications: NotificatoinResponse?) ->()) {
        self.dataManager.callAPI(router: router) { (result: Swift.Result<NotificatoinResponse, NetworkError>) in
            switch(result) {
            case .success(let response):
                completion(response)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                break;
            }

        }
    }
    
    public func updateReadNotifiction(userId: String?, notificationId: Int ,hasSeen: Bool, completion:@escaping (_ notification: NotificationItem?) ->()) {
        var request = NotificationPostRequest()
        request.hasSeen = hasSeen
        request.id = notificationId
        request.senderId = userId
        let router = NetworkRouter.updateNotification(request)
        self.dataManager.callAPI(router: router) { (result: Swift.Result<NotificationItem, NetworkError>) in
            switch(result) {
            case .success(let response):
                completion(response)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                break;
            }
        }
    }
    
    public func postNotifiction(userId: String?, pool: [String], message: String,typeId: Int = 1, notificationTemplate: eNotificationTemplates, from: Date = Date(), to: Date = Date(), completion:@escaping (_ notifications: NotificationItem?) ->()) {
        var request = NotificationPostRequest()
        request.hasSeen = false
        request.message = message
        request.templateId = notificationTemplate.rawValue
        request.typeId = typeId
        request.senderId = userId
        request.pool = pool
        request.recurringFrom = from.apiDateString(format: Constants.SERVER_DATE_FORMAT)
        request.recurringTo = to.apiDateString(format: Constants.SERVER_DATE_FORMAT)
        let router = NetworkRouter.postNotification(request)
        self.dataManager.callAPI(router: router) { (result: Swift.Result<NotificationItem, NetworkError>) in
            switch(result) {
            case .success(let response):
                completion(response)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                break;
            }

        }
    }
    
    public func getNotificationSummary(completion:@escaping(_ summary: NotificationSummary?) -> ()){
        let request = BaseRequest()
        let router = NetworkRouter.notificationSummary(request)
        self.dataManager.callAPI(router: router) { (result: Swift.Result<NotificationSummary, NetworkError>) in
            switch(result) {
            case .success(let response):
                completion(response)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                break;
            }

        }
    }
    
    
    public func updateUserProfile(profile: UserProfileUpdateRequest, completion: @escaping(_ userModel: UserModel?,_ error: Error?) ->()){
        let routerPath = NetworkRouter.updateUserProfile
        
        self.dataManager.uploadForm(path: routerPath, param: profile.encode(),fileData: profile.profileImage!.pngData()!, callback: { (response) in
                if(profile.id == SharedAppResource.instance.loggedInUser?.id){
                    Defaults[\.userDetail] = response
                }
                completion(response, nil)
        }) { (error) in
            //completion(nil,error)
            self.observers.errorMessageObserver.onNext(errorMessage(error: error))
        }
    }
}

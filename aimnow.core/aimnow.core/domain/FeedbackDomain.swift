//
//  FeedbackDomain.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 6/4/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

public class FeedbackDomain: NSObject {
    
    public static let instance = FeedbackDomain()
    public var dataManager: DataManager!
    public let observers = Observers.instance

    init(dataManager: DataManager = DataManager.sharedInstance) {
        self.dataManager = dataManager
    }
    
    public func updateFeedback(router: NetworkRouter, completion: @escaping(_ response: FeedBack?, _ errorDescription:String?) ->()) {
        self.dataManager.callAPI(router: router) { (result: Swift.Result<FeedBack, NetworkError>) in
            switch(result){
            case .success(let response):
                completion(response, nil)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                completion(nil, error.errorDescription)
                break;
            }
        }
    }
    
    public func updateAssesmentStats(router: NetworkRouter, completion: @escaping(_ response: Statistic?, _ errorDescription:String?) ->()) {
        self.dataManager.callAPI(router: router) { (result: Swift.Result<Statistic, NetworkError>) in
            switch(result){
            case .success(let response):
                completion(response, nil)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                completion(nil, error.errorDescription)
                break;
            }
        }
    }
    
    public func getFeedbacks(router: NetworkRouter, completion: @escaping(_ response: [FeedBack]?, _ errorDescription:String?) ->()) {
        self.dataManager.callAPI(router: router) { (result: Swift.Result<FeedbackListResponse, NetworkError>) in
            switch(result){
            case .success(let response):
                completion(response.pagedItems, nil)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                completion(nil, error.errorDescription)
                break;
            }
        }
    }
    
    public func getStatistics(router: NetworkRouter, completion: @escaping(_ response: [Statistic]?, _ errorDescription:String?) ->()) {
        self.dataManager.callAPI(router: router) { (result: Swift.Result<StatisticListResponse, NetworkError>) in
            switch(result){
            case .success(let response):
                completion(response.pagedItems, nil)
                break;
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                completion(nil, error.errorDescription)
                break;
            }
        }
    }
}

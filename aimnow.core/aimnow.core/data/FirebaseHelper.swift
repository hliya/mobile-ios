//
//  FirebaseHelper.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 6/1/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit
import Firebase
import SwiftyUserDefaults
import SwiftyJSON
import FirebaseDatabase

let Table_users = "users"
let Table_notifications = "notifications"
let Table_chats = "chats"
let Table_detail = "detail"
let Active_users = "active_users"
let Table_updates = "updates"

let Table_users_devices = "devices"
let Table_users_devices_iOS = "iOS"
let Table_conversations = "conversations"
let Table_conversations_messages = "conversationMessages"
let Table_conversations_users = "conversationUsers"
let Table_user_isActive = "isActive"
let Table_coversation_users = "conversationUsers"
let Table_conversation_ceeated_field = "createdBy"
let Table_user_unread_chat_count = "unReadMessageCount"
let Table_conversation_name = "conversationName"
let Column_seenBy = "seenBy"
let Firebase_table_conversations_column_subType = "subType"
let Firebase_table_conversations_column_subtype_data = "currentSubTypeData"

public class FirebaseHelper: NSObject {
    public static let instance:FirebaseHelper = FirebaseHelper()
    private var database: DatabaseReference!
    
    public func configure(options: FirebaseOptions){
        FirebaseApp.configure(options: options)
        database = Database.database().reference()
    }
    
    public static func initiateFirebaseConnectionStatus() -> Void{
        let connectedRef = Database.database().reference(withPath: ".info/connected")
        connectedRef.observe(.value, with: { (snapshot) in
            if let connected = snapshot.value as? Bool , connected {
                if let userId = SharedAppResource.instance.loggedInUser?.id
                {
                    let userRef = Database.database().reference().child(Table_users).child(userId).child(Table_user_isActive)
                    userRef.onDisconnectSetValue(false)
                    userRef.setValue(true)
                }
                SharedAppResource.instance.connectedToInternet.onNext(true)
            } else {
                SharedAppResource.instance.connectedToInternet.onNext(false)
            }
        })
    }
    
    public func observeNotificationsCount() {
        if let userId = SharedAppResource.instance.loggedInUser?.id {
            self.database.child(Table_users).child(userId).child(Table_notifications).observe(DataEventType.value,
                                                                                              with: { (rows) in
                                                                                                let data = rows.value as? [String : AnyObject] ?? [:]
                                                                                                SharedAppResource.instance.notificationCount.onNext(data.keys.count)
            })
        } else {
            SharedAppResource.instance.notificationCount.onNext(0)
        }
    }
    
    public func observeChatCount() {
        if let userId = SharedAppResource.instance.loggedInUser?.id {
            self.database.child(Table_users).child(userId)
                .child(Table_user_unread_chat_count).observe(.value, with: { (rows) in
                    if let count = rows.value as? Int{
                        SharedAppResource.instance.chatCount.onNext(count)
                    }else{
                        SharedAppResource.instance.chatCount.onNext(0)
                    }
                })
        } else {
            SharedAppResource.instance.chatCount.onNext(0)
        }
    }
    
    public func updateDevice (deviceToken:String) {
        if let userId = SharedAppResource.instance.loggedInUser?.id {
            let iOSRef = self.database.child(Table_users).child(userId).child(Table_users_devices).child(Table_users_devices_iOS)
            iOSRef.setValue(deviceToken)
        }
    }
    
    public func initiateUserAvailability(userId: String) {
        self.database.child(Table_users).child(userId).child(Table_user_isActive).observe(.value, with: { (snapshot) in
            if let isAvailable = snapshot.value as? Bool {
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.Notifications.userAvailable), object: isAvailable, userInfo: ["available":true,"userId":userId])
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.Notifications.userAvailable), object: false, userInfo: ["available":false,"userId":userId])
            }
        })
        self.database.child(Table_users).child(userId).child(Table_user_isActive).observe(.childChanged, with: { (snapshot) in
            if let isAvailable = snapshot.value as? Bool{
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.Notifications.userAvailable), object: isAvailable, userInfo: ["available":true,"userId":userId])
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.Notifications.userAvailable), object: false, userInfo: ["available":false,"userId":userId])
            }
        })
    }
    
    public func checkUserAvailability(userId: String, callback:@escaping (_ avaiable: Bool) -> ()){
        let chatRef =  self.database.child(Table_users).child(userId).child(Table_user_isActive)
        chatRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if let isAvailable = snapshot.value as? Bool{
                callback(isAvailable)
            }else{
                callback(false)
            }
        }) { (error) in
            callback(false)
        }
    }
    
    public func initiateUpdateObserver(userId: String) {
        self.database.child(Table_users).removeAllObservers()
        self.database.child(Table_users).child(userId).child(Table_updates).removeValue()
        if let userId = SharedAppResource.instance.selectedPlayer?.id {
            let chatRef = self.database.child(Table_users).child(userId).child(Table_updates)
            chatRef.observe(.childAdded, with: { (snapshot) in
                let data = snapshot.value as! [String : AnyObject]
                let updateInfo : UpdateModel = UpdateModel.init(object:data)
                Observers.instance.updateObserver.onNext(updateInfo)
                chatRef.removeValue()
            })
            
            chatRef.observe(.childChanged, with: { (snapshot) in
                let data = snapshot.value as! [String : AnyObject]
                let updateInfo : UpdateModel = UpdateModel.init(object:data)
                Observers.instance.updateObserver.onNext(updateInfo)
                chatRef.removeValue()
            })
        }
    }
    
    public func recordUpdateRequired(sender: String,receiver: String,type: eUpdateType){
        let updateRef =  self.database.child(Table_users).child(receiver).child(Table_updates)
        guard let nextKey = updateRef.childByAutoId().key else { return }
        let updateModel = UpdateModel()
        updateModel.sender = sender
        updateModel.receiver = receiver
        updateModel.updateType = type.rawValue

        updateRef.child(nextKey).setValue(updateModel.dictionary)
        
    }
    
//    func initiateNotificationObserver() -> Void {
//        if let userId = SharedAppResource.instance.loggedInUser?.id {
//            let chatRef = self.database.child(Table_users).child(userId).child(Table_notifications)
//            chatRef.observe(.childAdded, with: { (snapshot) in
//                let data = snapshot.value as! [String : AnyObject]
//                let notification : FNotification = FNotification.init(object:data)
//                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.Ancillary.Notifications.jobNotificationReceived), object: notification, userInfo: ["notification":notification])
//            })
//
//            chatRef.observe(.childChanged, with: { (snapshot) in
//                let data = snapshot.value as! [String : AnyObject]
//                let notification : FNotification = FNotification.init(object:data)
//                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.Ancillary.Notifications.jobNotificationReceived), object: notification, userInfo: ["notification":notification])
//            })
//        }
//    }
    
//    func getUserProfile(userId: String, callback:@escaping (_ user: ACProvider?) -> ()){
//        let chatRef = self.database.child(Table_users).child(userId).child(Table_detail)
//        chatRef.observe(.value, with: { (snapshot) in
//            if let data = snapshot.value as? [String : AnyObject] {
//                let user : ACProvider = ACProvider.init(object:data)
//                user.userId = userId
//                callback(user)
//            }
//        })
//    }
    
    public func initiateMultipartyChatObserver() -> Void {
        FirebaseHelper.initiateFirebaseConnectionStatus()
        if let userId = SharedAppResource.instance.loggedInUser?.id {
            let chatRef = self.database.child(Table_users).child(userId).child(Table_conversations)
            chatRef.observeSingleEvent(of: .value, with: { (dataSnapshot) in
                print("Chat Download Completed for :\(userId)")
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.Notifications.multipartyChatDownloadCompleted), object: nil, userInfo: nil)
            }) { (error) in
                print(error.localizedDescription)
            }
            chatRef.observe(.childAdded, with: { (snapshot) in
                let data = snapshot.value as! [String : AnyObject]
                let conversation : ChatModel = ChatModel.init(object:data)
                conversation.conversationId = snapshot.key
                if(!SharedAppResource.instance.multipartyChats.contains(where: {$0.conversationId == snapshot.key})){
                    SharedAppResource.instance.multipartyChats.append(conversation)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.Notifications.multipartyChatReceived), object: conversation, userInfo: ["conversation":conversation])
                }
            })
            
            chatRef.observe(.childChanged, with: { (snapshot) in
                let data = snapshot.value as! [String : AnyObject]
                let conversation : ChatModel = ChatModel.init(object:data)
                conversation.conversationId = snapshot.key
                if let index = SharedAppResource.instance.multipartyChats.firstIndex(where: {$0.conversationId == snapshot.key}){
                    SharedAppResource.instance.multipartyChats[index] = conversation
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.Notifications.multipartyChatModified), object: conversation, userInfo: ["conversation":conversation])
                }
            })
        }
    }
    
    public func listenToConversationMessages(conversationId: String) -> Void {
        FirebaseHelper.initiateFirebaseConnectionStatus()
        self.database.child(Table_conversations).child(conversationId).child(Table_conversations_messages).removeAllObservers()
        self.listenToConversationUsers(conversationId: conversationId)
        let chatRef = self.database.child(Table_conversations).child(conversationId).child(Table_conversations_messages)
        chatRef.observeSingleEvent(of: .value, with: { (dataSnapshot) in
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.Notifications.multipartyChatDownloadCompleted), object: nil, userInfo: nil)
        }) { (error) in
            print(error.localizedDescription)
        }
        
        chatRef.observe(.childAdded, with: { (snapshot) in
            let data = snapshot.value as! [String : AnyObject]
            let message : ChatMessageModel = ChatMessageModel.init(object:data)
            message.conversationMessageId = snapshot.key
            
            if let seenUsers = (data[Column_seenBy] as? [String : Any])?.values, seenUsers.count > 0 {
                for seenUser in seenUsers{
                    let user = SeenUser.init(json: JSON.init(seenUser))
                    if(!(message.seenBy?.contains(where: {$0.userId == user.userId}))!){
                        message.seenBy?.append(user)
                    }
                }
            }
            
            if let conversation = SharedAppResource.instance.conversationChats.first(where: {$0.conversationId == conversationId}){
                if(!conversation.messages.contains(where: {$0.conversationMessageId == message.conversationMessageId})){
                    conversation.messages.append(message)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: String.init(format: Constants.Notifications.multipartyChatMessageReceived, conversationId) ), object: message, userInfo: ["conversationMessage":message])
                }else{
                    if let index = conversation.messages.firstIndex(where: {$0.conversationMessageId == message.conversationMessageId}){
                        conversation.messages[index] = message
                        NotificationCenter.default.post(name: Notification.Name(rawValue: String.init(format: Constants.Notifications.multipartyChatMessageModified, conversationId) ), object: message, userInfo: ["conversationMessage":message])
                    }
                }
            }else{
                SharedAppResource.instance.conversationChats.append(ConversationMessage.init(conversationId: conversationId,messages: [message]))
                NotificationCenter.default.post(name: Notification.Name(rawValue: String.init(format: Constants.Notifications.multipartyChatMessageReceived, conversationId) ), object: message, userInfo: ["conversationMessage":message])
            }
        })
        
        chatRef.observe(.childChanged, with: { (snapshot) in
            let data = snapshot.value as! [String : AnyObject]
            let message : ChatMessageModel = ChatMessageModel.init(object:data)
            message.conversationMessageId = snapshot.key
            if let seenUsers = (data[Column_seenBy] as? [String : Any])?.values, seenUsers.count > 0 {
                for seenUser in seenUsers{
                    let user = SeenUser.init(json: JSON.init(seenUser))
                    if(!(message.seenBy?.contains(where: {$0.userId == user.userId}))!){
                        message.seenBy?.append(user)
                    }
                }
            }
            
            if let conversation = SharedAppResource.instance.conversationChats.first(where: {$0.conversationId == conversationId}){
                if let index = conversation.messages.firstIndex(where: {$0.conversationMessageId == message.conversationMessageId}){
                    conversation.messages[index] = message
                    NotificationCenter.default.post(name: Notification.Name(rawValue: String.init(format: Constants.Notifications.multipartyChatMessageModified, conversationId) ), object: message, userInfo: ["conversationMessage":message])
                }
            }
        })
    }
    
    public func updateSeenByMessage(conversationId: String, conversation: String){
        let seenBy: SeenUser = SeenUser.init()
        let chatRef = self.database.child(Table_conversations).child(conversationId).child(Table_conversations_messages).child(conversation).child(Column_seenBy)
        guard let nextKey = chatRef.childByAutoId().key else { return }
        seenBy.userId = SharedAppResource.instance.loggedInUser?.id
        seenBy.firstName = SharedAppResource.instance.loggedInUser?.firstName
        seenBy.seenTime = Date.init().apiDateString().apiDate()
        seenBy.userImage = SharedAppResource.instance.loggedInUser?.profileImage
        chatRef.child(nextKey).setValue(seenBy.dictionary)
        
    }
    
    public func listenToConversationUsers(conversationId: String) -> Void {
        FirebaseHelper.initiateFirebaseConnectionStatus()
        self.database.child(Table_conversations).child(conversationId).child(Table_conversations_users).removeAllObservers()
        self.database.child(Table_conversations).child(conversationId).child(Table_conversation_ceeated_field).observeSingleEvent(of: .value, with: { (snap) in
            if let createdBy = snap.value as? String {
                let chatRef = self.database.child(Table_conversations).child(conversationId).child(Table_conversations_users)
                chatRef.observe(.childAdded, with: { (snapshot) in
                    if let data = snapshot.value as? [String : AnyObject]{
                        let chatUser : ChatUsers = ChatUsers.init(object:data)
                        if let conversation = SharedAppResource.instance.conversationUsers.first(where: {$0.conversationId == conversationId}){
                            if(!conversation.users.contains(where: {$0.userId == chatUser.userId})){
                                conversation.users.append(chatUser)
                                NotificationCenter.default.post(name: Notification.Name(rawValue: String.init(format: Constants.Notifications.multipartyChatUserReceived, conversationId) ), object: chatUser, userInfo: ["conversationUser":chatUser])
                            }
                        }else{
                            SharedAppResource.instance.conversationUsers.append(ConversationUsers.init(conversationId: conversationId,users: [chatUser], createdBy:createdBy))
                            NotificationCenter.default.post(name: Notification.Name(rawValue: String.init(format: Constants.Notifications.multipartyChatUserReceived, conversationId) ), object: chatUser, userInfo: ["conversationUser":chatUser])
                        }
                    }
                })
            }
        })
    }
    
    public func removeAllLisnters() {
        self.database.child(Table_users).removeAllObservers()
        if let userId = SharedAppResource.instance.loggedInUser?.id {
            let iOSRef = self.database.child(Table_users).child(userId).child(Table_users_devices).child(Table_users_devices_iOS)
            iOSRef.removeAllObservers()
            self.database.child(Table_users).child(userId).child(Table_user_isActive).removeAllObservers()
            
        }
        self.database.child(Table_notifications).removeAllObservers()
        self.database.child(Table_chats).removeAllObservers()
        self.database.child(Table_detail).removeAllObservers()
        self.database.child(Active_users).removeAllObservers()
    }
    
    public func updateConversationMessage(conversationId: String, message: ChatMessageModel,callBack:@escaping (( _ success: Bool,  _ error: Error?) -> Void)) -> Void {
        let chatRef = self.database.child(Table_conversations).child(conversationId).child(Table_conversations_messages)
        chatRef.childByAutoId().setValue(message.dictionary) { (error, reference) in
            callBack((error != nil ? false: true), error)
            self.updateConversationInfoToConnectedUser(conversationId: conversationId, message: message)
//            self.sendNotification(conversationId: conversationId, message: message, callBack: {_,_ in })
        }
    }
    
    public func updateConversationTitle(conversationId: String, title: String,callBack:@escaping (( _ success: Bool,  _ error: Error?) -> Void)) -> Void {
        let chatRef = self.database.child(Table_conversations).child(conversationId).child(Table_conversations_users)
        chatRef.observe(.value, with: { (snapshot) in
            if let data = snapshot.value as? [String : AnyObject]{
                for key in data.keys{
                    let conversationRef = self.database.child(Table_users).child(key).child(Table_conversations).child(conversationId)
                    conversationRef.updateChildValues([Table_conversation_name: title])
                }
                callBack(true, nil)
            }
        })
    }
    
    public func updateConversationInfoToConnectedUser(conversationId: String, message: ChatMessageModel){
        let chatRef = self.database.child(Table_conversations).child(conversationId).child(Table_conversations_users)
        chatRef.observe(.value, with: { (snapshot) in
            if let data = snapshot.value as? [String : AnyObject]{
                for key in data.keys{
                    let conversationRef = self.database.child(Table_users).child(key).child(Table_conversations).child(conversationId)
                    conversationRef.updateChildValues(["lastMessage": message.message!])
                    conversationRef.updateChildValues(["lastMessageSentAt": message.timestamp!])
                    conversationRef.updateChildValues(["lastMessageSentAt": message.timestamp!])
                    (SharedAppResource.instance.loggedInUser?.id != key) ? conversationRef.updateChildValues(["isRead": false]) : conversationRef.updateChildValues(["isRead": true])
                    conversationRef.observeSingleEvent(of: .value, with: { (snap) in
                        if(snap.hasChild(Table_user_unread_chat_count)){
                            if let count = (snap.childSnapshot(forPath: Table_user_unread_chat_count)).value as? Int{
                                let newCount = count + 1
                                conversationRef.child(Table_user_unread_chat_count).setValue(newCount)
                            }
                        }else{
                            conversationRef.child(Table_user_unread_chat_count).setValue(0)
                        }
                    })
                    let userRef = self.database.child(Table_users).child(key)
                    userRef.child(Table_user_unread_chat_count).observeSingleEvent(of: .value, with: { (snap) in
                        if let count = snap.value as? Int{
                            let newCount = count + 1
                            userRef.child(Table_user_unread_chat_count).setValue(newCount)
                        }else{
                            userRef.child(Table_user_unread_chat_count).setValue(1)
                        }
                        self.updateReadCount(conversationId: conversationId)
                    })
                }
            }
        })
    }
    
    public func updateReadCount(conversationId: String, userId: String? = SharedAppResource.instance.loggedInUser?.id ?? nil) {
        if let userId = userId{
            let conversationRef = self.database.child(Table_users).child(userId).child(Table_conversations).child(conversationId)
            var currentCount  = 0
            conversationRef.child(Table_user_unread_chat_count).observeSingleEvent(of: .value, with: { (snap) in
                if let count = snap.value as? Int{
                    currentCount = count
                    conversationRef.child(Table_user_unread_chat_count).setValue(0)
                    let userRef = self.database.child(Table_users).child(userId)
                    userRef.child(Table_user_unread_chat_count).observeSingleEvent(of: .value, with: { (snap) in
                        if let count = snap.value as? Int{
                            var newCount = count - currentCount
                            (newCount < 0) ? (newCount = 0) : (newCount = newCount)
                            userRef.child(Table_user_unread_chat_count).setValue(newCount)
                        }else{
                            userRef.child(Table_user_unread_chat_count).setValue(0)
                        }
                    })
                }
            })
        }
    }
    
    public func getNextChatModelKey() -> String {
        let conversationRef = self.database.child(Table_conversations)
        return conversationRef.childByAutoId().key!
    }
    
    public func initGroupConversation(conversationId: String? = nil, sender:ChatUsers, receivers: [ChatUsers], subTypeData: CurrentSubTypeData? = nil,callBack:@escaping (( _ success: Bool,  _ error: Error?, _ coversation: ChatModel) -> Void)) -> Void {
        
        if let conversationId = conversationId{
            var oldUsers : [ChatUsers] = []
            let chatRef = self.database.child(Table_conversations).child(conversationId).child(Table_conversations_users)
            chatRef.observe(.childAdded, with: { (snapshot) in
                if let data = snapshot.value as? [String : AnyObject]{
                    let chatUser : ChatUsers = ChatUsers.init(object:data)
                    oldUsers.append(chatUser)
                }
            })
            chatRef.observeSingleEvent(of: .value, with: { (dataSnapshot) in
                let newUserList = receivers.filter { (user) -> Bool in
                    return oldUsers.contains(where: {$0.userId != user.userId})
                }
                let oldUserList = oldUsers.filter { (user) -> Bool in
                    return receivers.contains(where: {$0.userId != user.userId})
                }
                self.database.child(Table_conversations).child(conversationId).child(Table_conversations_users).removeValue { (error, ref) in
                    if error == nil {
                        let chatRefUpdateUsers = self.database.child(Table_conversations)
                        chatRefUpdateUsers.child(conversationId).child(Table_conversations_users).child(sender.userId!).setValue(sender.dictionary)
                        
                        let conversationNewModel = ChatModel.init()
                        conversationNewModel.conversationId = conversationId
                        conversationNewModel.conversationName = String.init(format: "%@ ,%@, %@", sender.getDisplayName(),receivers[0].getDisplayName(),receivers[1].getDisplayName())
                        conversationNewModel.isPrivate = false
                        conversationNewModel.isRead = false
                        conversationNewModel.unReadMessageCount = 0
                        if let subTypeData = subTypeData{
                            conversationNewModel.subTypeData = subTypeData
                        }
                        for user in newUserList{
                            chatRefUpdateUsers.child(conversationId).child(Table_conversations_users).child(user.userId!).setValue(user.dictionary)
                            let userRef = self.database.child(Table_users).child(user.userId!).child(Table_conversations).child(conversationId)
                            userRef.setValue(conversationNewModel.dictionary)
                        }
                        for user in oldUserList{
                            self.database.child(Table_users).child(user.userId!).child(Table_conversations).child(conversationId).removeValue { (error, ref) in
                                if error != nil {
                                    print("error \(error)")
                                }
                            }
                        }
                        callBack(true,nil, conversationNewModel)
                    }else{
                        print("error \(error)")
                    }
                }
            })
        }else{
            let chatRef = self.database.child(Table_conversations)
            guard let nextKey = chatRef.childByAutoId().key else { return }
            chatRef.child(nextKey).setValue([Table_conversation_ceeated_field: sender.userId])
            let conversationModel = ChatModel.init()
            conversationModel.conversationId = nextKey
            conversationModel.conversationName = String.init(format: "%@ ,%@, %@", sender.getDisplayName(),receivers[0].getDisplayName(),receivers[1].getDisplayName())
            conversationModel.senderImageUrl = sender.profileImageUrl
            conversationModel.isPrivate = false
            conversationModel.unReadMessageCount = 0
            if let subTypeData = subTypeData{
                conversationModel.subTypeData = subTypeData
            }
            let userRef = self.database.child(Table_users).child(sender.userId!).child(Table_conversations).child(nextKey)
            userRef.setValue(conversationModel.dictionary)
            
            chatRef.child(nextKey).child(Table_conversations_users).child(sender.userId!).setValue(sender.dictionary)
            for user in receivers{
                chatRef.child(nextKey).child(Table_conversations_users).child(user.userId!).setValue(user.dictionary)
                let userRef = self.database.child(Table_users).child(user.userId!).child(Table_conversations).child(nextKey)
                userRef.setValue(conversationModel.dictionary)
            }
            
            self.listenToConversationMessages(conversationId: nextKey)
            callBack(true,nil, conversationModel)
        }
        
    }
    
    public func initConversation(sender:ChatUsers, receiver: ChatUsers, subTypeData: CurrentSubTypeData? = nil,callBack:@escaping (( _ success: Bool,  _ error: Error?, _ coversation: ChatModel) -> Void)) -> Void {
        let chatRef = self.database.child(Table_conversations)
        guard let nextKey = chatRef.childByAutoId().key else { return }
        chatRef.child(nextKey).setValue([Table_conversation_ceeated_field: sender.userId])
        chatRef.child(nextKey).child(Table_conversations_users).child(sender.userId!).setValue(sender.dictionary)
        chatRef.child(nextKey).child(Table_conversations_users).child(receiver.userId!).setValue(receiver.dictionary)
        
        let chatModelSender = ChatModel.init()
        chatModelSender.conversationId = nextKey
        chatModelSender.senderName = sender.getDisplayName()
        chatModelSender.sender = sender.userId
        chatModelSender.senderImageUrl = sender.profileImageUrl
        chatModelSender.senderFirstName = sender.firstName
        chatModelSender.senderLastName = sender.lastName
        chatModelSender.receiver = receiver.userId
        chatModelSender.receiverName = receiver.getDisplayName()
        chatModelSender.receiverImageUrl = receiver.profileImageUrl
        chatModelSender.receiverFirstName = receiver.firstName
        chatModelSender.receiverLastName = receiver.lastName
        chatModelSender.isPrivate = true
        chatModelSender.lastMessage = ""
        chatModelSender.unReadMessageCount = 0
        chatModelSender.conversationName = receiver.getDisplayName()
        if let subTypeData = subTypeData{
            chatModelSender.subTypeData = subTypeData
        }
        let senderRef = self.database.child(Table_users).child(sender.userId!).child(Table_conversations)
        senderRef.child(nextKey).setValue(chatModelSender.dictionary)
        
        let chatModelReceiver = ChatModel.init()
        chatModelReceiver.conversationId = nextKey
        chatModelReceiver.senderName = receiver.getDisplayName()
        chatModelReceiver.sender = receiver.userId
        chatModelReceiver.senderImageUrl = receiver.profileImageUrl
        chatModelReceiver.senderFirstName = receiver.firstName
        chatModelReceiver.senderLastName = receiver.lastName
        chatModelReceiver.receiver = sender.userId
        chatModelReceiver.receiverName = sender.getDisplayName()
        chatModelReceiver.receiverImageUrl = sender.profileImageUrl
        chatModelReceiver.receiverFirstName = sender.firstName
        chatModelReceiver.receiverLastName = sender.lastName
        chatModelReceiver.isPrivate = true
        chatModelReceiver.lastMessage = ""
        chatModelReceiver.unReadMessageCount = 0
        chatModelReceiver.conversationName = sender.getDisplayName()
        if let subTypeData = subTypeData{
            chatModelReceiver.subTypeData = subTypeData
        }
        let receiverRef = self.database.child(Table_users).child(receiver.userId!).child(Table_conversations)
        receiverRef.child(nextKey).setValue(chatModelReceiver.dictionary)
        
        callBack(true,nil, chatModelSender)
    }
    
    
    public func addNewUserToConversation(conversationId:String, sender:ChatUsers, receiver: ChatUsers,callBack:@escaping (( _ success: Bool,  _ error: Error?) -> Void)) -> Void {
        let chatRef = self.database.child(Table_conversations)
        chatRef.child(conversationId).child(Table_conversations_users).child(receiver.userId!).setValue(receiver.dictionary)
        
        let chatModelReceiver = ChatModel.init()
        chatModelReceiver.conversationId = conversationId
        chatModelReceiver.senderName = receiver.getDisplayName()
        chatModelReceiver.sender = receiver.userId
        chatModelReceiver.senderImageUrl = receiver.profileImageUrl
        chatModelReceiver.senderFirstName = receiver.firstName
        chatModelReceiver.senderLastName = receiver.lastName
        chatModelReceiver.receiver = sender.userId
        chatModelReceiver.receiverName = sender.getDisplayName()
        chatModelReceiver.receiverImageUrl = sender.profileImageUrl
        chatModelReceiver.receiverFirstName = sender.firstName
        chatModelReceiver.receiverLastName = sender.lastName
        chatModelReceiver.isPrivate = true
        chatModelReceiver.conversationName = sender.getDisplayName()
        let receiverRef = self.database.child(Table_users).child(receiver.userId!).child(Table_conversations)
        receiverRef.child(conversationId).setValue(chatModelReceiver.dictionary)
        callBack(true,nil)
    }
    
    public func leaveConversation(user:ChatUsers, conversationId: String,callBack:@escaping (( _ success: Bool,  _ error: Error?) -> Void)) {
        let chatRef = self.database.child(Table_conversations)
        chatRef.child(conversationId).child(Table_users).child(user.userId!).removeValue()
        
        let receiverRef = self.database.child(Table_users).child(user.userId!).child(Table_conversations).child(conversationId)
        receiverRef.removeValue { (error, dbRef) in
            if (error == nil){
                callBack(true,nil)
            }else{
                callBack(false,error)
            }
        }
    }
    
//    func sendNotification(conversationId: String, message: ChatMessageModel, notificationType: eNotificationCategories = eNotificationCategories.Chat, callBack:@escaping (( _ success: Bool,  _ error: Error?) -> Void)) {
//        self.database.child(Table_notifications).removeAllObservers()
//        let notificationRef = self.database.child(Table_notifications)
//        guard let nextKey = notificationRef.childByAutoId().key else { return }
//
//        if let userId = SharedAppResource.instance.loggedInUser?.id
//        {
//            let notification = NotificationModel.init()
//            notification.senderId = userId
//            notification.message = message.message
//            notification.conversationId = conversationId
//            notification.type = notificationType.rawValue
//            getConversationUsers(conversationId: conversationId) { (users) in
//                notification.userList = users
//                notification.userList?.remove(object: userId)
//                notificationRef.child(nextKey).setValue(notification.toJSON())
//            }
//        }
//        callBack(true, nil)
//    }
    
    public func getConversationUsers(conversationId: String, callBack:@escaping (_ conversationUser:[String]) ->()){
        let conversationUsersRef = self.database.child(Table_conversations).child(conversationId).child(Table_conversations_users)
        var conversationUser: [String] = []
        conversationUsersRef.observe(.value, with: { (snapshot) in
            if let data = snapshot.value as? [String : AnyObject]{
                for key in data.keys{
                    conversationUser.append(key)
                }
                callBack(conversationUser)
            }
        })
    }
    
    public func getSubTypeDataForConversation(conversationId: String, callBack:@escaping (_ suvTypeData:CurrentSubTypeData) ->()){
        let conversationUsersRef = self.database.child(Table_conversations).child(conversationId).child(Firebase_table_conversations_column_subtype_data)
        conversationUsersRef.observe(.value, with: { (snapshot) in
            if let data = snapshot.value as? [String : AnyObject]{
                let currentSubTypeData : CurrentSubTypeData = CurrentSubTypeData.init(object:data)
                callBack(currentSubTypeData)
            }
        })
    }
    
    public func makeConversationSpecific(conversationId: String, subType:CurrentSubTypeData,callBack:@escaping (( _ success: Bool,  _ error: Error?) -> Void)) {
        let chatRef = self.database.child(Table_conversations)
        chatRef.child(conversationId).child(Firebase_table_conversations_column_subtype_data).setValue(subType.toString())
        callBack(true,nil)
    }
}

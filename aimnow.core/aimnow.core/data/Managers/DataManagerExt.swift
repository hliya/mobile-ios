//
//  DataManagerExt.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/5/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

public enum NetworkRouter {
    case authentication(ParamEncoder)
    case getDependantUsers(String, String)
    case getDependantUser(String, String, String)
    case uploadAttachment
    case getAllPrograms(ParamEncoder)
    case getAssignedPrograms(ParamEncoder)
    case getProgramStats(ParamEncoder)
    case getMasterSync(ParamEncoder)
    case location(Parameters)
    case requestForProgram(ParamEncoder)
    case getDetailedAssignedPrograms(ParamEncoder)
    case recordPlayerAttendance(ParamEncoder)
    case updateAssesmentStats(ParamEncoder)
    case UpsertFeedbackModel(ParamEncoder)
    case getFeedbackDetails(ParamEncoder)
    case getProgramOverallSummary(ParamEncoder)
    case registerDevice(ParamEncoder)
    case deleteDeviceRegistration(ParamEncoder)
    case getProgramUsers(ParamEncoder)
    case getProfile(String)
    case getNotifications(ParamEncoder)
    case updateNotification(ParamEncoder)
    case postNotification(ParamEncoder)
    case notificationSummary(ParamEncoder)
    case getFeedbackStatistics(ParamEncoder)
    case updateUserProfile
}

extension NetworkRouter: URLRequestConvertible {
    public var method: Alamofire.HTTPMethod {
        switch self {
        case .authentication:
            return .post
        case .getDependantUsers:
            return .get
        case .getDependantUser:
            return .get
        case .getAllPrograms:
            return .post
        case .getAssignedPrograms:
            return .post
        case .getProgramStats:
            return .post
        case .getMasterSync:
            return .post
        case .location:
            return .post
        case .requestForProgram:
            return .post
        case .getDetailedAssignedPrograms:
            return .post
        case .recordPlayerAttendance:
            return .post
        case .updateAssesmentStats:
            return .post
        case .UpsertFeedbackModel:
            return .post
        case .uploadAttachment:
            return .post
        case .getFeedbackDetails:
            return .post
        case .getProgramOverallSummary:
            return .post
        case .registerDevice:
            return .post
        case .deleteDeviceRegistration:
            return .post
        case .getProgramUsers:
            return .post
        case .getProfile:
            return .get
        case .getNotifications:
            return .post
        case .updateNotification:
            return .post
        case .postNotification:
            return .post
        case .notificationSummary:
            return .post
        case .getFeedbackStatistics:
            return .post
        case .updateUserProfile:
            return .post
        }
        
    }
    
    public var path: String {
        switch self {
        case .authentication:
            return Constants.authentication
        case .getDependantUsers(let unitId, let parentId):
            return String.init(format: Constants.getAllDependentUsers, unitId, parentId)
        case .getDependantUser(let unitId, let parentId, let dependantId):
            return String.init(format: Constants.getAllDependentUser, unitId, parentId, dependantId)
        case .getAllPrograms:
            return Constants.getAllPrograms
        case .getAssignedPrograms:
            return Constants.getAssignedPrograms
        case .getProgramStats:
            return Constants.getProgramStats
        case .getMasterSync:
            return Constants.getMasterSync
        case .location:
            return Constants.getLocation
        case .requestForProgram:
            return Constants.requestNewProgram
        case .getDetailedAssignedPrograms:
            return Constants.getDetailedAssignedPrograms
        case .recordPlayerAttendance:
            return Constants.playerAttendanceRecording
        case .updateAssesmentStats:
            return Constants.updateAssesmentStats
        case .UpsertFeedbackModel:
            return Constants.upsertFeedBack
        case .uploadAttachment:
            return Constants.upsertAttachment
        case .getFeedbackDetails:
            return Constants.getFeedbackDetails
        case .registerDevice:
            return Constants.registerDevice
        case .deleteDeviceRegistration:
            return Constants.deleteDeviceRegistration
        case .getProgramOverallSummary:
            return Constants.getProgramOverallSummary
        case .getProgramUsers:
            return Constants.programUsers
        case .getProfile(let userId):
            return String.init(format: Constants.profile, userId)
        case .getNotifications:
            return Constants.notifications
        case .updateNotification:
            return Constants.updateNotifications
        case .postNotification:
            return Constants.postNotifications
        case .notificationSummary:
            return Constants.notificationSummary
        case .getFeedbackStatistics:
            return Constants.getFeedbackStatistics
        case .updateUserProfile:
            return Constants.updateUserProfile
        }
    }
    
    public func asURLRequest() throws -> URLRequest {
        let escapedPathString = path.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
        let URL = Foundation.URL(string:Constants.BASE_API_URL)!
        let url = Foundation.URL(string: escapedPathString, relativeTo: URL)!
        var mutableURLRequest = URLRequest(url: url)
        mutableURLRequest.httpMethod = method.rawValue
        mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if let token = Defaults[\.token], !token.isEmpty{
            mutableURLRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        mutableURLRequest.setValue(SharedAppResource.instance.appId, forHTTPHeaderField: "appid")
        
        // Get query
        switch self {
        case .location(let parameters):
            do {
                let request = try  URLEncoding.init(boolEncoding: .literal).encode(mutableURLRequest, with: parameters)
                return request
            } catch {
                print(error)
            }
        default:break
        }
        
        // Post requests
        switch self{
        case .authentication(let reqData),
             .getAllPrograms(let reqData),
             .getProgramStats(let reqData),
             .getMasterSync(let reqData),
             .getAssignedPrograms(let reqData),
             .requestForProgram(let reqData),
             .getDetailedAssignedPrograms(let reqData),
             .recordPlayerAttendance(let reqData),
             .updateAssesmentStats(let reqData),
             .UpsertFeedbackModel(let reqData),
             .getFeedbackDetails(let reqData),
             .getProgramOverallSummary(let reqData),
             .registerDevice(let reqData),
             .deleteDeviceRegistration(let reqData),
             .getProgramUsers(let reqData),
             .getNotifications(let reqData),
             .updateNotification(let reqData),
             .postNotification(let reqData),
             .notificationSummary(let reqData),
             .getFeedbackStatistics(let reqData):
            mutableURLRequest.httpBody = reqData.toString().data(using: String.Encoding.utf8, allowLossyConversion: true)
        default: break
        }
        
        // all upload here
        switch self {
        case .uploadAttachment:
            mutableURLRequest.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
            return mutableURLRequest as URLRequest
        case .updateUserProfile:
            mutableURLRequest.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
            return mutableURLRequest as URLRequest
        default: break;
        }
        
        switch self {
        default: return mutableURLRequest as URLRequest
        }
    }
}


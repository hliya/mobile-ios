//
//  ParameterModel.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/5/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation

// MARK: API Parameters
public struct ParamLoginMail: ParamEncoder {
     var username: String
     var password: String
}

// MARK: Encoding
public protocol ParamEncoder: Codable {
    func toString() -> String
}

extension ParamEncoder {
    public func toString() -> String {
        do {
            let jsonData = try self.encoded()
            let jsonString = String(data: jsonData, encoding: .utf8)!
            return jsonString
        } catch(let error) {
            print("Encoding Error: \(error)")
        }
        return ""
    }
}

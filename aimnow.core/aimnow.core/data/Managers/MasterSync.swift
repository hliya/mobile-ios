//
//  MasterSync.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/9/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import Alamofire
import RealmSwift
import Realm
import RxSwift

public class MasterSync {
    public static let shared:MasterSync = MasterSync.init()
    let realm = try! Realm()
    var delegate:MasterSyncDelegate?
    var isRunning = false
    var dataManager = DataManager.sharedInstance
    var observers = Observers.instance
    
    init() {
    }
    
    public func startSync (delegate:MasterSyncDelegate?) {
        guard (!self.isRunning) else {
            return
        }
        
        self.delegate = delegate
        self.delegate?.onStart()
        let dispatchGroup = DispatchGroup()
        Enums.eMasterSync.allCases.forEach { (masterData) in
            dispatchGroup.enter()
            self.syncMasterData(masterDataType: masterData) { (masterData) in
                if(masterData.count > 0){
                    try! self.realm.write {
                        self.realm.add(masterData, update: Realm.UpdatePolicy.all)
                    }
                }
                dispatchGroup.leave()
            }
        }
        dispatchGroup.notify(queue: .main) {
            Defaults[\.isMasterSync] = true
        }
    }
    
    private func syncMasterData (masterDataType: Enums.eMasterSync, timeStamp:String? = nil, completion:@escaping(_ masterData: [MasterData]) -> ()) {
        let request = MasterDataRequestmodel.init(type: masterDataType.rawValue)
        let router = NetworkRouter.getMasterSync(request)
        
        self.dataManager.callAPI(router: router) { (result: Swift.Result<MasterDataResponse, NetworkError>) in
            switch(result){
            case .success(let response):
                switch masterDataType {
                case .Assigment:
                    completion(response.pagedItems?.map({MasterAssigment.init(value: $0)}) ?? [])
                case .Attachment:
                    completion(response.pagedItems?.map({MasterAttachment.init(value: $0)}) ?? [])
                case .Badge:
                    completion(response.pagedItems?.map({MasterBadge.init(value: $0)}) ?? [])
                case .Config:
                    completion(response.pagedItems?.map({MasterConfig.init(value: $0)}) ?? [])
                case .Device:
                    completion(response.pagedItems?.map({MasterDevice.init(value: $0)}) ?? [])
                case .Feedback:
                    completion(response.pagedItems?.map({MasterFeedback.init(value: $0)}) ?? [])
                case .Notification:
                    completion(response.pagedItems?.map({MasterNotification.init(value: $0)}) ?? [])
                case .Program:
                    completion(response.pagedItems?.map({MasterProgram.init(value: $0)}) ?? [])
                case .Tempate:
                    completion(response.pagedItems?.map({MasterTempate.init(value: $0)}) ?? [])
                case .Unit:
                    completion(response.pagedItems?.map({MasterUnit.init(value: $0)}) ?? [])
                case .User:
                    completion(response.pagedItems?.map({MasterUser.init(value: $0)}) ?? [])
                case .Status:
                    completion(response.pagedItems?.map({MasterStatus.init(value: $0)}) ?? [])
                }
            case .failure(let error):
                self.observers.errorMessageObserver.onNext(errorMessage(error: error.errorDescription))
                break;
            }
        }
    }
    
    private var isMasterSync:Bool {
        set (newValue) {
            Defaults[\.isMasterSync] = newValue
        }
        get {
            return Defaults[\.isMasterSync] ?? false
        }
    }
    
    public func clearAll () {
        self.isMasterSync = false
        
        try! self.realm.write {
            self.realm.deleteAll()
        }
    }
}

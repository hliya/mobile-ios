//
//  DataManager.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 3/5/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SwiftyUserDefaults

public enum NetworkError: Error {
    case connectionError
    case internalServerError(_ message: String)
    var errorDescription: String {
        switch self {
        case .connectionError:
            return "No network connection"
        case .internalServerError(let message):
            return message
        }
    }
}

public class DataManager: NSObject {
    let manager : Session
    
    public static let sharedInstance: DataManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpMaximumConnectionsPerHost = Constants.MAXIMUM_CONNECTION
        configuration.httpAdditionalHeaders = HTTPHeaders.default.dictionary
        return DataManager(manager: Session(configuration: configuration))
    }()
    
    init(manager: Session){
        self.manager = manager
        self.manager.session.configuration.timeoutIntervalForRequest = Constants.TIME_OUT_INTERVAL
        self.manager.session.configuration.timeoutIntervalForResource = Constants.TIME_OUT_INTERVAL
    }
    
    // MARK - Network functions
    public func callAPI<T: Codable>(router: NetworkRouter, completion:@escaping(Result<T, NetworkError>) -> ()) {
        if !NetworkReachabilityManager()!.isReachable {
            completion(.failure(.connectionError))
            return
        }
        let decoder = JSONDecoder()
        self.manager.request(router)
            .validate()
            .response { (response)  in
                switch response.result {
                case .success(let data):
                    do {
                        let model = try decoder.decode(T.self, from: data!)
                        completion(.success(model))
                    } catch (let error) {
                        let error = String.init(data: data!, encoding: .utf8) ?? error.localizedDescription
                        completion(.failure(.internalServerError(error)))
                    }
                case .failure(let error):
                    switch error.responseCode {
                    case 401:
                        Observers.instance.errorMessageObserver.onNext(errorMessage(code:401,error: error.localizedDescription,title: "Unauthorized"))
                        break
                    default:
                        if let resData = response.data, let error = try? decoder.decode(String.self, from: resData),!error.isEmpty {
                            completion(.failure(.internalServerError(error)))
                        } else {
                            completion(.failure(.internalServerError(error.localizedDescription)))
                        }
                        break
                    }
                }
        }
    }
    
    public func uploadAttachment (path: NetworkRouter, param: [String:String], fileData:Data,
                           callback:@escaping ([AttachmentResponse?])->(),
                           failure:@escaping(_ error: String) -> ()) {
        
        let request:URLRequestConvertible = path
        let multipartEncoding: (MultipartFormData) -> Void = { multipartFormData in
            if ((param["typeId"]) == "\(Enums.eAttachmentFileTypes.Images.rawValue)") {
                multipartFormData.append(fileData, withName: "file", fileName: "\(Utils.getCurrentTimeStamp()).png", mimeType: "image/png")
            } else{
                multipartFormData.append(fileData, withName: "file", fileName: "\(Utils.getCurrentTimeStamp()).mp4", mimeType: "video/mp4")
            }
            for (key, value) in param {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }
        
        self.manager.upload(multipartFormData: multipartEncoding, with: request).validate()
            .response { (response)  in
                switch response.result {
                case .success(let data):
                    var attachments:[AttachmentResponse?] = []
                    let json = try! JSON.init(data: data!)
                    if(json.type == .dictionary){
                        let decoder = JSONDecoder()
                        let model = try! decoder.decode(AttachmentResponse.self, from: json.rawData())
                        attachments.append(model)
                    }else if(json.type == .array){
                        let attachmentArray = json.arrayValue
                        let decoder = JSONDecoder()
                        for att in attachmentArray {
                            let model = try! decoder.decode(AttachmentResponse.self, from: att.rawData())
                            attachments.append(model)
                        }
                    }
                    callback(attachments)
                case .failure(let error):
                    failure(error.localizedDescription)
                }
        }
    }
    
    public func uploadForm (path: NetworkRouter, param: [String:String], fileData:Data,
                           callback:@escaping (UserModel)->(),
                           failure:@escaping(_ error: String) -> ()) {
        
        let request:URLRequestConvertible = path
        let multipartEncoding: (MultipartFormData) -> Void = { multipartFormData in
            if ((param["typeId"]) == "\(Enums.eAttachmentFileTypes.Images.rawValue)") {
                multipartFormData.append(fileData, withName: "file", fileName: "\(Utils.getCurrentTimeStamp()).png", mimeType: "image/png")
            } else{
                multipartFormData.append(fileData, withName: "file", fileName: "\(Utils.getCurrentTimeStamp()).mp4", mimeType: "video/mp4")
            }
            for (key, value) in param {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }
        
        self.manager.upload(multipartFormData: multipartEncoding, with: request).validate()
            .response { (response)  in
                switch response.result {
                case .success(let data): 
                    let json = try! JSON.init(data: data!)
                    let decoder = JSONDecoder()
                    let user = try! decoder.decode(UserModel.self, from: json.rawData())
                    callback(user)
                case .failure(let error):
                    failure(error.localizedDescription)
                }
        }
    }
}

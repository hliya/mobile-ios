//
//  aimnow_core.h
//  aimnow.core
//
//  Created by Hasitha Liyanage  on 7/20/19.
//  Copyright © 2019 Hasitha Liyanage . All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for aimnow_core.
FOUNDATION_EXPORT double aimnow_coreVersionNumber;

//! Project version string for aimnow_core.
FOUNDATION_EXPORT const unsigned char aimnow_coreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <aimnow_core/PublicHeader.h>



//
//  AfteEditTextField.swift
//  aimnow.core
//
//  Created by Isuru Ranasinghe on 6/29/20.
//  Copyright © 2020 Hasitha Liyanage . All rights reserved.
//

import UIKit

@IBDesignable
public class AfteEditTextField: UITextField {
    
    @IBInspectable var delayValue : Double = 0.5
    public var timer:Timer?
    
    public var actionClosure : (()->Void)?
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.addTarget(self, action: #selector(changedTextFieldValue), for: .editingChanged)
    }
    
    @objc public func changedTextFieldValue(){
        timer?.invalidate()
        //setup timer
        timer = Timer.scheduledTimer(timeInterval: delayValue, target: self, selector: #selector(self.executeAction), userInfo: nil, repeats: false)
    }
    
    @objc public func executeAction(){
        actionClosure?()
    }
}
